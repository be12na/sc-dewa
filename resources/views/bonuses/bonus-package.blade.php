@php
    $pageHeading = __('breadcrumb.bonus.package');
    $isCompany = $authUser->isGroup('admin');
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')

@if ($isCompany)
    {{-- <div class="card">
        @if ($isCompany)
            @include('partials.card-header', ['cardTitle' => __('header.bonus.extra.index')])
        @endif
        <div class="card-body px-0 py-2">
            <div class="btn-group d-none" id="button-controls">
                @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
            </div>
            <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap fs-auto" id="my-table">
                <thead>
                    <tr>
                        <th>@translate('text.date')</th>
                        @if ($isCompany)
                            <th>@translate('label.member')</th>
                            <th></th>
                        @endif
                        <th>@translate('label.from-member')</th>
                        <th></th>
                        <th>@translate('label.bonus') (@translate('format.currency.symbol.text'))</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div> --}}
@else
    @php
        // $summary = $authUser->summary_of_bonus_package;
    @endphp
@endif
@endsection

@section('vendorCSS')
    {{-- @include('partials.datatable.css') --}}
@endsection

@section('vendorJS')
    {{-- @include('partials.datatable.js') --}}
@endsection

@push('pageJS')

@endpush