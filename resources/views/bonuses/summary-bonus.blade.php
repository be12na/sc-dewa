@php
    $pageHeading = __('breadcrumb.bonus.summary');
    $isCompany = $authUser->isGroup('admin');
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')

@if ($isCompany)
    <div class="card">
        @include('partials.card-header', ['cardTitle' => __('header.bonus.summary.index')])
        <div class="card-body px-0 py-2">
            <div class="btn-group d-none" id="button-controls">
                @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
            </div>
            <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap fs-auto" id="my-table">
                <thead>
                    <tr>
                        <th>@translate('label.member')</th>
                        <th></th>
                        <th>@translate('label.summary.bonus.sponsor')</th>
                        @if (BONUS_EXTRA_ENABLED)
                            <th>@translate('label.summary.bonus.extra')</th>
                        @endif
                        @if (BONUS_RO_ENABLED)
                            <th>@translate('label.summary.bonus.ro')</th>
                        @endif
                        @if (BONUS_UPGRADE_ENABLED)
                            <th>@translate('label.summary.bonus.upgrade')</th>
                        @endif
                        <th>@translate('label.summary.bonus.total-bonus')</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@else
    <div class="row g-2 g-md-3 mb-2 mb-md-3">
        @php
            $summaries = $authUser->summary_of_bonuses;
            $summarySponsor = $authUser->summary_of_bonus_sponsor;
        @endphp
        <div class="col-12 col-sm-4 d-flex">
            <div class="flex-fill card bg-theme-2" style="--bs-bg-opacity:0.1;">
                <div class="card-body d-flex">
                    <div class="d-flex align-items-end flex-shrink-0">
                        <div><i class="fa fa-users-between-lines fs-2 text-theme-1" style="--bs-text-opacity:0.75;"></i></div>
                    </div>
                    <div class="flex-fill d-flex flex-column justify-content-end text-end fs-auto">
                        <div class="fw-bold">@translate('label.summary.bonus.sponsor')</div>
                        <div class="small text-muted">@translate('format.currency.symbol.text') @formatNumber($summarySponsor->total_bonus)</div>
                    </div>
                </div>
            </div>
        </div>
        @if (BONUS_EXTRA_ENABLED)
            @php
                $summaryExtra = $authUser->summary_of_bonus_extra;
            @endphp
            <div class="col-12 col-sm-4 d-flex">
                <div class="flex-fill card bg-theme-2" style="--bs-bg-opacity:0.1;">
                    <div class="card-body d-flex">
                        <div class="d-flex align-items-end flex-shrink-0">
                            <div><i class="fa fa-users-rays fs-2 text-theme-1" style="--bs-text-opacity:0.75;"></i></div>
                        </div>
                        <div class="flex-fill d-flex flex-column justify-content-end text-end fs-auto">
                            <div class="fw-bold">@translate('label.summary.bonus.extra')</div>
                            <div class="small text-muted">@translate('format.currency.symbol.text') @formatNumber($summaryExtra->total_bonus)</div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (BONUS_RO_ENABLED)
            @php
                $summaryRO = $authUser->summary_of_bonus_ro;
            @endphp
            <div class="col-12 col-sm-4 d-flex">
                <div class="flex-fill card bg-theme-2" style="--bs-bg-opacity:0.1;">
                    <div class="card-body d-flex">
                        <div class="d-flex align-items-end flex-shrink-0">
                            <div><i class="fa fa-money-bill-1 fs-2 text-theme-1" style="--bs-text-opacity:0.75;"></i></div>
                        </div>
                        <div class="flex-fill d-flex flex-column justify-content-end text-end fs-auto">
                            <div class="fw-bold">@translate('label.summary.bonus.ro')</div>
                            <div class="small text-muted">@translate('format.currency.symbol.text') @formatNumber($summaryRO->total_bonus)</div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (BONUS_UPGRADE_ENABLED)
            @php
                $summaryRO = $authUser->summary_of_bonus_upgrade;
            @endphp
            <div class="col-12 col-sm-4 d-flex">
                <div class="flex-fill card bg-theme-2" style="--bs-bg-opacity:0.1;">
                    <div class="card-body d-flex">
                        <div class="d-flex align-items-end flex-shrink-0">
                            <div><i class="fa fa-money-bill-trend-up fs-2 text-theme-1" style="--bs-text-opacity:0.75;"></i></div>
                        </div>
                        <div class="flex-fill d-flex flex-column justify-content-end text-end fs-auto">
                            <div class="fw-bold">@translate('label.summary.bonus.upgrade')</div>
                            <div class="small text-muted">@translate('format.currency.symbol.text') @formatNumber($summaryRO->total_bonus)</div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="col-12 col-sm-4 d-flex">
            <div class="flex-fill card bg-theme-3" style="--bs-bg-opacity:0.1;">
                <div class="card-body d-flex">
                    <div class="d-flex align-items-end flex-shrink-0">
                        <div><i class="fa fa-wallet fs-2 text-theme-1" style="--bs-text-opacity:0.75;"></i></div>
                    </div>
                    <div class="flex-fill d-flex flex-column justify-content-end text-end fs-auto">
                        <div class="fw-bold">@translate('label.summary.bonus.total-bonus')</div>
                        <div class="small text-muted">@translate('format.currency.symbol.text') @formatNumber($summaries->total_bonus)</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row g-2 g-md-3 mb-3">
        <div class="col-12 col-sm-6 d-flex">
            <div class="flex-fill card bg-light" style="--bs-bg-opacity:0.1;">
                <div class="card-body d-flex">
                    <div class="d-flex align-items-end flex-shrink-0">
                        <div><i class="fa fa-money-bill-transfer fs-2 text-theme-1" style="--bs-text-opacity:0.75;"></i></div>
                    </div>
                    <div class="flex-fill d-flex flex-column justify-content-end text-end fs-auto">
                        <div class="fw-bold">@translate('label.summary.bonus.withdrawed')</div>
                        <div class="small text-muted">@translate('format.currency.symbol.text') @formatNumber($summaries->total_withdraw)</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 d-flex">
            <div class="flex-fill card bg-light" style="--bs-bg-opacity:0.1;">
                <div class="card-body d-flex">
                    <div class="d-flex align-items-end flex-shrink-0">
                        <div><i class="fa fa-coins fs-2 text-theme-1"></i></div>
                    </div>
                    <div class="flex-fill d-flex flex-column justify-content-end text-end fs-auto">
                        <div class="fw-bold">@translate('label.summary.bonus.remaining')</div>
                        <div class="small text-muted">@translate('format.currency.symbol.text') @formatNumber($summaries->remaining_bonus)</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection

@if ($isCompany)
@section('vendorCSS')
    @include('partials.datatable.css')
@endsection

@section('vendorJS')
    @include('partials.datatable.js')
@endsection

@push('pageJS')
<script>
    let table;
    const tableLang = {!! json_encode(__('datatable')) !!};

    function reloadTable() {
        dataTableRefresh(table);
    }

    $(function() {
        table = $('#my-table').DataTable({
            dom: '<"container-fluid gx-3"<"row g-2 mb-2"<"col-auto me-auto me-sm-0"<"dt-control">><"col-auto me-sm-auto"l><"col-12 col-sm-auto"f>>>r<"table-responsive border-top border-translucent"t><"row gy-1 gx-0"<"col-12 col-md"i><"col-12 col-md"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("bonus.summary.dataTable") }}'
            },
            deferLoading: 50,
            language: tableLang,
            search: {
                return: true
            },
            columns: [
                {data: 'name'},
                {data: 'username', className: 'd-none'},
                {data: 'bonus_sponsor', className: 'dt-body-right', searchable: false},
                @if (BONUS_EXTRA_ENABLED)
                    {data: 'bonus_extra', className: 'dt-body-right', searchable: false},
                @endif
                @if (BONUS_RO_ENABLED)
                    {data: 'bonus_ro', className: 'dt-body-right', searchable: false},
                @endif
                @if (BONUS_UPGRADE_ENABLED)
                    {data: 'bonus_upgrade', className: 'dt-body-right', searchable: false},
                @endif
                {data: 'total_bonus', className: 'dt-body-right', searchable: false},
            ],
        });

        setDataTableLoading('my-table', '{{ __("datatable.processing") }}');
        setControlToDataTable('my-table', $('#button-controls'));
        $('.dataTables_filter label').addClass('d-block');
        $('.dataTables_filter .form-control').css({'margin-left': 0, 'width': '100%'});
        $('#my-table_filter .form-control').attr({placeholder: 'Member'});

        reloadTable();
    });
</script>
@endpush
@endif