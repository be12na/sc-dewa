<form class="modal-content remote-submit" action="{{ route('bonus.reward.postClaim', ['reward' => $reward->id]) }}" method="POST" data-alert-container="#alert-form-container">
    @include('partials.modals.modal-header', ['modalTitle' => __("header.confirmation")])
    <div class="modal-body">
        @csrf
        <input type="hidden" name="reward_id" value="{{ $reward->id }}">
        <div id="alert-form-container"></div>
        <div class="text-center fs-auto">
            <div>Klaim Reward</div>
            <p class="text-theme-1"><b>{{ $reward->value }}</b></p>
            <div><i class="fa fa-question fs-4 text-theme-3"></i></div>
        </div>
    </div>
    @include('partials.modals.modal-footer', [
        'modalFoterClass' => 'justify-content-center',
        'modalFooterContent' => [
            view('partials.buttons.submit', ['btnIcon' => 'save', 'btnText' => 'Klaim', 'btnClass' => 'btn-sm btn-success'])->render(),
            view('partials.buttons.close-modal', ['btnClass' => 'btn-sm btn-danger'])->render()
        ],
    ])
</form>