@php
    $reward = $summary->reward;
    $claims = $summary->claims
@endphp
<div class="fw-bold fs-auto mb-1">Reward Member Aktivasi</div>
<div class="d-block table-responsive border border-translucent mb-2 mb-lg-3">
    <table class="table table-hover table-striped table-header-centered table-header-bordered table-nowrap table-translucent fs-auto mb-2">
        <thead>
            <tr>
                <th rowspan="2">Reward</th>
                <th colspan="2">Syarat</th>
                <th rowspan="2">Klaim</th>
            </tr>
            <tr>
                <th class="border-start">Kiri</th>
                <th>Kanan</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $reward->value }}</td>
                <td class="text-center">@formatNumber($reward->left)</td>
                <td class="text-center">@formatNumber($reward->right)</td>
                <td class="text-center">
                    @if ($summary->can_claim)
                        @include('partials.buttons.open-modal', [
                            'isAjaxModal' => true,
                            'modalUrl' => route('bonus.reward.claim', ['reward' => $reward->id]),
                            'btnClass' => 'btn-sm btn-success',
                            'modalTarget' => '#my-modal',
                            'btnText' => 'Klaim',
                        ])
                    @else
                        ---
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
</div>
