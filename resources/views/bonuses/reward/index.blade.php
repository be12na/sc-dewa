@php
    $pageHeading = __('breadcrumb.bonus.reward');
@endphp

@extends('layouts.main')

@section('pageContent')
    @include('partials.alert')
    @if ($summary = $authUser->summary_of_bonus_reward)
        <div class="row g-2 g-lg-3 mb-2 mb-lg-3 text-center">
            <div class="col-12 @if ($summary->is_agen) col-lg-4 @else col-sm-4 @endif d-flex">
                <div class="card flex-fill bg-theme-1 text-light">
                    <div class="card-body d-flex flex-column justify-contet-center">
                        <div><img src="{{ $summary->current_package->image_logo }}" alt="{{ $summary->current_package->name }}" style="height: 40px; width: auto;"></div>
                        <div class="fw-bold text-uppercase">{{ $summary->current_package->name }}</div>
                        <div class="small text-light text-opacity-50">Paket Anda</div>
                    </div>
                </div>
            </div>
            <div class="col-12 @if ($summary->is_agen) col-sm-6 col-lg-4 @else col-sm-8 @endif d-flex">
                <div class="card flex-fill bg-theme-1" style="--bs-bg-opacity:0.1;">
                    <div class="card-body text-theme-1 d-flex flex-column">
                        <div class="mb-1 pb-1 border-bottom border-theme-3 fs-auto">Poin Aktivasi</div>
                        <div class="row g-1 fs-3 flex-fill">
                            <div class="col-5">@formatNumber($summary->reseller->remaining_left)</div>
                            <div class="col-2 d-flex justify-content-center">
                                <div class="border-start border-theme-3"></div>
                            </div>
                            <div class="col-5">@formatNumber($summary->reseller->remaining_right)</div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($summary->is_agen)
                <div class="col-12 col-sm-6 col-lg-4 d-flex">
                    <div class="card flex-fill bg-theme-1" style="--bs-bg-opacity:0.1;">
                        <div class="card-body text-theme-1 d-flex flex-column">
                            <div class="mb-1 pb-1 border-bottom border-theme-3 fs-auto">Poin Upgrade & RO</div>
                            <div class="row g-1 fs-3 flex-fill">
                                <div class="col-5">@formatNumber($summary->agen->remaining_left)</div>
                                <div class="col-2 d-flex justify-content-center">
                                    <div class="border-start border-theme-3"></div>
                                </div>
                                <div class="col-5">@formatNumber($summary->agen->remaining_right)</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        @include('bonuses.reward.reseller-reward', ['summary' => $summary->reseller])

        @if ($summary->is_agen)
            @include('bonuses.reward.agen-reward', ['summary' => $summary->agen])
        @endif

        @if (($resellerClaims = $summary->reseller->claims)->isNotEmpty())
            <div class="fw-bold fs-auto">Riwayat Reward Member Aktivasi</div>
            <div class="d-block table-responsive border border-translucent">
                <table class="table table-hover table-striped table-header-centered table-header-bordered table-nowrap table-translucent fs-auto mb-2">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Reward</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($resellerClaims->sortByDesc('id')->values() as $userReward)
                            <tr>
                                <td>{{ translateDatetime($userReward->created_at, __('format.datetime.medium')) }}</td>
                                <td>{{ $userReward->reward->value }}</td>
                                <td class="text-center">
                                    @if ($userReward->completed)
                                        @translate('label.status.complete')
                                    @elseif ($userReward->rejected)
                                        @translate('label.status.rejected')
                                        @if (!empty($userReward->reject_note))
                                            <div class="small text-muted">{{ $userReward->reject_note }}</div>
                                        @endif
                                    @else
                                        @translate('label.status.pending')
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    @endif
@endsection

@push('topContent')
@include('partials.modals.modal', ['bsModalId' => 'my-modal', 'scrollable' => true])
@endpush
