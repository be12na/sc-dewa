@if (!empty($summary))
    <div class="fw-bold fs-auto mb-1">Reward Member Upgrade dan RO</div>
    <div class="row g-2 g-lg-3 mb-2 mb-lg-3">
        {{-- <div class="col-12 col-md-4 d-flex">
            <div class="card flex-fill bg-theme-1 text-light">
                <div class="card-body text-center d-flex flex-column justify-contet-center">
                    <div><img src="{{ $summary->package->image_logo }}" alt="{{ $summary->package->name }}" style="height: 40px; width: auto;"></div>
                    <div class="fw-bold text-uppercase">{{ $summary->package->name }}</div>
                    <div class="small text-light text-opacity-50">Paket Anda</div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 d-flex">
            <div class="card flex-fill bg-theme-1" style="--bs-bg-opacity:0.1;">
                <div class="card-body text-center d-flex flex-column justify-content-center">
                    <div class="fs-auto">Total Poin Kiri</div>
                    <div class="fs-3">@formatNumber($summary->total_user_left)</div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 d-flex">
            <div class="card flex-fill bg-theme-1" style="--bs-bg-opacity:0.1;">
                <div class="card-body text-center d-flex flex-column justify-content-center">
                    <div class="fs-auto">Total Poin Kanan</div>
                    <div class="fs-4">@formatNumber($summary->total_user_right)</div>
                </div>
            </div>
        </div> --}}
        <div class="col-12">
            <div class="d-block table-responsive border border-translucent">
                <table class="table table-hover table-striped table-header-centered table-header-bordered table-nowrap table-translucent fs-auto mb-2">
                    <thead>
                        <tr>
                            <th rowspan="2">#</th>
                            <th rowspan="2">Reward</th>
                            <th colspan="2">Syarat</th>
                            <th rowspan="2">Klaim</th>
                        </tr>
                        <tr>
                            <th class="border-start">Kiri</th>
                            <th>Kanan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $noUrut = 1;
                        @endphp
                        @foreach ($summary->rewards as $reward)
                            <tr>
                                <td class="text-center">{{ $noUrut++ }}.</td>
                                <td>{{ $reward->reward->value }}</td>
                                <td class="text-center">@formatNumber($reward->reward->left)</td>
                                <td class="text-center">@formatNumber($reward->reward->right)</td>
                                <td class="text-center">
                                    @if ($reward->can_claim)
                                        @include('partials.buttons.open-modal', [
                                            'isAjaxModal' => true,
                                            'modalUrl' => route('bonus.reward.claim', ['reward' => $reward->reward->id]),
                                            'btnClass' => 'btn-sm btn-success',
                                            'modalTarget' => '#my-modal',
                                            'btnText' => 'Klaim',
                                        ])
                                    @else
                                        @if ($reward->claimed)
                                            @if ($reward->claim->completed)
                                                <span class="text-success">
                                                    @translate('label.status.complete')
                                                </span>
                                            @else
                                                @translate('label.status.pending')
                                            @endif
                                        @else
                                            ---
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif