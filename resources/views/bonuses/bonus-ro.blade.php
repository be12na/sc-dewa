@php
    $pageHeading = __('breadcrumb.bonus.ro');
    $isCompany = $authUser->isGroup('admin');
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')

@if (!$isCompany)
    @php
        $summary = $authUser->summary_of_bonus_ro;
    @endphp
    <div class="row g-2 g-md-3 mb-3">
        <div class="col-12 col-lg-4 d-flex">
            <div class="flex-fill card bg-theme-1" style="--bs-bg-opacity:0.1;">
                <div class="card-body d-flex">
                    <div class="d-flex align-items-end flex-shrink-0">
                        <div><i class="fa fa-business-time fs-2 text-primary" style="--bs-text-opacity:0.75;"></i></div>
                    </div>
                    <div class="flex-fill d-flex flex-column justify-content-end text-end fs-auto">
                        <div class="fw-bold">@translate('label.summary.bonus.total')</div>
                        <div class="small text-muted">@translate('format.currency.symbol.text') @formatNumber($summary->total_bonus)</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4 d-flex">
            <div class="flex-fill card bg-theme-2" style="--bs-bg-opacity:0.1;">
                <div class="card-body d-flex">
                    <div class="d-flex align-items-end flex-shrink-0">
                        <div><i class="fa fa-money-bill-transfer fs-2 text-success" style="--bs-text-opacity:0.75;"></i></div>
                    </div>
                    <div class="flex-fill d-flex flex-column justify-content-end text-end fs-auto">
                        <div class="fw-bold">@translate('label.summary.bonus.withdrawed')</div>
                        <div class="small text-muted">@translate('format.currency.symbol.text') @formatNumber($summary->total_withdraw)</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4 d-flex">
            <div class="flex-fill card bg-theme-3" style="--bs-bg-opacity:0.1;">
                <div class="card-body d-flex">
                    <div class="d-flex align-items-end flex-shrink-0">
                        <div><i class="fa fa-coins fs-2 text-info"></i></div>
                    </div>
                    <div class="flex-fill d-flex flex-column justify-content-end text-end fs-auto">
                        <div class="fw-bold">@translate('label.summary.bonus.remaining')</div>
                        <div class="small text-muted">@translate('format.currency.symbol.text') @formatNumber($summary->remaining_bonus)</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="card">
    @if ($isCompany)
        @include('partials.card-header', ['cardTitle' => __('header.bonus.ro.index')])
    @endif
    <div class="card-body px-0 py-2">
        <div class="btn-group d-none" id="button-controls">
            @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
        </div>
        <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap fs-auto" id="my-table">
            <thead>
                <tr>
                    <th>@translate('text.date')</th>
                    @if ($isCompany)
                        <th>@translate('label.member')</th>
                        <th></th>
                    @endif
                    <th>@translate('label.from-member')</th>
                    <th></th>
                    <th>@translate('label.bonus') (@translate('format.currency.symbol.text'))</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('vendorCSS')
    @include('partials.datatable.css')
@endsection

@section('vendorJS')
    @include('partials.datatable.js')
@endsection

@push('pageJS')
<script>
    let table;
    const tableLang = {!! json_encode(__('datatable')) !!};

    function reloadTable() {
        dataTableRefresh(table);
    }

    $(function() {
        table = $('#my-table').DataTable({
            dom: '<"container-fluid gx-3"<"row g-2 mb-2"<"col-auto me-auto me-sm-0"<"dt-control">><"col-auto me-sm-auto"l><"col-12 col-sm-auto"f>>>r<"table-responsive border-top border-translucent"t><"row gy-1 gx-0"<"col-12 col-md"i><"col-12 col-md"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("bonus.ro.dataTable") }}'
            },
            deferLoading: 50,
            @if ($isCompany)
                search: {
                    return: true
                },
            @else
                searching: false,
                sort: false,
                lengthChange: false,
            @endif
            language: tableLang,
            columns: [
                {data: 'bonus_date', searchable: false},
                @if ($isCompany)
                    {data: 'owner_name'},
                    {data: 'owner_username', className: 'd-none'},
                @endif
                {data: 'from_name'},
                {data: 'from_username', className: 'd-none'},
                {data: 'bonus_amount', className: 'dt-body-right', searchable: false},
            ],
        });

        setDataTableLoading('my-table', '{{ __("datatable.processing") }}');
        setControlToDataTable('my-table', $('#button-controls'));
        $('.dataTables_filter label').addClass('d-block');
        $('.dataTables_filter .form-control').css({'margin-left': 0, 'width': '100%'});
        $('#my-table_filter .form-control').attr({placeholder: 'Member'});

        reloadTable();
    });
</script>
@endpush