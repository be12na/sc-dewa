@extends('layouts.app-mail')

@section('title', __('header.withdraw.info'))

{{-- @php
    $summaries = $user->summary_of_bonuses;
@endphp --}}

@section('content')
<div>Halo {{ $user->name }}</div>
{{-- <div>Berikut Informasi Ringkasan Bonus Anda</div> --}}
{{-- @include('partials.mail.row-space') --}}
{{-- @include('partials.mail.row', [
    'labelWidth' => '200px',
    'rowLabel' => 'Total bonus dalam proses',
    'rowValue' => __('format.currency.symbol.text') . ' ' . formatNumber($summaries->remaining_bonus),
]) --}}
{{-- @include('partials.mail.row', [
    'labelWidth' => '200px',
    'rowLabel' => 'Total bonus dalam proses',
    'rowValue' => __('format.currency.symbol.text') . ' ' . formatNumber($summaries->remaining_bonus),
]) --}}
{{-- @include('partials.mail.row', [
    'labelWidth' => '200px',
    'rowLabel' => 'Total bonus yang ditarik',
    'rowValue' => __('format.currency.symbol.text') . ' ' . formatNumber($summaries->total_withdraw),
]) --}}
{{-- @include('partials.mail.row-line', [
    'lineSize' => '2px',
    'lineStyle' => 'solid',
    'lineColor' => '#3a3a3a',
]) --}}
{{-- @include('partials.mail.row', [
    'labelWidth' => '200px',
    'rowLabel' => 'Total Semua Bonus',
    'rowValue' => __('format.currency.symbol.text') . ' ' . formatNumber($summaries->total_bonus),
]) --}}
@include('partials.mail.row-space')
<div>Anda telah mendapatkan penarikan bonus sejumlah <b>@translate('format.currency.symbol.text') @formatNumber($totalWithdraw)</b></div>
@include('partials.mail.row-footer')
@endsection
