<div class="modal-content">
    @include('partials.modals.modal-header', [
        'modalTitle' => __('header.package.detail')
    ])
    <div class="modal-body">
        @include('transactions.package.detail-content', ['transPackage' => $transPackage])
    </div>
    @include('partials.modals.modal-footer', [
        'modalFoterClass' => 'justify-content-center',
        'modalFooterContent' => [
            view('partials.buttons.close-modal', ['closeText' => __('button.close')])->render()
        ],
    ])
</div>