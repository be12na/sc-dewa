@php
    if ($type == TRANS_PKG_UPGRADE) {
        $pageHeading = __('breadcrumb.package.upgrade');
        $btnText = __('button.upgrade');
        $routeName = 'package.upgrade.selected';
    } elseif ($type == TRANS_PKG_RO) {
        $pageHeading = __('breadcrumb.package.ro');
        $btnText = __('button.ro');
        $routeName = 'package.ro.selected';
    } else {
        $pageHeading = __('breadcrumb.package.activate');
        $btnText = __('button.activate');
        $routeName = 'package.activate.selected';
    }
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-xl-4 g-3">
    @foreach ($packages as $package)
        <div class="col d-flex">
            <div class="card flex-fill" style="--bs-card-bg:var(--bs-theme);">
                <div class="card-body text-center d-flex">
                    <div class="flex-fill d-flex flex-column justify-content-between">
                        <div class="mb-3">
                            <div class="d-flex justify-content-center align-items-center mb-2">
                                <div class="package-logo border-0 rounded-0">
                                    <img src="{{ $package->image_logo }}" alt="{{ $package->name }}">
                                </div>
                            </div>
                            <div class="fw-bold mb-2">{{ $package->name }}</div>
                            <div class="fw-bold mb-2">{{ __('format.currency.symbol.text') }} @formatNumber($package->actual_price)</div>
                            <div class="border-bottom mb-2"></div>
                            <div class="fw-bold">@translate('label.benefit'):</div>
                            <div>@translate('menu.bonus-sponsor')</div>
                            @if ($package->bonus_extra > 0)
                                <div>@translate('menu.bonus-extra')</div>
                            @endif
                            @if ($package->bonus_package && BONUS_PACKAGE_ENABLED)
                                <div>@translate('menu.bonus-package')</div>
                            @endif
                            @if ($package->get_reward)
                                <div>@translate('menu.reward')</div>
                            @endif
                        </div>
                        <div>
                            @include('partials.buttons.open-modal', [
                                'btnClass' => 'btn-pill btn-primary px-5',
                                'btnText' => $btnText,
                                'modalUrl' => route($routeName, ['package' => $package->id]),
                                'modalTarget' => '#my-order-package',
                                'isAjaxModal' => true,
                            ])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection

@push('topContent')
    @include('partials.modals.modal-sm', ['bsModalId' => 'my-order-package', 'scrollable' => true, 'noEscape' => true])
@endpush

@push('pageCSS')
<style>
    .package-logo {
        --pkg-logo-size: 100px;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        flex-shrink: 0;
        border: 1px solid var(--bs-border-color-translucent);
        border-radius: 50%;
        overflow: hidden;
    }
    .package-logo > img {
        width: var(--pkg-logo-size);
        height: var(--pkg-logo-size);
        line-height: var(--pkg-logo-size);
        text-align: center;
    }
</style>
@endpush