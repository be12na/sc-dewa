@php
    $btnText = ($type == TRANS_PKG_UPGRADE)
        ? __('button.upgrade')
        : (($type == TRANS_PKG_RO) ? __('button.ro') : __('button.activate'));
@endphp

<form class="modal-content remote-submit" action="{{ $postUrl }}" method="POST" data-alert-container="#alert-form-container">
    @include('partials.modals.modal-header', ['modalTitle' => __('header.confirmation')])
    <div class="modal-body">
        @csrf
        <input type="hidden" name="package_id" value="{{ $package->id }}">
        <div id="alert-form-container"></div>
        <div class="text-center">
            <div class="d-flex justify-content-center align-items-center mb-2">
                <div class="package-logo border-0 rounded-0">
                    <img src="{{ $package->image_logo }}" alt="{{ $package->name }}">
                </div>
            </div>
            <div class="fw-bold mb-2">{{ $package->name }}</div>
            <div class="fw-bold mb-2">{{ __('format.currency.symbol.text') }} @formatNumber($package->actual_price)</div>
            <div class="border-bottom mb-2"></div>
            <div class="fw-bold">@translate('label.benefit'):</div>
            <div>@translate('menu.bonus-sponsor')</div>
            @if ($package->bonus_extra > 0)
                <div>@translate('menu.bonus-extra')</div>
            @endif
            @if ($package->bonus_package && BONUS_PACKAGE_ENABLED)
                <div>@translate('menu.bonus-package')</div>
            @endif
            @if ($package->get_reward)
                <div>@translate('menu.reward')</div>
            @endif
        </div>
    </div>
    @include('partials.modals.modal-footer', [
        'modalFoterClass' => 'justify-content-center',
        'modalFooterContent' => [
            view('partials.buttons.submit', [
                'btnIcon' => 'check',
                'btnText' => $btnText,
                'btnClass' => 'btn-primary btn-pill'
            ])->render(),
        ],
    ])
</form>
