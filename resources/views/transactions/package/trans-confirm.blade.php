@php
    $postUrl = route('package.transaction.confirm', ['transPackage' => $transPackage->id]);
@endphp

<form class="modal-content remote-submit" action="{{ $postUrl }}" method="POST" data-alert-container="#alert-form-container">
    @include('partials.modals.modal-header', [
        'modalTitle' => $transPackage->is_upgrade ? __('header.package.confirm-upgrade') : ($transPackage->is_repeat_order ? __('header.package.confirm-ro') : __('header.package.confirm-activate'))
    ])
    <div class="modal-body">
        @csrf
        <input id="confirm-action" type="hidden" name="action" value="">
        <div id="alert-form-container"></div>
        @include('transactions.package.detail-content', ['transPackage' => $transPackage])
    </div>
    @include('partials.modals.modal-footer', [
        'modalFooterContent' => [
            view('partials.buttons.submit', [
                'btnIcon' => 'handshake-simple',
                'btnText' => __('button.confirm'),
                'btnAttrs' => [
                    'name' => 'action',
                    'onclick' => "$('#confirm-action').val('confirm');"
                ],
            ])->render(),
            view('partials.buttons.submit', [
                'btnIcon' => 'hand',
                'btnText' => __('button.reject'),
                'btnClass' => 'btn-warning',
                'btnAttrs' => [
                    'name' => 'action',
                    'onclick' => "$('#confirm-action').val('reject');"
                ],
            ])->render(),
            view('partials.buttons.close-modal')->render()
        ],
    ])
</form>