@extends('layouts.app-mail')

@section('title', __('header.package.purchase-info', ['attr' => $transPackage->trans_type_name]))

@section('content')
@if (in_array($sendTo, ['admin', 'sponsor']))
    @php
        $buyer = $transPackage->buyer;
    @endphp

    @include('partials.mail.rows-header', ['textHeader' => __('label.member') ])
    @include('partials.mail.row', [
        'rowLabel' => __('label.user.name'),
        'rowValue' => "<div>{$buyer->name} ($buyer->username)</div><div>{$buyer->phone}</div>"
    ])
    @include('partials.mail.row-space')
@endif

@php
    $package = $transPackage->package;
    // $packageName = $package->name;
    // $sellerName = $transPackage->seller_name;
    // if (!$transPackage->is_company_seller) {
    //     $sellerName = "{$sellerName} ({$transPackage->seller_username})";
    // }

    // $sellerPhone = $transPackage->seller_phone;
@endphp

@include('partials.mail.rows-header', ['textHeader' => __('label.package.title')])
@include('partials.mail.row', [
    'rowLabel' => __('label.trans-no'),
    'rowValue' => $transPackage->code,
])
@include('partials.mail.row', [
    'rowLabel' => 'Jenis',
    'rowValue' => $transPackage->trans_type_name
])
@include('partials.mail.row', [
    'rowLabel' => __('label.package.name'),
    'rowValue' => $package->name
])
@include('partials.mail.row', [
    'rowLabel' => __('label.package.price'),
    'rowValue' => __('format.currency.symbol.text') . ' ' . formatNumber($transPackage->package_price)
])
@include('partials.mail.row', [
    'rowLabel' => __('label.package.unique-digit'),
    'rowValue' => formatNumber($transPackage->unique_digit)
])
@include('partials.mail.row', [
    'rowLabel' => __('label.package.total-transfer'),
    'rowValue' => __('format.currency.symbol.text') . ' ' . formatNumber($transPackage->package_price + $transPackage->unique_digit)
])
@include('partials.mail.row-space')

@if (!in_array($sendTo, ['admin', 'sponsor']))
    {{-- @include('partials.mail.row', [
        'rowLabel' => __('label.order-to'),
        'rowValue' => "<div>{$sellerName}</div><div>{$sellerPhone}</div>"
    ]) --}}

    @if ($transPackage->seller_banks->isNotEmpty())
        @include('partials.mail.row-space')
        <div>@translate('text.please-transfer-to'):</div>
        <ol>
            @foreach ($transPackage->seller_banks as $userBank)
                <li><span style="font-weight:600; margin-right:5px;">{{ $userBank->bank_code }}</span><span style="margin-right:5px;">-</span><span style="margin-right:5px;">{{ $userBank->acc_no }}</span><span style="margin-right:5px;">({{ $userBank->acc_name }})</span></li>
            @endforeach
        </ol>
    @endif

    @include('partials.mail.row-space')
    <div>{!! __('text.confirm-whatsapp', ['whatsapp' => $transPackage->seller_phone]) !!}</div>
    @include('partials.mail.row-space')
    @include('partials.mail.row-space')
    @include('partials.mail.rows-header', ['textHeader' => config('app.name') ])
@endif
@endsection
