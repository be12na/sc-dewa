@php
    $pageHeading = __('breadcrumb.package.transaction.list');
    $hasRole = hasRole(['admin.super', 'admin.master', 'member.member']);
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<div class="card">
    @include('partials.card-header', ['cardTitle' => __('header.package.transaction')])
    <div class="card-body px-0 py-2">
        <div class="btn-group d-none" id="button-controls">
            @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
        </div>
        <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap" id="my-table">
            <thead>
                <tr>
                    <th>@translate('label.date')</th>
                    <th>@translate('label.trans-no')</th>
                    @if (!$authUser->is_member_user)
                        <th>@translate('label.member')</th>
                        <th></th>
                        <th></th>
                    @endif
                    <th>Jenis</th>
                    <th>@translate('label.package.title')</th>
                    @if ($hasRole)
                        <th></th>
                    @endif
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('topContent')
    @include('partials.modals.modal', ['bsModalId' => 'my-modal', 'scrollable' => true, 'noEscape' => true])
@endpush

@section('vendorCSS')
    @include('partials.datatable.css')
@endsection

@section('vendorJS')
    @include('partials.datatable.js')
@endsection

@push('pageJS')
<script>
    let table;
    const tableLang = {!! json_encode(__('datatable')) !!};

    function reloadTable() {
        dataTableRefresh(table);
    }

    $(function() {
        table = $('#my-table').DataTable({
            dom: '<"container-fluid gx-3"<"row g-2 mb-2"<"col-auto me-auto me-sm-0"<"dt-control">><"col-auto me-sm-auto"l><"col-12 col-sm-auto"f>>>r<"table-responsive border-top border-translucent"t><"row gy-1 gx-0"<"col-12 col-md"i><"col-12 col-md"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("package.transaction.dataTable") }}'
            },
            deferLoading: 50,
            language: tableLang,
            @if ($authUser->is_member_user)
                searching: false,
                sort: false,
                lengthChange: false,
            @endif
            columns: [
                {data: 'created_at', searchable: false},
                {data: 'code'},
                @if (!$authUser->is_member_user)
                    {data: 'buyer_name'},
                    {data: 'username', className: 'd-none'},
                    {data: 'phone', className: 'd-none'},
                @endif
                {data: 'trans_type', searchable: false},
                {data: 'package_name'},
                @if ($hasRole)
                    {data: 'action', className: 'dt-body-center', orderable: false, searchable: false},
                @endif
            ],
        });

        setDataTableLoading('my-table', '{{ __("datatable.processing") }}');
        setControlToDataTable('my-table', $('#button-controls'));

        @if (!$authUser->is_member_user)
            $('.dataTables_filter label').addClass('d-block');
            $('.dataTables_filter .form-control').css({'margin-left': 0, 'width': '100%'});
            // $('#my-table_filter .form-control').attr({placeholder: 'Cari {{ __("label.trans-no") }}'});
        @endif

        reloadTable();
    });
</script>
@endpush
