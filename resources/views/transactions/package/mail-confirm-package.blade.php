@extends('layouts.app-mail')

@section('title', __('header.package.purchase-info', ['attr' => $transPackage->trans_type_name]))

@section('content')
<div style="margin-bottom:1rem;">
    Hi... <span style="font-size:20px; font-weight:600;">{{ $transPackage->buyer->name }}</span> ({{ $transPackage->buyer->username }})
</div>
<div style="margin-bottom: 2rem;">
    @if ($transPackage->is_upgrade)
        {{ __('message.mail.upgraded') . ' ' . __('text.to-be') . ' ' . $transPackage->package->name }}
    @elseif ($transPackage->is_repeat_order)
        @translate('message.mail.repeat-order')
    @else
        {{ __('message.mail.activated') . ' ' . __('text.as') . ' ' . $transPackage->package->name }}
    @endif
</div>
@include('partials.mail.row-space')
<a href="{{ route('welcome') }}">{{ config('app.name') }}</a>
@endsection
