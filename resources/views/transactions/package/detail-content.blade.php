<div class="row g-2 mb-3">
    <div class="col-12 d-flex flex-nowrap">
        <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">@translate('label.package.title')</div>
        <div class="flex-fill ms-3">{{ $transPackage->package->name }}</div>
    </div>
    <div class="col-12 d-flex flex-nowrap">
        <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">Jenis</div>
        <div class="flex-fill ms-3">{{ $transPackage->is_repeat_order ? 'Repeat Order' : ($transPackage->is_upgrade ? 'Upgrade' : 'Aktivasi') }}</div>
    </div>
    <div class="col-12 d-flex flex-nowrap">
        <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">@translate('label.package.price')</div>
        <div class="flex-fill ms-3">@translate('format.currency.symbol.text') @formatNumber($transPackage->package_price)</div>
    </div>
    <div class="col-12 d-flex flex-nowrap">
        <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">@translate('label.package.unique-digit')</div>
        <div class="flex-fill ms-3">@formatNumber($transPackage->unique_digit)</div>
    </div>
    <div class="col-12 d-flex flex-nowrap">
        <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">@translate('label.package.total-transfer')</div>
        <div class="flex-fill ms-3">@translate('format.currency.symbol.text') @formatNumber($transPackage->package_price + $transPackage->unique_digit)</div>
    </div>
    <div class="col-12 d-flex flex-nowrap">
        <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">Status</div>
        <div class="flex-fill ms-3"><span class="text-{{ $transPackage->status_color }}">{{ $authUser->is_member_user ? $transPackage->status_text_member : $transPackage->status_text_admin }}</span></div>
    </div>
    @if ($authUser->is_member_user && ($transPackage->trans_order || $transPackage->trans_rejected))
        @if ($transPackage->seller_banks->isNotEmpty())
            <div class="col-12 pt-2">
                <div>@translate('text.please-transfer-to'):</div>
                <ol class="mb-0">
                    @foreach ($transPackage->seller_banks as $userBank)
                        <li><span class="fw-bold me-1">{{ $userBank->bank_code }}</span><span class="me-1">-</span><span class="me-1">{{ $userBank->acc_no }}</span><span class="me-1">({{ $userBank->acc_name }})</span></li>
                    @endforeach
                </ol>
            </div>
        @endif
        <div class="col-12 pt-3">
            {!! __('text.confirm-whatsapp', ['whatsapp' => $transPackage->seller_phone]) !!}
        </div>
    @endif
</div>
@if (!$authUser->is_member_user)
    <div class="fw-bold mb-2">@translate('label.member')</div>
    <div class="row g-2">
        <div class="col-12 d-flex flex-nowrap">
            <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">@translate('label.user.name')</div>
            <div class="flex-fill ms-3">{{ $transPackage->buyer->name }}</div>
        </div>
        <div class="col-12 d-flex flex-nowrap">
            <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">@translate('label.user.username')</div>
            <div class="flex-fill ms-3">{{ $transPackage->buyer->username }}</div>
        </div>
        <div class="col-12 d-flex flex-nowrap">
            <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">@translate('label.user.phone')</div>
            <div class="flex-fill ms-3">{{ $transPackage->buyer->phone }}</div>
        </div>
    </div>
@endif