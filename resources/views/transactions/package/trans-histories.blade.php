@php
    $pageHeading = __('breadcrumb.package.history');
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<div class="card">
    @include('partials.card-header', ['cardTitle' => __('header.package.history')])
    <div class="card-body px-0 py-2">
        <div class="btn-group d-none" id="button-controls">
            @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
        </div>
        <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap" id="my-table">
            <thead>
                <tr>
                    <th>@translate('label.date')</th>
                    <th>@translate('label.trans-no')</th>
                    @if (!$authUser->is_member_user)
                        <th>@translate('label.member')</th>
                        <th></th>
                        <th></th>
                    @endif
                    <th>Jenis</th>
                    <th>@translate('label.package.title')</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('topContent')
    @include('partials.modals.modal', ['bsModalId' => 'my-modal', 'scrollable' => true, 'noEscape' => true])
@endpush

@section('vendorCSS')
    @include('partials.datatable.css')
@endsection

@section('vendorJS')
    @include('partials.datatable.js')
@endsection

@push('pageJS')
<script>
    let table;
    const tableLang = {!! json_encode(__('datatable')) !!};

    function reloadTable() {
        dataTableRefresh(table);
    }

    $(function() {
        table = $('#my-table').DataTable({
            dom: '<"container-fluid gx-3"<"row g-2 mb-2"<"col-auto me-auto me-sm-0"<"dt-control">><"col-auto me-sm-auto"l><"col-12 col-sm-auto"f>>>r<"table-responsive border-top border-translucent"t><"row gy-1 gx-0"<"col-12 col-md"i><"col-12 col-md"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("package.history.dataTable") }}'
            },
            deferLoading: 50,
            language: tableLang,
            @if ($authUser->is_member_user)
                searching: false,
                sort: false,
                lengthChange: false,
            @else
                columnDefs: [
                    {searchable: false, targets: [0, 2, 3, 4]},
                    {orderable: false, targets: [4]},
                ],
            @endif
            columns: [
                // {data: 'created_at'},
                // {data: 'code'},
                // @if (!$authUser->is_member_user)
                //     {data: 'buyer_id'},
                // @endif
                // {data: 'package_id'},
                // {data: 'status', className: 'dt-body-center'},

                {data: 'created_at', searchable: false},
                {data: 'code'},
                @if (!$authUser->is_member_user)
                    {data: 'buyer_name'},
                    {data: 'username', className: 'd-none'},
                    {data: 'phone', className: 'd-none'},
                @endif
                {data: 'trans_type', searchable: false},
                {data: 'package_name'},
                {data: 'status', className: 'dt-body-center', searchable: false},
            ],
        });

        setDataTableLoading('my-table', '{{ __("datatable.processing") }}');
        setControlToDataTable('my-table', $('#button-controls'));

        @if (!$authUser->is_member_user)
            $('.dataTables_filter label').addClass('d-block');
            $('.dataTables_filter .form-control').css({'margin-left': 0, 'width': '100%'});
            // $('#my-table_filter .form-control').attr({placeholder: 'Cari {{ __("label.trans-no") }}'});
        @endif

        reloadTable();
    });
</script>
@endpush
