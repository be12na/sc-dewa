@php
    $pesan = isset($message) ? $message : session()->pull('message');
    $pesanClass = isset($messageClass) ? $messageClass : session()->pull('messageClass');
@endphp

@if ($pesan && $pesanClass)
    @php
        $alertClass = ($pesanClass == 'error') ? 'danger' : $pesanClass;
        $showBtnClose = isset($showBtnClose)
            ? ($showBtnClose === true)
            : (session()->pull('messageShowClose', true) === true);
        $showHeader = isset($showHeader)
            ? ($showHeader === true)
            : (session()->pull('messageShowHeader', true) === true);

        $alertTitle = '';

        if ($alertClass == 'danger') {
            $iconClass = 'times-circle';
            $alertTitle = __('alert.title.danger');
        } elseif ($alertClass == 'success') {
            $iconClass = 'check-circle';
            $alertTitle = __('alert.title.success');
        } elseif ($alertClass == 'warning') {
            $iconClass = 'exclamation-circle';
            $alertTitle = __('alert.title.warning');
        } elseif ($alertClass == 'info') {
            $iconClass = 'info-circle';
            $alertTitle = __('alert.title.info');
        } else {
            $iconClass = 'question-circle';
            $alertTitle = __('alert.title.question');
        }
    @endphp
    <div class="alert alert-{{ $alertClass }} alert-dismissible {{ $showHeader ? 'pe-3' : '' }}" role="alert">
        @if ($showHeader)
            <h5 class="alert-heading pe-5">
                {{ $alertTitle }}
                @if ($showBtnClose)
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" style="top:1.35rem;"></button>
                @endif
            </h5>
        @endif
        <div class="d-flex flex-nowrap">
            <div class="flex-shrink-0 me-3 lh-1 fs-3"><i class="fa fa-{{ $iconClass }}"></i></div>
            <div>{!! $pesan !!}</div>
            @if (!$showHeader && $showBtnClose)
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" style="top:1.35rem;"></button>
            @endif
        </div>
    </div>
@endif

