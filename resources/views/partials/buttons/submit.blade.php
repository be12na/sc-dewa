@php
    $attrs = '';
    if (isset($btnAttrs) && is_array($btnAttrs)) {
        $attrs = implode(' ', array_map(function($key, $value) {
            return "{$key}=\"{$value}\"";
        }, array_keys($btnAttrs), array_values($btnAttrs)));

        unset($btnAttrs);
    }
@endphp

<button type="submit" class="btn {{ (isset($btnClass) && $btnClass) ? $btnClass : 'btn-success' }}" {!! $attrs !!}>
    @isset($btnIcon)
        <i class="fa fa-{{ $btnIcon }} me-2"></i>
    @endisset
    {!! $btnText !!}
</button>