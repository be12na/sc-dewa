@php
    $isAjaxModal = (isset($isAjaxModal) && ($isAjaxModal === true));
    $attrs = [
        'data-bs-toggle="modal"',
        'data-bs-target="' . $modalTarget . '"',
    ];

    if ($isAjaxModal) {
        $attrs[] =  'data-modal-url="' . $modalUrl . '"';
    }
@endphp

<button type="button" class="btn {{ (isset($btnClass) && $btnClass) ? $btnClass : '' }}" {!! implode(' ', $attrs) !!}>
    {!! $btnText !!}
</button>