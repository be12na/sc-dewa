<button type="button" class="btn {{ (isset($btnClass) && $btnClass) ? $btnClass : 'btn-danger' }}" data-bs-dismiss="modal">
    <i class="fa fa-{{ isset($closeIcon) ? $closeIcon : 'times' }} me-2"></i>
    {{ isset($closeText) ? $closeText : __('button.cancel') }}
</button>