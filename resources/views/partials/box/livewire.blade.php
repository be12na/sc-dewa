@livewire($livewireName, $livewireArgs)

@php
    $hasArgs = is_array($livewireArgs) ? (count($livewireArgs) > 0) : false;
@endphp

<livewire:{{ $livewireName }} @if($hasArgs) @foreach($livewireArgs as $key => $value) :{{ $key }}="{{ $value }}" @endforeach @endif>