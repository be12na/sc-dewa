<div class="col-9 d-flex flex-column">
    <div class="flex-fill pt-2 px-3 d-flex flex-column justify-content-end {{ $hasFooter ? 'border-bottom border-gray-500 pb-1' : 'pb-2' }}">
        {!! $contentBoxBody !!}
    </div>
    @if ($hasFooter)
        <div class="flex-shrink-0 pt-1 pb-2 px-3 d-flex justify-content-between align-items-end">
            {!! $contentBoxFooter !!}
        </div>
    @endif
</div>