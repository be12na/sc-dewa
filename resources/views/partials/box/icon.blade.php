<div class="col-3 d-flex">
    {{-- bg-theme-1 bg-opacity-75 --}}
    <div class="flex-fill flex-shrink-0 d-flex align-items-center justify-content-center px-2 py-3 rounded-4 {{ $background }}">
        {{-- <img src="{{ $authUser->package->image_logo }}" alt="{{ $authUser->package->name }}" style="width:50px; height:auto; max-width:60%;"> --}}
        {!! $contentBoxIcon !!}
    </div>
</div>