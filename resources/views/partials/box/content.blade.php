<div class="flex-fill d-flex rounded-4 {{ implode(' ', [$boxBorder, $boxBackground, $boxColor]) }}">
    <div class="row flex-fill g-0 flex-nowrap">
        @include('partials.box.icon', [
            'background' => $backgroundIcon,
            'contentBoxIcon' => $contentIcon,
        ])
        @include('partials.box.body', [
            'contentBoxBody' => $contentBody,
            'hasFooter' => $boxHasFooter,
            'contentBoxFooter' => $contentFooter,
        ])
    </div>
</div>