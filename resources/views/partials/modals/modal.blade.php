<div class="modal fade" id="{{ isset($bsModalId) ? $bsModalId : 'myModal' }}" @if(!isset($noEscape) || ($noEscape !== true)) tabindex="-1" @endif>
    <div class="modal-dialog {{ (isset($modalCentered) && $modalCentered === true) ? 'modal-dialog-centered' : '' }} {{ (isset($scrollable) && $scrollable === true) ? 'modal-dialog-scrollable' : '' }}" id="{{ (isset($bsModalId) ? $bsModalId : 'myModal') . '-dialog' }}">
    </div>
</div>