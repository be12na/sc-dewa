@php
    $modalIcon = (isset($useIcon) && ($useIcon === true)) ? env('IMAGE_MODAL_ICON') : false;
@endphp
<div class="modal-header">
    <div class="modal-title fw-bold">
        @if ($modalIcon)
            <img src="{{ asset($modalIcon) }}" style="height: 10px;" class="me-3">
        @endif
        {{ $modalTitle }}
    </div>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>