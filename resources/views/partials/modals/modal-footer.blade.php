<div class="modal-footer {{ isset($modalFoterClass) ? $modalFoterClass : '' }}">
    @php
        $modalFooterContent = is_array($modalFooterContent) ? implode('', $modalFooterContent) : $modalFooterContent;
    @endphp
    {!! $modalFooterContent !!}
</div>