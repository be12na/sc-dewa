<div class="modal fade" id="{{ isset($bsModalId) ? $bsModalId : 'myModalLG' }}" @if(!isset($noEscape) || ($noEscape !== true)) tabindex="-1" @endif>
    <div class="modal-dialog modal-lg {{ (isset($modalCentered) && $modalCentered === true) ? 'modal-dialog-centered' : '' }} {{ (isset($scrollable) && $scrollable === true) ? 'modal-dialog-scrollable' : '' }}" id="{{ (isset($bsModalId) ? $bsModalId : 'myModalLG') . '-dialog' }}">
    </div>
</div>