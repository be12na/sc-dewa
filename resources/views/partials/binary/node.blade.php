@if ($level <= $maxLevel)
    @php
        $isPlacement = (isset($isPlacement) && ($isPlacement === true));
        $nextLevel = $level + 1;
        $topMember = isset($topMember) ? $topMember : $authUser;
    @endphp
    <li>
        @if (empty($node))
            @if ($isPlacement)
                <a href="#" class="node-link node-placement" data-bs-toggle="modal" data-bs-target="#tree-modal" data-modal-url="{{ route('member.placement.select', ['member' => $parentMember, 'position' => $position]) }}">
                    <i class="fa fa-user-plus text-theme-3 fs-3"></i>
                </a>
            @else
                <a href="#" class="node-link node-empty">
                    <i class="fa fa-user-slash text-danger fs-3"></i>
                </a>
                @if ($nextLevel <= $maxLevel)
                    <ul>
                        @include('partials.binary.node', [
                            'node' => null,
                            'isPlacement' => false,
                            'isTopNode' => false,
                            'parentMember' => null,
                            'level' => $nextLevel,
                            'maxLevel' => $maxLevel,
                        ])
                        @include('partials.binary.node', [
                            'node' => null,
                            'isPlacement' => false,
                            'isTopNode' => false,
                            'parentMember' => null,
                            'level' => $nextLevel,
                            'maxLevel' => $maxLevel,
                        ])
                    </ul>
                @endif
            @endif
        @else
            @php
                $isTopNode = ($isTopNode === true);
                $nodeUser = $node->user;
                $package = $nodeUser->package;
                $openChild = !$isTopNode;
                $routeParams = [
                    'member' => $node->user_id,
                    'opt' => $isPlacement ? 'placement' : 'detail',
                ];
                if (!$isTopNode) {
                    $routeParams['open'] = 1;
                }
                if ($nodeUser->id != $authUser->id) {
                    $routeParams['me'] = 1;
                }
            @endphp
            <a href="#" class="node-link" data-bs-toggle="modal" data-bs-target="#tree-modal" data-modal-url="{{ route('network.tree.detail', $routeParams) }}">
                @include('partials.binary.node-header', ['nodeUser' => $nodeUser])
                @include('partials.binary.node-body', ['nodeUser' => $nodeUser])
            </a>

            @if ($nextLevel <= $maxLevel)
                <ul>
                    @if ($node->hasChildren())
                        @php
                            $children = $node->children->sortBy('side_position')->values();
                        @endphp
                        @if ($children->count() > 1)
                            @foreach ($children->sortBy('side_position')->values() as $childNode)
                                @include('partials.binary.node', [
                                    'node' => $childNode,
                                    'isPlacement' => $isPlacement,
                                    'isTopNode' => false,
                                    'level' => $nextLevel,
                                    'maxLevel' => $maxLevel,
                                    'topMember' => $topMember,
                                ])
                            @endforeach
                        @else
                            @php
                                $child = $children->first();
                                $isToLeft = ($child->side_position > USER_LEFT_POSITION);
                            @endphp
                            @if ($isToLeft)
                                @include('partials.binary.node', [
                                    'node' => null,
                                    'isPlacement' => $isPlacement,
                                    'isTopNode' => false,
                                    'parentMember' => $node->user_id,
                                    'position' => USER_LEFT_POSITION,
                                    'level' => $nextLevel,
                                    'maxLevel' => $maxLevel,
                                ])
                                @include('partials.binary.node', [
                                    'node' => $child,
                                    'isPlacement' => $isPlacement,
                                    'isTopNode' => false,
                                    'level' => $nextLevel,
                                    'maxLevel' => $maxLevel,
                                    'topMember' => $topMember,
                                ])
                            @else
                                @include('partials.binary.node', [
                                    'node' => $child,
                                    'isPlacement' => $isPlacement,
                                    'isTopNode' => false,
                                    'level' => $nextLevel,
                                    'maxLevel' => $maxLevel,
                                    'topMember' => $topMember,
                                ])
                                @include('partials.binary.node', [
                                    'node' => null,
                                    'isPlacement' => $isPlacement,
                                    'isTopNode' => false,
                                    'parentMember' => $node->user_id,
                                    'position' => USER_RIGHT_POSITION,
                                    'level' => $nextLevel,
                                    'maxLevel' => $maxLevel,
                                ])
                            @endif
                        @endif
                    @else
                        @include('partials.binary.node', [
                            'node' => null,
                            'isPlacement' => $isPlacement,
                            'isTopNode' => false,
                            'parentMember' => $node->user_id,
                            'position' => USER_LEFT_POSITION,
                            'level' => $nextLevel,
                            'maxLevel' => $maxLevel,
                        ])
                        @include('partials.binary.node', [
                            'node' => null,
                            'isPlacement' => $isPlacement,
                            'isTopNode' => false,
                            'parentMember' => $node->user_id,
                            'position' => USER_RIGHT_POSITION,
                            'level' => $nextLevel,
                            'maxLevel' => $maxLevel,
                        ])
                    @endif
                </ul>
            @endif
        @endif
    </li>
@endif
