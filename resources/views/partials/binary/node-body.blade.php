@php
    $isAgen = ($nodeUser->my_package_id == TOP_PACKAGE);
    $summaries = $nodeUser->summary_of_bonus_reward;
    $summary = $isAgen ? $summaries->agen : $summaries->reseller;
    $structure = $nodeUser->structure;
@endphp

<div class="node-title">{{ $nodeUser->name }}</div>
<div class="node-body">
    {{-- @foreach ($packages as $package)
        @php
            $pkgCount = $structure->descendants()->whereHas('user', function($user) use($package) {
                return $user->where('package_id', '=', $package->id);
            })->count();
        @endphp
        <div class="d-flex justify-content-between">
            <span>{{ strtoupper(substr($package->name, 0, 1)) }}</span>
            <span>@formatNumber($pkgCount)</span>
        </div>
    @endforeach --}}
    {{-- <div class="py-1 border-bottom"></div> --}}
    {{-- <div class="text-center">Member</div> --}}
    <div class="d-flex flex-nowrap text-center">
        <div style="flex-basis:50%;">@formatNumber($nodeUser->count_left)</div>
        <div class="border-start" style="flex-basis:50%;">@formatNumber($nodeUser->count_right)</div>
    </div>
    {{-- <div class="py-1 border-bottom"></div>
    <div class="text-center">Poin</div>
    <div class="d-flex flex-nowrap text-center">
        <div style="flex-basis:50%;">@formatNumber($summary->remaining_left)</div>
        <div class="border-start" style="flex-basis:50%;">@formatNumber($summary->remaining_right)</div>
    </div> --}}
</div>
