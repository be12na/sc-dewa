@php
    $isPlacement = (isset($isPlacement) && ($isPlacement === true));
    $containerClass = isset($containerClass) ? $containerClass : '';
    $packages = \App\Models\Package::query()->orderBy('id')->get();
@endphp

@push('pageCSS')
    <link rel="stylesheet" href="{{ asset('assets/css/tree.css') }}?v={{ $fileV }}">
@endpush

<div class="binary-container {{ $containerClass }}">
    <div class="d-flex">
        <div class="tree mx-auto">
            <ul>
                @include('partials.binary.node', [
                    'node' => $structure,
                    'isPlacement' => $isPlacement,
                    'isTopNode' => true,
                    'level' => 0,
                    'maxLevel' => $maxLevel,
                    'topMember' => $topMember,
                ])
            </ul>
        </div>
    </div>
</div>

@push('topContent')
@include('partials.modals.modal', [
    'bsModalId' => 'tree-modal',
    'scrollable' => true,
    'modalCentered' => true,
])
@endpush
