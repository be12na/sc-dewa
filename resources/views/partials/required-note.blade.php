@if (isset($textOnly) && ($textOnly === true))
    <span class="text-danger me-1">*</span>
    <span class="fst-italic small">Wajib diisi</span>
@else
    <div class="d-block text-nowrap">
        <span class="text-danger me-1">*</span>
        <span class="fst-italic small">Wajib diisi</span>
    </div>
@endif