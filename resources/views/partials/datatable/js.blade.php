<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/DataTables-1.11.4/js/dataTables.bootstrap5.min.js') }}"></script>
{{-- @if (isset($responsive) && ($responsive === true))
    <script src="{{ asset('assets/vendor/datatables/RowReorder-1.2.8/js/dataTables.rowReorder.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables/RowReorder-1.2.8/js/rowReorder.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables/Responsive-2.2.9/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables/Responsive-2.2.9/js/responsive.bootstrap5.min.js') }}"></script>
@endif --}}
<script src="{{ asset('assets/js/dataTables.js') }}"></script>
<script>
    const datatableLanguange = {
        lengthMenu: "{{ __('datatable.lengthMenu') }}",
        zeroRecords: "{{ __('datatable.zeroRecords') }}",
        info: "{{ __('datatable.info') }}",
        infoEmpty: "{{ __('datatable.infoEmpty') }}",
        infoFiltered: "{{ __('datatable.infoFiltered') }}",
        search: "{{ __('datatable.search') }}",
        paginate: {
            first: "{!! __('datatable.paginate.first') !!}",
            previous: "{!! __('datatable.paginate.previous') !!}",
            next: "{!! __('datatable.paginate.next') !!}",
            last: "{!! __('datatable.paginate.last') !!}"
        }
    };
</script>