<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/DataTables-1.11.4/css/dataTables.bootstrap5.min.css') }}">

@if (isset($responsive) && ($responsive === true))
    <link rel="stylesheet" href="{{ asset('assets/vendor/datatables/RowReorder-1.2.8/css/rowReorder.bootstrap5.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/datatables/Responsive-2.2.9/css/responsive.bootstrap5.min.css') }}">
@endif