@if (isset($link) && !empty($link))
    @php
        $isScript = (isset($mode) && ($mode === 'script'));
        $isStyle = (isset($mode) && ($mode === 'style'));
    @endphp

    @if ($isScript)
        <script>
            let copyClipboard;
            function copySuccess()
            {
                copyClipboard.addClass('show');

                setTimeout(() => {
                    copyClipboard.addClass('hidding');

                    setTimeout(() => {
                        copyClipboard.removeClass('show hidding');
                    }, 1200);
                }, 3000);
            }
            $(function() {
                copyClipboard = $('#copyClipboard');

                $('#btn-copy-reflink').on('click', function() {
                    var $temp = $("<input>");
                    $("body").append($temp);
                    $temp.val($('#reflink-text').val()).select();
                    document.execCommand("copy");
                    $temp.remove();
                    copySuccess()
                });
            });
        </script>
    @elseif ($isStyle)
        <style>
            .copy-clipboard {
                display: inline-block;
                padding: 0.125rem 0.25rem;
                background-color: rgba(var(--bs-info-rgb), 0.5);
                line-height: 1;
                font-size: 12px;
                border-radius: 4px;
                opacity: 1;
                transition: opacity 1.5s ease, line-height 1.5s ease, font-size 1.5s ease, padding 1.5s ease;
            }
            .copy-clipboard:not(.show) {
                display: none;
            }
            .copy-clipboard.hidding {
                opacity: 0;
                line-height: 0;
                font-size: 0;
                padding: 0;
            }
            .btn-shortcut {
                --bs-btn-color: var(--bs-gray-100);
                --bs-btn-bg: rgba(var(--bs-dark-rgb), 0.5);
                --bs-btn-hover-bg: rgba(var(--bs-dark-rgb), 0.6);
                --bs-btn-active-bg: rgba(var(--bs-dark-rgb), 0.7);
                --bs-btn-border-color: rgba(var(--bs-dark-rgb), 0.5);
                --bs-btn-hover-border-color: rgba(var(--bs-dark-rgb), 0.6);
                --bs-btn-active-border-color: rgba(var(--bs-dark-rgb), 0.7);
                display: block;
                width: 100%;
            }
        </style>
    @else
        <div class="flex-fill row g-2">
            <div class="col-12 text-center text-md-end text-lg-start text-wrap user-select-none">
                <div class="d-block small">{!! contentUrl($link) !!}</div>
                {{-- <div class="d-none" id="reflink-text">{{ $link }}</div> --}}
                <div class="copy-clipboard" id="copyClipboard">Sudah di-copy ke clipboard</div>
                <input type="hidden" id="reflink-text" value="{{ $link }}">
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-end justify-content-lg-start">
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-outline-dark" id="btn-copy-reflink" title="Copy" onclick="">
                        <i class="fa fa-copy"></i>
                    </button>
                    <a target="_blank" href="whatsapp://send?text={{ $link }}" class="btn btn-sm btn-outline-success" title="Share to whatsapp">
                        <i class="fa-brands fa-whatsapp"></i>
                    </a>
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ $link }}" class="btn btn-sm btn-outline-primary" title="Share to facebook">
                        <i class="fa-brands fa-facebook"></i>
                    </a>
                </div>
            </div>
        </div>
    @endif
@endif