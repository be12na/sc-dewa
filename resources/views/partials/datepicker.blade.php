@php
    $format = 'd F Y';
    $dateValue = isset($dateValue) ? translateDatetime($dateValue, $format) : '';
    $endDate = (isset($endDate) && !empty($endDate)) ? translateDatetime($endDate, $format) : '';
    $inputDateId = isset($dateId) ? $dateId : 'date-' . mt_rand(0, 999999);
    $inputDateName = isset($dateName) ? $dateName : '';
    $datePickerFormat = isset($datePickerFormat) ? $datePickerFormat : 'dd MM yyyy';
    $inputRequired = (isset($required) && ($required === true)) ? 'required' : '';
    $inputPlaceholder = isset($placeholder) ? $placeholder : 'Tanggal';
    $inputSize = isset($inputSize) ? "form-control-{$inputSize}" : '';
@endphp
<div class="input-group">
    <input type="text" id="{{ $inputDateId }}" @if($inputDateName != '') name="{{ $inputDateName }}" @endif class="form-control {{ $inputSize }} bg-white bs-date" value="{{ $dateValue }}" data-date-format="{{ $datePickerFormat }}" data-date-end-date="{{ $endDate }}" placeholder="{{ $inputPlaceholder }}" autocomplete="off" readonly {{ $inputRequired }}>
    <label for="{{ $inputDateId }}" class="input-group-text cursor-pointer">
        <i class="fa-solid fa-calendar-days"></i>
    </label>
</div>