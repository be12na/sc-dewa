<script>
    const datatableLanguange = {
        lengthMenu: "{{ __('datatable.lengthMenu') }}",
        zeroRecords: "{{ __('datatable.zeroRecords') }}",
        info: "{{ __('datatable.info') }}",
        infoEmpty: "{{ __('datatable.infoEmpty') }}",
        infoFiltered: "{{ __('datatable.infoFiltered') }}",
        search: "{{ __('datatable.search') }}",
        paginate: {
            first: "{!! __('datatable.paginate.first') !!}",
            previous: "{!! __('datatable.paginate.previous') !!}",
            next: "{!! __('datatable.paginate.next') !!}",
            last: "{!! __('datatable.paginate.last') !!}"
        }
    };
</script>