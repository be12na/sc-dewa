<div class="card-header {{ isset($headerClass) ? $headerClass : '' }}">
    @php
        $cardIcon = (isset($useCardIcon) && ($useCardIcon === true)) ? env('IMAGE_CARD_ICON') : false;
    @endphp
    @if ($cardIcon)
        <img src="{{ asset($cardIcon) }}" class="me-2" style="height:20px; width:auto;">
    @endif
    <span class="fw-bold">{{ $cardTitle }}</span>
</div>