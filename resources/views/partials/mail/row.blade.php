@php
    $labelWidth = isset($labelWidth) ? $labelWidth : '150px';
@endphp
<div style="display:flex; flex-wrap:nowrap;">
    <span style="display:inline-block; flex-shrink:0; width: {{ $labelWidth }}; max-width: 50%; margin-right:0.5rem;">
        {!! $rowLabel !!}
    </span>
    <span style="flex-shrink:0; margin-right:0.5rem;">:</span>
    <span style="flex:1 1 auto;">
        {!! $rowValue !!}
    </span>
</div>
@php
    unset($labelWidth);
@endphp