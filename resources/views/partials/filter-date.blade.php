@php
    $startValue = isset($filterDates) ? $filterDates['start'] : \Carbon\Carbon::today();
    $startId = isset($startId) ? $startId : 'filter-date-start';

    $endValue = isset($filterDates) ? $filterDates['end'] : \Carbon\Carbon::today();
    $endId = isset($endId) ? $endId : 'filter-date-end';
    
    $endDate = isset($endFilterDate) ? $endFilterDate : \Carbon\Carbon::today();
    $inputSize = isset($inputSize) ? $inputSize : '';
    $filterId = isset($filterId) ? $filterId : '';
    $initClass = isset($initClass) ? $initClass : '';
@endphp
<div class="row g-1 dual-datepicker {{ $initClass }}" @if($filterId != '') id="{{ $filterId }}" @endif>
    @include('partials.datepicker', [
        'dateId' => $startId,
        'dateValue' => $startValue,
        'endDate' => $endDate,
        'inputSize' => $inputSize,
    ])
    @include('partials.datepicker', [
        'dateId' => $endId,
        'dateValue' => $endValue,
        'endDate' => $endDate,
        'inputSize' => $inputSize,
    ])
</div>