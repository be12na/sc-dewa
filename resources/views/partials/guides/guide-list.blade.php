@php
    $guideList = (isset($guideList) && is_array($guideList)) ? $guideList : [];
@endphp

<div class="small">
    <div class="mb-1 fw-bold">Petunjuk & Keterangan:</div>
    <ul>
        @if (!empty($guideList))
            @foreach ($guideList as $guide)
                @php
                    if (substr($guide, 0, 3) != '<li') $guide = "<li>{$guide}</li>";
                @endphp
                {!! $guide !!}
            @endforeach
        @endif
    </ul>
</div>