@php
    $format = isset($valueFormat) ? $valueFormat : 'd M Y H:i';
    $dateValue = isset($dateValue) ? translateDatetime($dateValue, $format) : date($format);
    $endDate = (isset($endDate) && !empty($endDate)) ? translateDatetime($endDate, $format) : '';
    $inputDateTimeId = isset($inputId) ? $inputId : 'date-' . mt_rand(0, 999999);
    $inputDateTimeName = isset($inputName) ? $inputName : '';
    $datePickerFormat = isset($datePickerFormat) ? $datePickerFormat : 'dd M yyyy HH:ii';
    $inputRequired = (isset($required) && ($required === true)) ? 'required' : '';
    $inputPlaceholder = isset($placeholder) ? $placeholder : 'Tanggal';
    $initScript = (isset($initScript) && ($initScript === true));
@endphp
<div class="input-group">
    <input type="text" id="{{ $inputDateTimeId }}" @if($inputDateTimeName != '') name="{{ $inputDateTimeName }}" @endif class="form-control bg-white bs-date-time" value="{{ $dateValue }}" data-date-format="{{ $datePickerFormat }}" data-date-enddate="{{ $endDate }}" placeholder="{{ $inputPlaceholder }}" autocomplete="off" readonly {{ $inputRequired }}>
    <label for="{{ $inputDateTimeId }}" class="input-group-text cursor-pointer">
        <i class="fa-solid fa-calendar-days"></i>
    </label>
</div>
@if ($initScript)
<script>
$(function() {
    initDateTimePicker();
});
</script>
@endif