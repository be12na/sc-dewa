@php
    $total = 0;
@endphp

@auth
    @php
        if (in_array($scope, ['left-right', 'package'])) {
            if (!empty($userStructure = $authUser->structure)) {
                if ($scope == 'left-right') {
                    $total = [
                        (object) ['title' => __('text.left'), 'total' => formatNumber($userStructure->count_left)],
                        (object) ['title' => __('text.right'), 'total' => formatNumber($userStructure->count_right)],
                    ];
                } else {
                    $total = [];

                    foreach($packages as $package) {
                        $row = (object) [
                            'title' => $package->name,
                            'total' => $userStructure->descendants()->whereHas('user', function($user) use($package) {
                                return $user->where('package_id', '=', $package->id);
                            })->count(),
                        ];

                        $total[] = $row;
                    }
                }
            }
        } elseif ($scope == 'sponsor') {
            $total = $authUser->sponsored()->byStatusActivated()->count();
        }
    @endphp
@endauth

<div class="{{ $cssClass }}" @auth @if($poll) wire:poll.{{ $poll }} @endif @endauth>
    @if (is_array($total))
        @foreach ($total as $rowTotal)
            <div class="d-flex justify-content-between flex-nowrap">
                <span class="me-1">{{ $rowTotal->title }}</span>
                <span>@formatNumber($rowTotal->total)</span>
            </div>
        @endforeach
    @else
        @formatNumber($total)
    @endif
</div>
