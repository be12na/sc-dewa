@php
    $total = 0;
@endphp

@auth
    @php
        if ($bonusCode == 'sponsor') {
            $relatedBonus = 'bonusSponsor';
        } elseif ($bonusCode == 'extra') {
            $relatedBonus = 'bonusExtra';
        } elseif ($bonusCode == 'upgrade') {
            $relatedBonus = 'bonusUpgrade';
        } elseif ($bonusCode == 'ro') {
            $relatedBonus = 'bonusRO';
        } elseif ($bonusCode == 'pair') {
            $relatedBonus = 'bonusPair';
        } elseif ($bonusCode == 'level') {
            $relatedBonus = 'bonusLevel';
        } else {
            $relatedBonus = 'bonuses';
        }

        $userBonuses = $authUser->$relatedBonus();

        if ($status == 'withdraw') {
            $userBonuses = $userBonuses->whereHas('withdraw');
        } elseif ($status == 'pending') {
            $userBonuses = $userBonuses->whereDoesntHave('withdraw');
        }

        $total = $userBonuses->sum('bonus_amount');
    @endphp
@endauth

<div class="{{ $cssClass }}" @auth @if($poll) wire:poll.{{ $poll }} @endif @endauth>@formatNumber($total)</div>
