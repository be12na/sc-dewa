@extends('layouts.app')

@push('pageCSS')
<style>
    body {
        background-color: var(--bs-light);
        background-image: url("{{ asset('assets/images/background.jpg') }}?v={{ $fileV }}");
        background-position: top left;
        background-repeat: no-repeat;
        background-size: 100% 100%;
    }
    .login {
        display: block;
        width:400px;
        max-width:100%;
    }
    .login .input-group-text {
        justify-content: center;
        width: 40px;
    }
</style>
@endpush

@section('content')
<div class="container-fluid d-flex align-items-center justify-content-center py-3 py-sm-4 py-lg-5" style="min-height: 100vh;">
    <form action="{{ route('login') }}" method="POST" class="login">
        @csrf
        <div class="text-center p-2 p-md-3 rounded-top bg-gray-200 border border-bottom-0">
            <img src="{{ asset(env('IMAGE_LARGE_LOGO')) }}?v={{ $fileV }}" style="max-width:40%; height:auto;">
        </div>
        <div class="p-2 p-md-3 rounded-bottom border border-top-0 bg-white">
            @include('partials.alert', ['showBtnClose' => false])
            <div class="input-group input-group-sm mb-2">
                <label for="username" class="input-group-text">
                    <i class="fa fa-user"></i>
                </label>
                <input id="username" type="text" name="username" class="form-control" placeholder="User ID" value="{{ old('username') }}" autocomplete="off" required autofocus>
            </div>
            <div class="input-group input-group-sm mb-3">
                <label for="input-password" class="input-group-text">
                    <i class="fa fa-key"></i>
                </label>
                <input id="input-password" type="password" name="password" class="form-control" placeholder="Password" autocomplete="off" required>
            </div>
            <div class="form-check mb-3">
                <input type="checkbox" name="remember" id="remember" class="form-check-input" value="1" {{ checkboxChecked(old('remember', 0), 1) }}>
                <label class="form-check-label" for="remember">Remember Me</label>
            </div>
            <div >
                <button type="submit" class="btn btn-block btn-success">Login</button>
            </div>
            {{-- <div class="mb-3">
                <a href="#" class="btn btn-block btn-dark">Register</a>
            </div> --}}
            {{-- <div class="text-center">
                <div class="text-light">Forgot Password ?</div>
                <a href="#" class="link-warning">Click Here...</a>
            </div> --}}
        </div>
    </form>
</div>
@endsection
