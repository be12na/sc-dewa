<form class="modal-content remote-submit" method="POST" action="{{ route('withdraw.saveTransfer') }}" data-alert-container="#alert-from-container">
    @include('partials.modals.modal-header', ['modalTitle' => __('header.confirmation')])
    <div class="modal-body fs-auto">
        @csrf
        <div class="d-block" id="alert-from-container"></div>
        <input type="hidden" name="type" value="{{ $bonusType }}">
        @foreach ($checks as $check)
            <input type="hidden" name="checks[]" value="{{ $check }}">
        @endforeach
        <div class="d-block">
            @translate('message.withdraw.confirm-transfer')
        </div>
    </div>
    <div class="modal-footer justify-content-center py-1">
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa fa-check me-2"></i>@translate('text.uf-yes')
        </button>
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa fa-times me-2"></i>@translate('text.uf-no')
        </button>
    </div>
</form>