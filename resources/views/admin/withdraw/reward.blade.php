@php
    $pageHeading = __("breadcrumb.withdraw.reward");
    $hasRole = hasRole(['admin.super', 'admin.master']);
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')

<div class="card">
    @include('partials.card-header', ['cardTitle' => __("header.withdraw.reward")])
    <div class="card-body px-0 py-2">
        <div class="row g-2 d-none" id="datatables-controls">
            <div class="col-12 col-sm-auto">
                <div class="row gy-1 gx-2">
                    <div class="col-12 col-sm-auto d-flex align-items-center">@translate('menu.package')</div>
                    <div class="col-12 col-sm-auto">
                        <select id="select-package" class="form-select form-select-sm">
                            @foreach ($packages as $package)
                                <option value="{{ $package->id }}">{{ $package->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-10 col-sm-auto">
                <div class="row gy-1 gx-2">
                    <div class="col-12 col-sm-auto d-flex align-items-center">Status</div>
                    <div class="col-12 col-sm-auto">
                        <select id="select-status" class="form-select form-select-sm">
                            <option value="">-- Semua --</option>
                            <option value="{{ WD_PROCESSING }}">@translate('label.status.pending')</option>
                            <option value="{{ WD_COMPLETE }}">@translate('label.status.complete')</option>
                            {{-- <option value="{{ WD_REJECT }}">@translate('label.status.rejected')</option> --}}
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-2 col-sm-auto d-flex align-items-end">
                <div class="btn-group">
                    @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
                </div>
            </div>
        </div>
        <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap fs-auto" id="my-table">
            <thead>
                <tr>
                    <th>@translate('text.date')</th>
                    <th>@translate('label.withdraw.code')</th>
                    <th>@translate('label.member')</th>
                    <th></th>
                    <th></th>
                    <th>@translate('label.reward')</th>
                    @if ($hasRole)
                        <th></th>
                    @endif
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection

@if ($hasRole)
@push('topContent')
    @include('partials.modals.modal', [
        'bsModalId' => 'my-modal',
        'scrollable' => true,
    ]);
@endpush
@endif

@section('vendorCSS')
    @include('partials.datatable.css')
@endsection

@section('vendorJS')
    @include('partials.datatable.js')
@endsection

@push('pageJS')
<script>
    let table;
    const tableLang = {!! json_encode(__('datatable')) !!};

    function reloadTable() {
        dataTableRefresh(table);
    }

    $(function() {
        const pkg = $('#select-package');
        const sts = $('#select-status');

        table = $('#my-table').DataTable({
            dom: '<"container-fluid gx-3"<"row g-2 mb-2"<"col-12 col-sm-auto"<"dt-control">><"col-auto me-sm-auto"l><"col-12 col-lg-auto d-flex align-items-end"f>>>r<"table-responsive border-top border-translucent"t><"row gy-1 gx-0"<"col-12 col-md"i><"col-12 col-md"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("withdraw.reward.dataTable") }}',
                data: function(d) {
                    d.pkg_id = pkg.val();
                    d.rs = sts.val();
                }
            },
            deferLoading: 50,
            search: {
                return: true
            },
            lengthChange: false,
            order: [[0, 'asc']],
            language: tableLang,
            columns: [
                {data: 'created_at', searchable: false},
                {data: 'id'},
                {data: 'name'},
                {data: 'username', className: 'd-none'},
                {data: 'phone', className: 'd-none'},
                {data: 'reward_value'},
                @if ($hasRole)
                    {data: 'action', className: 'dt-body-center', searchable: false, orderable: false},
                @endif
            ],
        });

        setDataTableLoading('my-table', '{{ __("datatable.processing") }}');
        setControlToDataTable('my-table', $('#datatables-controls'));
        $('.dataTables_filter').addClass('w-100');
        $('.dataTables_filter label').addClass('d-block');
        $('.dataTables_filter .form-control').css({'margin-left': 0, 'width': '100%'});

        pkg.on('change', function(e) {
            reloadTable();
        });
        
        sts.on('change', function(e) {
            reloadTable();
        }).change();
    });
</script>
@endpush
