@php
    $pageHeading = __("breadcrumb.withdraw.bonus.{$suffix}");
    $hasRole = hasRole(['admin.super', 'admin.master']);
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')

<div class="card">
    @include('partials.card-header', ['cardTitle' => __("header.withdraw.{$suffix}")])
    <div class="card-body px-0 py-2">
        <div class="btn-group d-none" id="button-controls">
            @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
        </div>
        @if ($hasRole)
            <div class="btn-group me-1 d-none" id="button-action">
                <button type="button" id="btn-submit-check" class="btn btn-sm btn-primary" title="Submit">
                    <i class="fa-solid fa-save me-1" style="line-height: inherit"></i>
                    Submit
                </button>
                <button type="button" id="btn-cancel" class="btn btn-sm btn-warning" title="Batal" onclick="transferAction('hide');">
                    <i class="fa-solid fa-times me-1" style="line-height: inherit"></i>
                    Batal
                </button>
            </div>
            <div class="btn-group me-1 d-none" id="button-transfer">
                <button type="button" class="btn btn-sm btn-success" title="Transfer" onclick="transferAction('show');">
                    Transfer
                </button>
            </div>
        @endif
        <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap fs-auto" id="my-table">
            <thead>
                <tr>
                    @if ($hasRole)
                        <th class="border-end d-none">
                            <input type="checkbox" class="mx-3" id="check-all" title="Pilih Semua" autocomplete="off" onclick="selectCheck($(this));">
                        </th>
                    @endif
                    <th>@translate('text.date')</th>
                    <th>@translate('label.withdraw.code')</th>
                    <th>@translate('label.member')</th>
                    <th></th>
                    <th>@translate('label.withdraw.bank')</th>
                    <th></th>
                    <th></th>
                    <th>@translate('label.withdraw.total-bonus') (@translate('format.currency.symbol.text'))</th>
                    <th>@translate('label.withdraw.admin-fee') (@translate('format.currency.symbol.text'))</th>
                    <th>@translate('label.withdraw.total-transfer') (@translate('format.currency.symbol.text'))</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection

@if ($hasRole)
@push('topContent')
    @include('partials.modals.modal', [
        'bsModalId' => 'modal-transfer',
        'scrollable' => true,
    ]);
@endpush
@endif

@section('vendorCSS')
    @include('partials.datatable.css')
@endsection

@section('vendorJS')
    @include('partials.datatable.js')
@endsection

@push('pageJS')
<script>
    let table;
    const tableLang = {!! json_encode(__('datatable')) !!};

    function reloadTable() {
        dataTableRefresh(table);
    }

    @if ($hasRole)
        let showHide = 'hide';
        
        function showHideCheck() {
            const th = $('table.dataTable thead > tr > :first-child');
            const td = $('table.dataTable tbody > tr > :first-child:not([colspan])');
            const b1 = $('#button-action');
            const b2 = $('#button-transfer');
            if (showHide === 'show') {
                th.removeClass('d-none');
                td.removeClass('d-none');
                b1.removeClass('d-none');
                b2.addClass('d-none');
            } else if (showHide === 'hide') {
                th.addClass('d-none');
                td.addClass('d-none');
                b1.addClass('d-none');
                b2.removeClass('d-none');
            }
        }

        function selectCheck(ch)
        {
            const ca = $('#check-all');
            const tc = $('table.dataTable tbody .check-row');
            if (!ch.hasClass('check-row')) {
                tc.prop({checked: ca.is(':checked')}).trigger('change');
            } else {
                const checked = $('table.dataTable tbody .check-row:checked');
                ca.prop({checked: (checked.length == tc.length)});
            }
        }

        function transferAction(am)
        {
            showHide = am;
            showHideCheck();
        }
    @endif

    $(function() {
        table = $('#my-table').DataTable({
            dom: '<"container-fluid gx-3"<"row g-2 mb-2"<"col-auto me-auto me-sm-0"<"dt-control">><"col-auto me-sm-auto"l><"col-12 col-sm-auto"f>>>r<"table-responsive border-top border-translucent"t><"row gy-1 gx-0"<"col-12 col-md"i><"col-12 col-md"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("withdraw.{$suffix}.dataTable") }}'
            },
            deferLoading: 50,
            search: {
                return: true
            },
            @if ($hasRole)
            order: [[1, 'asc']],
            @endif
            language: tableLang,
            columns: [
                @if ($hasRole)
                    {data: 'check', className: 'dt-body-center', orderable: false, searchable: false},
                @endif
                {data: 'wd_date', searchable: false},
                {data: 'wd_code'},
                {data: 'name'},
                {data: 'username', className: 'd-none'},
                {data: 'bank_name', searchable: false},
                {data: 'bank_acc_no', className: 'd-none'},
                {data: 'bank_acc_name', className: 'd-none'},
                {data: 'total_bonus', className: 'dt-body-right', searchable: false},
                {data: 'fee', className: 'dt-body-right', searchable: false},
                {data: 'total_transfer', className: 'dt-body-right', searchable: false}
            ],
            @if ($hasRole)
                drawCallback: function( settings ) {
                    showHideCheck();
                    const ca = $('#check-all');
                    ca.prop({checked: false});
                    selectCheck(ca);
                },
            @endif
        });

        setDataTableLoading('my-table', '{{ __("datatable.processing") }}');
        @if ($hasRole)
            setControlToDataTable('my-table', $('#button-transfer'));
            setControlToDataTable('my-table', $('#button-action'));
        @endif
        setControlToDataTable('my-table', $('#button-controls'));
        $('.dataTables_filter label').addClass('d-block');
        $('.dataTables_filter .form-control').css({'margin-left': 0, 'width': '100%'});
        // $('#my-table_filter .form-control').attr({placeholder: 'Username / {{ __("text.name") }} / Handphone'});

        
        @if ($hasRole)
            $('#button-action').addClass('d-none');
            $(document).on('click', 'table.dataTable tbody .check-row', function(e) {
                selectCheck($(this));
            }).on('change', 'table.dataTable tbody .check-row', function(e) {
                const me = $(this);
                const tr = me.closest('tr');
                if (me.is(':checked')) {
                    tr.addClass('selected');
                } else {
                    tr.removeClass('selected');
                }
            });

            $('#btn-submit-check').on('click', function(e) {
                const checks = $('table.dataTable tbody .check-row:checked');
                let arrCheck = [];
                $.each(checks, function(k, v) {
                    arrCheck.push($(v).val());
                });

                const me = $(this);
                const p = arrCheck.length ? '&ids=' + arrCheck.join(',') : '';
                let url = '{{ route("withdraw.viewTransfer") }}?type={{ $suffix }}' + p;
                me.data('modal-url', url);

                APP.openModal(me, '#modal-transfer', false);
            });
        @endif

        reloadTable();
    });
</script>
@endpush
