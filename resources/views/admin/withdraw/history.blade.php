@php
    $pageHeading = __("breadcrumb.withdraw.histories");
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')

<div class="card">
    @include('partials.card-header', ['cardTitle' => __("header.withdraw.history")])
    <div class="card-body px-0 py-2">
        <div class="btn-group d-none" id="button-controls">
            @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
        </div>
        <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap fs-auto" id="my-table">
            <thead>
                <tr>
                    <th>@translate('text.date')</th>
                    <th>@translate('label.withdraw.code')</th>
                    <th>@translate('label.withdraw.type')</th>
                    <th>@translate('label.member')</th>
                    <th></th>
                    <th>@translate('label.withdraw.bank')</th>
                    <th></th>
                    <th></th>
                    <th>@translate('label.withdraw.total-bonus') (@translate('format.currency.symbol.text'))</th>
                    <th>@translate('label.withdraw.admin-fee') (@translate('format.currency.symbol.text'))</th>
                    <th>@translate('label.withdraw.total-transfer') (@translate('format.currency.symbol.text'))</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('vendorCSS')
    @include('partials.datatable.css')
@endsection

@section('vendorJS')
    @include('partials.datatable.js')
@endsection

@push('pageJS')
<script>
    let table;
    const tableLang = {!! json_encode(__('datatable')) !!};

    function reloadTable() {
        dataTableRefresh(table);
    }

    $(function() {
        table = $('#my-table').DataTable({
            dom: '<"container-fluid gx-3"<"row g-2 mb-2"<"col-auto me-auto me-sm-0"<"dt-control">><"col-auto me-sm-auto"l><"col-12 col-sm-auto"f>>>r<"table-responsive border-top border-translucent"t><"row gy-1 gx-0"<"col-12 col-md"i><"col-12 col-md"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("withdraw.histories.dataTable") }}'
            },
            deferLoading: 50,
            search: {
                return: true
            },
            language: tableLang,
            columns: [
                {data: 'wd_date', searchable: false},
                {data: 'wd_code'},
                {data: 'wd_bonus_type', searchable: false},
                {data: 'name'},
                {data: 'username', className: 'd-none'},
                {data: 'bank_name', searchable: false},
                {data: 'bank_acc_no', className: 'd-none'},
                {data: 'bank_acc_name', className: 'd-none'},
                {data: 'total_bonus', className: 'dt-body-right', searchable: false},
                {data: 'fee', className: 'dt-body-right', searchable: false},
                {data: 'total_transfer', className: 'dt-body-right', searchable: false},
                {data: 'status', className: 'dt-body-center', searchable: false}
            ],
        });

        setDataTableLoading('my-table', '{{ __("datatable.processing") }}');
        setControlToDataTable('my-table', $('#button-controls'));
        $('.dataTables_filter label').addClass('d-block');
        $('.dataTables_filter .form-control').css({'margin-left': 0, 'width': '100%'});
        // $('#my-table_filter .form-control').attr({placeholder: 'Username / {{ __("text.name") }} / Handphone'});

        reloadTable();
    });
</script>
@endpush
