@php
    $rewardValue = $userReward->reward_value;
    $memberName = $userReward->user->name;
    $memberUsername = $userReward->user->username;
@endphp

<form class="modal-content remote-submit" method="POST" action="{{ route('withdraw.reward.postAction', ['userReward' => $userReward->id, 'action' => $action]) }}" data-alert-container="#alert-from-container">
    @include('partials.modals.modal-header', ['modalTitle' => __('header.confirmation')])
    <div class="modal-body fs-auto">
        @csrf
        <div class="d-block" id="alert-from-container"></div>
        @if ($action == 'confirm')
            <div class="text-center">
                <div>{!! __('message.reward.alert-confirm', ['reward' => "<b>{$rewardValue}</b>", 'member' => "<b>{$memberName} ({$memberUsername})</b>"]) !!}</div>
                <div><i class="fa fa-question"></i></div>
            </div>
        @else
            <label class="d-block required">@translate('label.reject-reason')</label>
            <textarea name="reject_note" class="form-control" maxlength="255" required></textarea>
        @endif
    </div>
    <div class="modal-footer justify-content-center py-1">
        @if (in_array($action, ['confirm', 'reject']))
            <button type="submit" class="btn btn-sm btn-{{ ($action == 'confirm') ? 'success' : 'danger' }}">
                <i class="fa fa-check me-2"></i>@translate('text.uf-yes')
            </button>
        @endif
        <button type="button" class="btn btn-sm btn-warning" data-bs-dismiss="modal">
            <i class="fa fa-times me-2"></i>@translate('text.uf-no')
        </button>
    </div>
</form>