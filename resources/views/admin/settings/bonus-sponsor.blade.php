@php
    $pageHeading = __('breadcrumb.settings.bonus.sponsor');
    $hasRole = hasRole(['admin.super']);
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<div class="row justify-content-center">
    <div class="col-sm-10 col-md-8 col-lg-6">
        <div class="card">
            <div class="card-body py-4 py-lg-5">
                <div class="d-flex flex-nowrap justify-content-center fs-3 text-center">
                    @if (!isTrueConfig(BONUS_SPONSOR_TYPE, 'percent'))
                        <div class="me-2">@translate('format.currency.symbol.text')</div>
                    @endif
                    <div>
                        @formatNumber($bonus, isTrueConfig(BONUS_SPONSOR_TYPE, 'percent') ? PERCENT_DECIMAL : null, !isTrueConfig(BONUS_SPONSOR_TYPE, 'percent'))
                    </div>
                    @if (isTrueConfig(BONUS_SPONSOR_TYPE, 'percent'))
                        <div class="ms-2">%</div>
                    @endif
                </div>
                @if ($hasRole)
                    <div class="text-center mt-3">
                        @include('partials.buttons.open-modal', [
                            'btnText' => __('button.edit'),
                            'btnClass' => 'btn-sm btn-warning btn-pill px-4',
                            'modalTarget' => '#my-modal',
                        ])
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@if ($hasRole)
    @push('topContent')
        <div class="modal fade" id="my-modal" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered" id="myModal-dialog">
                <form class="modal-content remote-submit" action="{{ route('setting.bonus.sponsor.update') }}" method="POST" data-alert-container="#alert-form-container">
                    @include('partials.modals.modal-header', ['modalTitle' => __('header.bonus.sponsor.edit')])
                    <div class="modal-body">
                        @csrf
                        <div id="alert-form-container"></div>
                        <label class="form-label required">{{ __('label.bonus-sponsor') }}</label>
                        <div class="input-group">
                            @if (!isTrueConfig(BONUS_SPONSOR_TYPE, 'percent'))
                                <label for="input-bonus" class="input-group-text">@translate('format.currency.symbol.text')</label>
                            @endif
                            <input type="number" id="input-bonus" name="bonus_sponsor" class="form-control" value="{{ $bonus }}" min="0" step="@inputNumberStep(isTrueConfig(BONUS_SPONSOR_TYPE, 'percent'))" autocomplete="off" required>
                            @if (isTrueConfig(BONUS_SPONSOR_TYPE, 'percent'))
                                <label for="input-bonus" class="input-group-text">%</label>
                            @endif
                        </div>
                    </div>
                    @include('partials.modals.modal-footer', [
                        'modalFoterClass' => 'justify-content-center',
                        'modalFooterContent' => [
                            view('partials.buttons.submit', ['btnIcon' => 'save', 'btnText' => __('button.update')])->render(),
                        ],
                    ])
                </form>
            </div>
        </div>
    @endpush
@endif