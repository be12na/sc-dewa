@php
    $pageHeading = __('breadcrumb.settings.bonus.level');
    $hasRole = hasRole(['admin.super']);
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<div class="card">
    @include('partials.card-header', ['cardTitle' => __('header.bonus.level.index')])
    <div class="card-body px-0 py-2">
        <div class="row g-2 d-none" id="table-controls">
            <div class="col-auto">
                @if ($hasRole)
                    <div class="btn-group">
                        @include('partials.buttons.open-modal', [
                            'isAjaxModal' => true,
                            'modalUrl' => route('setting.bonus.level.add'),
                            'btnClass' => 'btn-sm btn-primary btn-theme',
                            'modalTarget' => '#my-modal',
                            'btnText' => __('button.add'),
                        ])

                        @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
                    </div>
                @else
                    @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
                @endif
            </div>
            @if (BONUS_LEVEL_PER_PACKAGE)
                <div class="col-auto">
                    <div class="input-group input-group-sm">
                        <label for="select-package" class="input-group-text">@translate('label.bonus-level.package')</label>
                        <select id="select-package" class="form-select" autocomplete="off">
                            @foreach ($packages as $package)
                                <option value="{{ $package->id }}" @optionSelected($packageId, $package->id)>{{ $package->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif
        </div>
        <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap" id="my-table">
            <thead>
                <tr>
                    {{-- <th>No.</th> --}}
                    {{-- <th>@translate('label.bonus-level.package')</th> --}}
                    <th>@translate('label.bonus-level.level')</th>
                    <th>@translate('label.bonus-level.bonus')</th>
                    @if ($hasRole)
                        <th></th>
                    @endif
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@if ($hasRole)
    @push('topContent')
        @include('partials.modals.modal', ['bsModalId' => 'my-modal', 'scrollable' => true])
    @endpush
@endif

@section('vendorCSS')
    @include('partials.datatable.css')
@endsection

@section('vendorJS')
    @include('partials.datatable.js')
@endsection

@push('pageJS')
<script>
    let table;
    const tableLang = {!! json_encode(__('datatable')) !!};

    function reloadTable() {
        dataTableRefresh(table);
    }

    $(function() {
        table = $('#my-table').DataTable({
            dom: dataTableDom({
                control: true
            }),
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("setting.bonus.level.dataTable") }}'
                @if (BONUS_LEVEL_PER_PACKAGE)
                    , data: function(d) {
                        d.package = $('#select-package').val();
                    }
                @endif
            },
            deferLoading: 50,
            info: false,
            searching: false,
            sort: false,
            paging: false,
            lengthChange: false,
            language: tableLang,
            columns: [
                {data: 'level', className: 'dt-body-center'},
                {data: 'bonus', className: 'dt-body-right'},
                @if ($hasRole)
                    {data: 'action', orderable: false, searchable: false, className: 'dt-body-center'},
                @endif
            ],
        });

        setDataTableLoading('my-table', '{{ __("datatable.processing") }}');
        setControlToDataTable('my-table', $('#table-controls'));

        $('#select-package').on('change', function(e) {
            reloadTable();
        }).change();
    });
</script>
@endpush
