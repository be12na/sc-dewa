@php
    $bonusLevel = isset($bonusLevel) ? $bonusLevel : null;
    $isEdit = !empty($bonusLevel);
    $postRoute = $isEdit
        ? route('setting.bonus.level.update', ['bonusLevel' => $bonusLevel->id])
        : route('setting.bonus.level.store');
    $modalHeader = $isEdit ? __('header.bonus.level.edit') : __('header.bonus.level.add');

    $bonusLevel = optional($bonusLevel);
    $packageId = $bonusLevel->package_id;
@endphp
<form class="modal-content remote-submit" action="{{ $postRoute }}" method="POST" data-alert-container="#alert-form-container">
    @include('partials.modals.modal-header', ['modalTitle' => $modalHeader])
    <div class="modal-body">
        @csrf
        <div id="alert-form-container"></div>
        <div class="row g-2">
            @if (BONUS_LEVEL_PER_PACKAGE)
                <div class="col-12">
                    <label class="form-label required">{{ __('label.bonus-level.package') }}</label>
                    <select name="package_id" class="form-select" autocomplete="off" required>
                        @foreach ($packages as $package)
                            <option value="{{ $package->id }}" @optionSelected($package->id, $packageId)>{{ $package->name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="col-12">
                <label class="form-label required">{{ $labelLevel = __('label.bonus-level.level') }}</label>
                <input type="number" class="form-control" name="level" value="{{ $bonusLevel->level ?? 1 }}" min="1" placeholder="{{ $labelLevel }}" autocomplete="off" required>
            </div>
            <div class="col-12">
                <label class="form-label required">{{ __('label.bonus-level.bonus') }}</label>
                {{-- <input type="number" class="form-control" name="bonus" value="{{ $bonusLevel->bonus ?? 0 }}" min="0" placeholder="{{ $labelBonus }}" autocomplete="off" required> --}}
                <div class="input-group">
                    @if (!isTrueConfig(BONUS_LEVEL_TYPE, 'percent'))
                        <label for="input-bonus" class="input-group-text">@translate('format.currency.symbol.text')</label>
                    @endif
                    <input type="number" id="input-bonus" name="bonus_level" class="form-control" value="{{ $bonusLevel->bonus ?? 0 }}" min="0" step="@inputNumberStep(isTrueConfig(BONUS_LEVEL_TYPE, 'percent'))" autocomplete="off" required>
                    @if (isTrueConfig(BONUS_LEVEL_TYPE, 'percent'))
                        <label for="input-bonus" class="input-group-text">%</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('partials.modals.modal-footer', ['modalFooterContent' => [
        view('partials.buttons.submit', ['btnIcon' => 'save', 'btnText' => $isEdit ? __('button.update') : __('button.save')])->render(),
        view('partials.buttons.close-modal')->render()
    ]])
</form>