<form class="modal-content remote-submit" action="{{ route('setting.package.update', ['package' => $package->id]) }}" method="POST" data-alert-container="#alert-form-container">
    @include('partials.modals.modal-header', ['modalTitle' => __('header.package.edit')])
    <div class="modal-body">
        @csrf
        <div id="alert-form-container"></div>
        <div class="row g-2">
            <div class="col-12">
                <label class="form-label required">{{ $labelName = __('label.package.name') }}</label>
                <input type="text" class="form-control" name="package_name" value="{{ $package->name }}" placeholder="{{ $labelName }}" autocomplete="off" required>
            </div>
            @if (isPackageRequirePIN() && PIN_ENABLED)
                <div class="col-12">
                    <label class="form-label required">{{ $labelPinCount = __('label.package.pin-count') }}</label>
                    <input type="number" class="form-control" name="pin_count" value="{{ $package->pin_count }}" placeholder="{{ $labelPinCount }}" min="1" autocomplete="off" required>
                </div>
            @else
                <div class="col-12">
                    <label class="form-label required">{{ $labelPrice = __('label.package.price') }}</label>
                    <input type="number" class="form-control" name="price" value="{{ $package->price }}" placeholder="{{ $labelPrice }}" min="0" autocomplete="off" required>
                </div>
            @endif
            @if (BONUS_SPONSOR_BY_PACKAGE)
                <div class="col-12">
                    <label class="form-label required">{{ $labelBonusSp = __('label.package.bonus-sponsor') }}</label>
                    <div class="input-group">
                        @if (isTrueConfig(BONUS_SPONSOR_TYPE, 'percent'))
                            <input type="number" id="bonus-sponsor" class="form-control" name="bonus_sponsor" value="{{ $package->bonus_sponsor }}" min="0" max="50" step="0.01" placeholder="{{ $labelBonusSp }}" autocomplete="off" required>
                            <label class="input-group-text" for="bonus-sponsor">%</label>
                        @else
                            <label class="input-group-text" for="bonus-sponsor">@translate('format.currency.symbol.text')</label>
                            <input type="number" id="bonus-sponsor" class="form-control" name="bonus_sponsor" value="{{ $package->bonus_sponsor }}" min="0" placeholder="{{ $labelBonusSp }}" autocomplete="off" required>
                        @endif
                    </div>
                </div>
            @endif
            @if (BONUS_EXTRA_ENABLED)
                <div class="col-12">
                    <label class="form-label required">{{ $labelBonusExtra = __('label.package.bonus-extra') }}</label>
                    <input type="number" class="form-control" name="bonus_extra" value="{{ $package->bonus_extra }}" min="0" placeholder="{{ $labelBonusExtra }}" autocomplete="off" required>
                </div>
            @endif
            @if (BONUS_UPGRADE_ENABLED)
                <div class="col-12">
                    <label class="form-label required">{{ $labelBonusRo = __('label.package.bonus-upgrade') }}</label>
                    <input type="number" class="form-control" name="bonus_upgrade" value="{{ $package->bonus_upgrade }}" min="0" placeholder="{{ $labelBonusRo }}" autocomplete="off" required>
                </div>
            @endif
            @if (BONUS_RO_ENABLED)
                <div class="col-12">
                    <label class="form-label required">{{ $labelBonusRo = __('label.package.bonus-ro') }}</label>
                    <input type="number" class="form-control" name="bonus_ro" value="{{ $package->bonus_ro }}" min="0" placeholder="{{ $labelBonusRo }}" autocomplete="off" required>
                </div>
            @endif
            @if (BONUS_PAIR_ENABLED && isTrueConfig(BONUS_PAIR_TYPE, 'point'))
                <div class="col-12">
                    <label class="form-label required">{{ $labelBonusPair = __('label.package.pair-point') }}</label>
                    <input type="number" class="form-control" name="pair_point" value="{{ $package->pair_point }}" min="1" placeholder="{{ $labelBonusPair }}" autocomplete="off" required>
                </div>
            @endif
            @if (BONUS_PACKAGE_ENABLED)
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row g-2">
                                <div class="col-12">
                                    <div class="form-check check-warning checked-success">
                                        <input type="checkbox" id="get-pkg" class="form-check-input" name="get_bonus_package" value="1" autocomplete="off" @checked($package->bonus_package, 1, false)>
                                        <label class="form-check-label" for="get-pkg">{{ __('label.package.bonus-package') }}</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label required">{{ $labelPkgLeft = __('label.package.bonus-package-count-left') }}</label>
                                    <input type="number" class="form-control" name="package_left" value="{{ $package->package_left }}" min="1" placeholder="{{ $labelPkgLeft }}" autocomplete="off">
                                </div>
                                <div class="col-6">
                                    <label class="form-label required">{{ $labelPkgRight = __('label.package.bonus-package-count-right') }}</label>
                                    <input type="number" class="form-control" name="package_right" value="{{ $package->package_right }}" min="1" placeholder="{{ $labelPkgRight }}" autocomplete="off">
                                </div>
                                <div class="col-12">
                                    <label class="form-label">{{ $labelBonusPkg = __('label.package.bonus-package-description') }}</label>
                                    <input type="text" class="form-control" name="package_bonus" value="{{ $package->package_bonus }}" placeholder="{{ $labelBonusPkg }}" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if (REWARD_ENABLED)
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row g-2">
                                <div class="col-12">
                                    <div class="form-check check-warning checked-success">
                                        <input type="checkbox" id="get-reward" class="form-check-input" name="check_get_reward" value="1" autocomplete="off" @checked($package->get_reward, 1, true)>
                                        <label class="form-check-label" for="get-reward">{{ __('label.package.get-reward') }}</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-check check-warning checked-success">
                                        <input type="checkbox" id="give-reward" class="form-check-input" name="check_give_reward" value="1" autocomplete="off" @checked($package->give_reward, 1, true)>
                                        <label class="form-check-label" for="give-reward">{{ __('label.package.give-reward') }}</label>
                                    </div>
                                </div>
                                @if (isTrueConfig(REWARD_BY_TYPE, 'point'))
                                    <div class="col-12">
                                        <label class="form-label required">{{ $labelRewardpoint = __('label.package.reward-point') }}</label>
                                        <input type="number" class="form-control" name="reward_point" value="{{ $package->reward_point }}" min="0" placeholder="{{ $labelRewardpoint }}" autocomplete="off" required>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-12">
                <label class="form-label">{{ $labelDescription = __('label.package.description') }}</label>
                <textarea class="form-control" maxlength="250" name="description" placeholder="{{ $labelDescription }}" autocomplete="off">{{ $package->description }}</textarea>
            </div>
        </div>
    </div>
    @include('partials.modals.modal-footer', ['modalFooterContent' => [
        view('partials.buttons.submit', ['btnIcon' => 'save', 'btnText' => __('button.update')])->render(),
        view('partials.buttons.close-modal')->render()
    ]])
</form>