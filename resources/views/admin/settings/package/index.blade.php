@php
    $pageHeading = __('breadcrumb.settings.package');
    $hasRole = hasRole(['admin.super']);
    $colMax = 2;

    // if ($isMultiPlan) $colMax += 1;
    if (PACKAGE_USE_PIN && PIN_ENABLED) $colMax += 1;
    if (BONUS_SPONSOR_BY_PACKAGE) $colMax += 1;
    if (BONUS_EXTRA_ENABLED) $colMax += 1;
    if (BONUS_PACKAGE_ENABLED) $colMax += 1;
    if (REWARD_ENABLED) $colMax += 1;

    // if (REWARD_ENABLED) {
    //     $colMax += 2;

    //     if (isTrueConfig(REWARD_BY_TYPE, 'point')) {
    //         $colMax += 1;
    //     }
    // }
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<div class="card">
    @include('partials.card-header', ['cardTitle' => __('header.package.list')])
    <div class="card-body px-0 pt-0 pb-2">
        <div class="d-block w-100 table-responsive">
            <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>@translate('label.package.name')</th>
                        @if (PACKAGE_USE_PIN && PIN_ENABLED)
                            <th>@translate('label.package.pin-count')</th>
                        @else
                            <th>@translate('label.package.price') (@translate('format.currency.symbol.text'))</th>
                        @endif
                        @if (BONUS_SPONSOR_BY_PACKAGE)
                            <th>
                                @translate('label.package.bonus-sponsor') ({{ isTrueConfig(BONUS_SPONSOR_TYPE, 'percent') ? '%' : __('format.currency.symbol.text') }})
                            </th>
                        @endif
                        @if (BONUS_EXTRA_ENABLED)
                            <th>
                                @translate('label.package.bonus-extra') ({{ isTrueConfig(BONUS_EXTRA_TYPE, 'percent') ? '%' : __('format.currency.symbol.text') }})
                            </th>
                        @endif
                        {{-- @if (BONUS_PACKAGE_ENABLED)
                            <th>@translate('label.package.bonus-package')</th>
                        @endif --}}
                        @if (BONUS_UPGRADE_ENABLED)
                            <th>@translate('label.package.bonus-upgrade') ({{ isTrueConfig(BONUS_EXTRA_TYPE, 'percent') ? '%' : __('format.currency.symbol.text') }})</th>
                        @endif
                        @if (BONUS_RO_ENABLED)
                            <th>@translate('label.bonus-ro') ({{ isTrueConfig(BONUS_RO_TYPE, 'percent') ? '%' : __('format.currency.symbol.text') }})</th>
                        @endif
                        @if (REWARD_ENABLED)
                            <th>Reward</th>
                        @endif
                        @if ($colMax <= 4)
                            <th></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @php
                        $number = 1;
                    @endphp
                    @foreach ($packages as $package)
                        <tr>
                            <td>{{ $number++ }}.</td>
                            <td>
                                <div>{{ $package->name }}</div>
                                <div class="small">
                                    <a href="#" class="link-primary me-3" data-bs-toggle="modal" data-bs-target="#my-modal" data-modal-url="{{ route('setting.package.detail', ['package' => $package->id]) }}">
                                        {{ __('button.detail') }}
                                    </a>
                                    @if($hasRole && $package->is_active)
                                        <a href="#" class="link-success" data-modal-url="{{ route('setting.package.edit', ['package' => $package->id]) }}" data-bs-toggle="modal" data-bs-target="#my-modal">@translate('button.edit')</a>
                                    @endif
                                </div>
                            </td>
                            @if (PACKAGE_USE_PIN && PIN_ENABLED)
                                <td class="text-center">@formatNumber($package->pin_count)</td>
                            @else
                                <td class="text-end">@formatNumber($package->price)</td>
                            @endif
                            @if (BONUS_SPONSOR_BY_PACKAGE)
                                <td class="text-end">@formatNumber($package->bonus_sponsor, isTrueConfig(BONUS_SPONSOR_TYPE, 'percent') ? 2 : null, !isTrueConfig(BONUS_SPONSOR_TYPE, 'percent'))</td>
                            @endif
                            @if (BONUS_EXTRA_ENABLED)
                                <td class="text-end">@formatNumber($package->bonus_extra, isTrueConfig(BONUS_EXTRA_TYPE, 'percent') ? 2 : null, !isTrueConfig(BONUS_EXTRA_TYPE, 'percent'))</td>
                            @endif
                            {{-- @if (BONUS_PACKAGE_ENABLED)
                                <td>
                                    @if ($package->bonus_package)
                                        <div class="inline me-2">
                                            <span class="badge badge-info text-dark">@formatNumber($package->package_left, 0)</span>
                                            <span>:</span>
                                            <span class="badge badge-info text-dark">@formatNumber($package->package_left, 0)</span>
                                            <span>{{ $package->package_bonus }}</span>
                                        </div>
                                    @else
                                        @translate('text.nothing')
                                    @endif
                                </td>
                            @endif --}}
                            @if (BONUS_UPGRADE_ENABLED)
                                <td class="text-end">@formatNumber($package->bonus_upgrade, isTrueConfig(BONUS_UPGRADE_TYPE, 'percent') ? 2 : null, !isTrueConfig(BONUS_UPGRADE_TYPE, 'percent'))</td>
                            @endif
                            @if (BONUS_RO_ENABLED)
                                <td class="text-end">@formatNumber($package->bonus_ro, isTrueConfig(BONUS_RO_TYPE, 'percent') ? 2 : null, !isTrueConfig(BONUS_RO_TYPE, 'percent'))</td>
                            @endif
                            @if (REWARD_ENABLED)
                                <td class="text-center">@contentCheck($package->get_reward)</td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('topContent')
@include('partials.modals.modal', ['bsModalId' => 'my-modal', 'scrollable' => true])
@endpush