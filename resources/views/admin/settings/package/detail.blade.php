<div class="modal-content">
    @include('partials.modals.modal-header', ['modalTitle' => __('header.package.detail')])
    <div class="modal-body">
        <div class="row g-2">
            <div class="col-12 d-flex flex-nowrap">
                <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.name') }}</div>
                <div class="flex-fill ms-3">{{ $package->name }}</div>
            </div>
            @if (isPackageRequirePIN() && PIN_ENABLED)
                <div class="col-12 d-flex flex-nowrap">
                    <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.pin-count') }}</div>
                    <div class="flex-fill ms-3">@formatNumber($package->pin_count)</div>
                </div>
            @endif
            <div class="col-12 d-flex flex-nowrap">
                <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">@translate('label.package.price')</div>
                <div class="flex-fill ms-3">@translate('format.currency.symbol.text') @formatNumber($package->actual_price)</div>
            </div>
            @if (isPackageRequirePIN() && PIN_ENABLED)
                <div class="col-12 d-flex flex-nowrap">
                    <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.pin-type.plan') }}</div>
                    <div class="flex-fill ms-3">{{ planName(optional($package->pinType)->plan ?? PLAN_A) }}</div>
                </div>
            @endif
            @if (BONUS_SPONSOR_BY_PACKAGE)
                <div class="col-12 d-flex flex-nowrap">
                    <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.bonus-sponsor') }}</div>
                    <div class="flex-fill ms-3">
                        {{ (!($bsPercent = isTrueConfig(BONUS_SPONSOR_TYPE, 'percent')) ? __('format.currency.symbol.text') . ' ' : '') .  formatNumber($package->bonus_sponsor) . ' ' . ($bsPercent ? '%' : '')}}
                    </div>
                </div>
            @endif
            @if (BONUS_EXTRA_ENABLED)
                <div class="col-12 d-flex flex-nowrap">
                    <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.bonus-extra') }}</div>
                    <div class="flex-fill ms-3">
                        {{ (!($bePercent = isTrueConfig(BONUS_EXTRA_TYPE, 'percent')) ? __('format.currency.symbol.text') . ' ' : '') .  formatNumber($package->bonus_extra) . ' ' . ($bePercent ? '%' : '')}}
                    </div>
                </div>
            @endif
            @if (BONUS_UPGRADE_ENABLED)
                <div class="col-12 d-flex flex-nowrap">
                    <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.bonus-upgrade') }}</div>
                    <div class="flex-fill ms-3">
                        {{ (!($buPercent = isTrueConfig(BONUS_UPGRADE_TYPE, 'percent')) ? __('format.currency.symbol.text') . ' ' : '') .  formatNumber($package->bonus_ro) . ' ' . ($buPercent ? '%' : '')}}
                    </div>
                </div>
            @endif
            @if (BONUS_RO_ENABLED)
                <div class="col-12 d-flex flex-nowrap">
                    <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.bonus-ro') }}</div>
                    <div class="flex-fill ms-3">
                        {{ (!($broPercent = isTrueConfig(BONUS_RO_TYPE, 'percent')) ? __('format.currency.symbol.text') . ' ' : '') .  formatNumber($package->bonus_ro) . ' ' . ($broPercent ? '%' : '')}}
                    </div>
                </div>
            @endif
            @if (BONUS_PAIR_ENABLED && isTrueConfig(BONUS_PAIR_TYPE, 'point'))
                <div class="col-12 d-flex flex-nowrap">
                    <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.pair-point') }}</div>
                    <div class="flex-fill ms-3">@formatNumber($package->pair_point)</div>
                </div>
            @endif
            @if (BONUS_PACKAGE_ENABLED)
                <div class="col-12 d-flex flex-nowrap">
                    <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.bonus-package') }}</div>
                    <div class="flex-fill ms-3">
                        <div>{{ $package->package_bonus }}</div>
                        <div class="text-muted">
                            {{ __('text.left') . formatNumber($package->package_left) }}, {{ __('text.right') . formatNumber($package->package_right) }}
                        </div>
                    </div>
                </div>
            @endif
            @if (REWARD_ENABLED)
                <div class="col-12 d-flex flex-nowrap">
                    <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.get-reward') }}</div>
                    <div class="flex-fill ms-3">{{ $package->get_reward ? __('text.uf-yes') : __('text.uf-no') }}</div>
                </div>
                <div class="col-12 d-flex flex-nowrap">
                    <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.give-reward') }}</div>
                    <div class="flex-fill ms-3">{{ $package->give_reward ? __('text.uf-yes') : __('text.uf-no') }}</div>
                </div>
                @if (isTrueConfig(REWARD_BY_TYPE, 'point'))
                    <div class="col-12 d-flex flex-nowrap">
                        <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.reward-point') }}</div>
                        <div class="flex-fill ms-3">@formatNumber($package->reward_point)</div>
                    </div>
                @endif
            @endif
            <div class="col-12 d-flex flex-nowrap">
                <div class="flex-shrink-0 d-flex justify-content-between with-semicolon" style="width:40%;">{{ __('label.package.description') }}</div>
                <div class="flex-fill ms-3">{{ $package->description ?? '-' }}</div>
            </div>
        </div>
    </div>
    @include('partials.modals.modal-footer', [
        'modalFooterContent' => view('partials.buttons.close-modal', [
            'closeText' => __('button.close')
        ])->render(),
        'modalFoterClass' => 'justify-content-center',
    ])
</div>