@php
    $pageHeading = __('header.member.list');
    $hasRole = hasRole(['admin.super', 'admin.master']);
@endphp

@extends('layouts.main')

@section('pageContent')
    @include('partials.alert')

    <div class="card">
        @include('partials.card-header', ['cardTitle' => __('header.member.list')])
        <div class="card-body px-0 py-2">
            <div class="btn-group d-none" id="table-controls">
                @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
            </div>
            <table
                class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap"
                id="my-table">
                <thead>
                    <tr>
                        <th>@translate('label.table.member.username')</th>
                        <th>@translate('label.table.member.name')</th>
                        <th>@translate('label.table.member.phone')</th>
                        {{-- <th>@translate('label.table.member.profile')</th> --}}
                        <th>@translate('label.table.member.register_at')</th>
                        <th>@translate('label.table.member.status')</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
@endsection

@if ($hasRole)
    @push('topContent')
        @include('partials.modals.modal', ['bsModalId' => 'my-modal', 'scrollable' => true])
    @endpush
@endif

@section('vendorCSS')
    @include('partials.datatable.css')
@endsection

@section('vendorJS')
    @include('partials.datatable.js')
@endsection

@push('pageJS')
    <script>
        let table;
        const tableLang = {!! json_encode(__('datatable')) !!};

        function reloadTable() {
            dataTableRefresh(table);
        }

        $(function() {
            table = $('#my-table').DataTable({
                dom: '<"container-fluid gx-3"<"row g-2 mb-2"<"col-auto me-auto me-sm-0"<"dt-control">><"col-auto me-sm-auto"l><"col-12 col-sm-auto"f>>>r<"table-responsive border-top border-translucent"t><"row gy-1 gx-0"<"col-12 col-md"i><"col-12 col-md"p>>',
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('member.dataTable') }}'
                },
                deferLoading: 50,
                language: tableLang,
                search: {
                    return: true
                },
                pagingType: 'full_numbers',
                order: [
                    [3, 'desc']
                ],
                columnDefs: [{
                        className: 'dt-body-center',
                        targets: [3, 4, 5]
                    },
                    {
                        searchable: false,
                        targets: [3, 4, 5]
                    },
                    {
                        orderable: false,
                        targets: [5]
                    },
                ],
                columns: [{
                        data: 'username'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'phone'
                    },
                    // {data: 'has_profile'},
                    {
                        data: 'registered_at'
                    },
                    {
                        data: 'user_status'
                    },
                    {
                        data: 'action'
                    },
                ],
            });

            setDataTableLoading('my-table', '{{ __('datatable.processing') }}');
            setControlToDataTable('my-table', $('#table-controls'));
            $('.dataTables_filter label').addClass('d-block');
            $('.dataTables_filter .form-control').css({
                'margin-left': 0,
                'width': '100%'
            });

            reloadTable();
        });
    </script>
@endpush
