@php
    $isEditPassword = ($editMode == 'password');
    $postRoute = route('member.edit.update', ['userMember' => $userMember->id]);
    $modalHeader = ($isEditPassword ? __('header.member.edit-password') : __('header.member.edit-data')) . ' ' . $userMember->username;
@endphp
<form class="modal-content remote-submit" action="{{ $postRoute }}" method="POST" data-alert-container="#alert-form-container">
    @include('partials.modals.modal-header', ['modalTitle' => $modalHeader])
    <div class="modal-body">
        @csrf
        <input type="hidden" name="edit_mode" value="{{ $editMode }}">
        <div id="alert-form-container"></div>
        <div class="row g-2 row-cols-1">
            @if ($isEditPassword)
                <div class="col">
                    <div class="form-floating">
                        <input type="password" class="form-control" id="input-password" name="password" value="" placeholder="{{ $labelPass = __('label.password.new') }}" autocomplete="off" required>
                        <label for="input-password" class="required">{{ $labelPass }}</label>
                    </div>
                </div>
                <div class="col">
                    <div class="form-floating">
                        <input type="password" class="form-control" id="input-retype" name="password_confirmation" value="" placeholder="{{ $labelRetype = __('label.password.retype-new') }}" autocomplete="off" required>
                        <label for="input-retype" class="required">{{ $labelRetype }}</label>
                    </div>
                </div>
            @else
                <div class="col">
                    <div class="form-floating">
                        <input type="text" class="form-control" id="input-username" name="username" value="{{ $userMember->username }}" placeholder="{{ $labelUsername = __('label.user.username') }}" autocomplete="off" required>
                        <label for="input-username" class="required">{{ $labelUsername }}</label>
                    </div>
                </div>
                <div class="col">
                    <div class="form-floating">
                        <input type="text" class="form-control" id="input-name" name="name" value="{{ $userMember->name }}" placeholder="{{ $labelName = __('label.user.name') }}" autocomplete="off" required>
                        <label for="input-name" class="required">{{ $labelName }}</label>
                    </div>
                </div>
                <div class="col">
                    <div class="form-floating">
                        <input type="email" class="form-control" id="input-email" name="email" value="{{ $userMember->email }}" placeholder="{{ $labelEmail = __('label.user.email') }}" autocomplete="off" required>
                        <label for="input-email" class="required">{{ $labelEmail }}</label>
                    </div>
                </div>
                <div class="col">
                    <div class="form-floating">
                        <input type="text" class="form-control" id="input-phone" name="phone" value="{{ $userMember->phone }}" placeholder="{{ $labelPhone = __('label.user.phone') }}" autocomplete="off" required>
                        <label for="input-phone" class="required">{{ $labelPhone }}</label>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @include('partials.modals.modal-footer', ['modalFooterContent' => [
        view('partials.buttons.submit', ['btnIcon' => 'save', 'btnText' => __('button.save')])->render(),
        view('partials.buttons.close-modal')->render()
    ]])
</form>