@php
    $pageHeading = __('header.dashboard');
@endphp

@extends('layouts.main')

@section('pageContent')
<div class="row g-3 mb-3">
    <div class="col-6 col-md-4 d-flex">
        <div class="flex-fill  p-2 p-md-3 rounded-3 shadow-sm">
            <div class="d-flex align-items-end justify-content-between mb-2 pb-2 border-bottom lh-1">
                <i class="fs-1 text-blue-300 fas fa-users"></i>
                <span class="text-end text-muted fs-auto">Total Member</span>
            </div>
            <div class="text-center text-muted remote-data" data-remote-url="{{ route('dashboard.item') }}" data-remote-key="member" data-remote-function="total" data-return-default="0"></div>
        </div>
    </div>
    <div class="col-6 col-md-4 d-flex">
        <div class="flex-fill  p-2 p-md-3 rounded-3 shadow-sm">
            <div class="d-flex align-items-end justify-content-between mb-2 pb-2 border-bottom lh-1">
                <i class="fs-1 text-green-300 fas fa-users-line"></i>
                <span class="text-end text-muted fs-auto">Member Aktif</span>
            </div>
            <div class="text-center text-muted remote-data" data-remote-url="{{ route('dashboard.item') }}" data-remote-key="member" data-remote-function="active" data-return-default="0"></div>
        </div>
    </div>
    <div class="col-md-4 order-first order-md-last d-flex">
        <div class="flex-fill  p-2 p-md-3 rounded-3 shadow-sm">
            <div class="d-flex align-items-end justify-content-between mb-2 pb-2 border-bottom lh-1">
                <i class="fs-1 text-cyan-300 fas fa-coins"></i>
                <span class="text-end text-muted fs-auto">Total Omzet</span>
            </div>
            <div class="text-center text-muted remote-data" data-remote-url="{{ route('dashboard.item') }}" data-remote-key="member" data-remote-function="omzet" data-return-default="0"></div>
            {{-- <div class="text-center text-muted">5.000.000</div> --}}
        </div>
    </div>
</div>
@endsection
