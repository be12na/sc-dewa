@php
    $pageHeading = [__('menu.member'), __('menu.register')];
@endphp

@extends('layouts.main')

@section('pageContent')
    <div class="row justify-content-center">
        <div class="col-md-10 col-lg-8">
            @include('partials.alert')
            <form class="card" action="{{ route('member.register.store') }}" method="POST">
                @csrf
                @include('partials.card-header', ['cardTitle' => __('header.member.registration')])
                <div class="card-body">
                    @include('register.form')
                </div>
                <div class="card-footer">
                    <div class="row justify-content-sm-center">
                        <div class="col-sm-6 col-xl-4">
                            @include('partials.buttons.submit', [
                                'btnClass' => 'btn-block btn-primary btn-pill',
                                'btnIcon' => 'save',
                                'btnText' => __('button.register')
                            ])
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
