<div class="row g-3">
    <div class="col-12">
        <div class="form-floating @error('name') is-invalid @enderror">
            <input type="text" class="form-control" id="input-name" name="name" placeholder="{{ $labelName = __('label.user.name') }}" value="{{ old('name') }}" autocomplete="off" required autofocus>
            <label for="input-name" class="required">{{ $labelName }}</label>
        </div>
        @error('name')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-xl-6">
        <div class="form-floating @error('email') is-invalid @enderror">
            <input type="email" class="form-control" id="input-email" name="email" placeholder="{{ $labelEmail = __('label.user.email') }}" value="{{ old('email') }}" autocomplete="off" required>
            <label for="input-email" class="required">{{ $labelEmail }}</label>
        </div>
        @error('email')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-xl-6">
        <div class="form-floating @error('phone') is-invalid @enderror">
            <input type="text" class="form-control" id="input-phone" name="phone" placeholder="{{ $labelPhone = __('label.user.phone') }}" value="{{ old('phone') }}" autocomplete="off" required>
            <label for="input-phone" class="required">{{ $labelPhone }}</label>
        </div>
        @error('phone')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-12">
        <div class="form-floating @error('username') is-invalid @enderror">
            <input type="text" class="form-control" id="input-username" name="username" placeholder="{{ $labelUsername = __('label.user.username') }}" value="{{ old('username') }}" autocomplete="off" required>
            <label for="input-username" class="required">{{ $labelUsername }}</label>
        </div>
        @error('username')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-xl-6">
        <div class="form-floating @error('password') is-invalid @enderror">
            <input type="password" class="form-control" id="input-password" name="password" placeholder="{{ $labelPassword = __('label.password.text') }}" autocomplete="off" required>
            <label for="input-password" class="required">{{ $labelPassword }}</label>
        </div>
        @error('password')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-xl-6">
        <div class="form-floating">
            <input type="password" class="form-control" id="input-confirmation" name="password_confirmation" placeholder="{{ $labelConfirm = __('label.password.retype') }}" autocomplete="off" required>
            <label id="input-confirmation" class="required">{{ $labelConfirm }}</label>
        </div>
    </div>
</div>