@extends('layouts.app-mail')

@section('title', __('header.member.registration-info'))

@section('content')
<div style="margin-bottom: 1rem;">
    <div>{!! __('text.welcome', ['member' => '<b>' . $user->name . '</b>']) !!},</div>
    <div>{!! __('text.thank-to-join', ['appname' => '<b>' . config('app.name') . '</b>']) !!}.</div>
    <div>{!! __('text.here-your-info') !!}:</div>
</div>
@include('partials.mail.rows-header', ['textHeader' => __('header.member.private-data') ])
@include('partials.mail.row', [
    'rowLabel' => __('label.user.name'),
    'rowValue' => $user->name,
])
@include('partials.mail.row', [
    'rowLabel' => __('label.user.email'),
    'rowValue' => $user->email,
])
@include('partials.mail.row', [
    'rowLabel' => __('label.user.phone'),
    'rowValue' => $user->phone,
])
@include('partials.mail.row', [
    'rowLabel' => __('label.user.username'),
    'rowValue' => $user->username,
])

@if (isset($password))
@include('partials.mail.row', [
    'rowLabel' => __('label.password.text'),
    'rowValue' => $password,
])
@endif

{{-- @include('partials.mail.rows-header', ['textHeader' => __('header.member.referral-data') ])
@include('partials.mail.row', [
    'rowLabel' => __('label.user.username'),
    'rowValue' => $user->referral->username,
])
@include('partials.mail.row', [
    'rowLabel' => __('label.user.phone'),
    'rowValue' => $user->referral->phone ?? '-',
]) --}}

@include('partials.mail.row-footer')
@endsection
