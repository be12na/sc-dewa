@extends('layouts.app')

@push('iconCSS')
<link rel="stylesheet" href="{{ asset('assets/extensions/bootstrap-icons/font/bootstrap-icons.css') }}">
@endpush

@push('pageCSS')
<link rel="stylesheet" href="{{ asset('assets/css/welcome.css') }}">
@endpush

@section('content')
<div class="welcome-jumbo text-light" style="--bs-welcome-header-image:url('{{ asset(env('IMAGE_BANNER_BACKGROUND')) }}')">
    <div class="container gx-5">
        <div class="d-flex justify-content-center text-center mb-3">
            <img src="{{ asset(env('IMAGE_LARGE_LOGO')) }}" class="welcome-image">
        </div>
        <div class="row justify-content-center mb-5">
            <div class="col-sm-10 col-md-8 col-lg-6">
                <p class="my-0 text-center">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum accusamus ea laudantium, illo dicta sequi, modi deleniti natus consequuntur perspiciatis expedita ducimus aut quas nulla! Distinctio accusantium amet ut sit.
                </p>
            </div>
        </div>
        <div class="row g-3 justify-content-center">
            <div class="col-sm-4 col-md-3 col-lg-2">
                <a href="{{ route('login') }}" class="btn btn-dark rounded-pill w-100 bg-gradient">Login</a>
            </div>
            <div class="col-sm-4 col-md-3 col-lg-2">
                {{-- <a href="{{ route('register') }}" class="btn btn-dark rounded-pill w-100 bg-gradient">Register</a> --}}
                <a href="#" class="btn btn-dark rounded-pill w-100 bg-gradient">Register</a>
            </div>
        </div>
    </div>
</div>
<div class="bg-light text-dark">
    <div class="container gx-3 py-4">
        <h3 class="text-center mb-3 mb-md-4">Feature</h3>
        <div class="row g-2 g-sm-3 g-lg-4">
            <div class="col-sm-6 d-flex">
                <div class="flex-fill border rounded-4 shadow-sm bg-white p-3 p-lg-4">
                    <h4 class="border-bottom">Referral</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                    <a href="#">More detail...</a>
                </div>
            </div>
            <div class="col-sm-6 d-flex">
                <div class="flex-fill border rounded-4 shadow-sm bg-white p-3 p-lg-4">
                    <h4 class="border-bottom">Bonus</h4>
                    <p>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                    <a href="#">More detail...</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-gray-300 text-dark">
    <div class="container gx-3 py-4">
        <h3 class="text-center mb-3 mb-md-4">Top Members</h3>
        <div class="row g-2 g-sm-3 g-lg-4 justify-content-center">
            <div class="col-sm-6 col-lg-4 d-flex">
                <div class="flex-fill border rounded-4 shadow-sm bg-white p-3 p-lg-4">
                    <div class="row g-3 mb-3">
                        <div class="col-md-4 text-center text-md-start">
                            <img src="{{ asset('assets/images/teams/user2.jpg') }}" class="welcome-team-image team-sm team-lg" style="--team-size-sm:40px; --team-size-lg:60px;">
                            <div class="fw-bold mt-3 lh-1">Andya Berna</div>
                        </div>
                        <div class="col-md-8">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="{{ route('register', ['ref' => 'andyaberna']) }}">Follow</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 d-flex">
                <div class="flex-fill border rounded-4 shadow-sm bg-white p-3 p-lg-4">
                    <div class="row g-3 mb-3">
                        <div class="col-md-4 text-center text-md-start">
                            <img src="{{ asset('assets/images/teams/user8.jpg') }}" class="welcome-team-image team-sm team-lg" style="--team-size-sm:40px; --team-size-lg:60px;">
                            <div class="fw-bold mt-3 lh-1">Tatang S</div>
                        </div>
                        <div class="col-md-8">
                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <a href="{{ route('register', ['ref' => 'tatangs']) }}">Follow</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 d-flex">
                <div class="flex-fill border rounded-4 shadow-sm bg-white p-3 p-lg-4">
                    <div class="row g-3 mb-3">
                        <div class="col-md-4 text-center text-md-start">
                            <img src="{{ asset('assets/images/teams/user5.jpg') }}" class="welcome-team-image team-sm team-lg" style="--team-size-sm:40px; --team-size-lg:60px;">
                            <div class="fw-bold mt-3 lh-1">Dewi Shinta</div>
                        </div>
                        <div class="col-md-8">
                            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <a href="{{ route('register', ['ref' => 'dewishinta']) }}">Follow</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-dark text-light pb-4">
    <div class="container gx-3 py-4">
        <div class="row g-2 g-sm-3 g-lg-4">
            <div class="col-md-4">
                <div class="p-3 p-lg-4">
                    <h4 class="border-bottom">Disclaimer</h4>
                    <p class="m-0">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla expedita quos, vitae qui porro distinctio cum itaque in repellat praesentium voluptate laudantium dignissimos? Explicabo sequi dolorem, illo rerum consequuntur eum.
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="p-3 p-lg-4">
                    <h4 class="border-bottom">Contact</h4>
                    <div class="row gx-1 gy-2">
                        <div class="col-3 d-flex flex-nowrap justify-content-between">
                            <span>Name</span>
                            <span>:</span>
                        </div>
                        <div class="col-9">{{ env('CONTACT_CENTER_NAME') }}</div>
                        <div class="col-3 d-flex flex-nowrap justify-content-between">
                            <span>Email</span>
                            <span>:</span>
                        </div>
                        <div class="col-9">{{ env('CONTACT_CENTER_EMAIL') }}</div>
                        <div class="col-3 d-flex flex-nowrap justify-content-between">
                            <span>Phone</span>
                            <span>:</span>
                        </div>
                        <div class="col-9">
                            {{ env('CONTACT_CENTER_PHONE') }}
                        </div>
                        @php
                            $sosmedEnable = env('CONTACT_CENTER_SOSMED_ENABLE', false);
                        @endphp
                        @if ($sosmedEnable == true)
                            <div class="row g-3 mt-3 fs-2 justify-content-center">
                                <div class="col-auto">
                                    <a href="#" target="_blank" class="text-decoration-none link-light rounded-4 bi bi-whatsapp" title="Whatsapp"></a>
                                </div>
                                @if ($telegram = env('CONTACT_CENTER_TELEGRAM'))
                                    <div class="col-auto">
                                        <a href="#" target="_blank" class="text-decoration-none link-light rounded-4 bi bi-telegram" title="Telegram"></a>
                                    </div>
                                @endif
                                @if ($facebook = env('CONTACT_CENTER_FACEBOOK'))
                                    <div class="col-auto">
                                        <a href="#" target="_blank" class="text-decoration-none link-light rounded-4 bi bi-facebook" title="Facebook"></a>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="p-3 p-lg-4">
                    <h4 class="border-bottom">Support</h4>
                    <p class="m-0">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla expedita quos, vitae qui porro distinctio cum itaque in repellat praesentium voluptate laudantium dignissimos? Explicabo sequi dolorem, illo rerum consequuntur eum.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection