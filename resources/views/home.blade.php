@extends('layouts.main', [
    'pageHeading' => 'Home'
])

@section('pageContent')
<div class="row g-3 mb-3">
    <div class="col-md-4 d-flex">
        <div class="card box flex-fill">
            <div class="card-body">
                <div class="row g-1">
                    <div class="col-4 col-md-12 d-flex align-items-center align-items-sm-start align-items-md-center">
                        <div class="box-icon bg-primary bg-opacity-50">
                            <i class="fas fa-user"></i>
                        </div>
                    </div>
                    <div class="col-8 col-md-12 pt-0 pt-md-3">
                        <div class="text-end text-md-start text-muted">{{ $authUser->name }}</div>
                        <div class="text-end text-md-start info-text fs-5">Platinum</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-4 d-flex">
        <div class="card box flex-fill">
            <div class="card-body">
                <div class="row g-1">
                    <div class="col-12 col-sm-4 col-md-12 d-flex align-items-center align-items-sm-start align-items-md-center">
                        <div class="box-icon bg-success bg-opacity-50">
                            <i class="fas fa-users"></i>
                        </div>
                    </div>
                    <div class="col-12 col-sm-8 col-md-12 pt-3 pt-sm-0 pt-md-3">
                        <div class="text-sm-end text-md-start text-muted">Member Aktif</div>
                        <div class="text-sm-end text-md-start info-text fs-5">100</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-4 d-flex">
        <div class="card box flex-fill">
            <div class="card-body">
                <div class="row g-1">
                    <div class="col-12 col-sm-4 col-md-12 d-flex align-items-center align-items-sm-start align-items-md-center">
                        <div class="box-icon bg-info bg-opacity-50">
                            <i class="fas fa-user-plus"></i>
                        </div>
                    </div>
                    <div class="col-12 col-sm-8 col-md-12 pt-3 pt-sm-0 pt-md-3">
                        <div class="text-sm-end text-md-start text-muted">Member Baru</div>
                        <div class="text-sm-end text-md-start info-text fs-5">5</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
