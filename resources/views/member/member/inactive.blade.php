@php
    $pageHeading = __('breadcrumb.member.inactivated');
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<div class="card">
    @include('partials.card-header', ['cardTitle' => __('header.member.inactivated')])
    <div class="card-body px-0 py-2">
        <div class="btn-group d-none" id="button-controls">
            @include('partials.buttons.table-refresh', ['reloadFunction' => 'reloadTable()'])
        </div>
        <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap" id="my-table">
            <thead>
                <tr>
                    <th>@translate('label.user.name')</th>
                    <th>@translate('label.user.username')</th>
                    <th>@translate('label.user.phone')</th>
                    <th>@translate('label.user.register-at')</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('vendorCSS')
    @include('partials.datatable.css')
@endsection

@section('vendorJS')
    @include('partials.datatable.js')
@endsection

@push('pageJS')
<script>
    let table;
    const tableLang = {!! json_encode(__('datatable')) !!};

    function reloadTable() {
        dataTableRefresh(table);
    }

    $(function() {
        table = $('#my-table').DataTable({
            dom: '<"container-fluid gx-3"<"row g-2 mb-2"<"col-auto me-auto me-sm-0"<"dt-control">><"col-auto me-sm-auto"l><"col-12 col-sm-auto"f>>>r<"table-responsive border-top border-translucent"t><"row gy-1 gx-0"<"col-12 col-md"i><"col-12 col-md"p>>',
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("member.inactive.dataTable") }}'
            },
            search: {
                return: true
            },
            deferLoading: 50,
            language: tableLang,
            columns: [
                {data: 'name'},
                {data: 'username', className: 'd-none'},
                {data: 'phone'},
                {data: 'registered_at', className: 'dt-body-center', searchable: false},
                {data: 'pkg_status', className: 'dt-body-center', searchable: false, orderable: false},
            ],
        });

        setDataTableLoading('my-table', '{{ __("datatable.processing") }}');
        setControlToDataTable('my-table', $('#button-controls'));
        $('.dataTables_filter label').addClass('d-block');
        $('.dataTables_filter .form-control').css({'margin-left': 0, 'width': '100%'});
        $('#my-table_filter .form-control').attr({placeholder: 'Username / Nama / Handphone'});

        reloadTable();
    });
</script>
@endpush
