<form class="modal-content remote-submit" action="{{ route('member.placement.save') }}" method="POST" data-alert-container="#alert-form-container">
    @include('partials.modals.modal-header', ['modalTitle' => __('header.member.placement-select')])
    <div class="modal-body">
        @csrf
        <input type="hidden" name="parent_member" value="{{ $parentMember->id }}">
        <input type="hidden" name="position" value="{{ $position }}">
        <div id="alert-form-container"></div>
        <label class="d-block required">@translate('label.member')</label>
        <select class="form-select" name="user_id" required>
            @foreach ($members as $member)
                <option value="{{ $member->id }}">{{ $member->name }} ({{ $member->username }})</option>
            @endforeach
        </select>
    </div>
    @include('partials.modals.modal-footer', [
        'modalFoterClass' => 'justify-content-center',
        'modalFooterContent' => [
            view('partials.buttons.submit', [
                'btnIcon' => 'save',
                'btnClass' => 'btn-sm btn-primary',
                'btnText' => __('button.placement')
            ])->render(),
            view('partials.buttons.close-modal', [
                'btnClass' => 'btn-sm btn-danger',
            ])->render()
        ],
    ])
</form>