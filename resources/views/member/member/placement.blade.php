@php
    $pageHeading = __('breadcrumb.member.placement');
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<div class="card">
    @include('partials.card-header', ['cardTitle' => __('header.member.placement')])
    <div class="card-body p-0">
        @include('partials.binary.container', [
            'structure' => $structure,
            'isPlacement' => true,
        ])
    </div>
</div>
@endsection