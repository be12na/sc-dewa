@php
    $buttons = [];

    if ($isOpenMe) {
        $buttons[] = '<a href="' . $indexRoute . '" class="btn btn-sm btn-primary"><i class="fa fa-user me-2"></i>' . __('text.me') . '</a>';
    }

    if (($member->id != $authUser->id) && !$isOpen && ($depth > 1)) {
        $buttons[] = '<a href="' . $indexRoute . '?user_id=' . $member->structure->parent->user_id . '" class="btn btn-sm btn-success"><i class="fa fa-arrow-up me-2"></i>Upline</a>';
    }

    if ($isOpen) {
        $buttons[] = '<a href="' . $indexRoute . '?user_id=' . $member->id . '" class="btn btn-sm btn-warning"><i class="fa fa-arrow-down me-2"></i>' . __('button.open') . '</a>';
    }

    $buttons[] = view('partials.buttons.close-modal', [
        'closeText' => __('button.close'),
        'btnClass' => 'btn-sm btn-danger',
    ])->render();

    $package = $member->package;
    $structure = $member->structure;
    $isAgen = ($member->my_package_id == TOP_PACKAGE);
    $summaries = $member->summary_of_bonus_reward;
    // $summary = $isAgen ? $summaries->agen : $summaries->reseller;
    $packages = \App\Models\Package::query()->orderBy('id')->get();
@endphp

<div class="modal-content">
    @include('partials.modals.modal-header', ['modalTitle' => __('header.member.detail')])
    <div class="modal-body fs-auto">
        <div class="d-flex flex-nowrap align-items-end mb-2 pb-1 border-bottom border-theme-1">
            <img src="{{ $package->image_logo }}" alt="{{ $package->name }}" class="flex-shrink-0" style="height: auto; width: 40px; max-width: 50%;">
            <span class="ms-2 fw-bold text-theme-1 fs-4 lh-1">{{ $package->name }}</span>
        </div>
        <div class="row gy-1 gx-2">
            {{-- username --}}
            <div class="col-5 d-flex flex-nowrap justify-content-between">
                <span>@translate('label.user.username')</span><span>:</span>
            </div>
            <div class="col-7">{{ $member->username }}</div>
            {{-- name --}}
            <div class="col-5 d-flex flex-nowrap justify-content-between">
                <span>@translate('label.user.name')</span><span>:</span>
            </div>
            <div class="col-7">{{ $member->name }}</div>
            {{-- phone --}}
            <div class="col-5 d-flex flex-nowrap justify-content-between">
                <span>@translate('label.user.phone')</span><span>:</span>
            </div>
            <div class="col-7">{{ $member->phone }}</div>
            <div class="col-12"><div class="border-bottom"></div></div>
            {{-- network info --}}
            @foreach ($packages as $package)
                @php
                    $pkgCount = $structure->descendants()->whereHas('user', function($user) use($package) {
                        return $user->where('package_id', '=', $package->id);
                    })->count();
                @endphp
                <div class="col-5 d-flex flex-nowrap justify-content-between">
                    <span>- {{ $package->name }}</span><span>:</span>
                </div>
                <div class="col-7">@formatNumber($pkgCount)</div>
            @endforeach

            <div class="col-12"><div class="border-bottom"></div></div>
            <div class="col-5"></div>
            <div class="col-7">
                <div class="row g-1">
                    <div class="col-6 text-center">@translate('text.left')</div>
                    <div class="col-6 text-center">@translate('text.right')</div>
                </div>
            </div>
            {{-- info member --}}
            <div class="col-5 d-flex flex-nowrap justify-content-between">
                <span>Member</span><span>:</span>
            </div>
            <div class="col-7">
                <div class="row g-1">
                    <div class="col-6 text-center">@formatNumber($member->count_left)</div>
                    <div class="col-6 text-center">@formatNumber($member->count_right)</div>
                </div>
            </div>
            <div class="col-5 d-flex flex-nowrap justify-content-between">
                <span>Poin Aktivasi</span><span>:</span>
            </div>
            <div class="col-7">
                <div class="row g-1">
                    <div class="col-6 text-center">@formatNumber($summaries->reseller->remaining_left)</div>
                    <div class="col-6 text-center">@formatNumber($summaries->reseller->remaining_right)</div>
                </div>
            </div>
            @if ($summaries->is_agen)
                <div class="col-5 d-flex flex-nowrap justify-content-between">
                    <span>Poin Upgrade & RO</span><span>:</span>
                </div>
                <div class="col-7">
                    <div class="row g-1">
                        <div class="col-6 text-center">@formatNumber($summaries->agen->remaining_left)</div>
                        <div class="col-6 text-center">@formatNumber($summaries->agen->remaining_right)</div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @include('partials.modals.modal-footer', [
        'modalFoterClass' => 'justify-content-center',
        'modalFooterContent' => $buttons,
    ])
</div>