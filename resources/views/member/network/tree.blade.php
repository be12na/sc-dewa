@php
    $pageHeading = __('breadcrumb.network.tree');
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<div class="card">
    @include('partials.card-header', ['cardTitle' => __('header.network.tree')])
    <div class="card-body p-0 fs-auto">
        @include('partials.binary.container', [
            'structure' => $structure,
            'isPlacement' => false,
        ])
    </div>
</div>
@endsection