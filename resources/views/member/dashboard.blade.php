@php
    $pageHeading = __('header.dashboard');
    $hasPackage = $authUser->has_package;
    $package = $hasPackage ? $authUser->package : null;
    $packageLogo = ($hasPackage ? $package->image_logo : asset(env('IMAGE_LARGE_LOGO'))) . '?v=' . $fileV;
    $packageAlt = $hasPackage ? $package->name : config('app.name');
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')

<div class="row g-2 g-md-3">
    <div class="col-12 col-lg-4 d-flex">
        @php
            $box1Body = "<div>{$authUser->name} ({$authUser->username})</div>";
            $box1Footer = '';
            if ($hasPackage) {
                $box1Body .= "<div>{$package->name}</div>";
                $box1Footer = translateDatetime($authUser->activated_at, __('format.date.full'));
            }
        @endphp
        @include('partials.box.content', [
            'boxBackground' => 'bg-theme-1 bg-opacity-75',
            'boxColor' => 'text-gray-200',
            'boxBorder' => '',
            'backgroundIcon' => 'bg-theme-1 bg-opacity-75',
            'contentIcon' => "<img src=\"{$packageLogo}\" alt=\"{$packageAlt}\" style=\"width:50px; height:auto; max-width:60%;\">",
            'contentBody' => $box1Body,
            'boxHasFooter' => $hasPackage,
            'contentFooter' => "<span class=\"small lh-1\">{$box1Footer}</span>",
        ])
    </div>
    <div class="col-12 col-lg-8 d-flex">
        {{-- referral link --}}
    </div>
    <div class="col-12 col-lg-4 d-flex">
        {{-- total bonus --}}
        @include('partials.box.content', [
            'boxBackground' => 'bg-theme-3 bg-opacity-75',
            'boxColor' => 'text-gray-200',
            'boxBorder' => '',
            'backgroundIcon' => 'bg-theme-3 bg-opacity-75',
            'contentIcon' => '<i class="bi bi-cash-stack fs-2 text-warning"></i>',
            'contentBody' => '<div>Total Bonus</div>' . view('partials.box.livewire', [
                'livewireName' => 'member.total-bonus',
                'livewireArgs' => [
                    'bonusCode' => 'all',
                    'status' => 'all',
                    'cssClass' => '',
                    'poll' => '15s',
                ],
            ])->render(),
            'boxHasFooter' => true,
            'contentFooter' => '<span class="small lh-1">Total Semua Bonus</span>',
        ])
    </div>
    <div class="col-12 col-sm-6 col-lg-4 d-flex">
        {{-- bonus ditarik / wd --}}
        @include('partials.box.content', [
            'boxBackground' => 'bg-theme-3 bg-opacity-75',
            'boxColor' => 'text-gray-200',
            'boxBorder' => '',
            'backgroundIcon' => 'bg-theme-3 bg-opacity-75',
            'contentIcon' => '<i class="bi bi-wallet fs-2 text-warning"></i>',
            'contentBody' => '<div>Total Penarikan</div>' . view('partials.box.livewire', [
                'livewireName' => 'member.total-bonus',
                'livewireArgs' => [
                    'bonusCode' => 'all',
                    'status' => 'withdraw',
                    'cssClass' => '',
                    'poll' => '15s',
                ],
            ])->render(),
            'boxHasFooter' => true,
            'contentFooter' => '<span class="small lh-1">Total bonus yang ditarik</span>',
        ])
    </div>
    <div class="col-12 col-sm-6 col-lg-4 d-flex">
        {{-- bonus dalam proses --}}
        @include('partials.box.content', [
            'boxBackground' => 'bg-theme-3 bg-opacity-75',
            'boxColor' => 'text-gray-200',
            'boxBorder' => '',
            'backgroundIcon' => 'bg-theme-3 bg-opacity-75',
            'contentIcon' => '<i class="bi bi-cpu fs-2 text-warning"></i>',
            'contentBody' => '<div>Sisa Bonus</div>' . view('partials.box.livewire', [
                'livewireName' => 'member.total-bonus',
                'livewireArgs' => [
                    'bonusCode' => 'all',
                    'status' => 'pending',
                    'cssClass' => '',
                    'poll' => '15s',
                ],
            ])->render(),
            'boxHasFooter' => true,
            'contentFooter' => '<span class="small lh-1">Total bonus dalam proses</span>',
        ])
    </div>
    <div class="col-12 col-lg-4 d-flex">
        {{-- total member per paket --}}
        @include('partials.box.content', [
            'boxBackground' => 'bg-theme-1 bg-opacity-75',
            'boxColor' => 'text-gray-200',
            'boxBorder' => '',
            'backgroundIcon' => 'bg-theme-3 bg-opacity-75',
            'contentIcon' => '<i class="bi bi-people fs-2 text-warning"></i>',
            'contentBody' => view('partials.box.livewire', [
                'livewireName' => 'member.total-member',
                'livewireArgs' => [
                    'scope' => 'package',
                    'status' => 'active',
                    'cssClass' => '',
                    'poll' => '17s',
                ],
            ])->render(),
            'boxHasFooter' => true,
            'contentFooter' => '<span class="small lh-1">Jumlah member per-paket</span>',
        ])
    </div>
    <div class="col-12 col-sm-6 col-lg-4 d-flex">
        {{-- total member disponsor --}}
        @include('partials.box.content', [
            'boxBackground' => 'bg-theme-1 bg-opacity-75',
            'boxColor' => 'text-gray-200',
            'boxBorder' => '',
            'backgroundIcon' => 'bg-theme-3 bg-opacity-75',
            'contentIcon' => '<i class="bi bi-person-down fs-2 text-warning"></i>',
            'contentBody' => '<div>Jumlah Disponsori</div>' . view('partials.box.livewire', [
                'livewireName' => 'member.total-member',
                'livewireArgs' => [
                    'scope' => 'sponsor',
                    'status' => 'active',
                    'cssClass' => '',
                    'poll' => '17s',
                ],
            ])->render(),
            'boxHasFooter' => true,
            'contentFooter' => '<span class="small lh-1">&nbsp;</span>',
        ])
    </div>
    <div class="col-12 col-sm-6 col-lg-4 d-flex">
        {{-- bonus dalam proses --}}
        @include('partials.box.content', [
            'boxBackground' => 'bg-theme-1 bg-opacity-75',
            'boxColor' => 'text-gray-200',
            'boxBorder' => '',
            'backgroundIcon' => 'bg-theme-3 bg-opacity-75',
            'contentIcon' => '<i class="bi bi-diagram-2 fs-2 text-warning"></i>',
            'contentBody' => view('partials.box.livewire', [
                'livewireName' => 'member.total-member',
                'livewireArgs' => [
                    'scope' => 'left-right',
                    'status' => 'active',
                    'cssClass' => '',
                    'poll' => '17s',
                ],
            ])->render(),
            'boxHasFooter' => true,
            'contentFooter' => '<span class="small lh-1">Jumlah terpasang</span>',
        ])
    </div>
</div>
@endsection

@section('livewireScript')
    @livewireScripts
@endsection

@push('pageJS')
    <script>
        window.livewire.onError(statusCode => {
            return false
        });
    </script>
@endpush
