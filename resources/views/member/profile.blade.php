@php
    $pageHeading = __('breadcrumb.profile');
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<form class="card" method="POST" action="{{ route('profile.update') }}">
    @include('partials.card-header', ['cardTitle' => __('header.user.profile')])
    <div class="card-body">
        @csrf
        <div class="row g-3">
            <div class="col-xl-4">
                <div class="form-floating @error('name') is-invalid @enderror">
                    <input type="text" class="form-control" id="input-name" name="name" value="{{ old('name', $authUser->name) }}" placeholder="{{ $labelName = __('label.user.name') }}" autocomplete="off" required autofocus>
                    <label for="input-name" class="required">{{ $labelName }}</label>
                </div>
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="form-floating @error('email') is-invalid @enderror">
                    <input type="email" class="form-control" id="input-email" name="email" value="{{ old('email', $authUser->email) }}" placeholder="{{ $labelEmail = __('label.user.email') }}" autocomplete="off" required>
                    <label for="input-email" class="required">{{ $labelEmail }}</label>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="form-floating @error('phone') is-invalid @enderror">
                    <input type="text" class="form-control" id="input-phone" name="phone" value="{{ old('phone', $authUser->phone) }}" placeholder="{{ $labelPhone = __('label.user.phone') }}" autocomplete="off" required>
                    <label for="input-phone" class="required">{{ $labelPhone }}</label>
                </div>
            </div>
            <div class="col-12">
                <div class="form-floating @error('address') is-invalid @enderror">
                    <input type="text" class="form-control" id="input-address" name="address" value="{{ old('address', $authUser->address) }}" placeholder="{{ $labelAddress = __('label.user.address') }}" autocomplete="off" required>
                    <label for="input-address" class="required">{{ $labelAddress }}</label>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="row justify-content-sm-center justify-content-lg-start">
            <div class="col-sm-auto">
                @include('partials.buttons.submit', [
                    'btnClass' => 'btn-block btn-primary btn-pill',
                    'btnIcon' => 'save',
                    'btnText' => 'Update Profile'
                ])
            </div>
        </div>
    </div>
</form>
@endsection