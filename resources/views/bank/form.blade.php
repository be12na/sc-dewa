@php
    $mode = strtolower(isset($bankMode) ? $bankMode : 'new');
    $isNew = ($mode == 'new');
    $isActivate = ($mode == 'activate');
    $isDeactivate = ($mode == 'deactivate');
    $isConfirmForm = ($isActivate || $isDeactivate);
    $userBank = optional((isset($userBank) && !$isNew) ? $userBank : null);
    $isAdminGroup = $authUser->isGroup('admin');

    if ($isConfirmForm) {
        $submitIcon = $isDeactivate ? 'ban' : 'check';
        $submitText = $isDeactivate ? __('button.deactivate') : __('button.activate');
        $postUrl = $isAdminGroup 
            ? route('setting.bank.updateStatus', ['userBank' => $userBank->id])
            : route('bank.updateStatus', ['userBank' => $userBank->id]);
    } else {
        $submitIcon = 'save';
        $submitText = $isNew ? __('button.save') : __('button.update');
        $postUrl = $isAdminGroup
            ? ($isNew 
                ? route('setting.bank.store')
                : route('setting.bank.update', ['userBank' => $userBank->id])
            ) : ($isNew
                ? route('bank.store')
                : route('bank.update', ['userBank' => $userBank->id])
            );
    }
@endphp
<form class="modal-content remote-submit" action="{{ $postUrl }}" method="POST" data-alert-container="#alert-form-container">
    @include('partials.modals.modal-header', ['modalTitle' => __("header.bank.{$mode}")])
    <div class="modal-body">
        @csrf
        <div id="alert-form-container"></div>

        @if (!$isConfirmForm)
            <div class="mb-3">
                <label class="form-label required">Bank</label>
                <select name="bank_id" class="form-select" autocomplete="off" required>
                    @foreach ($banks as $bank)
                        <option value="{{ $bank->id }}" @optionSelected($bank->id, $userBank->bank_id ?? 0)>{{ $bank->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label class="form-label required">{{ $label_acc_no = __('label.bank.account-no') }}</label>
                <input type="text" class="form-control" name="{{ $isAdminGroup ? 'acc_no_company' : 'acc_no_member' }}" value="{{ $userBank->acc_no }}" placeholder="{{ $label_acc_no }}" autocomplete="off" required>
            </div>
            <div>
                <label class="form-label required">{{ $label_acc_name = __('label.bank.account-name') }}</label>
                <input type="text" class="form-control" name="acc_name" value="{{ $userBank->acc_name }}" placeholder="{{ $label_acc_name }}" autocomplete="off" required>
            </div>
        @else
            <input type="hidden" name="mode" value="{{ $isDeactivate ? 456 : 358 }}">
            <div class="text-center @if($isDeactivate) text-red-300 @endif fw-bold">@translate($isDeactivate ? 'message.bank.deactivate' : 'message.bank.activate')</div>
        @endif
    </div>
    @include('partials.modals.modal-footer', [
        'modalFoterClass' => $isConfirmForm ? 'justify-content-center' : '',
        'modalFooterContent' => [
            view('partials.buttons.submit', ['btnIcon' => $submitIcon, 'btnText' => $submitText, 'btnClass' => 'btn-sm btn-success'])->render(),
            view('partials.buttons.close-modal', ['btnClass' => 'btn-sm btn-danger'])->render()
        ],
    ])
</form>