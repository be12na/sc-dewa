@php
    $pageHeading = $authUser->isGroup('admin') ? __('breadcrumb.settings.bank') : __('breadcrumb.bank');
    $hasRuleBank = hasRole(['admin.super', 'member.member']);
    $isCompany = $authUser->isGroup('admin');
@endphp

@extends('layouts.main')

@section('pageContent')
@include('partials.alert')
<div class="card">
    @include('partials.card-header', ['cardTitle' => __('header.bank.list')])
    <div class="card-body px-0 pt-0 pb-2">
        @if ($hasRuleBank)
            @php
                $routeAdd = $authUser->isGroup('admin') ? route('setting.bank.add') : route('bank.add');
            @endphp
            <div class="p-2">
                @include('partials.buttons.open-modal', [
                    'isAjaxModal' => true,
                    'modalUrl' => $routeAdd,
                    'btnClass' => 'btn-sm btn-primary btn-theme',
                    'modalTarget' => '#my-modal',
                    'btnText' => __('button.add'),
                ])
            </div>
        @endif
        <div class="d-block w-100 table-responsive border-top border-translucent">
            <table class="table table-hover table-striped table-header-centered table-header-bordered table-translucent table-nowrap">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Bank</th>
                        <th>@translate('label.bank.account-no')</th>
                        <th>@translate('label.bank.account-name')</th>
                        <th>@translate('label.is-active')</th>
                        @if ($hasRuleBank)
                            <th></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @if ($authUser->user_banks->isNotEmpty())
                        @php
                            $number = 1;
                        @endphp
                        @foreach ($authUser->user_banks as $userBank)
                            <tr>
                                <td>{{ $number++ }}</td>
                                <td>{{ $userBank->bank_code }}</td>
                                <td>{{ $userBank->acc_no }}</td>
                                <td>{{ $userBank->acc_name }}</td>
                                <td class="text-center">@contentCheck($userBank->is_active)</td>
                                @if ($hasRuleBank)
                                    <td class="text-center">
                                        @if ($userBank->is_active)
                                            @php
                                                $routeEdit = $authUser->isGroup('admin')
                                                    ? route('setting.bank.edit', ['userBank' => $userBank->id])
                                                    : route('bank.edit', ['userBank' => $userBank->id]);
                                            @endphp
                                            @include('partials.buttons.open-modal', [
                                                'isAjaxModal' => true,
                                                'modalUrl' => $routeEdit,
                                                'btnClass' => 'btn-sm btn-warning',
                                                'modalTarget' => '#my-modal',
                                                'btnText' => __('button.edit'),
                                            ])
                                        @endif

                                        @if ($isCompany || (!$isCompany && !$userBank->is_active))
                                            @include('partials.buttons.open-modal', [
                                                'isAjaxModal' => true,
                                                'modalUrl' => $isCompany 
                                                    ? route('setting.bank.status', ['userBank' => $userBank->id, 'userBankStatus' => $userBank->is_active ? 'deactivate' : 'activate'])
                                                    : route('bank.status', ['userBank' => $userBank->id, 'userBankStatus' => $userBank->is_active ? 'deactivate' : 'activate']),
                                                'btnClass' => 'btn-sm ' . ($userBank->is_active ? 'btn-danger' : 'btn-secondary'),
                                                'modalTarget' => '#my-modal',
                                                'btnText' => $userBank->is_active ? __('button.deactivate') : __('button.activate'),
                                            ])
                                        @endif
                                        {{-- TODO: activate / deactivate bank account --}}
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="{{ $hasRuleBank ? 6 : 5 }}">{{ __('datatable.emptyTable') }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('topContent')
@include('partials.modals.modal', ['bsModalId' => 'my-modal', 'scrollable' => true])
@endpush