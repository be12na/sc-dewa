@php
    $pageHeading = __('breadcrumb.password');
@endphp

@extends('layouts.main')

@section('pageContent')
<div class="row justify-content-center">
    <div class="col-sm-10 col-md-8 col-lg-6">
        <form class="card" method="POST" action="{{ route('password.update') }}">
            <div class="card-header fw-bold text-center">@translate('header.password.change')</div>
            <div class="card-body">
                @csrf
                @include('partials.alert')
                <div class="mb-3">
                    <div class="form-floating @error('old_password') is-invalid @enderror">
                        <input type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" id="old-password" placeholder="@translate('label.password.old')" autocomplete="off" required autofocus>
                        <label for="old-password">@translate('label.password.old')</label>
                    </div>
                    @error('old_password')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <hr/>
                <div class="mb-3">
                    <div class="form-floating @error('new_password') is-invalid @enderror">
                        <input type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" id="new-password" placeholder="@translate('label.password.new')" autocomplete="off" required>
                        <label for="new-password">@translate('label.password.new')</label>
                    </div>
                    @error('new_password')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control" name="new_password_confirmation" id="retype-password" placeholder="@translate('label.password.retype-new')" autocomplete="off" required>
                    <label for="retype-password">@translate('label.password.retype-new')</label>
                </div>
            </div>
            <div class="card-footer">
                @include('partials.buttons.submit', ['btnIcon' => 'save', 'btnText' => __('button.update'), 'btnClass' => 'btn-block'])
            </div>
        </form>
    </div>
</div>
@endsection
