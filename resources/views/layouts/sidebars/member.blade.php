<li class="menu-item">
    <a href="{{ route('dashboard.index') }}" class="menu-link @menuCollapsed(['dashboard.index'])">
        <span class="menu-icon fa fa-home"></span>
        <span class="menu-text">{{ __('menu.dashboard') }}</span>
    </a>
</li>
<li class="menu-item">
    <a href="#" class="menu-link @menuCollapsed(['member'])" data-bs-toggle="collapse" data-bs-target="#menu-members">
        <span class="menu-icon fa fa-users"></span>
        <span class="menu-text">{{ __('menu.member') }}</span>
    </a>
    <ul class="menu collapse @menuShow(['member'])" id="menu-members" data-bs-parent="#my-sidebar-menu">
        <li class="menu-item">
            <a href="{{ route('member.register.index') }}" class="menu-link @menuActive(['member.register'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.register') }}</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{ route('member.inactive.index') }}" class="menu-link @menuActive(['member.inactive'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.inactivated') }}</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{ route('member.placement.index') }}" class="menu-link @menuActive(['member.placement'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.placement') }}</span>
            </a>
        </li>
    </ul>
</li>
<li class="menu-item">
    <a href="#" class="menu-link @menuCollapsed(['network'])" data-bs-toggle="collapse" data-bs-target="#menu-network">
        <span class="menu-icon fa fa-network-wired"></span>
        <span class="menu-text">{{ __('menu.networks') }}</span>
    </a>
    <ul class="menu collapse @menuShow(['network'])" id="menu-network" data-bs-parent="#my-sidebar-menu">
        <li class="menu-item">
            <a href="{{ route('network.tree.index') }}" class="menu-link @menuActive(['network.tree'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.structure-tree') }}</span>
            </a>
        </li>
        {{-- <li class="menu-item">
            <a href="#" class="menu-link">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.network-list') }}</span>
            </a>
        </li> --}}
        <li class="menu-item">
            <a href="{{ route('network.sponsored.index') }}" class="menu-link @menuActive(['network.sponsored'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.sponsored') }}</span>
            </a>
        </li>
    </ul>
</li>
<li class="menu-item">
    <a href="#" class="menu-link @menuCollapsed(['package.transaction', 'package.history', 'package.upgrade', 'package.ro'])" data-bs-toggle="collapse" data-bs-target="#menu-package">
        <span class="menu-icon fa fa-layer-group"></span>
        <span class="menu-text">{{ __('menu.package') }}</span>
    </a>
    <ul class="menu collapse @menuShow(['package.transaction', 'package.history', 'package.upgrade', 'package.ro'])" id="menu-package" data-bs-parent="#my-sidebar-menu">
        @if ($authUser->has_package)
            @if ($authUser->upgradable_package)
                <li class="menu-item">
                    <a href="{{ route('package.upgrade.index') }}" class="menu-link @menuActive(['package.upgrade'])">
                        <span class="menu-icon"></span>
                        <span class="menu-text">{{ __('menu.upgrade') }}</span>
                    </a>
                </li>
            @else
                <li class="menu-item">
                    <a href="{{ route('package.ro.index') }}" class="menu-link @menuActive(['package.ro'])">
                        <span class="menu-icon"></span>
                        <span class="menu-text">{{ __('menu.ro') }}</span>
                    </a>
                </li>
            @endif
        @endif
        <li class="menu-item">
            <a href="{{ route('package.transaction.index') }}" class="menu-link @menuActive(['package.transaction'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.transaction') }}</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{ route('package.history.index') }}" class="menu-link @menuActive(['package.history'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.history') }}</span>
            </a>
        </li>
    </ul>
</li>
<li class="menu-item">
    <a href="#" class="menu-link @menuCollapsed(['bonus'])" data-bs-toggle="collapse" data-bs-target="#menu-bonus">
        <span class="menu-icon fa fa-coins"></span>
        <span class="menu-text">{{ __('menu.bonus') }}</span>
    </a>
    <ul class="menu collapse @menuShow(['bonus'])" id="menu-bonus" data-bs-parent="#my-sidebar-menu">
        <li class="menu-item">
            <a href="{{ route('bonus.summary.index') }}" class="menu-link @menuActive(['bonus.summary'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.bonus-summary') }}</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{ route('bonus.sponsor.index') }}" class="menu-link @menuActive(['bonus.sponsor'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.bonus-sponsor') }}</span>
            </a>
        </li>
        @if (BONUS_EXTRA_ENABLED)
            <li class="menu-item">
                <a href="{{ route('bonus.extra.index') }}" class="menu-link @menuActive(['bonus.extra'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-extra') }}</span>
                </a>
            </li>
        @endif
        @if (BONUS_RO_ENABLED)
            <li class="menu-item">
                <a href="{{ route('bonus.ro.index') }}" class="menu-link @menuActive(['bonus.ro'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-ro') }}</span>
                </a>
            </li>
        @endif
        @if (BONUS_UPGRADE_ENABLED)
            <li class="menu-item">
                <a href="{{ route('bonus.upgrade.index') }}" class="menu-link @menuActive(['bonus.upgrade'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-upgrade') }}</span>
                </a>
            </li>
        @endif
        @if (BONUS_PACKAGE_ENABLED)
            <li class="menu-item">
                <a href="{{ route('bonus.package.index') }}" class="menu-link @menuActive(['bonus.package'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-package') }}</span>
                </a>
            </li>
        @endif
        @if (REWARD_ENABLED && $authUser->has_package && $authUser->package->get_reward)
            <li class="menu-item">
                <a href="{{ route('bonus.reward.index') }}" class="menu-link @menuActive(['bonus.reward'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-reward') }}</span>
                </a>
            </li>
        @endif
    </ul>
</li>
{{-- <li class="menu-item">
    <a href="#" class="menu-link @menuCollapsed(['withdraw.sponsor', 'withdraw.level'])" data-bs-toggle="collapse" data-bs-target="#menu-withdraw">
        <span class="menu-icon fa fa-wallet"></span>
        <span class="menu-text">{{ __('menu.withdraw') }}</span>
    </a>
    <ul class="menu collapse @menuShow(['withdraw.sponsor', 'withdraw.level'])" id="menu-withdraw" data-bs-parent="#my-sidebar-menu">
        <li class="menu-item">
            <a href="#" class="menu-link">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.bonus-sponsor') }}</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="#" class="menu-link">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.bonus-level') }}</span>
            </a>
        </li>
    </ul>
</li> --}}