<li class="menu-item">
    <a href="{{ route('dashboard.index') }}" class="menu-link @menuCollapsed(['dashboard.index'])">
        <span class="menu-icon fa fa-home"></span>
        <span class="menu-text">{{ __('menu.dashboard') }}</span>
    </a>
</li>
<li class="menu-item">
    <a href="{{ route('member.index') }}" class="menu-link @menuActive('member')">
        <span class="menu-icon fa fa-users"></span>
        <span class="menu-text">{{ __('menu.member') }}</span>
    </a>
</li>
<li class="menu-item">
    <a href="#" class="menu-link @menuCollapsed(['package.transaction', 'package.history'])" data-bs-toggle="collapse" data-bs-target="#menu-packages">
        <span class="menu-icon fa fa-layer-group"></span>
        <span class="menu-text">{{ __('menu.package') }}</span>
    </a>
    <ul class="menu collapse @menuShow(['package.transaction', 'package.history'])" id="menu-packages" data-bs-parent="#my-sidebar-menu">
        <li class="menu-item">
            <a href="{{ route('package.transaction.index') }}" class="menu-link @menuActive(['package.transaction'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.transaction') }}</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{ route('package.history.index') }}" class="menu-link @menuActive(['package.history'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.history') }}</span>
            </a>
        </li>
    </ul>
</li>
<li class="menu-item">
    <a href="#" class="menu-link @menuCollapsed(['bonus'])" data-bs-toggle="collapse" data-bs-target="#menu-bonus">
        <span class="menu-icon fa fa-coins"></span>
        <span class="menu-text">{{ __('menu.bonus') }}</span>
    </a>
    <ul class="menu collapse @menuShow(['bonus'])" id="menu-bonus" data-bs-parent="#my-sidebar-menu">
        <li class="menu-item">
            <a href="{{ route('bonus.summary.index') }}" class="menu-link @menuActive(['bonus.summary'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.summary') }}</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{ route('bonus.sponsor.index') }}" class="menu-link @menuActive(['bonus.sponsor'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.bonus-sponsor') }}</span>
            </a>
        </li>
        @if (BONUS_EXTRA_ENABLED)
            <li class="menu-item">
                <a href="{{ route('bonus.extra.index') }}" class="menu-link @menuActive(['bonus.extra'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-extra') }}</span>
                </a>
            </li>
        @endif
        @if (BONUS_RO_ENABLED)
            <li class="menu-item">
                <a href="{{ route('bonus.ro.index') }}" class="menu-link @menuActive(['bonus.ro'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.ro') }}</span>
                </a>
            </li>
        @endif
        @if (BONUS_UPGRADE_ENABLED)
            <li class="menu-item">
                <a href="{{ route('bonus.upgrade.index') }}" class="menu-link @menuActive(['bonus.upgrade'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.upgrade') }}</span>
                </a>
            </li>
        @endif
    </ul>
</li>
<li class="menu-item">
    <a href="#" class="menu-link @menuCollapsed(['withdraw'])" data-bs-toggle="collapse" data-bs-target="#menu-withdraw">
        <span class="menu-icon fa fa-money-bills"></span>
        <span class="menu-text">{{ __('menu.withdraw') }}</span>
    </a>
    <ul class="menu collapse @menuShow(['withdraw'])" id="menu-withdraw" data-bs-parent="#my-sidebar-menu">
        <li class="menu-item">
            <a href="{{ route('withdraw.histories.index') }}" class="menu-link @menuActive(['withdraw.histories'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.history') }}</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{ route('withdraw.sponsor.index') }}" class="menu-link @menuActive(['withdraw.sponsor'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.bonus-sponsor') }}</span>
            </a>
        </li>
        @if (BONUS_EXTRA_ENABLED)
            <li class="menu-item">
                <a href="{{ route('withdraw.extra.index') }}" class="menu-link @menuActive(['withdraw.extra'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-extra') }}</span>
                </a>
            </li>
        @endif
        @if (BONUS_RO_ENABLED)
            <li class="menu-item">
                <a href="{{ route('withdraw.ro.index') }}" class="menu-link @menuActive(['withdraw.ro'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-ro') }}</span>
                </a>
            </li>
        @endif
        @if (BONUS_UPGRADE_ENABLED)
            <li class="menu-item">
                <a href="{{ route('withdraw.upgrade.index') }}" class="menu-link @menuActive(['withdraw.upgrade'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-upgrade') }}</span>
                </a>
            </li>
        @endif
        @if (REWARD_ENABLED)
            <li class="menu-item">
                <a href="{{ route('withdraw.reward.index') }}" class="menu-link @menuActive(['withdraw.reward'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-reward') }}</span>
                </a>
            </li>
        @endif
    </ul>
</li>
<li class="menu-item">
    <a href="#" class="menu-link @menuCollapsed(['setting.bank', 'setting.package', 'setting.bonus.sponsor.index', 'setting.bonus.level'])" data-bs-toggle="collapse"
        data-bs-target="#menu-settings">
        <span class="menu-icon fa fa-cogs"></span>
        <span class="menu-text">{{ __('menu.setting') }}</span>
    </a>
    <ul class="menu collapse @menuShow(['setting.bank', 'setting.package', 'setting.bonus.sponsor.index', 'setting.bonus.level'])" id="menu-settings" data-bs-parent="#my-sidebar-menu">
        <li class="menu-item">
            <a href="{{ route('setting.bank.index') }}" class="menu-link @menuActive('setting.bank')">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.bank') }}</span>
            </a>
        </li>
        <li class="menu-item">
            <a href="{{ route('setting.package.index') }}" class="menu-link @menuActive(['setting.package'])">
                <span class="menu-icon"></span>
                <span class="menu-text">{{ __('menu.package') }}</span>
            </a>
        </li>
        @if (!BONUS_SPONSOR_BY_PACKAGE)
            <li class="menu-item">
                <a href="{{ route('setting.bonus.sponsor.index') }}" class="menu-link @menuActive(['setting.bonus.sponsor.index'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-sponsor') }}</span>
                </a>
            </li>
        @endif
        @if (BONUS_LEVEL_ENABLED)
            <li class="menu-item">
                <a href="{{ route('setting.bonus.level.index') }}" class="menu-link @menuActive(['setting.bonus.level'])">
                    <span class="menu-icon"></span>
                    <span class="menu-text">{{ __('menu.bonus-level') }}</span>
                </a>
            </li>
        @endif
    </ul>
</li>
