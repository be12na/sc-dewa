<div style="padding:1rem; background-color:#dee2e6; font-size:0.85rem; line-height:1.5;">
    <div style="background-color:#f8f9fa; padding:1.5rem;">
        <div style="margin-bottom:1rem; text-align:center;">
            <img src="{{ asset(env('IMAGE_LARGE_LOGO')) }}?v={{ $fileV }}" style="width:120px;height:auto;">
        </div>
        <div style="text-align:center; font-size:1.25rem;font-weight:600;margin-bottom:1rem;padding-bottom:0.5rem;border-bottom:2px solid #212529">
            @yield('title')
        </div>
        @yield('content')
    </div>
</div>