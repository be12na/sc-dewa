<li>
    <div class="dropdown-header py-2 rounded-top border-bottom mb-1">
        <div class="fw-bold">{{ $authUser->name }}</div>
        @if ($authUser->is_member_user)
            <div class="d-flex flex-nowrap align-items-end justify-content-between small">
                <span class="text-muted me-2">{{ $authUser->username }}</span>
                @if ($authUser->package)
                    <span class="badge bg-success">{{ $authUser->package->name }}</span>
                @endif
            </div>
        @endif
    </div>
</li>

@if ($authUser->is_member_user)
    <li><a href="{{ route('profile.index') }}" class="dropdown-item @menuActive('profile.index')">Profile</a></li>
    @if (MEMBER_BANK)
        <li><a href="{{ route('bank.index') }}" class="dropdown-item @menuActive('bank')">@translate('menu.bank')</a></li>
    @endif
@endif

<li><a class="dropdown-item @menuActive('password.change')" href="{{ route('password.change') }}">Password</a></li>
<li>
    <hr class="dropdown-divider">
</li>
<li><a class="dropdown-item" href="{{ route('logout') }}">@translate('menu.logout')</a></li>