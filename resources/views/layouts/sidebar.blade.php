<div class="offcanvas offcanvas-lg offcanvas-start offcanvas-sidebar @if($authUser->is_member_user) bg-theme-1 @endif shadow" data-bs-scroll="true" tabindex="-1" id="offcanvasSidebar" aria-labelledby="offcanvasSidebarLabel">
    <div class="offcanvas-header border-bottom border-theme-3 bg-light py-2" style="min-height:52px;">
        <a href="{{ route('dashboard.index') }}" class="offcanvas-title" id="offcanvasSidebarLabel">
            <img src="{{ asset(env('IMAGE_LOGO')) }}?v={{ $fileV }}" alt="{{ config('app.name') }}" style="width: auto; height:36px;">
            <span class="text-theme-1 user-select-none">{{ config('app.name') }}</span>
        </a>
        <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
        <ul class="menu with-icon" id="my-sidebar-menu">
            @if ($authUser->isGroup('admin'))
                @include('layouts.sidebars.admin')
            @else
                @include('layouts.sidebars.member')
            @endif
        </ul>
    </div>
</div>