@extends('layouts.app')

@section('content')
<div id="app">
    @include('layouts.sidebar')
    <div class="page-content pb-4">
        <nav class="navbar navbar-expand sticky-top bg-light shadow-sm flex-wrap pb-0 pb-lg-2">
            <div class="container-fluid gx-md-5">
                <button class="sidebar-toggler d-lg-none" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasSidebar" aria-controls="offcanvasSidebar">
                    <i class="fa fa-bars"></i>
                </button>
                <a href="{{ route('home') }}" class="navbar-brand d-flex align-items-center justify-content-center text-center text-theme-1 user-select-none me-0 d-lg-none lh-1 fs-6">
                    <img src="{{ asset(env('IMAGE_LOGO')) }}" alt="{{ config('app.name') }}" class="me-1" style="width: auto; height:25px;">
                    <span>{{ config('app.name') }}</span>
                </a>

                @isset($pageHeading)
                    <ol class="breadcrumb breadcrumb-custom-divider mb-0 d-none d-lg-flex">
                        <li class="breadcrumb-item text-theme-1">
                            <img src="{{ asset(env('IMAGE_LOGO')) }}" alt="{{ config('app.name') }}" style="width: auto; height:16px;">
                        </li>
                        @if (is_array($pageHeading))
                            @php
                                $lastBreadcrumb = count($pageHeading) - 1;
                            @endphp
                            @foreach ($pageHeading as $breadIndex => $breadcrumb)
                                <li class="breadcrumb-item @if($breadIndex == $lastBreadcrumb) active @endif"  @if($breadIndex == $lastBreadcrumb) aria-current="page" @endif>{{ $breadcrumb }}</li>
                            @endforeach
                        @else
                            <li class="breadcrumb-item active" aria-current="page">{{ $pageHeading }}</li>
                        @endif
                    </ol>
                @endisset

                <ul class="navbar-nav ms-lg-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link nav-link-circle bg-light text-theme-3" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <i class="fa fa-circle-user"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end pt-0">
                            @include('layouts.navbar-menu')
                        </ul>
                    </li>
                </ul>
            </div>
            @isset($pageHeading)
                <div class="container-fluid gx-md-5 d-lg-none py-2  small w-100 mt-2 border-top">
                    <ol class="breadcrumb breadcrumb-custom-divider mb-0">
                        <li class="breadcrumb-item text-theme-1">
                            <img src="{{ asset(env('IMAGE_LOGO')) }}" alt="{{ config('app.name') }}" style="width: auto; height:16px;">
                        </li>
                        @if (is_array($pageHeading))
                            @php
                                $lastBreadcrumb = count($pageHeading) - 1;
                            @endphp
                            @foreach ($pageHeading as $breadIndex => $breadcrumb)
                                <li class="breadcrumb-item @if($breadIndex == $lastBreadcrumb) active @endif"  @if($breadIndex == $lastBreadcrumb) aria-current="page" @endif>{{ $breadcrumb }}</li>
                            @endforeach
                        @else
                            <li class="breadcrumb-item active" aria-current="page">{{ $pageHeading }}</li>
                        @endif
                    </ol>
                </div>
            @endisset
        </nav>
        <div class="container-fluid gx-md-5 py-3 py-md-4">
            @yield('pageContent')
        </div>
    </div>
</div>
@stop

@push('pageCSS')
    @if ($authUser->is_member_user)
        <style>
            .menu {
                --bs-menu-color: var(--bs-gray-200);
                --bs-menu-hover-color: var(--bs-gray-300);
                --bs-menu-hover-bg: var(--app-theme-3);
                --bs-menu-active-color: var(--bs-gray-100);
                --bs-menu-active-bg: var(--app-theme-3);
                --bs-menu-font-size: 14px;
                --bs-menu-sub-bg: rgba(var(--app-theme-2-rgb),0.125);
            }
        </style>
    @endif
@endpush

@section('appJS')
    <script src="{{ asset('assets/js/app.js') }}?v={{ $fileV }}"></script>
@endsection