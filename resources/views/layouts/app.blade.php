<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @php
            $windowTitle = isset($windowTitle) ? $windowTitle . ' | ' : '';
        @endphp
        {{ $windowTitle . config('app.name', 'Website') }}
    </title>
    <link rel="shortcut icon" href="{{ asset(env('IMAGE_WINDOW_ICON')) }}" type="{{ env('IMAGE_WINDOW_ICON_TYPE', 'image/x-icon') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/font/bootstrap-icons.css') }}?v={{ $fileV }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/fontawesome/css/all.min.css') }}?v={{ $fileV }}">
    <link rel="stylesheet" href="{{ asset('assets/css/app.min.css') }}?v={{ $fileV }}">
    {{-- vendor css --}}
    @yield('vendorCSS')
    {{-- custom css --}}
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}?v={{ $fileV }}">
    {{-- theme css --}}
    <link rel="stylesheet" href="{{ asset('assets/css/theme-ds.css') }}?v={{ $fileV }}">
    @auth
        <style>
            body {
                background-color: var(--bs-light);
            }
            body.sidebar {
                --bs-sidebar-width: 240px;
            }
            .offcanvas-sidebar {
                --bs-offcanvas-padding-x: 0.5rem;
            }
            @media (max-width: 575.99px) {
                .offcanvas-sidebar {
                    min-width: 100%;
                }
            }
        </style>
    @endauth
    {{-- page custom css --}}
    @stack('pageCSS')
</head>
<body @auth class="sidebar" @endauth>
    @yield('content')
    @stack('topContent')

    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    {{-- vendor js --}}
    @yield('vendorJS')
    @yield('livewireScript')
    @yield('appJS')
    {{-- custom page js --}}
    @stack('pageJS')
</body >
</html>
