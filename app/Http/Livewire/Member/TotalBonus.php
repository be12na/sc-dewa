<?php

namespace App\Http\Livewire\Member;

use Livewire\Component;

class TotalBonus extends Component
{
    public $bonusCode;
    public $status;
    public $cssClass;
    public $poll;

    public function mount($bonusCode, $status, $cssClass, $poll)
    {
        $this->bonusCode = $bonusCode;
        $this->status = $status;
        $this->cssClass = $cssClass;
        $this->poll = $poll;
    }

    public function render()
    {
        return view('livewire.member.total-bonus', [
            'bonusCode' => $this->bonusCode,
            'status' => $this->status,
            'cssClass' => $this->cssClass,
            'poll' => $this->poll,
        ]);
    }
}
