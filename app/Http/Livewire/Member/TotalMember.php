<?php

namespace App\Http\Livewire\Member;

use App\Models\Package;
use Livewire\Component;

class TotalMember extends Component
{
    public $scope;
    public $status;
    public $cssClass;
    public $poll;

    public function mount($scope, $status, $cssClass, $poll)
    {
        $this->scope = $scope;
        $this->status = $status;
        $this->cssClass = $cssClass;
        $this->poll = $poll;
    }

    public function render()
    {
        return view('livewire.member.total-member', [
            'scope' => $this->scope,
            'status' => $this->status,
            'cssClass' => $this->cssClass,
            'poll' => $this->poll,
            'packages' => ($this->scope === 'package') ? Package::query()->orderBy('id')->get() : collect()
        ]);
    }
}
