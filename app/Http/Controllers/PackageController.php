<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Admin\AdminTransactionPackage;
use App\Http\Controllers\Traits\Member\MemberTransactionPackage;
use App\Models\Package;
use App\Models\TransPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\HtmlString;

class PackageController extends Controller
{
    use AdminTransactionPackage, MemberTransactionPackage;

    public function index(Request $request)
    {
        $packages = Package::query()->byActive()->with('pinType')
            ->orderBy('id')
            ->get();

        return view('admin.settings.package.index', [
            'packages' => $packages,
        ]);
    }

    public function edit(Request $request)
    {
        return view('admin.settings.package.form', [
            'package' => $request->package,
        ]);
    }

    public function update(Request $request)
    {
        $package = $request->package;
        $values = $request->except(['_token']);

        $rules = [
            'package_name' => ['required', 'string', 'max:30', "unique:packages,name,{$package->id},id"],
            'description' => ['nullable', 'string', 'max:250'],
        ];

        if (PIN_ENABLED && isTrueConfig(PACKAGE_USE_PIN, true)) {
            $rules['pin_count'] = ['required', 'integer', 'min:1'];
        } else {
            $rules['price'] = ['required', 'integer', 'min:0'];
            $values['pin_count'] = 0;
        }

        $values['name'] = $values['package_name'];

        if (BONUS_SPONSOR_BY_PACKAGE) {
            $usePercent = isTrueConfig(BONUS_SPONSOR_TYPE, 'percent');
            $bonusSponsorType = $usePercent ? 'numeric' : 'integer';
            $rules['bonus_sponsor'] = ['required', $bonusSponsorType, 'min:0'];

            if ($usePercent) {
                $rules['bonus_sponsor'][] = 'max:50';
            }
        } else {
            $values['bonus_sponsor'] = 0;
        }

        if (BONUS_EXTRA_ENABLED) {
            $usePercent = isTrueConfig(BONUS_EXTRA_TYPE, 'percent');
            $bonusExtraType = $usePercent ? 'numeric' : 'integer';
            $rules['bonus_extra'] = ['required', $bonusExtraType, 'min:0'];

            if ($usePercent) {
                $rules['bonus_extra'][] = 'max:50';
            }
        } else {
            $values['bonus_extra'] = 0;
        }

        if (BONUS_UPGRADE_ENABLED) {
            $usePercent = isTrueConfig(BONUS_UPGRADE_TYPE, 'percent');
            $bonusUpgradeType = $usePercent ? 'numeric' : 'integer';
            $rules['bonus_upgrade'] = ['required', $bonusUpgradeType, 'min:0'];

            if ($usePercent) {
                $rules['bonus_upgrade'][] = 'max:50';
            }
        } else {
            $values['bonus_upgrade'] = 0;
        }

        if (BONUS_RO_ENABLED) {
            $usePercent = isTrueConfig(BONUS_RO_TYPE, 'percent');
            $bonusROType = $usePercent ? 'numeric' : 'integer';
            $rules['bonus_ro'] = ['required', $bonusROType, 'min:0'];

            if ($usePercent) {
                $rules['bonus_ro'][] = 'max:50';
            }
        } else {
            $values['bonus_ro'] = 0;
        }

        if (BONUS_PACKAGE_ENABLED) {
            $values['bonus_package'] = isset($values['get_bonus_package']);
            $rules['package_left'] = ['required', 'integer', 'min:1'];
            $rules['package_right'] = ['required', 'integer', 'min:1'];
            $rules['package_bonus'] = [$values['bonus_package'] ? 'required' : 'nullable', 'string', 'max:250'];
        } else {
            $values['bonus_package'] = false;
        }

        if (BONUS_PAIR_ENABLED && isTrueConfig(BONUS_PAIR_TYPE, 'point')) {
            $rules['pair_point'] = ['required', 'integer', 'min:1'];
        } else {
            $values['pair_point'] = 1;
        }

        $values['get_reward'] = false;
        $values['give_reward'] = false;

        if (REWARD_ENABLED) {
            $values['get_reward'] = isset($values['check_get_reward']);
            $values['give_reward'] = isset($values['check_give_reward']);

            if (!isTrueConfig(REWARD_BY_TYPE, 'point')) {
                if (!$values['give_reward']) {
                    $values['reward_point'] = 1;
                }
            } else {
                if ($values['give_reward']) {
                    $rules['reward_point'] = ['required', 'integer', 'min:1'];
                }
            }
        }

        $validator = Validator::make($values, $rules);

        if ($validator->fails()) {
            return response(ajaxValidationError($validator), 400);
        }

        try {
            $package->update($values);

            return $this->successAjaxFormResponse(route('setting.package.index'), __('message.package.updated'));
        } catch (\Exception $e) {
            return $this->errorAjaxFormResponse($e);
        }
    }

    public function detail(Request $request)
    {
        return view('admin.settings.package.detail', [
            'package' => $request->package,
        ]);
    }

    // transaction
    public function transactions(Request $request)
    {
        if ($request->route()->getName() != 'package.transaction.dataTable') {
            return view('transactions.package.trans-list');
        }

        $user = $request->user();
        $isUserAdmin = $this->isAdmin($user);

        $transStatus = TRANS_ORDER;

        $query = DB::table('trans_packages')
            ->join('users', 'users.id', '=', 'trans_packages.buyer_id')
            ->join('packages', 'packages.id', '=', 'trans_packages.package_id')
            ->selectRaw('trans_packages.id, trans_packages.code, trans_packages.seller_id, trans_packages.buyer_id, trans_packages.package_id, trans_packages.pin_count, trans_packages.trans_type, trans_packages.package_price, trans_packages.unique_digit, trans_packages.status, trans_packages.status_at, trans_packages.created_at, users.username, users.name as buyer_name, users.phone, packages.name as package_name')
            ->whereRaw("trans_packages.status={$transStatus}");

        if (!$isUserAdmin) {
            $userId = $user->id;
            $query = $query->whereRaw("trans_packages.buyer_id={$userId}");
        }

        $query = $query->toSql();

        $rows = datatables()->query(DB::table(DB::raw("({$query}) as trans")))
            ->editColumn('code', function ($row) use ($isUserAdmin) {
                if ($isUserAdmin) {
                    $url = route('package.transaction.viewConfirm', ['transPackage' => $row->id]);
                } else {
                    $url = route('package.detail', ['transPackage' => $row->id]);
                }

                $link = '<a href="#" data-bs-toggle="modal" data-bs-target="#my-modal" data-modal-url="%s">%s</a>';

                return new HtmlString(sprintf($link, $url, $row->code));
            })
            ->editColumn('trans_type', function ($row) {
                $text = ($row->trans_type == TRANS_PKG_UPGRADE)
                    ? 'Upgrade'
                    : (($row->trans_type == TRANS_PKG_RO)
                        ? 'Repeat Order' : 'Aktifasi'
                    );

                return $text;
            })
            ->editColumn('created_at', function ($row) {
                return translateDatetime($row->created_at, __('format.datetime.medium'));
            })
            ->addColumn('action', function ($row) use ($user, $isUserAdmin) {
                if ($row->buyer_id != $user->id) {
                    $transType = __('label.confirm');

                    $url = route('package.transaction.viewConfirm', ['transPackage' => $row->id]);

                    $action = "<button type=\"button\" class=\"btn btn-sm btn-secondary\" data-bs-toggle=\"modal\" data-bs-target=\"#my-modal\" data-modal-url=\"{$url}\">{$transType}</button>";
                } else {
                    $label = __('label.confirming');
                    $action = "<span class=\"py-1 px-2 small bg-info text-dark\">{$label}</span>";
                }

                return new HtmlString($action);
            });

        if ($isUserAdmin) {
            $rows = $rows->editColumn('buyer_name', function ($row) {
                $format = '<div>%s</div><div class="small text-muted">%s<div><div class="small text-muted">%s<div></div>';

                return new HtmlString(sprintf($format, $row->buyer_name, $row->username, $row->phone));
            });
        }

        return $rows
            ->escapeColumns()
            ->toJson();
    }

    public function transConfirmation(Request $request)
    {
        if (!$request->isMethod('POST')) {
            $transPackage = $request->transPackage;
            // $user = $request->user();
            // $isAdmin = $this->isAdmin($user);
            // if (($isAdmin && ($transPackage->seller_id != 0)) || (!$isAdmin && ($transPackage->seller_id != $user->id))) {
            //     return ajaxResponse(__('message.not-found'), 404);
            // }
            if ($transPackage->seller_id != 0) {
                return ajaxResponse(__('message.not-found'), 404);
            }

            return view('transactions.package.trans-confirm', [
                'transPackage' => $request->transPackage,
            ]);
        }

        // return $this->isAdmin($request)
        //     ? $this->adminConfirmTransaction($request)
        //     : $this->memberConfirmTransaction($request);
        return $this->adminConfirmTransaction($request);
    }

    public function transHistories(Request $request)
    {
        if ($request->route()->getName() != 'package.history.dataTable') {
            return view('transactions.package.trans-histories');
        }

        $user = $request->user();
        $isUserAdmin = $this->isAdmin($user);
        // $eloquent = $user->transQuery(as: $isUserAdmin ? 'seller' : 'buyer')->with('package');

        // if ($isUserAdmin) {
        //     $eloquent = $eloquent->with('buyer');
        // }

        // $rows = datatables()->eloquent($eloquent)

        $query = DB::table('trans_packages')
            ->join('users', 'users.id', '=', 'trans_packages.buyer_id')
            ->join('packages', 'packages.id', '=', 'trans_packages.package_id')
            ->selectRaw('trans_packages.id, trans_packages.code, trans_packages.seller_id, trans_packages.buyer_id, trans_packages.package_id, trans_packages.pin_count, trans_packages.trans_type, trans_packages.package_price, trans_packages.unique_digit, trans_packages.status, trans_packages.status_at, trans_packages.created_at, users.username, users.name as buyer_name, users.phone, packages.name as package_name');

        if (!$isUserAdmin) {
            $userId = $user->id;
            $query = $query->whereRaw("trans_packages.buyer_id={$userId}");
        }

        $query = $query->toSql();

        $rows = datatables()->query(DB::table(DB::raw("({$query}) as trans")))
            ->editColumn('code', function ($row) {
                $link = '<a href="#" data-bs-toggle="modal" data-bs-target="#my-modal" data-modal-url="%s">%s</a>';

                return new HtmlString(sprintf($link, route('package.detail', ['transPackage' => $row->id]), $row->code));
            })
            ->editColumn('trans_type', function ($row) {
                $text = ($row->trans_type == TRANS_PKG_UPGRADE)
                    ? 'Upgrade'
                    : (($row->trans_type == TRANS_PKG_RO)
                        ? 'Repeat Order' : 'Aktifasi'
                    );

                return $text;
            })
            ->editColumn('created_at', function ($row) {
                return translateDatetime($row->created_at, __('format.datetime.medium'));
            })
            ->editColumn('status', function ($row) use ($isUserAdmin) {
                $label = '<span class="text-%s">%s</span>';

                return new HtmlString(sprintf($label, TransPackage::statusTextColor($row->status), $isUserAdmin ? TransPackage::statusTextAdmin($row->status) : TransPackage::statusTextMember($row->status)));
            });

        if ($isUserAdmin) {
            $rows = $rows->editColumn('buyer_name', function ($row) {
                $format = '<div>%s</div><div class="small text-muted">%s<div><div class="small text-muted">%s<div></div>';

                return new HtmlString(sprintf($format, $row->buyer_name, $row->username, $row->phone));
            });
        }

        return $rows
            ->escapeColumns()
            ->toJson();
    }

    public function transDetail(Request $request)
    {
        return view('transactions.package.detail', [
            'transPackage' => $request->transPackage,
        ]);
    }
    // transaction:end
}
