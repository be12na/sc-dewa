<?php

namespace App\Http\Controllers\Traits\Member;

use App\Models\User;
use Illuminate\Http\Request;

trait MemberStructure
{
    protected function binaryTree(Request $request, bool $placement)
    {
        $owner = $request->user();
        $fromId = $request->get('user_id', $owner->id);
        $from = $owner;

        if ($fromId != $owner->id) {
            $listMemberId = $owner->structure->getDescendants()->pluck('user_id')->toArray();
            $from = User::byId($fromId)->first();

            if (!in_array($fromId, $listMemberId) || empty($from)) {
                return redirect()->route($placement ? 'member.placement.index' : 'network.tree.index')
                    ->with('message', __('message.member.not-found'))
                    ->with('messageClass', 'danger');
            }
        };

        return view($placement ? 'member.member.placement' : 'member.network.tree', [
            'structure' => $from->structure,
            'maxLevel' => 2,
            'topMember' => $from,
            'owner' => $owner,
        ]);
    }

    public function treeNodeDetail(Request $request)
    {
        $user = $request->user();
        $opt = $request->get('opt');
        $isPlacement = ($opt === 'placement');
        $indexRouteName = $isPlacement ? 'member.placement.index' : 'network.tree.index';
        $indexRoute = route($indexRouteName);
        $member = $request->member;
        $isOpen = ($request->get('open', 0) == 1);
        $isOpenMe = ($request->get('me', 0) == 1);
        $depth = 0;
        if ($user->id != $member->id) {
            $depth = $user->structure->getDescendants()->where('user_id', '=', $member->id)->first()->depth;
        }

        $isOpenMe = ($isOpenMe & ($depth <= 2) & !$isOpen);

        return view('member.network.detail', [
            'isPlacement' => $isPlacement,
            'indexRoute' => $indexRoute,
            'member' => $member,
            'isOpen' => $isOpen,
            'isOpenMe' => $isOpenMe,
            'depth' => $depth,
        ]);
    }
}
