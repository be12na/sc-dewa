<?php

namespace App\Http\Controllers\Traits\Member;

use App\Models\Package;
use App\Models\TransPackage;
use App\Notifications\TransPackageBuyer;
use App\Notifications\TransPackageSeller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait MemberTransactionPackage
{
    private function getMessageOrder(bool $isRO): string
    {
        $formatMessage = '<div>%s</div><div>%s</div><div>%s</div>';
        $message = sprintf($formatMessage, $isRO ? __('message.package.reordered') : __('message.package.ordered'), __('text.trans-detail-payment'), __('text.confirm-whatsapp', ['whatsapp' => env('CONTACT_CENTER_PHONE', '')]));

        return $message;
    }

    private function orderPackage(Request $request, int $orderType)
    {
        $user = $request->user();
        $package = ($orderType == TRANS_PKG_RO) ? $user->package : $request->package;

        if (!$request->isMethod('POST')) {
            $routeNames = explode('.', $request->route()->getName());
            $orderMethod = $routeNames[2];

            if ($orderMethod != 'selected') {
                if ($orderType == TRANS_PKG_RO) {
                    $packages = collect([$package]);
                } else {
                    $packages = Package::query()
                        ->byActive()
                        ->orderBy('id');

                    if ($orderType == TRANS_PKG_UPGRADE) {
                        $packages = $packages->byUpgrade($user->my_package_id);
                    } else {
                        $packages = $packages->byStart();
                    }

                    $packages = $packages->get();
                }

                return view('transactions.package.select', [
                    'packages' => $packages,
                    'type' => $orderType,
                ]);
            }

            $postRouteName = ($orderType == TRANS_PKG_RO)
                ? 'package.ro.confirmed'
                : (($orderType == TRANS_PKG_UPGRADE)
                    ? 'package.upgrade.confirmed'
                    : 'package.activate.confirmed');

            return view('transactions.package.select-confirm', [
                'package' => $package,
                'postUrl' => route($postRouteName, ['package' => $package->id]),
                'type' => $orderType,
            ]);
        }

        $values = [
            'code' => TransPackage::makeCode($orderType),
            'seller_id' => 0,
            'buyer_id' => $user->id,
            'package_id' => $package->id,
            'pin_count' => $package->pin_count,
            'trans_type' => $orderType,
            'package_price' => $package->actual_price,
            'unique_digit' => mt_rand(103, 192),
            'status' => TRANS_ORDER,
        ];

        DB::beginTransaction();
        try {
            $transPackage = TransPackage::create($values);

            // email dan whatsapp member
            $user->notify(new TransPackageBuyer($transPackage, 'database', 'mail'));
            $user->notify(new TransPackageBuyer($transPackage, 'database', 'whatsapp'));
            // email penjual (admin / sponsor)
            $user->notify(new TransPackageSeller($transPackage, 'database', 'mail'));

            DB::commit();

            return $this->successAjaxFormResponse(route('package.transaction.index'), $this->getMessageOrder($orderType == TRANS_PKG_RO));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }
    }

    public function memberPackageActivate(Request $request)
    {
        return $this->orderPackage($request, TRANS_PKG_START);
    }

    public function memberPackageUpgrade(Request $request)
    {
        return $this->orderPackage($request, TRANS_PKG_UPGRADE);
    }

    public function memberPackageRepeatOrder(Request $request)
    {
        return $this->orderPackage($request, TRANS_PKG_RO);
    }
}
