<?php

namespace App\Http\Controllers\Traits\Admin;

use App\Models\BonusSponsor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

trait SettingBonusSponsor
{
    public function indexSettingBonusSponsor(Request $request)
    {
        return view('admin.settings.bonus-sponsor', [
            'bonus' => BonusSponsor::bonusAmount(),
        ]);
    }

    public function updateSettingBonusSponsor(Request $request)
    {
        $values = $request->except('_token');

        $validator = Validator::make($values, [
            'bonus_sponsor' => ['required', BONUS_SPONSOR_USE_PERCENT ? 'numeric' : 'integer', 'min:0'],
        ]);

        if ($validator->fails()) {
            return response(ajaxValidationError($validator), 400);
        }

        $values['created_by'] = $request->user()->id;

        DB::beginTransaction();
        try {
            BonusSponsor::updateBonus($values);

            DB::commit();

            return $this->successAjaxFormResponse(route('setting.bonus.sponsor.index'), __('message.bonus.sponsor.updated'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }
    }
}
