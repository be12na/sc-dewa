<?php

namespace App\Http\Controllers\Traits\Admin;

use App\Mail\TransConfirmPackageMail;
use App\Models\UserBonus;
use App\Notifications\TransPackageConfirmed;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

trait AdminTransactionPackage
{
    public function adminConfirmTransaction(Request $request)
    {
        $transPackage = $request->transPackage;
        $user = $transPackage->buyer;
        $now = Carbon::now();

        $action = $request->get('action', 'confirm');
        $isReject = ($action == 'reject');

        $isRO = $transPackage->is_repeat_order;
        $isUpgrade = $transPackage->is_upgrade;

        if (!$isReject) {
            if (!$isRO) {
                if ($user->my_package_id >= $transPackage->package_id) {
                    return ajaxResponse(__('message.package.admin-top-level-reached'), 400);
                }

                $userValues = [
                    'package_id' => $transPackage->package_id,
                ];

                if ($user->user_inactive || $user->user_banned) {
                    return ajaxResponse($user->user_inactive ? __('message.member.inactive') : __('message.member.banned'), 400);
                } else {
                    if ($user->is_user_register) {
                        $userValues['user_status'] = USER_STATUS_ACTIVATED;
                        $userValues['activated_at'] = $now;
                    }
                }
            }
        }

        $message = $isReject ? __('message.package.rejected')  : __('message.package.confirmed');

        DB::beginTransaction();
        try {
            $transPackage->update([
                'status' => $isReject ? TRANS_REJECTED : TRANS_CONFIRMED,
                'status_at' => $now,
            ]);

            if (!$isReject) {
                if ($isRO) {
                    UserBonus::createBonusRO($user, $now);
                } else {
                    $user->update($userValues);
                    if ($isUpgrade) {
                        UserBonus::createBonusUpgrade($user, $now);
                    } else {
                        UserBonus::createBonusSponsor($user, $now);
                        UserBonus::createBonusExtra($user, $now);
                    }
                }

                $user->notify(new TransPackageConfirmed($transPackage, 'database', 'mail'));
                $user->notify(new TransPackageConfirmed($transPackage, 'database', 'whatsapp'));
            }

            DB::commit();

            return $this->successAjaxFormResponse(route('package.transaction.index'), $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }
    }
}
