<?php

namespace App\Http\Controllers\Traits\Admin;

use App\Models\TransPackage;
use App\Models\User;
use Illuminate\Http\Request;

trait DashboardAdmin
{
    protected function getItemDashboardAdmin(Request $request)
    {
        $key = $request->get('key');
        $result = '';
        $status = 404;

        if (!empty($key)) {
            if ($key == 'member') {
                $fn = $request->get('function');
                $status = in_array($fn, ['total', 'active', 'omzet']) ? 200 : 404;
                $jml = null;

                if ($status == 200) {
                    if ($fn == 'total') {
                        $jml = User::query()->byMemberUser()->count();
                    } elseif ($fn == 'active') {
                        $jml = User::query()->byMemberUser()->byUserStatus(USER_STATUS_ACTIVATED)->count();
                    } elseif ($fn == 'omzet') {
                        $jml = TransPackage::query()->byTransStatus(TRANS_CONFIRMED)->with('package')->get()->sum('package.price');
                    }
                }

                $result = is_null($jml) ? '' : formatNumber($jml);
            }
        }

        return response($result, $status);
    }
}
