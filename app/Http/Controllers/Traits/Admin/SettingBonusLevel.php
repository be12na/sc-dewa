<?php

namespace App\Http\Controllers\Traits\Admin;

use App\Models\BonusLevel;
use App\Models\Package;
use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Validation\Rules\Unique;

trait SettingBonusLevel
{
    private function getActivePackagesBonusLevel(): Collection
    {
        return BONUS_LEVEL_PER_PACKAGE ? Package::activePackages(PLAN_A) : collect();
    }

    public function indexSettingBonusLevel(Request $request)
    {
        return view('admin.settings.bonus-level.index', [
            'packages' => $this->getActivePackagesBonusLevel(),
            'packageId' => BONUS_LEVEL_PER_PACKAGE ? session('bonus_level.package', 0) : 0,
        ]);
    }

    public function dataTableSettingBonusLevel(Request $request)
    {
        session(['bonus_level.package' => $packageId = $request->get('package')]);

        $dtTables = datatables()->eloquent(BonusLevel::getQueryByPackage($packageId)->orderBy('level'))
            ->editColumn('bonus', function ($row) {
                $isPercent = isTrueConfig(BONUS_LEVEL_TYPE, 'percent');
                return formatNumber($row->bonus, $isPercent ? PERCENT_DECIMAL : null, $isPercent);
            });

        if (hasRole(['admin.super'])) {
            $dtTables = $dtTables->addColumn('action', function ($row) {
                return view('partials.buttons.open-modal', [
                    'isAjaxModal' => true,
                    'modalUrl' => route('setting.bonus.level.edit', ['bonusLevel' => $row->id]),
                    'btnClass' => 'btn-sm btn-warning',
                    'modalTarget' => '#my-modal',
                    'btnText' => __('button.edit'),
                ])->render();
            });
        }

        return $dtTables
            ->escapeColumns()
            ->toJson();
    }

    private function validatorBonusLevel(array &$values, Request $request, BonusLevel $bonusLevel = null): ValidatorContract
    {
        $values = $request->except('_token');
        $values['bonus'] = $values['bonus_level'];

        $uniqueLevel = new Unique('bonus_levels', 'level');
        $uniqueLevel = $uniqueLevel->whereNull('deleted_at');

        if (!is_null($bonusLevel)) {
            $uniqueLevel = $uniqueLevel->ignore($bonusLevel->id, 'id');
        }

        $rules = [];

        if (BONUS_LEVEL_PER_PACKAGE) {
            $uniqueLevel = $uniqueLevel->where('package_id', $values['package_id']);

            $existPackage = new Exists('packages', 'id');
            $existPackage = $existPackage->where('is_active', true);

            $rules['package_id'] = ['required', $existPackage];
        } else {
            $values['package_id'] = 0;
        }

        $rules['level'] = ['required', 'integer', 'min:1', $uniqueLevel];
        $rules['bonus_level'] = ['required', isTrueConfig(BONUS_LEVEL_TYPE, 'percent') ? 'numeric' : 'integer', 'min:0'];

        return Validator::make($values, $rules);
    }

    public function addSettingBonusLevel(Request $request)
    {
        return view('admin.settings.bonus-level.form', [
            'packages' => $this->getActivePackagesBonusLevel(),
        ]);
    }

    public function storeSettingBonusLevel(Request $request)
    {
        $values = [];
        $validator = $this->validatorBonusLevel($values, $request);

        if ($validator->fails()) {
            return response(ajaxValidationError($validator), 400);
        }

        $values['created_by'] = $request->user()->id;

        DB::beginTransaction();
        try {
            BonusLevel::setBonusLevel($values);

            DB::commit();

            session(['bonus_level.package' => $values['package_id']]);

            return $this->successAjaxFormResponse(route('setting.bonus.level.index'), __('message.bonus.level.added'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }
    }

    public function editSettingBonusLevel(Request $request)
    {
        return view('admin.settings.bonus-level.form', [
            'bonusLevel' => $request->bonusLevel,
            'packages' => $this->getActivePackagesBonusLevel(),
        ]);
    }

    public function updateSettingBonusLevel(Request $request)
    {
        $values = [];
        $bonusLevel = $request->bonusLevel;
        $validator = $this->validatorBonusLevel($values, $request, $bonusLevel);

        if ($validator->fails()) {
            return response(ajaxValidationError($validator), 400);
        }

        $values['created_by'] = $request->user()->id;

        DB::beginTransaction();
        try {
            BonusLevel::setBonusLevel($values, $bonusLevel);

            DB::commit();

            session(['bonus_level.package' => $values['package_id']]);

            return $this->successAjaxFormResponse(route('setting.bonus.level.index'), __('message.bonus.level.updated'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }

        // $values = $request->except('_token');

        // $validator = Validator::make($values, [
        //     'bonus_sponsor' => ['required', BONUS_SPONSOR_USE_PERCENT ? 'numeric' : 'integer', 'min:0'],
        // ]);

        // if ($validator->fails()) {
        //     return response(ajaxValidationError($validator), 400);
        // }

        // DB::beginTransaction();
        // try {

        //     DB::commit();

        //     return $this->successAjaxFormResponse(route('setting.bonus.sponsor.index'), __('message.bonus.sponsor.updated'));
        // } catch (\Exception $e) {
        //     DB::rollBack();

        //     return $this->errorAjaxFormResponse($e);
        // }
    }
}
