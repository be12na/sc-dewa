<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Admin\DashboardAdmin;
use App\Http\Controllers\Traits\Member\DashboardMember;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    use DashboardAdmin, DashboardMember;

    public function index(Request $request)
    {
        return $this->isAdmin($request) ? view('admin.dashboard') : view('member.dashboard');
    }

    public function dashboardItem(Request $request)
    {
        if ($this->isAdmin($request)) {
            return $this->getItemDashboardAdmin($request);
        } else {
            $status = 200;
            $result = '';
            return response($result, $status);
        }
    }
}
