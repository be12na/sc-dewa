<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function isAdmin(Request|User $requestUser)
    {
        $user = ($requestUser instanceof Request) ? $requestUser->user() : $requestUser;

        return $user->isGroup('admin');
    }

    protected function successAjaxFormResponse(string $redirectUrl, string $message = null)
    {
        if (!empty($message)) {
            session([
                'message' => $message,
                'messageClass' => 'success'
            ]);
        }

        return ajaxResponse($redirectUrl, 200);
    }

    protected function errorAjaxFormResponse(\Exception $exception)
    {
        return ajaxResponse(errorMessageContent(isLive() ? MESSAGE_500 : $exception->getMessage(), 'danger'), 500);
    }

    protected function successPageFormResponse(string $routeName, string $message, array $routeParam = null)
    {
        return pageMessageResponse($message, $routeName, $routeParam, 'success');
    }

    protected function errorPageFormResponse(\Exception $exception)
    {
        return redirect()->back()->withInput()
            ->with('message', isLive() ? MESSAGE_500 : $exception->getMessage())
            ->with('messageClass', 'danger');
    }
}
