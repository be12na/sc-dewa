<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Admin\SettingBonusLevel;
use App\Http\Controllers\Traits\Admin\SettingBonusSponsor;
use App\Models\UserBonus;
use App\Models\UserReward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;

class BonusController extends Controller
{
    use SettingBonusSponsor, SettingBonusLevel;

    // bonus sponsor
    public function indexBonusSponsor(Request $request)
    {
        return view('bonuses.bonus-sponsor');
    }

    public function dataBonusSponsor(Request $request)
    {
        $user = $request->user();
        $type = BONUS_SPONSOR;

        $query = DB::table('user_bonuses')
            ->join(DB::raw('users as user_owner'), 'user_owner.id', '=', 'user_bonuses.user_id')
            ->leftJoin(DB::raw('users as user_from'), 'user_from.id', '=', 'user_bonuses.from_user_id')
            ->selectRaw('user_bonuses.user_id, user_bonuses.bonus_date, user_bonuses.bonus_amount, user_bonuses.from_user_id, user_owner.name as owner_name, user_owner.username as owner_username, user_from.name as from_name, user_from.username as from_username')
            ->whereRaw("user_bonuses.bonus_type = {$type}");

        if ($user->is_member_user) {
            $userId = $user->id;
            $query = $query->whereRaw("user_bonuses.user_id = {$userId}");
        }

        $query = $query->toSql();

        return datatables()->query(DB::table(DB::raw("({$query}) as bonus_sponsor")))
            ->editColumn('bonus_date', function ($row) {
                return translateDatetime($row->bonus_date, __('format.date.medium'));
            })
            ->editColumn('owner_name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->owner_name, $row->owner_username));
            })
            ->editColumn('from_name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->from_name, $row->from_username));
            })
            ->editColumn('bonus_amount', function ($row) {
                return formatNumber($row->bonus_amount, 0, true);
            })
            ->escapeColumns()
            ->toJson();
    }
    // bonus sponsor:end

    // bonus extra
    public function indexBonusExtra(Request $request)
    {
        return view('bonuses.bonus-extra');
    }

    public function dataBonusExtra(Request $request)
    {
        $user = $request->user();
        $type = BONUS_EXTRA;

        $query = DB::table('user_bonuses')
            ->join(DB::raw('users as user_owner'), 'user_owner.id', '=', 'user_bonuses.user_id')
            ->leftJoin(DB::raw('users as user_from'), 'user_from.id', '=', 'user_bonuses.from_user_id')
            ->selectRaw('user_bonuses.user_id, user_bonuses.bonus_date, user_bonuses.bonus_amount, user_bonuses.from_user_id, user_owner.name as owner_name, user_owner.username as owner_username, user_from.name as from_name, user_from.username as from_username')
            ->whereRaw("user_bonuses.bonus_type = {$type}");

        if ($user->is_member_user) {
            $userId = $user->id;
            $query = $query->whereRaw("user_bonuses.user_id = {$userId}");
        }

        $query = $query->toSql();

        return datatables()->query(DB::table(DB::raw("({$query}) as bonus_extra")))
            ->editColumn('bonus_date', function ($row) {
                return translateDatetime($row->bonus_date, __('format.date.medium'));
            })
            ->editColumn('owner_name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->owner_name, $row->owner_username));
            })
            ->editColumn('from_name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->from_name, $row->from_username));
            })
            ->editColumn('bonus_amount', function ($row) {
                return formatNumber($row->bonus_amount, 0, true);
            })
            ->escapeColumns()
            ->toJson();
    }
    // bonus extra:end

    // bonus ro
    public function indexBonusRO(Request $request)
    {
        return view('bonuses.bonus-ro');
    }

    public function dataBonusRO(Request $request)
    {
        $user = $request->user();
        $type = BONUS_RO;

        $query = DB::table('user_bonuses')
            ->join(DB::raw('users as user_owner'), 'user_owner.id', '=', 'user_bonuses.user_id')
            ->leftJoin(DB::raw('users as user_from'), 'user_from.id', '=', 'user_bonuses.from_user_id')
            ->selectRaw('user_bonuses.user_id, user_bonuses.bonus_date, user_bonuses.bonus_amount, user_bonuses.from_user_id, user_owner.name as owner_name, user_owner.username as owner_username, user_from.name as from_name, user_from.username as from_username')
            ->whereRaw("user_bonuses.bonus_type = {$type}");

        if ($user->is_member_user) {
            $userId = $user->id;
            $query = $query->whereRaw("user_bonuses.user_id = {$userId}");
        }

        $query = $query->toSql();

        return datatables()->query(DB::table(DB::raw("({$query}) as bonus_ro")))
            ->editColumn('bonus_date', function ($row) {
                return translateDatetime($row->bonus_date, __('format.date.medium'));
            })
            ->editColumn('owner_name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->owner_name, $row->owner_username));
            })
            ->editColumn('from_name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->from_name, $row->from_username));
            })
            ->editColumn('bonus_amount', function ($row) {
                return formatNumber($row->bonus_amount, 0, true);
            })
            ->escapeColumns()
            ->toJson();
    }
    // bonus ro:end

    // bonus upgrade
    public function indexBonusUpgrade(Request $request)
    {
        return view('bonuses.bonus-upgrade');
    }

    public function dataBonusUpgrade(Request $request)
    {
        $user = $request->user();
        $type = BONUS_UPGRADE;

        $query = DB::table('user_bonuses')
            ->join(DB::raw('users as user_owner'), 'user_owner.id', '=', 'user_bonuses.user_id')
            ->leftJoin(DB::raw('users as user_from'), 'user_from.id', '=', 'user_bonuses.from_user_id')
            ->selectRaw('user_bonuses.user_id, user_bonuses.bonus_date, user_bonuses.bonus_amount, user_bonuses.from_user_id, user_owner.name as owner_name, user_owner.username as owner_username, user_from.name as from_name, user_from.username as from_username')
            ->whereRaw("user_bonuses.bonus_type = {$type}");

        if ($user->is_member_user) {
            $userId = $user->id;
            $query = $query->whereRaw("user_bonuses.user_id = {$userId}");
        }

        $query = $query->toSql();

        return datatables()->query(DB::table(DB::raw("({$query}) as bonus_ro")))
            ->editColumn('bonus_date', function ($row) {
                return translateDatetime($row->bonus_date, __('format.date.medium'));
            })
            ->editColumn('owner_name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->owner_name, $row->owner_username));
            })
            ->editColumn('from_name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->from_name, $row->from_username));
            })
            ->editColumn('bonus_amount', function ($row) {
                return formatNumber($row->bonus_amount, 0, true);
            })
            ->escapeColumns()
            ->toJson();
    }
    // bonus upgrade:end

    // summary bonus
    public function indexBonusSummary(Request $request)
    {
        return view('bonuses.summary-bonus');
    }

    public function dataBonusSummary(Request $request)
    {
        $inTypes = [BONUS_SPONSOR];
        $bonusTypeSponsor = BONUS_SPONSOR;

        $selectRaw = "user_bonuses.user_id, users.name, users.username, SUM(user_bonuses.bonus_amount) as total_bonus, SUM(CASE WHEN user_bonuses.bonus_type = {$bonusTypeSponsor} THEN user_bonuses.bonus_amount ELSE 0 END) as bonus_sponsor";

        if (BONUS_EXTRA_ENABLED) {
            $bonusTypeExtra = BONUS_EXTRA;
            $inTypes[] = $bonusTypeExtra;

            $selectRaw .= ", SUM(CASE WHEN user_bonuses.bonus_type = {$bonusTypeExtra} THEN user_bonuses.bonus_amount ELSE 0 END) as bonus_extra";
        }

        if (BONUS_RO_ENABLED) {
            $bonusTypeRo = BONUS_RO;
            $inTypes[] = $bonusTypeRo;

            $selectRaw .= ", SUM(CASE WHEN user_bonuses.bonus_type = {$bonusTypeRo} THEN user_bonuses.bonus_amount ELSE 0 END) as bonus_ro";
        }

        if (BONUS_UPGRADE_ENABLED) {
            $bonusTypeUpgrade = BONUS_UPGRADE;
            $inTypes[] = $bonusTypeUpgrade;

            $selectRaw .= ", SUM(CASE WHEN user_bonuses.bonus_type = {$bonusTypeUpgrade} THEN user_bonuses.bonus_amount ELSE 0 END) as bonus_upgrade";
        }

        $whereInTypes = implode(', ', $inTypes);

        $query = DB::table('user_bonuses')
            ->join('users', 'users.id', '=', 'user_bonuses.user_id')
            ->selectRaw($selectRaw)
            ->whereRaw("user_bonuses.bonus_type in ({$whereInTypes})")
            ->groupBy('user_bonuses.user_id', 'users.username', 'users.name')
            ->toSql();

        $result = datatables()->query(DB::table(DB::raw("({$query}) as summary_bonus")))
            ->editColumn('name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->name, $row->username));
            })
            ->editColumn('bonus_sponsor', function ($row) {
                return formatNumber($row->bonus_sponsor);
            })
            ->editColumn('total_bonus', function ($row) {
                $total = $row->bonus_sponsor;

                if (BONUS_EXTRA_ENABLED) $total += $row->bonus_extra;
                if (BONUS_RO_ENABLED) $total += $row->bonus_ro;
                if (BONUS_UPGRADE_ENABLED) $total += $row->bonus_upgrade;

                return formatNumber($total);
            });

        if (BONUS_EXTRA_ENABLED) {
            $result = $result->editColumn('bonus_extra', function ($row) {
                return formatNumber($row->bonus_extra);
            });
        }

        if (BONUS_RO_ENABLED) {
            $result = $result->editColumn('bonus_ro', function ($row) {
                return formatNumber($row->bonus_ro);
            });
        }

        if (BONUS_UPGRADE_ENABLED) {
            $result = $result->editColumn('bonus_upgrade', function ($row) {
                return formatNumber($row->bonus_upgrade);
            });
        }

        return $result->escapeColumns()->toJson();
    }
    // summary bonus:end

    // bonus paket
    // public function indexBonusPackage(Request $request)
    // {
    //     return view('bonuses.bonus-package');
    // }
    // bonus paket:end

    // bonus reward
    public function indexBonusReward(Request $request)
    {
        return view('bonuses.reward.index');
    }

    public function claimBonusReward(Request $request)
    {
        $reward = $request->reward;

        if (!$request->isMethod('POST')) {
            return view('bonuses.reward.claim', [
                'reward' => $reward
            ]);
        }

        $rewardId = $request->get('reward_id');

        if ($rewardId != $reward->id) {
            return $this->errorAjaxFormResponse(new \Exception('Invalid Reward'));
        }

        $user = $request->user();
        $summaryReward = $user->summary_of_bonus_reward;
        $isAgen = ($reward->package_id == TOP_PACKAGE);
        $key = $isAgen ? 'agen' : 'reseller';

        $summarySelected = $summaryReward->$key;
        $summary = (object) [
            'total_user_left' => $summarySelected->total_user_left,
            'total_user_right' => $summarySelected->total_user_right,
            'remaining_left' => $summarySelected->remaining_left,
            'remaining_right' => $summarySelected->remaining_right,
            'claimed' => false,
            'can_claim' => false,
            'trans_ro' => $summarySelected->trans_ro
        ];

        if ($isAgen) {
            $summaryAgen = $summarySelected->rewards->where('reward.id', '=', $reward->id)->first();

            if (empty($summaryAgen)) return $this->errorAjaxFormResponse(new \Exception('Reward tidak ditemukan.'));

            $summary->can_claim = $summaryAgen->can_claim;
            $summary->claimed = $summaryAgen->claimed;
        } else {
            $summary->can_claim = $summarySelected->can_claim;
            $summary->claimed = !$summary->can_claim;
        }

        if ($summary->claimed) {
            return $this->errorAjaxFormResponse(new \Exception(__('message.reward.claimed')));
        }

        if (!$summary->can_claim) {
            return $this->errorAjaxFormResponse(new \Exception(__('message.reward.cant-claim')));
        }

        $values = [
            'user_id' => $user->id,
            'reward_id' => $reward->id,
            'point_left' => $isAgen ? $summary->total_user_left : $summary->remaining_left,
            'point_right' => $isAgen ? $summary->total_user_right : $summary->remaining_right,
            'reward_value' => $reward->value,
            'reward_left' => $reward->left,
            'reward_right' => $reward->right,
            'ro_id' => $summary->trans_ro ? $summary->trans_ro->id : null,
        ];

        DB::beginTransaction();
        try {
            UserReward::create($values);

            DB::commit();

            return $this->successAjaxFormResponse(route('bonus.reward.index'), __('message.reward.claim-success'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }
    }
    // bonus reward:end
}
