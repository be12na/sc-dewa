<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\UserBank;
use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Unique;

class BankController extends Controller
{
    public function index(Request $request)
    {
        return view('bank.index');
    }

    private function getValidatorValues(bool $isCompany, array &$values, Request $request, UserBank $userBank = null): ValidatorContract
    {
        $values = $request->except(['_token']);
        $bankId = $values['bank_id'];

        $accountRule = (new Unique('user_banks', 'acc_no'))->where('bank_id', $bankId);

        if (!$isCompany) {
            $companyBankIds = UserBank::query()->byCompanyBank()->get()->pluck('id')->toArray();
            $memberBankIds = UserBank::query()
                ->byMemberBank()
                ->withCount('together as together_count')
                ->get()
                ->where('together_count', '>=', MEMBER_BANK_LIMIT)
                ->values()
                ->pluck('id')
                ->toArray();

            $bankIds = array_unique(array_merge($companyBankIds, $memberBankIds));
            $accountRule = $accountRule->whereNotIn('id', $bankIds);
        }

        if ($userBank) {
            $accountRule = $accountRule->ignore($userBank->id, 'id');
        }

        $accNoColumn = $isCompany ? 'acc_no_company' : 'acc_no_member';
        $values['acc_no'] = $values[$accNoColumn];

        $validator = Validator::make($values, [
            'bank_id' => ['required', 'exists:banks,id'],
            $accNoColumn => ['required', $accountRule],
            'acc_name' => ['required', 'string', 'max:100'],
        ]);

        return $validator;
    }

    public function add(Request $request)
    {
        $banks = Bank::query()->orderBy('name')->get();

        return view('bank.form', [
            'bankMode' => 'new',
            'banks' => $banks,
        ]);
    }

    public function store(Request $request)
    {
        $user = $request->user();
        $isAdmin = $this->isAdmin($user);
        $values = [];
        $validator = $this->getValidatorValues($isAdmin, $values, $request);

        if ($validator->fails()) {
            return ajaxResponse(ajaxValidationError($validator), 400);
        }

        $values['user_id'] = $isAdmin ? 0 : $user->id;

        DB::beginTransaction();
        try {
            UserBank::setNewAccount($values);

            DB::commit();

            return $this->successAjaxFormResponse($isAdmin ? route('setting.bank.index') : route('bank.index'), __('message.bank.added'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }
    }

    public function edit(Request $request)
    {
        $banks = Bank::query()->orderBy('name')->get();

        return view('bank.form', [
            'bankMode' => 'edit',
            'banks' => $banks,
            'userBank' => $request->userBank,
        ]);
    }

    public function update(Request $request)
    {
        $user = $request->user();
        $isAdmin = $this->isAdmin($user);
        $userBank = $request->userBank;
        $bankUserId = $isAdmin ? 0 : $user->id;

        if ($userBank->user_id != $bankUserId) {
            $message = __('message.not-owner', ['attribute' => __('menu.bank')]);
            return $this->errorAjaxFormResponse(new \Exception($message));
        }

        $values = [];
        $validator = $this->getValidatorValues($isAdmin, $values, $request, $userBank);

        if ($validator->fails()) {
            return response(ajaxValidationError($validator), 400);
        }

        try {
            $userBank->update($values);

            return $this->successAjaxFormResponse($isAdmin ? route('setting.bank.index') : route('bank.index'), __('message.bank.updated'));
        } catch (\Exception $e) {
            return $this->errorAjaxFormResponse($e);
        }
    }

    public function status(Request $request)
    {
        return view('bank.form', [
            'bankMode' => $request->userBankStatus,
            'userBank' => $request->userBank,
        ]);
    }

    public function updateStatus(Request $request)
    {
        $mode = intval($request->get('mode', 0));

        if (!in_array($mode, [456, 358])) {
            return $this->errorAjaxFormResponse(new \Exception('Invalid mode'));
        }

        $isActivating = ($mode == 358);
        $userBank = $request->userBank;
        $isAdmin = $this->isAdmin($request);

        DB::beginTransaction();
        try {
            $userBank->setStatus($isActivating);

            DB::commit();

            return $this->successAjaxFormResponse($isAdmin ? route('setting.bank.index') : route('bank.index'), __($isActivating ? 'message.bank.activated' : 'message.bank.deactivated'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }
    }
}
