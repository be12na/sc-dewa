<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\UserReward;
use App\Models\UserWithdraw;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\HtmlString;

class WithdrawController extends Controller
{
    private function getSuffix(Request $request)
    {
        $routeNames = explode('.', $request->route()->getName());

        return (count($routeNames) > 2) ? $routeNames[1] : 'unknown';
    }

    public function indexAdmin(Request $request)
    {
        $suffix = $this->getSuffix($request);

        return view('admin.withdraw.index', [
            'suffix' => $suffix,
        ]);
    }

    public function dataTableAdmin(Request $request)
    {
        $suffix = $this->getSuffix($request);
        $bonusType = 0;

        if ($suffix == 'sponsor') {
            $bonusType = BONUS_SPONSOR;
        } elseif ($suffix == 'extra') {
            $bonusType = BONUS_EXTRA;
        } elseif ($suffix == 'ro') {
            $bonusType = BONUS_RO;
        } elseif ($suffix == 'upgrade') {
            $bonusType = BONUS_UPGRADE;
        }

        $processing = WD_PROCESSING;
        $hasRole = hasRole(['admin.super', 'admin.master']);

        $query = DB::table('user_withdraws')
            ->join('users', 'users.id', '=', 'user_withdraws.user_id')
            ->selectRaw("user_withdraws.id, user_withdraws.wd_code, user_withdraws.user_id, user_withdraws.wd_date, user_withdraws.bank_code, user_withdraws.bank_name, user_withdraws.bank_acc_no, user_withdraws.bank_acc_name, user_withdraws.wd_bonus_type, user_withdraws.total_bonus, user_withdraws.fee, user_withdraws.total_transfer, user_withdraws.status, user_withdraws.status_at, users.username, users.name, users.phone")
            ->whereRaw("user_withdraws.wd_bonus_type={$bonusType}")
            ->whereRaw("user_withdraws.status={$processing}")
            ->toSql();

        $result = datatables()->query(DB::table(DB::raw("({$query}) as wd")))
            ->editColumn('name', function ($row) {
                $format = '<div>%s</div><div class="small text-muted">%s - %s</div>';

                return new HtmlString(sprintf($format, $row->name, $row->username, $row->phone));
            })
            ->editColumn('bank_name', function ($row) {
                $format = '<div>%s</div><div class="text-muted">%s</div><div class="small text-muted">%s</div>';

                return new HtmlString(sprintf($format, $row->bank_code, $row->bank_acc_no, $row->bank_acc_name));
            })
            ->editColumn('total_bonus', function ($row) {
                return formatNumber($row->total_bonus);
            })
            ->editColumn('fee', function ($row) {
                return formatNumber($row->fee);
            })
            ->editColumn('total_transfer', function ($row) {
                return formatNumber($row->total_transfer);
            });

        if ($hasRole) {
            $result = $result->addColumn('check', function ($row) {
                $check = '<input type="checkbox" class="check-row" value="' . $row->id . '" style="margin-right:2px;">';
                $check = new HtmlString($check);

                return $check;
            });
        }

        return $result->escapeColumns()->toJson();
    }

    public function indexHistoriesAdmin(Request $request)
    {
        return view('admin.withdraw.history');
    }

    public function dataTableHistoriesAdmin(Request $request)
    {
        $bonusType = implode(',', [BONUS_SPONSOR, BONUS_EXTRA, BONUS_RO, BONUS_UPGRADE]);

        $query = DB::table('user_withdraws')
            ->join('users', 'users.id', '=', 'user_withdraws.user_id')
            ->selectRaw("user_withdraws.id, user_withdraws.wd_code, user_withdraws.user_id, user_withdraws.wd_date, user_withdraws.bank_code, user_withdraws.bank_name, user_withdraws.bank_acc_no, user_withdraws.bank_acc_name, user_withdraws.wd_bonus_type, user_withdraws.total_bonus, user_withdraws.fee, user_withdraws.total_transfer, user_withdraws.status, user_withdraws.status_at, users.username, users.name, users.phone")
            ->whereRaw("user_withdraws.wd_bonus_type in({$bonusType})")
            ->toSql();

        $result = datatables()->query(DB::table(DB::raw("({$query}) as wd")))
            ->editColumn('name', function ($row) {
                $format = '<div>%s</div><div class="small text-muted">%s - %s</div>';

                return new HtmlString(sprintf($format, $row->name, $row->username, $row->phone));
            })
            ->editColumn('bank_name', function ($row) {
                $format = '<div>%s</div><div class="text-muted">%s</div><div class="small text-muted">%s</div>';

                return new HtmlString(sprintf($format, $row->bank_code, $row->bank_acc_no, $row->bank_acc_name));
            })
            ->editColumn('total_bonus', function ($row) {
                return formatNumber($row->total_bonus);
            })
            ->editColumn('fee', function ($row) {
                return formatNumber($row->fee);
            })
            ->editColumn('total_transfer', function ($row) {
                return formatNumber($row->total_transfer);
            })
            ->editColumn('wd_bonus_type', function ($row) {
                if ($row->wd_bonus_type == BONUS_SPONSOR) {
                    $type = __('label.bonus-sponsor');
                } elseif ($row->wd_bonus_type == BONUS_EXTRA) {
                    $type = __('label.bonus-extra');
                } elseif ($row->wd_bonus_type == BONUS_RO) {
                    $type = __('label.bonus-ro');
                } elseif ($row->wd_bonus_type == BONUS_UPGRADE) {
                    $type = __('label.bonus-upgrade');
                } else {
                    $type = __('Unknown');
                }

                return $type;
            })
            ->editColumn('status', function ($row) {
                $format = '<div class="text-%s">%s</div><div class="small text-muted">%s</div>';
                $isPending = false;

                if ($row->status == WD_REJECT) {
                    $status = __('label.status.rejected');
                    $color = 'danger';
                } elseif ($row->status == WD_COMPLETE) {
                    $status = __('label.status.complete');
                    $color = 'success';
                } else {
                    $isPending = true;
                    $status = __('label.status.pending');
                    $color = 'warning';
                }

                return new HtmlString(sprintf($format, $color, $status, $isPending ? '' : translateDatetime($row->status_at, __('format.datetime.medium'))));
            });

        return $result->escapeColumns()->toJson();
    }

    public function transfer(Request $request)
    {
        $bonusType = $request->get('type');

        if (!in_array($bonusType, ['sponsor', 'extra', 'ro', 'upgrade'])) {
            return ajaxResponse(__('message.withdraw.not-available'), 400);
        }

        if (!$request->isMethod('POST')) {
            $ids = $request->get('ids');
            if (empty($ids)) {
                return ajaxResponse(__('message.withdraw.not-selection'), 400);
            }

            $checks = explode(',', $ids);

            return view('admin.withdraw.transfer-comfirm', [
                'bonusType' => $bonusType,
                'checks' => $checks,
            ]);
        }

        $bonusIds = $request->get('checks', []);
        if (count($bonusIds) == 0) {
            return ajaxResponse(__('message.not-found'), 404);
        }

        $values = [
            'status' => WD_COMPLETE,
            'status_at' => Carbon::now(),
        ];

        DB::beginTransaction();
        try {
            UserWithdraw::query()->whereIn('id', $bonusIds)->where('status', '=', WD_PROCESSING)->update($values);

            DB::commit();

            return $this->successAjaxFormResponse(route("withdraw.{$bonusType}.index"), __('message.withdraw.transfer-success'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }
    }

    // reward
    public function indexRewardAdmin(Request $request)
    {
        $packages = Package::query()->orderBy('id')->get();

        return view('admin.withdraw.reward', [
            'packages' => $packages,
        ]);
    }

    public function dataTableRewardAdmin(Request $request)
    {
        $pkgId = $request->get('pkg_id');
        $status = $request->get('rs');
        $hasRole = hasRole(['admin.super', 'admin.master']);
        $reject = WD_REJECT;

        $query = DB::table('user_rewards')
            ->join('users', 'users.id', '=', 'user_rewards.user_id')
            ->join('rewards', 'rewards.id', '=', 'user_rewards.reward_id')
            ->selectRaw("user_rewards.*, users.name, users.username, users.phone, rewards.package_id")
            ->whereRaw("user_rewards.status!={$reject}");

        if (!is_null($pkgId)) {
            $query = $query->whereRaw("rewards.package_id={$pkgId}");
        }

        if (!is_null($status)) {
            $query = $query->whereRaw("user_rewards.status={$status}");
        }

        $query = $query->toSql();

        $result = datatables()->query(DB::table(DB::raw("({$query}) as claims")))
            ->editColumn('id', function ($row) {
                return UserReward::getCode($row);
            })->editColumn('name', function ($row) {
                $format = '<div>%s</div><div class="small text-muted">%s - %s</div>';

                return new HtmlString(sprintf($format, $row->name, $row->username, $row->phone));
            });

        if ($hasRole) {
            $result = $result->addColumn('action', function ($row) {
                $html = '';
                if ($row->status == WD_REJECT) {
                    $html = '<span class="text-success">' . __('label.status.rejected') . '</span>';
                } elseif ($row->status == WD_COMPLETE) {
                    $html = '<span class="text-success">' . __('label.status.complete') . '</span>';
                } else {
                    $routeConfirm = route('withdraw.reward.viewAction', ['userReward' => $row->id, 'action' => 'confirm']);
                    $routeReject = route('withdraw.reward.viewAction', ['userReward' => $row->id, 'action' => 'reject']);

                    $html = view('partials.buttons.open-modal', ['btnClass' => 'btn-sm btn-success mx-1', 'isAjaxModal' => true, 'modalTarget' => '#my-modal', 'modalUrl' => $routeConfirm, 'btnText' => __('button.confirm')])->render();
                    $html .= view('partials.buttons.open-modal', ['btnClass' => 'btn-sm btn-danger mx-1', 'isAjaxModal' => true, 'modalTarget' => '#my-modal', 'modalUrl' => $routeReject, 'btnText' => __('button.reject')])->render();
                }

                return new HtmlString($html);
            });
        }

        return $result->escapeColumns()->toJson();
    }

    public function actionRewardAdmin(Request $request)
    {
        $userReward = $request->userReward;
        $action = $request->action;

        if (!in_array($action, ['confirm', 'reject'])) {
            return ajaxResponse(__('message.withdraw.not-available'), 400);
        }

        if (!$request->isMethod('POST')) {
            return view('admin.withdraw.reward-comfirm', [
                'userReward' => $userReward,
                'action' => $action,
            ]);
        }

        $isConfirm = ($action == 'confirm');
        $values = [
            'status' => $isConfirm ? WD_COMPLETE : WD_REJECT,
            'status_at' => Carbon::now(),
        ];

        if (!$isConfirm) {
            $values['reject_note'] = $request->get('reject_note');
            $validator = Validator::make($values, [
                'reject_note' => ['required', 'string', 'max:255'],
            ], [], [
                'reject_note' => __('label.reject-reason'),
            ]);

            if ($validator->fails()) {
                return ajaxResponse(ajaxValidationError($validator, __('message.reward.confirm-fail')), 400);
            }
        }

        DB::beginTransaction();
        try {
            $userReward->update($values);

            DB::commit();

            return $this->successAjaxFormResponse(route("withdraw.reward.index"), __('message.reward.confirm-success'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }
    }
    // reward:end
}
