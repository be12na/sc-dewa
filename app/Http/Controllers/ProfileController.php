<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        return view('member.profile');
    }

    public function update(Request $request)
    {
        $values = $this->validate($request, [
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'max:100', 'email'],
            'phone' => ['required', 'min:9', 'max:15', 'starts_with:+628,08'],
            'address' => ['required', 'string', 'max:250'],
        ]);

        $values['has_profile'] = true;
        $user = $request->user();
        $user->update($values);

        return $this->successPageFormResponse('profile.index', __('message.profile.updated'));
    }
}
