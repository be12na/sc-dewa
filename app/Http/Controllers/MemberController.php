<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Member\MemberStructure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\HtmlString;
use Illuminate\Validation\Rules\Password;

class MemberController extends Controller
{
    use MemberStructure;

    // admin role
    public function index(Request $request)
    {
        return view('admin.member.index');
    }

    public function dataTable(Request $request)
    {
        $hasRole = hasRole(['admin.super', 'admin.master'], $request->user());

        return datatables()->eloquent(User::query()->byMemberUser())
            ->editColumn('has_profile', function ($row) {
                return new HtmlString(contentCheck($row->has_profile));
            })
            ->editColumn('user_status', function ($row) {
                $status = ($row->user_status == USER_STATUS_INACTIVE)
                    ? __('label.user.status.inactive')
                    : ($row->user_status == USER_STATUS_BANNED
                        ? __('label.user.status.banned')
                        : __('label.user.status.active')
                    );

                return $status;
            })
            ->editColumn('registered_at', function ($row) {
                return translateDatetime($row->registered_at, 'j M Y H:i');
            })
            ->addColumn('action', function ($row) use ($hasRole) {
                if ($hasRole) {
                    $eData = view('partials.buttons.open-modal', [
                        'btnTitle' => 'Edit Data ' . $row->username,
                        'isAjaxModal' => true,
                        'modalUrl' => route('member.edit.data', ['userMember' => $row->id]),
                        'btnClass' => 'btn-sm btn-primary mx-1',
                        'modalTarget' => '#my-modal',
                        'btnText' => '<i class="fa fa-user"></i>',
                    ])->render();

                    $ePassword = view('partials.buttons.open-modal', [
                        'btnTitle' => 'Edit Password ' . $row->username,
                        'isAjaxModal' => true,
                        'modalUrl' => route('member.edit.password', ['userMember' => $row->id]),
                        'btnClass' => 'btn-sm btn-warning mx-1',
                        'modalTarget' => '#my-modal',
                        'btnText' => '<i class="fa fa-key"></i>',
                    ])->render();

                    return new HtmlString($eData . $ePassword);
                }

                return '';
            })
            ->escapeColumns()
            ->toJson();
    }

    public function editMemberByAdmin(Request $request)
    {
        return view('admin.member.form', [
            'userMember' => $request->userMember,
            'editMode' => ($request->route()->getName() == 'member.edit.password') ? 'password' : 'data',
        ]);
    }

    public function updateByAdmin(Request $request)
    {
        $userMember = $request->userMember;
        $memberId = $userMember->id;
        $mode = $request->get('edit_mode');
        $editPassword = ($mode == 'password');
        $values = $request->except(['_token', 'edit_mode']);

        $validator = Validator::make(
            $values,
            $editPassword
                ? [
                    'password' => ['required', 'confirmed', (new Password(6))->letters()->numbers()],
                ]
                : [
                    'name' => ['required', 'string', 'max:100'],
                    'email' => ['required', 'string', 'max:100', 'email', "unique:users,email,$memberId,id"],
                    'username' => ['required', 'string', 'min:5', 'max:32', "unique:users,username,$memberId,id"],
                    'phone' => ['required', 'min:9', 'max:15', 'starts_with:+628,08'],
                ]
        );

        if ($validator->fails()) {
            return response(ajaxValidationError($validator), 400);
        }

        DB::beginTransaction();
        try {
            if ($editPassword) {
                unset($values['password_confirmation']);
                $values['password'] = Hash::make($values['password']);
            }

            $userMember->update($values);

            DB::commit();

            return $this->successAjaxFormResponse(route('member.index'), $editPassword ? __('message.member.password-changed') : __('message.member.updated'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }
    }
    // admin role:end

    // member role
    public function indexInactive(Request $request)
    {
        $user = $request->user();

        return view('member.member.inactive');
    }

    public function dataTableInactive(Request $request)
    {
        $user = $request->user();

        $eloquent = User::query()
            ->byReferral($user)
            ->byInactiveMember()
            ->with('startPackage');

        return datatables()->eloquent($eloquent)
            ->editColumn('name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->name, $row->username));
            })
            ->editColumn('registered_at', function ($row) {
                return translateDatetime($row->registered_at, __('format.datetime.medium'));
            })
            ->addColumn('pkg_status', function ($row) {
                $hasOrder = !empty($row->startPackage);
                $status = '';

                if ($hasOrder) {
                    $text = __('label.status.confirm');
                    $status = "<span class=\"text-primary\">{$text}</span>";
                }

                return new HtmlString($status);
            })
            ->escapeColumns()
            ->toJson();
    }

    public function placement(Request $request)
    {
        $owner = $request->user();

        if (!$request->isMethod('POST')) {
            if ($request->route()->getName() != 'member.placement.select') {
                return $this->binaryTree($request, true);
            }

            $placementMembers = $owner->sponsored()->byUserStatus(USER_STATUS_ACTIVATED)
                ->whereDoesntHave('structure')
                ->with('package')
                ->orderBy('name')
                ->get();

            return view('member.member.placement-select', [
                'parentMember' => $request->member,
                'members' => $placementMembers,
                'position' => $request->position,
            ]);
        }

        $parentId = $request->get('parent_member');
        $structureUserIds = $owner->structure->descendantsWithSelf()->get()->pluck('user_id')->toArray();
        $parent = $owner;

        if ($parentId != $owner->id) {
            $parent = User::query()->byId($parentId)->byMemberUser()->byUserStatus(USER_STATUS_ACTIVATED)
                ->whereHas('structure')
                ->first();

            if (!in_array($parentId, $structureUserIds) || empty($parent)) {
                return ajaxResponse(__('message.not-found'), 404);
            }
        }

        $userId = $request->get('user_id');
        $newMember = User::query()->byId($userId)->byMemberUser()->byUserStatus(USER_STATUS_ACTIVATED)
            ->whereDoesntHave('structure')
            ->first();

        if (empty($newMember)) {
            return ajaxResponse(__('message.not-found'), 404);
        }

        $backParams = ($parent->id != $owner->id) ? ['user_id' => $parent->id] : [];

        DB::beginTransaction();

        try {
            $newStructure = $newMember->structure()->create([
                'side_position' => $request->get('position', USER_LEFT_POSITION)
            ]);
            $parent->structure->addChild($newStructure);

            DB::commit();

            return $this->successAjaxFormResponse(route('member.placement.index', $backParams), __('message.member.success-placement'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorAjaxFormResponse($e);
        }

        return;
    }
    // member role:end
}
