<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Member\MemberStructure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\HtmlString;

class NetworkController extends Controller
{
    use MemberStructure;

    public function indexSponsored(Request $request)
    {
        return view('member.network.sponsored');
    }

    public function dataTableSponsored(Request $request)
    {
        $eloquent = User::query()
            ->byReferral($request->user())
            ->byUserStatus(USER_STATUS_ACTIVATED)
            ->with(['package']);

        return datatables()->eloquent($eloquent)
            ->editColumn('name', function ($row) {
                $html = '<div>%s</div><div class="small text-muted">(%s)</div>';

                return new HtmlString(sprintf($html, $row->name, $row->username));
            })
            ->editColumn('package_id', function ($row) {
                return $row->package ? $row->package->name : '';
            })
            ->editColumn('registered_at', function ($row) {
                return translateDatetime($row->registered_at, __('format.datetime.medium'));
            })
            ->editColumn('activated_at', function ($row) {
                return translateDatetime($row->activated_at, __('format.datetime.medium'));
            })
            ->escapeColumns()
            ->toJson();
    }

    public function indexTree(Request $request)
    {
        return $this->binaryTree($request, false);
    }
}
