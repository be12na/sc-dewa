<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Notifications\Registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Validation\Rules\Password;

class RegistrationController extends Controller
{
    public function index(Request $request)
    {
        if (auth()->check()) {
            return view('register.index');
        }

        return view('register.referral', [
            'referralUser' => $request->referralUser,
        ]);
    }

    public function store(Request $request)
    {
        $referralRules = [];

        if (!auth()->check()) {
            // $referralExists = new Exists('users', 'username');
            $referralExists = new Exists(User::class, 'username');
            $referralExists = $referralExists
                ->where('user_type', USER_TYPE_MEMBER)
                ->where('user_status', USER_STATUS_ACTIVATED)
                ->where(function ($where) {
                    return $where->whereHas('startPackage', function ($transPkg) {
                        return $transPkg->byTransStatus(TRANS_CONFIRMED);
                    });
                })
                ->where('has_profile', true);

            $referralRules = [
                'referral_username' => ['required', $referralExists],
            ];
        }

        $values = $this->validate($request, array_merge($referralRules, [
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'max:100', 'email'],
            'username' => ['required', 'string', 'min:5', 'max:32', 'unique:users,username'],
            'password' => ['required', 'confirmed', (new Password(6))->letters()->numbers()],
            'phone' => ['required', 'min:9', 'max:15', 'starts_with:+628,08'],
        ]));

        $goto = 'login';

        if (auth()->check()) {
            $values['referral_id'] = $request->user()->id;
            $goto = 'member.register.index';
        } else {
            $referral = User::query()->byUsername($values['referral_username'])->first();
            $values['referral_id'] = $referral->id;
        }

        $inputPassword = $values['password'];

        $values['user_type'] = USER_TYPE_MEMBER;
        $values['user_status'] = USER_STATUS_REGISTERED;
        $values['user_login'] = USER_REGISTER_LOGIN;
        $values['password'] = Hash::make($inputPassword);
        $values['registered_at'] = date('Y-m-d H:i:s');

        DB::beginTransaction();
        try {
            $user = User::create($values);

            // Mail::to($user->email)->send(new RegistrationMail($user));

            $user->notify(new Registration($user, 'database', 'mail', ['password' => $inputPassword]));
            $user->notify(new Registration($user, 'database', 'whatsapp', ['password' => $inputPassword]));

            DB::commit();

            return $this->successPageFormResponse($goto, __('message.registration.success'));
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->errorPageFormResponse($e);
        }
    }
}
