<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class PasswordController extends Controller
{
    public function updatePassword(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return view('password-change');
        }

        $values = $this->validate($request, [
            'old_password' => ['required', 'current_password'],
            'new_password' => ['required', 'confirmed', (new Password(6))->letters()->numbers()],
        ]);

        try {
            $request->user()->update([
                'password' => Hash::make($values['new_password']),
            ]);

            return $this->successPageFormResponse('password.change', __('message.password.changed'));
        } catch (\Exception $e) {
            return $this->errorPageFormResponse($e);
        }
    }
}
