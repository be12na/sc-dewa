<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthPackage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $param = 0)
    {
        // param = 1 berarti tidak wajib punya paket atau order paket yang belum dikonfirmasi
        // param = ? berarti wajib sudah punya paket yang sudah dikonfirmasi (status sudah akitf member)
        $user = $request->user();

        if ($user) {
            if ($user->isGroup('member')) {
                $param = intval($param);

                // param = 1 (jika sudah punya paket atau belum punya paket tapi sudah order)
                // param = 2 (jika sudah punya paket atau belum punya paket tapi mau order / upgrade)
                // param = 3 (wajib sudah punya paket, jika sudah login)

                if (!$user->has_package) {
                    if (!$user->has_pending_order_package) {
                        if ($param != 2) {
                            $message = __('message.package.dont-have');
                            $toRoute = 'package.activate.index';

                            return $request->ajax()
                                ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                                : pageMessageResponse($message, $toRoute, [], 'warning');
                        }
                    } else {
                        if ($param != 1) {
                            $message = __('message.package.incomplete');
                            $toRoute = 'package.transaction.index';

                            return $request->ajax()
                                ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                                : pageMessageResponse($message, $toRoute, [], 'warning');
                        }
                    }
                } else {
                    if ($param == 2) {
                        if ($user->has_pending_order_package) {
                            $message = __('message.package.incomplete');

                            return $request->ajax()
                                ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                                : pageMessageResponse($message, 'package.transaction.index', [], 'warning');
                        }

                        if (!$user->upgradable_package) {
                            $message = __('message.package.top-level-reached');

                            return $request->ajax()
                                ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                                : pageMessageResponse($message, 'dashboard.index', [], 'warning');
                        }
                    }
                }
            }
        }

        return $next($request);
    }
}
