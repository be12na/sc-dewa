<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();

        if (!$user->isGroup('admin') && !$user->has_profile) {
            $message = __('message.profile.incomplete');

            return $request->ajax()
                ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                : pageMessageResponse($message, 'profile.index', [], 'warning');
        }

        return $next($request);
    }
}
