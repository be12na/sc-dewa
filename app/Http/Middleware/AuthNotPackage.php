<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthNotPackage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();

        if ($user->isGroup('admin')) {
            $message = __('message.forbidden');

            return $request->ajax()
                ? ajaxResponse(errorMessageContent($message, 'error', false, false), 403)
                : pageMessageResponse($message, 'dashboard.index', [], 'error');
        }

        if ($user->has_Package || (!$user->has_Package && $user->has_pending_order_package)) {
            $message = $user->has_Package ? __('message.package.completed') : __('message.package.incomplete');

            return $request->ajax()
                ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                : pageMessageResponse($message, $user->has_Package ? 'package.history.index' : 'package.transaction.index', [], 'warning');
        }

        return $next($request);
    }
}
