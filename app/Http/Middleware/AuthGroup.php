<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$group)
    {
        if (!hasGroup($group)) {
            $isAjax = $request->ajax();
            $message = __('message.forbidden', ['attribute' => $isAjax ? __('text.data') : __('text.page')]);

            return $isAjax
                ? ajaxResponse(errorMessageContent($message, 'danger', false, false), 403)
                : pageMessageResponse($message, 'dashboard.index', [], 'danger');
        }

        return $next($request);
    }
}
