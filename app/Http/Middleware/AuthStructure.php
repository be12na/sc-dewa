<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthStructure
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (empty($request->user()->structure)) {
            $message = __('message.member.not-placement');

            return $request->ajax()
                ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                : pageMessageResponse($message, 'dashboard.index', [], 'warning');
        }

        return $next($request);
    }
}
