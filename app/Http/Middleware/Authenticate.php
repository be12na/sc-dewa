<?php

namespace App\Http\Middleware;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('login');
        }
    }

    /**
     * Handle an unauthenticated user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function unauthenticated($request, array $guards)
    {
        $message = __('auth.end');
        $please = ucfirst(__('text.please'));

        if ($request->ajax()) {
            $route = route('login');
            throw new Exception(errorMessageContent("<div>{$message}</div><div>{$please} <a href=\"{$route}\" class=\"link-primary\">login</a>.</div>", 'danger', false, false));
        }

        // session([
        //     'message' => $message,
        //     'messageClass' => 'danger',
        //     'messageShowHeader' => false,
        //     'messageShowClose' => true,
        // ]);

        throw new AuthenticationException($message, $guards, $this->redirectTo($request));
    }
}
