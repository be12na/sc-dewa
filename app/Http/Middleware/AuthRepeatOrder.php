<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthRepeatOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();

        if ($user->isGroup('member')) {
            if (!$user->has_package) {
                $message = __('message.package.dont-have');
                $toRoute = 'package.activate.index';

                return $request->ajax()
                    ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                    : pageMessageResponse($message, $toRoute, [], 'warning');
            }

            if ($user->upgradable_package) {
                $message = __('message.package.cant-ro');
                $toRoute = 'dashboard.index';

                return $request->ajax()
                    ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                    : pageMessageResponse($message, $toRoute, [], 'warning');
            }

            if ($user->has_pending_order_package) {
                $message = __('message.package.incomplete');

                return $request->ajax()
                    ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                    : pageMessageResponse($message, 'package.transaction.index', [], 'warning');
            }
        } else {
            $message = __('message.forbidden');

            return $request->ajax()
                ? ajaxResponse(errorMessageContent($message, 'warning', false, false), 403)
                : pageMessageResponse($message, 'dashboard.index', [], 'warning');
        }

        return $next($request);
    }
}
