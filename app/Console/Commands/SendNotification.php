<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:send-notification {driver}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notifications that have been recorded in the database based on the installed drivers';

    private int $limit = 5;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->sendNow();
    }

    private function sendNow()
    {
        $driver = strtolower($this->argument('driver'));
        if ($driver == 'whatsapp') $driver = 'onesender';

        $notifications = DB::table('notifications')
            ->whereJsonContains('data->driver', $driver);

        if ($driver == 'onesender') {
            $notifications = $notifications->orWhereJsonContains('data->driver', 'whatsapp');
        }

        $notifications = $notifications
            ->orderBy('created_at')
            ->limit($this->limit)
            ->get();

        $numberSuccess = 0;
        $numberFail = 0;

        foreach ($notifications as $notification) {
            $data = json_decode($notification->data, true);
            $recipientClass = $notification->notifiable_type;
            $notificationClass = $notification->type;
            $recipient = $recipientClass::query()->byId($notification->notifiable_id)->first();
            $modelData = $recipient;

            if (($notificationClass != $data['model']) || (($recipientClass == $data['model']) && ($notification->notifiable_id != $data['model_id']))) {
                $modelData = $data['model']::query()->byId($data['model_id'])->first();
            }

            if (!empty($recipient) && !empty($modelData)) {
                $optionalData = [];

                if (count($data) > 3) {
                    $optionalData = $data;
                    unset($optionalData['driver'], $optionalData['model'], $optionalData['model_id']);
                }

                try {
                    $recipient->notify(new $notificationClass($modelData, $data['driver'], null, $optionalData));

                    $this->sendSuccess($notification->id);
                    $numberSuccess += 1;
                } catch (\Exception $e) {
                    $this->sendError($notification->id, $data, $e->getMessage());
                    $numberFail += 1;
                }
            }
        }

        if ($numberSuccess > 0) $this->info("Notifikasi sukses: {$numberSuccess}");
        if ($numberFail > 0) $this->info("Notifikasi gagal: {$numberFail}");
    }

    private function sendSuccess($notificationId): void
    {
        DB::table('notifications')->where('id', '=', $notificationId)->delete();
    }

    private function sendError($notificationId, array $data, $message): void
    {
        $data['error'] = $message;

        $values = [
            'data' => json_encode($data),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        DB::table('notifications')->where('id', '=', $notificationId)->update($values);
    }
}
