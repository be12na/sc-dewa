<?php

namespace App\Console\Commands;

use App\Models\TransPackage;
use App\Models\UserBonus;
use App\Models\UserWithdraw;
use Illuminate\Console\Command;

class FixBonusExtra extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:bonus-extra';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix Mis given Bonus Extra';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        UserBonus::query()->bonusExtra()->delete();
        UserWithdraw::query()->bonusExtra()->delete();
        $transStart = TransPackage::query()
            ->byStarter()
            ->byConfirmed()
            ->orderBy('id')
            ->get();

        foreach ($transStart as $transPackage) {
            UserBonus::createBonusExtra($transPackage->buyer, $transPackage->status_at);
        }

        return Command::SUCCESS;
    }
}
