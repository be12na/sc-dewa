<?php

namespace App\Console\Commands;

use App\Models\UserBonus;
use App\Models\UserWithdraw;
use App\Notifications\SummaryBonus;
use Carbon\Carbon;
use Closure;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as rowCollection;
use Illuminate\Support\Facades\DB;

class Withdraw extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:withdraw';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Withdraw available bonus to withdraw';

    private array $existsCode = [];
    private rowCollection $usersGetWD;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->usersGetWD = collect();
        $this->existsCode = UserWithdraw::query()->get('wd_code')->pluck('wd_code')->toArray();

        // bonus yg bisa ditarik tanpa syarat
        $freeTypes = [BONUS_SPONSOR];
        $title = 'Bonus Sponsor';

        if (BONUS_EXTRA_ENABLED) {
            $freeTypes[] = BONUS_EXTRA;
            $title .= ' dan Extra';
        }

        $this->setWithdrawBonus($this->queryBonuses($freeTypes), $title);
        ///////////////////////////////////////////////

        // bonus yg bisa ditarik harus cukup syarat
        if (BONUS_RO_ENABLED) {
            // $roBonuses = $this->queryBonuses(BONUS_RO, function ($user) {
            //     return $user->whereHas('transROForBonusRO');
            // }, 'transROForBonusRO');
            // $this->setWithdrawBonus($roBonuses, 'Bonus RO', true, 'transROForBonusRO');

            $this->setWithdrawBonus($this->queryBonuses(BONUS_RO), 'Bonus RO');
        }

        if (BONUS_UPGRADE_ENABLED) {
            // $upgradeBonuses = $this->queryBonuses(BONUS_UPGRADE, function ($user) {
            //     return $user->whereHas('transROForBonusUpgrade');
            // }, 'transROForBonusUpgrade');
            // $this->setWithdrawBonus($upgradeBonuses, 'Bonus Upgrade', true, 'transROForBonusUpgrade');

            $this->setWithdrawBonus($this->queryBonuses(BONUS_UPGRADE), 'Bonus Upgrade');
        }

        $this->sendNotificationsWithdraw();
    }

    private function queryBonuses(array|int $type, Closure $userConditions = null, string $userWith = null): Collection
    {
        if (!is_array($type)) $type = [$type];

        return UserBonus::query()->byBonusType($type)
            ->forWithdraw(operator: '<=', userConditions: $userConditions)
            ->with('user', function ($user) use ($userWith) {
                if (!empty($userWith)) $user = $user->with($userWith);

                return $user->with('memberActiveBank');
            })->get();
    }

    private function setWithdrawBonus(Collection $bonuses, string $title, bool $useRO = null, string $roName = null): void
    {
        if ($bonuses->isNotEmpty()) {

            DB::beginTransaction();

            try {
                // group by bonus date
                foreach ($bonuses->groupBy('bonus_date') as $onDate => $bonusesByDate) {
                    $dateCarbon = Carbon::createFromTimeString($onDate);
                    $wdDate = $dateCarbon->format('Y-m-d');
                    // group by user
                    foreach ($bonusesByDate->groupBy('user_id') as $userId => $bonusesByUser) {
                        $user = $bonusesByUser->first()->user;
                        $userBank = $user->memberActiveBank;
                        $userWithdraws = [];

                        // group by bonus type
                        foreach ($bonusesByUser->groupBy('bonus_type') as $bonusType => $bonusesByType) {
                            $bonusIds = $bonusesByType->pluck('id')->toArray();
                            $totalBonus = $bonusesByType->sum('bonus_amount');
                            $fee = WD_FEE;
                            $totalTransfer = $totalBonus - $fee;
                            $wdCode = UserWithdraw::makeCode($bonusType, $dateCarbon, $this->existsCode);

                            $userWithdraws[] = [
                                'bonus_type' => $bonusType,
                                'total_bonus' => $totalBonus,
                                'fee' => $fee,
                                'transfer' => $totalTransfer,
                            ];

                            $wdValues = [
                                'wd_code' => $wdCode,
                                'user_id' => $userId,
                                'wd_date' => $wdDate,
                                'bank_code' => $userBank->bank_code,
                                'bank_name' => $userBank->bank_name,
                                'bank_acc_no' => $userBank->acc_no,
                                'bank_acc_name' => $userBank->acc_name,
                                'wd_bonus_type' => $bonusType,
                                'total_bonus' => $totalBonus,
                                'fee' => $fee,
                                'total_transfer' => $totalTransfer,
                                'status' => WD_PROCESSING,
                            ];

                            $wd = UserWithdraw::create($wdValues);
                            $bonusValues = ['wd_id' => $wd->id];

                            if ($useRO && !empty($roName)) {
                                $ro = $user->$roName;
                                $bonusValues['ro_id'] = $ro->id;
                            }

                            UserBonus::query()->byId($bonusIds)->update($bonusValues);

                            $this->existsCode[] = $wdCode;
                        }

                        if (empty($this->usersGetWD->where('user.id', '=', $user->id)->first())) {
                            $this->usersGetWD->push((object) ['user' => $user, 'withdraw' => $userWithdraws]);
                        }
                    }
                }

                DB::commit();

                $this->info("Withdraw {$title} berhasil");
            } catch (\Exception $e) {
                DB::rollBack();

                $this->error("Withdraw {$title} error:" . $e->getMessage());
            }
        } else {
            $this->warn("Tidak ada {$title} yang dapat ditarik.");
        }
    }

    private function sendNotificationsWithdraw(): void
    {
        if ($this->usersGetWD->isNotEmpty()) {
            foreach ($this->usersGetWD as $object) {
                $user = $object->user;
                $user->notify(new SummaryBonus($user, 'database', 'mail', ['withdraw' => $object->withdraw]));
                $user->notify(new SummaryBonus($user, 'database', 'whatsapp', ['withdraw' => $object->withdraw]));
            }
        }
    }
}
