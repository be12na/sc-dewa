<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;

class SummaryBonus extends BaseNotification
{
    private function getTotalWithdraw(): int
    {
        $total = 0;
        if (!empty($this->optionalData) && array_key_exists('withdraw', $this->optionalData)) {
            foreach ($this->optionalData['withdraw'] as $type) {
                $total += $type['total_bonus'];
            }
        }

        return $total;
    }

    protected function setWhatsappContent(): void
    {
        $user = $this->model;
        // $summaries = $user->summary_of_bonuses;
        $currency = __('format.currency.symbol.text');

        // $contents = [
        //     "Halo *{$user->name}*",
        //     "Berikut Informasi Ringkasan Bonus Anda",
        //     "",
        //     "Total bonus dalam proses: {$currency} " . formatNumber($summaries->remaining_bonus),
        //     "Total bonus dalam ditarik: {$currency} " . formatNumber($summaries->total_withdraw),
        //     "",
        //     "Total Semua Bonus: {$currency} " . formatNumber($summaries->total_bonus),
        // ];

        $contents = [
            "Halo *{$user->name}*",
            "",
            "Anda telah mendapatkan penarikan bonus sejumlah *{$currency} " . formatNumber($this->getTotalWithdraw()) . "*",
        ];

        $this->content = implode("\r\n", array_merge($contents, $this->whatsappFooter()));
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('header.bonus.summary.info'))
            ->view('bonuses.mail.member-summary', [
                'user' => $this->model,
                'totalWithdraw' => $this->getTotalWithdraw()
            ]);
    }
}
