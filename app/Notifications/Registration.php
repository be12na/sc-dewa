<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;

class Registration extends BaseNotification
{
    protected function setWhatsappContent(): void
    {
        $user = $this->model;
        // $referral = $user->referral;
        $appName = config('app.name');

        $contents = [
            "*" . __('header.member.registration-info') . "*",
            "",
            __('text.welcome', ['member' => "*{$user->name}*."]),
            __('text.thank-to-join', ['appname' => "*{$appName}*."]),
            __('text.here-your-info') . ':',
            "",
            "*" . __('header.member.private-data') . "*",
            __('label.user.name') . ": {$user->name}",
            __('label.user.email') . ": {$user->email}",
            __('label.user.phone') . ": {$user->phone}",
            __('label.user.username') . ": {$user->username}",
        ];

        if (!empty($this->optionalData) && array_key_exists('password', $this->optionalData)) {
            $contents[] = __('label.password.text') . ': ' . $this->optionalData['password'];
        }

        // if ($referral) {
        //     $contents = array_merge($contents, [
        //         "",
        //         "*" . __('header.member.referral-data') . "*",
        //         __('label.user.name') . ": {$referral->name} ({$referral->username})",
        //         __('label.user.phone') . ": {$referral->phone}",
        //     ]);
        // }

        $this->content = implode("\r\n", array_merge($contents, $this->whatsappFooter()));
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $viewParams = array_merge([
            'user' => $this->model,
        ], $this->optionalData);

        return (new MailMessage)
            ->subject(__('header.member.registration-info'))
            ->view('register.mail', $viewParams);
    }
}
