<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BaseNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected Model $model;
    protected array $sendTo;
    protected string|null $driverData;
    protected array $optionalData;

    // for whatsapp
    public $content;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Model $model, string $sendTo, string $driverData = null, array $optionalData = null)
    {
        $this->model = $model;
        $this->driverData = $driverData;
        $this->optionalData = is_null($optionalData) ? [] : $optionalData;

        if ($sendTo == 'whatsapp') $sendTo = 'onesender';

        $this->sendTo = [$sendTo];

        if (in_array('onesender', $this->sendTo)) $this->setWhatsappContent();
    }

    protected function whatsappFooter(): array
    {
        return [
            "",
            '*' . config('app.name') . '*',
        ];
    }

    protected function setWhatsappContent(): void
    {
        $contents = $this->whatsappFooter();
        array_shift($content);

        $this->content = implode("\r\n", $contents);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->sendTo;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $result = [
            'driver' => $this->driverData,
            'model' => get_class($this->model),
            'model_id' => $this->model->id,
        ];

        if (!empty($this->optionalData)) $result = array_merge($result, $this->optionalData);

        return $result;
    }
}
