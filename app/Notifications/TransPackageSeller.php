<?php

namespace App\Notifications;

use App\Mail\TransOrderPackageMail;

class TransPackageSeller extends TransPackage
{
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new TransOrderPackageMail($this->model, $this->model->is_company_seller ? 'admin' : 'sponsor'))
            ->subject($this->getSubject())
            ->to($this->model->seller_email);
    }
}
