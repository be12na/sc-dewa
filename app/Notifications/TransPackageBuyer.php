<?php

namespace App\Notifications;

use App\Mail\TransOrderPackageMail;

class TransPackageBuyer extends TransPackage
{
    protected function setWhatsappContent(): void
    {
        $transPackage = $this->model;
        $package = $transPackage->package;

        $contents = [
            $this->getSubject(true),
            '',
            '*' . __('label.package.title') . '*',
            __('label.trans-no') . ": {$transPackage->code}",
            "Jenis: {$transPackage->trans_type_name}",
            __('label.package.name') . ": {$package->name}",
            __('label.package.price') . ": " . __('format.currency.symbol.text') . " " . formatNumber($transPackage->package_price),
            __('label.package.unique-digit') . ": " . formatNumber($transPackage->unique_digit),
            __('label.package.total-transfer') . ": " . __('format.currency.symbol.text') . " " . formatNumber($transPackage->package_price + $transPackage->unique_digit),
        ];

        $sellerBanks = [];

        if ($transPackage->seller_banks->isNotEmpty()) {
            $sellerBanks = [
                "",
                __('text.please-transfer-to') . ':',
            ];
            $bankUrut = 0;
            foreach ($transPackage->seller_banks as $userBank) {
                $bankUrut++;

                $sellerBanks[] = "{$bankUrut}. {$userBank->bank_code} - {$userBank->acc_no} a/n: {$userBank->acc_name}";
            }
        }

        $confirm = [
            "",
            __('text.confirm-whatsapp', ['whatsapp' => $transPackage->seller_phone]),
        ];

        $this->content = implode("\r\n", array_merge($contents, $sellerBanks, $confirm, $this->whatsappFooter()));
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new TransOrderPackageMail($this->model, 'member'))
            ->subject($this->getSubject())
            ->to($this->model->buyer->email);
    }
}
