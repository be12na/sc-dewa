<?php

namespace App\Notifications;

class TransPackage extends BaseNotification
{
    protected function getSubject(bool $forWhatsapp = false): string
    {
        $result = __('header.package.purchase-info', ['attr' => $this->model->trans_type_name]);

        return $forWhatsapp ? "*{$result}*" : $result;
    }
}
