<?php

namespace App\Notifications;

use App\Mail\TransConfirmPackageMail;

class TransPackageConfirmed extends TransPackage
{
    protected function setWhatsappContent(): void
    {
        $transPackage = $this->model;
        $package = $transPackage->package;
        $buyer = $transPackage->buyer;

        $contents = [
            $this->getSubject(true),
            '',
            "Hi... *{$buyer->name}* ({$buyer->username})",
        ];

        if ($transPackage->is_repeat_order) {
            $contents[] = __('message.mail.repeat-order');
        } elseif ($transPackage->is_upgrade) {
            $contents[] = __('message.mail.upgraded') . ' ' . __('text.to-be') . ' *' . $package->name . '*';
        } else {
            $contents[] = __('message.mail.activated') . ' ' . __('text.as') . ' *' . $package->name . '*';
        }

        $this->content = implode("\r\n", array_merge($contents, $this->whatsappFooter()));
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new TransConfirmPackageMail($this->model))
            ->subject($this->getSubject())
            ->to($this->model->buyer->email);
    }
}
