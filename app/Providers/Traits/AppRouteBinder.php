<?php

namespace App\Providers\Traits;

use Illuminate\Support\Facades\Route;

trait AppRouteBinder
{
    protected function routeModelBinding(): void
    {
        Route::bind('referralUser', function ($value) {
            return fallbackRouteBinding(
                \App\Models\User::query()
                    ->byUsername($value)
                    ->whereHas('package')
                    ->whereHas('startPackage', function ($transPkg) {
                        return $transPkg->byTransStatus(TRANS_CONFIRMED);
                    })
                    ->first(),
                __('message.not-found'),
                'login'
            );
        });

        Route::bind('userMember', function ($value) {
            return \App\Models\User::query()->byMemberUser()->byId($value)->first();
        });

        Route::bind('userBank', function ($value) {
            return fallbackRouteBinding(\App\Models\UserBank::query()->byId($value)->first());
        });

        Route::bind('package', function ($value) {
            return fallbackRouteBinding(\App\Models\Package::query()->byId($value)->first());
        });

        Route::bind('bonusLevel', function ($value) {
            return fallbackRouteBinding(\App\Models\BonusLevel::query()->byId($value)->first());
        });

        Route::bind('transPackage', function ($value) {
            return fallbackRouteBinding(\App\Models\TransPackage::query()->byId($value)->first());
        });

        Route::bind('member', function ($value) {
            return fallbackRouteBinding(\App\Models\User::query()->byId($value)->byMemberUser()->first());
        });

        Route::bind('reward', function ($value) {
            return fallbackRouteBinding(\App\Models\Reward::query()->byId($value)->first());
        });

        Route::bind('userReward', function ($value) {
            return fallbackRouteBinding(\App\Models\UserReward::query()->byId($value)->first());
        });
    }
}
