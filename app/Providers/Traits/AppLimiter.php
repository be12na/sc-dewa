<?php

namespace App\Providers\Traits;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;

trait AppLimiter
{
    protected function configureRateLimiting(): void
    {
        RateLimiter::for('byIP100', function (Request $request) {
            return Limit::perMinute(100)->by($request->ip())->response(function () {
                return response('Too many request', 429);
            });
        });

        RateLimiter::for('byIP200', function (Request $request) {
            return Limit::perMinute(200)->by($request->ip())->response(function () {
                return response('Too many request', 429);
            });
        });

        RateLimiter::for('bySession20', function (Request $request) {
            return Limit::perMinute(20)->by($request->session_id)->response(function () {
                return response('Too many request', 429);
            });
        });

        RateLimiter::for('bySession50', function (Request $request) {
            return Limit::perMinute(50)->by($request->session_id)->response(function () {
                return response('Too many request', 429);
            });
        });

        RateLimiter::for('bySession100', function (Request $request) {
            return Limit::perMinute(100)->by($request->session_id)->response(function () {
                return response('Too many request', 429);
            });
        });

        RateLimiter::for('bySession200', function (Request $request) {
            return Limit::perMinute(200)->by($request->session_id)->response(function () {
                return response('Too many request', 429);
            });
        });
    }
}
