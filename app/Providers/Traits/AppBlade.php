<?php

namespace App\Providers\Traits;

use Illuminate\Support\Facades\Blade;

trait AppBlade
{
    protected function bladeDirective(): void
    {
        Blade::directive('menuActive', function ($routes) {
            return "<?php echo matchRoute($routes) ? 'active' : ''; ?>";
        });

        Blade::directive('menuCollapsed', function ($routes) {
            return "<?php echo !matchRoute($routes) ? 'collapsed' : 'active'; ?>";
        });

        Blade::directive('menuShow', function ($routes) {
            return "<?php echo matchRoute($routes) ? 'show' : ''; ?>";
        });

        Blade::directive('translate', function ($key) {
            return "<?php echo selfTranslate($key); ?>";
        });

        Blade::directive('formatNumber', function ($value) {
            return "<?php echo formatNumber($value); ?>";
        });

        Blade::directive('inputNumberStep', function ($usePercent) {
            return "<?php echo 1 / ((($usePercent === true) && (PERCENT_DECIMAL > 0)) ? pow(10, PERCENT_DECIMAL) : 1); ?>";
        });

        Blade::directive('contentCheck', function ($isTrue) {
            return "<?php echo contentCheck($isTrue); ?>";
        });

        Blade::directive('optionSelected', function ($value) {
            return "<?php echo optionSelected($value); ?>";
        });

        Blade::directive('checked', function ($value) {
            return "<?php echo checkboxChecked($value); ?>";
        });
    }

    protected function bladeIf(): void
    {
        Blade::if('hasGroup', function ($group) {
            return hasGroup($group);
        });

        Blade::if('hasRole', function ($role) {
            return hasRole($role);
        });
    }
}
