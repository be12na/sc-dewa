<?php

namespace App\Providers;

use App\Providers\Traits\AppBlade;
use App\Providers\Traits\AppLimiter;
use App\Providers\Traits\AppRouteBinder;
use Illuminate\Support\ServiceProvider;

class MyAppServiceProvider extends ServiceProvider
{
    use AppLimiter, AppRouteBinder, AppBlade;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();
        $this->routeModelBinding();
        $this->bladeDirective();
        $this->bladeIf();
    }
}
