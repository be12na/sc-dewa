<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function () {
            $sharedVars = [
                'fileV' => mt_rand(100001, 999999),
            ];

            if (auth()->check()) {
                $sharedVars['authUser'] = auth()->user();
            }

            view()->share($sharedVars);
        });
    }
}
