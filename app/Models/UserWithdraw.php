<?php

namespace App\Models;

use App\Models\Traits\ModelId;
use App\Models\Traits\ModelTransStatus;
use App\Models\Traits\ModelUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserWithdraw extends Model
{
    use HasFactory;
    use ModelId, ModelUser, ModelTransStatus;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'wd_code',
        'user_id',
        'wd_date',
        'bank_code',
        'bank_name',
        'bank_acc_no',
        'bank_acc_name',
        'wd_bonus_type',
        'total_bonus',
        'fee',
        'total_transfer',
        'status',
        'status_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'wd_date' => 'date',
        'wd_bonus_type' => 'integer',
        'total_bonus' => 'float',
        'fee' => 'float',
        'total_transfer' => 'float',
        'status' => 'integer',
        'status_at' => 'datetime',
    ];

    // relationship
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function bonuses()
    {
        return $this->hasMany(UserBonus::class, 'wd_id', 'id');
    }
    // relationship:end

    // scope
    public function scopeByCode(Builder $builder, string $code): Builder
    {
        return $builder->where('wd_code', '=', $code);
    }

    public function scopeByBonusType(Builder $builder, array|int $type): Builder
    {
        if (is_array($type)) return $builder->whereIn('wd_bonus_type', $type);

        return $builder->where('wd_bonus_type', '=', $type);
    }

    public function scopeBonusSponsor(Builder $builder): Builder
    {
        return $builder->byBonusType(BONUS_SPONSOR);
    }

    public function scopeBonusExtra(Builder $builder): Builder
    {
        return $builder->byBonusType(BONUS_EXTRA);
    }

    public function scopeBonusPair(Builder $builder): Builder
    {
        return $builder->byBonusType(BONUS_PAIR);
    }

    public function scopeBonusLevel(Builder $builder): Builder
    {
        return $builder->byBonusType(BONUS_LEVEL);
    }
    // scope:end

    // static
    public static function makeCode(int $type, Carbon|string $date, array $exists = null): string
    {
        $cabonDate = ($date instanceof Carbon) ? $date : Carbon::createFromTimeString($date);
        $dateFormat = $cabonDate->format('ymd');

        $wdType = '';
        switch ($type) {
            case BONUS_SPONSOR:
                $wdType = 'SP';
                break;
            case BONUS_EXTRA:
                $wdType = 'EX';
                break;
            case BONUS_PAIR:
                $wdType = 'MT';
                break;
            case BONUS_LEVEL:
                $wdType = 'LV';
                break;
            case BONUS_RO:
                $wdType = 'RO';
                break;
            case BONUS_UPGRADE:
                $wdType = 'UP';
                break;
        }

        $random = str_pad(mt_rand(123456, 987654), 6, '0', STR_PAD_LEFT);
        $code = "WD{$wdType}-{$dateFormat}{$random}";

        if (is_null($exists)) {
            $wdExists = static::query()->byCode($code)->first();

            if ($wdExists) return static::makeCode($type, $date, $exists);
        } else {
            if (in_array($code, $exists)) return static::makeCode($type, $date, $exists);
        }

        return $code;
    }
    // static:end
}
