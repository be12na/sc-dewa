<?php

namespace App\Models;

use App\Models\Traits\ModelId;
use App\Models\Traits\ModelTransStatus;
use App\Models\Traits\ModelUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserReward extends Model
{
    use HasFactory;
    use ModelId, ModelUser, ModelTransStatus;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'reward_id',
        'point_left',
        'point_right',
        'reward_value',
        'reward_left',
        'reward_right',
        'status',
        'status_at',
        'reject_note',
        'ro_id',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'user_left' => 'integer',
        'user_right' => 'integer',
        'reward_left' => 'integer',
        'reward_right' => 'integer',
        'status' => 'integer',
        'status_at' => 'datetime',
    ];

    // relationship
    public function reward()
    {
        return $this->belongsTo(Reward::class, 'reward_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function transRO()
    {
        return $this->belongsTo(TransPackage::class, 'ro_id', 'id')->byRO();
    }
    // relationship:end

    // static
    public static function getCode($userReward): string
    {
        $id = str_pad(strval($userReward->id), 5, '0', STR_PAD_LEFT);
        $userId = str_pad(strval($userReward->user_id), 5, '0', STR_PAD_LEFT);
        $date = Carbon::createFromTimeString($userReward->created_at)->format('ymd');
        $pkgId = $userReward->package_id;

        return "R{$pkgId}-{$date}{$userId}{$id}";
    }
    // static:end

    // accessor
    public function getCodeAttribute()
    {
        return UserReward::getCode($this);
    }

    public function getPackageIdAttribute()
    {
        return $this->reward->package_id;
    }

    public function getCompletedAttribute()
    {
        return ($this->status == WD_COMPLETE);
    }

    public function getRejectedAttribute()
    {
        return ($this->status == WD_REJECT);
    }

    public function getProcessingAttribute()
    {
        return ($this->status == WD_PROCESSING);
    }
    // accessor:end
}
