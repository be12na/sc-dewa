<?php

namespace App\Models;

use App\Models\Traits\ModelActive;
use App\Models\Traits\ModelId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    use HasFactory;
    use ModelId, ModelActive;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'package_id',
        'value',
        'left',
        'right',
        'repeatable',
        'is_active',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'left' => 'integer',
        'right' => 'integer',
        'repeatable' => 'boolean',
        'is_active' => 'boolean',
    ];

    // relationship
    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }
    // relationship:end

    // scope
    public function scopeByPackage(Builder $builder, Package|int $package): Builder
    {
        return $builder->where('package_id', '=', ($package instanceof Package) ? $package->id : $package);
    }
    // scope:end
}
