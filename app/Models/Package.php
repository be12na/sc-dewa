<?php

namespace App\Models;

use App\Models\Traits\ModelActive;
use App\Models\Traits\ModelId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    use ModelId, ModelActive;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'pin_type_id',
        'pin_count',
        'price',
        'bonus_sponsor',
        'bonus_extra',
        'bonus_ro',
        'bonus_upgrade',
        'bonus_package',
        'package_left',
        'package_right',
        'package_bonus',
        'pair_point',
        'get_reward',
        'give_reward',
        'reward_point',
        'is_active',
        'logo',
        'description',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'pin_count' => 'integer',
        'price' => 'float',
        'bonus_sponsor' => 'float',
        'bonus_extra' => 'float',
        'bonus_ro' => 'float',
        'bonus_upgrade' => 'float',
        'bonus_package' => 'boolean',
        'package_left' => 'integer',
        'package_right' => 'integer',
        'pair_point' => 'integer',
        'get_reward' => 'boolean',
        'give_reward' => 'boolean',
        'reward_point' => 'integer',
        'is_active' => 'boolean',
    ];

    // relationship
    public function pinType()
    {
        return $this->belongsTo(PinType::class, 'pin_type_id', 'id')
            ->byType(
                isPackageRequirePIN()
                    ? (PIN_GLOBAL ? PIN_TYPE_GLOBAL : PIN_TYPE_PACKAGE)
                    : PIN_TYPE_PACKAGE
            );
    }
    // relationship:end

    // scope
    public function scopeByName(Builder $builder, string $name): Builder
    {
        return $builder->where('name', '=', $name);
    }

    public function scopeByPlan(Builder $builder, int $plan): Builder
    {
        return $builder->whereHas('pinType', function ($pinType) use ($plan) {
            return $pinType->byPlan($plan);
        });
    }

    public function scopeByStart(Builder $builder): Builder
    {
        return $builder->where('id', '<', TOP_PACKAGE);
    }

    public function scopeByUpgrade(Builder $builder, self|int $current = null): Builder
    {
        $currendId = is_null($current) ? 0 : (($current instanceof self) ? $current->id : $current);

        return $builder->where('id', '>', $currendId);
    }
    // scope:end

    // static
    public static function isRequirePIN()
    {
        return (PACKAGE_USE_PIN && PIN_ENABLED);
    }

    public static function activePackages(int $plan = null): Collection
    {
        $query = static::query()->byActive();

        if (!empty($plan)) {
            $query = $query->byPlan($plan);
        }

        return $query->get();
    }
    // static:end

    // accessor
    public function getActualBonusSponsorAttribute()
    {
        // if (!BONUS_SPONSOR_BY_PACKAGE) {
        //     return BonusSponsor::bonusAmount();
        // }

        return $this->bonus_sponsor;
    }

    public function getActualPriceAttribute()
    {
        return static::isRequirePIN()
            ? $this->pinType->price * $this->pin_count
            : $this->price;
    }

    public function getImageLogoAttribute()
    {
        return asset('assets/images/' . ($this->logo ?? 'logo/logo.png'));
    }
    // accessor:end
}
