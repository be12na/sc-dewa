<?php

namespace App\Models;

use App\Casts\UppercaseCast;
use App\Models\Traits\ModelCode;
use App\Models\Traits\ModelId;
use App\Models\Traits\ModelTransStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransPackage extends Model
{
    use HasFactory;
    use ModelId, ModelCode, ModelTransStatus;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'seller_id',
        'buyer_id',
        'package_id',
        'pin_count',
        'trans_type',
        'package_price',
        'unique_digit',
        'status',
        'status_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'code' => UppercaseCast::class,
        'pin_count' => 'integer',
        'trans_type' => 'integer',
        'package_price' => 'float',
        'unique_digit' => 'integer',
        'status' => 'integer',
        'status_at' => 'datetime',
    ];

    // static
    public static function makeCode(int $type): string
    {
        $typePrefix = ($type == TRANS_PKG_RO)
            ? 'R'
            : (($type == TRANS_PKG_UPGRADE)
                ? 'U' : 'A');

        return $typePrefix . date('ymd') . mt_rand(100001, 999999);
    }

    public static function statusTextAdmin(int $status): string
    {
        // if ($status == TRANS_ORDER) return __('label.status.pending');
        if ($status == TRANS_CONFIRMED) return __('label.status.complete');
        if ($status == TRANS_CANCELED) return __('label.status.canceled');
        if ($status == TRANS_REJECTED) return __('label.status.rejected');

        return __('label.status.confirm');
    }

    public static function statusTextMember(int $status): string
    {
        if ($status == TRANS_CONFIRMED) return __('label.status.complete');
        if ($status == TRANS_CANCELED) return __('label.status.canceled');

        $text = __('label.status.confirm');
        if ($status == TRANS_REJECTED) $text = __('label.status.rejected') . ' / ' . $text;

        return $text;
    }

    public static function statusTextColor(int $status): string
    {
        if ($status == TRANS_ORDER) return 'yellow-700';
        if ($status == TRANS_CONFIRMED) return 'success';
        if ($status == TRANS_CANCELED) return 'red-300';
        if ($status == TRANS_REJECTED) return 'red-300';

        return 'cyan-300';
    }
    // static:end

    // relationship
    public function buyer()
    {
        return $this->belongsTo(User::class, 'buyer_id', 'id');
    }

    public function seller()
    {
        return $this->belongsTo(User::class, 'seller_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }

    public function bonusRO()
    {
        return $this->hasMany(UserBonus::class, 'ro_id', 'id')->bonusRO();
    }

    public function bonusUpgrade()
    {
        return $this->hasMany(UserBonus::class, 'ro_id', 'id')->bonusUpgrade();
    }

    public function bonusReward()
    {
        return $this->hasOne(UserReward::class, 'ro_id', 'id')->byTransStatus([WD_PROCESSING, WD_COMPLETE]);
    }
    // relationship:end

    // scope
    public function scopeByTransType(Builder $builder, int|array $type): Builder
    {
        if (!is_array($type)) $type = [$type];

        return $builder->whereIn('trans_type', $type);
    }

    public function scopeByStarter(Builder $builder): Builder
    {
        return $builder->byTransType(TRANS_PKG_START);
    }

    public function scopeByUpgrade(Builder $builder): Builder
    {
        return $builder->byTransType(TRANS_PKG_UPGRADE);
    }

    public function scopeByRO(Builder $builder): Builder
    {
        return $builder->byTransType(TRANS_PKG_RO);
    }

    public function scopeByNotRO(Builder $builder): Builder
    {
        return $builder->byTransType([TRANS_PKG_START, TRANS_PKG_UPGRADE]);
    }

    public function scopeByConfirmed(Builder $builder): Builder
    {
        return $builder->byTransStatus(TRANS_CONFIRMED);
    }
    // scope:end

    // accessor
    public function getTransConfirmedAttribute()
    {
        return ($this->status == TRANS_CONFIRMED);
    }

    public function getTransRejectedAttribute()
    {
        return ($this->status == TRANS_REJECTED);
    }

    public function getTransOrderAttribute()
    {
        return ($this->status == TRANS_ORDER);
    }

    public function getTransTransferredAttribute()
    {
        return ($this->status == TRANS_TRANSFERRED);
    }

    public function getTransCancelAttribute()
    {
        return ($this->status == TRANS_CANCELED);
    }

    public function getStatusTextMemberAttribute()
    {
        // if ($this->trans_confirmed) return __('label.status.complete');
        // if ($this->trans_canceled) return __('label.status.canceled');

        // $text = __('label.status.confirm');
        // if ($this->trans_rejected) $text = __('label.status.rejected') . ' / ' . $text;

        // return $text;

        return TransPackage::statusTextMember($this->status);
    }

    public function getStatusTextAdminAttribute()
    {
        // if ($this->trans_order) return __('label.status.pending');
        // if ($this->trans_confirmed) return __('label.status.complete');
        // if ($this->trans_canceled) return __('label.status.canceled');
        // if ($this->trans_rejected) return __('label.status.rejected');

        // return __('label.status.confirm');

        return TransPackage::statusTextAdmin($this->status);
    }

    public function getStatusColorAttribute()
    {
        return TransPackage::statusTextColor($this->status);
    }

    public function getIsCompanySellerAttribute()
    {
        return ($this->seller_id == 0);
    }

    public function getSellerBanksAttribute()
    {
        $seller = !$this->is_company_seller ? $this->seller : User::query()->byId(1)->first();

        return $seller->user_banks;
    }

    public function getIsUpgradeAttribute()
    {
        return ($this->trans_type == TRANS_PKG_UPGRADE);
    }

    public function getIsActivateAttribute()
    {
        return ($this->trans_type == TRANS_PKG_START);
    }

    public function getIsRepeatOrderAttribute()
    {
        return ($this->trans_type == TRANS_PKG_RO);
    }

    public function getSellerNameAttribute()
    {
        if ($this->is_company_seller) {
            return config('app.name');
        }

        return $this->seller->name;
    }

    public function getSellerUsernameAttribute()
    {
        if ($this->is_company_seller) {
            return '';
        }

        return $this->seller->username;
    }

    public function getSellerPhoneAttribute()
    {
        if ($this->is_company_seller) {
            return env('CONTACT_CENTER_PHONE');
        }

        return $this->seller->phone;
    }

    public function getSellerEmailAttribute()
    {
        if ($this->is_company_seller) {
            return env('CONTACT_CENTER_EMAIL');
        }

        return $this->seller->email;
    }

    public function getTransTypeNameAttribute()
    {
        return $this->is_repeat_order
            ? 'Repeat Order'
            : ($this->is_upgrade ? 'Upgrade' : 'Aktivasi');
    }
    // accessor:end
}
