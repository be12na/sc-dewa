<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Models\Traits\ModelId;
use App\Models\Traits\User\AuthSession;
use App\Models\Traits\User\UserBank;
use App\Models\Traits\User\UserBonuses;
use App\Models\Traits\User\UserPackage;
use App\Models\Traits\User\UserStatus;
use App\Models\Traits\User\UserType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use ModelId;
    use AuthSession, UserType, UserStatus, UserBank, UserPackage, UserBonuses;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'user_type',
        'user_status',
        'status_note',
        'user_login',
        'referral_id',
        'package_id',
        'has_profile',
        'address',
        'pos_code',
        'phone',
        'registered_at',
        'inactive_at',
        'activated_at',
        'banned_at',
        'session_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'session_id',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'registered_at' => 'datetime',
        'inactive_at' => 'datetime',
        'activated_at' => 'datetime',
        'banned_at' => 'datetime',
        'has_profile' => 'boolean',
        'user_login' => 'boolean',
        'package_id' => 'integer',
        'user_type' => 'integer',
        'user_status' => 'integer',
    ];

    // relationship
    public function structure()
    {
        return $this->hasOne(UserStructure::class, 'user_id', 'id');
    }

    public function referral()
    {
        return $this->belongsTo(static::class, 'referral_id', 'id');
    }

    public function sponsored()
    {
        return $this->hasMany(static::class, 'referral_id', 'id');
    }
    // relationship:end

    // scope
    public function scopeByUsername(Builder $builder, string $username): Builder
    {
        return $builder->where('username', '=', $username);
    }

    public function scopeByHasProfile(Builder $builder): Builder
    {
        return $builder->where('has_profile', '=', true);
    }

    public function scopeByReferral(Builder $builder, self|int $referral): Builder
    {
        return $builder->where('referral_id', '=', is_int($referral) ? $referral : $referral->id);
    }
    // scope:end

    // accessor
    public function getChildLeftAttribute()
    {
        if (!$this->is_member_user) return null;

        return $this->structure ? $this->structure->childLeft : null;
    }

    public function getCountLeftAttribute()
    {
        if (!$this->is_member_user) return 0;

        return $this->structure ? $this->structure->count_left : 0;
    }

    public function getChildRightAttribute()
    {
        if (!$this->is_member_user) return null;

        return $this->structure ? $this->structure->childRight : null;
    }

    public function getCountRightAttribute()
    {
        if (!$this->is_member_user) return 0;

        return $this->structure ? $this->structure->count_right : 0;
    }

    public function getCountMemberAttribute()
    {
        if (!$this->is_member_user) return 0;

        return $this->structure ? $this->structure->countDescendants() : 0;
    }
    // accessor:end
}
