<?php

namespace App\Models;

use Franzose\ClosureTable\Models\ClosureTable;

class UserStructureClosure extends ClosureTable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_structure_closure';
}
