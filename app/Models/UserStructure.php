<?php

namespace App\Models;

use Franzose\ClosureTable\Models\Entity;
use Illuminate\Database\Eloquent\Builder;

class UserStructure extends Entity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_structures';

    /**
     * ClosureTable model instance.
     *
     * @var \App\Models\UserStructureClosure
     */
    protected $closure = 'App\Models\UserStructureClosure';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'side_position',
    ];

    // relationship
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->with('package');
    }

    public function childLeft()
    {
        return $this->hasOne(get_class($this), $this->getParentIdColumn())->byLeftSide();
    }

    public function childRight()
    {
        return $this->hasOne(get_class($this), $this->getParentIdColumn())->byRightSide();
    }
    // relationship:end

    // scope
    public function scopeByLeftSide(Builder $builder): Builder
    {
        return $builder->where('side_position', '=', USER_LEFT_POSITION);
    }

    public function scopeByRightSide(Builder $builder): Builder
    {
        return $builder->where('side_position', '=', USER_RIGHT_POSITION);
    }

    public function scopeByDescendantsPackage(Builder $builder, Package|int $package): Builder
    {
        return $builder->whereHas('user', function ($user) use ($package) {
            return $user->byPackage($package);
        });
    }
    // scope:end

    // accessor
    public function getLeftMembersAttribute()
    {
        return $this->childLeft
            ? $this->childLeft->descendantsWithSelf()->with('user', function ($user) {
                return $user
                    ->with('package')
                    ->with('startPackage', function ($transStart) {
                        return $transStart->byConfirmed();
                    })
                    ->with('transUpgrade', function ($transUpgrade) {
                        return $transUpgrade->byConfirmed();
                    })
                    ->with('transRO', function ($order) {
                        return $order->byConfirmed();
                    });
            })->get()
            : collect();
    }

    public function getRightMembersAttribute()
    {
        return $this->childRight
            ? $this->childRight->descendantsWithSelf()->with('user', function ($user) {
                return $user
                    ->with('package')
                    ->with('startPackage', function ($transStart) {
                        return $transStart->byConfirmed();
                    })
                    ->with('transUpgrade', function ($transUpgrade) {
                        return $transUpgrade->byConfirmed();
                    })
                    ->with('transRO', function ($transRO) {
                        return $transRO->byConfirmed();
                    });
            })->get()
            : collect();
    }

    public function getCountLeftAttribute()
    {
        return $this->left_members->count();
    }

    public function getCountRightAttribute()
    {
        return $this->right_members->count();
    }
    // accessor:end
}
