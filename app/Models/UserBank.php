<?php

namespace App\Models;

use App\Models\Traits\ModelActive;
use App\Models\Traits\ModelId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    use HasFactory;
    use ModelId, ModelActive;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'bank_id',
        'acc_no',
        'acc_name',
        'is_active',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    // relation
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id', 'id');
    }

    public function together()
    {
        return $this->hasMany(static::class, 'acc_no', 'acc_no');
    }
    // relation:end

    // scope
    public function scopeByCompanyBank(Builder $builder): Builder
    {
        return $builder->where('user_id', '=', 0);
    }

    public function scopeByMemberBank(Builder $builder, int $memberId = null): Builder
    {
        $builder = $builder->where('user_id', '>', 0);

        if (!is_null($memberId)) return $builder->where('user_id', '=', $memberId);

        return $builder;
    }
    // scope:end

    // static
    public static function setNewAccount(array $values): void
    {
        $isCompany = ($values['user_id'] == 0);

        if (!$isCompany) {
            static::query()->byMemberBank($values['user_id'])->update(['is_active' => false]);
        }

        $values['is_active'] = true;

        static::create($values);
    }
    // static:end

    // public
    public function setStatus(bool $status): void
    {
        $values = ['is_active' => $status];

        if (!$this->is_company_bank && $status) {
            static::query()->byMemberBank($this->user_id)
                ->where('id', '!=', $this->id)
                ->update(['is_active' => false]);
        }

        $this->update($values);
    }
    // public:end

    // accessor
    public function getBankCodeAttribute()
    {
        return optional($this->bank)->code;
    }

    public function getBankNameAttribute()
    {
        return optional($this->bank)->name;
    }

    public function getIsCompanyBankAttribute()
    {
        return ($this->user_id == 0);
    }

    public function getUserNameAttribute()
    {
        return $this->is_company_bank ? config('app.name') : optional($this->user)->name;
    }
    // accessor:end
}
