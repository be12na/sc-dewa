<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BonusSponsor extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'bonus',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'bonus' => 'float',
    ];

    // static
    public static function setting(): self|null
    {
        return static::query()->first();
    }

    public static function bonusAmount(self $row = null): float
    {
        if (BONUS_SPONSOR_BY_PACKAGE) return 0;

        return optional($row ?? static::setting())->bonus ?? 0;
    }

    public static function updateBonus(array $values): void
    {
        $data = static::setting();

        if ($data) $data->delete();

        static::create($values);
    }
    // static:end
}
