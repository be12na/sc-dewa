<?php

namespace App\Models;

use App\Models\Traits\ModelId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BonusLevel extends Model
{
    use HasFactory, SoftDeletes;
    use ModelId;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'package_id',
        'level',
        'bonus',
        'created_by',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'level' => 'integer',
        'bonus' => 'float',
    ];

    // relationship
    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }
    // relationship:end

    // scope
    public function scopeByPackage(Builder $builder, Package|int $package): Builder
    {
        $packageId = ($package instanceof Package) ? $package->id : $package;

        return $builder->where('package_id', '=', $packageId);
    }
    // scope:end

    // static
    public static function getQueryByPackage(Package|int $package = null): Builder
    {
        $packageId = BONUS_LEVEL_PER_PACKAGE
            ? (is_null($package) ? -1 : (
                ($package instanceof Package) ? $package->id : $package
            ))
            : 0;

        return static::query()->byPackage($packageId);
    }

    public static function setBonusLevel($values, self $bonusLevel = null): void
    {
        if (!is_null($bonusLevel)) $bonusLevel->delete();

        static::create($values);
    }
    // static:end
}
