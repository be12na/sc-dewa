<?php

namespace App\Models\Traits;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

trait ModelUser
{
    public function scopeByUser(Builder $builder, User|int $user, string $userField = null): Builder
    {
        if (is_null($userField)) $userField = 'user_id';

        return $builder->where($userField, '=', ($user instanceof User) ? $user->id : $user);
    }
}
