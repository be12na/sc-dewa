<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait ModelTransStatus
{
    // scope
    public function scopeByTransStatus(Builder $builder, array|int $value, bool $not = null): Builder
    {
        if (is_null($not)) $not = false;

        if (is_array($value)) {
            return empty($value)
                ? $builder
                : ($not ? $builder->whereNotIn('status', $value) : $builder->whereIn('status', $value));
        }

        return $builder->where('status', $not ? '!=' : '=', $value);
    }
    // scope:end
}
