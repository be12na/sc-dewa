<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait ModelActive
{
    public function scopeByActive(Builder $builder, bool $isActive = null): Builder
    {
        if (is_null($isActive)) $isActive = true;

        return $builder->where('is_active', '=', $isActive);
    }
}
