<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait ModelCode
{
    public function scopeByCode(Builder $builder, string $value, string $codeColumn = null): Builder
    {
        if (empty($codeColumn)) $codeColumn = 'code';

        return $builder->where($codeColumn, '=', $value);
    }
}
