<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait ModelId
{
    public function scopeById(Builder $builder, array|int $value): Builder
    {
        if (is_array($value)) {
            if (empty($value)) $value = [0];

            return $builder->whereIn('id', $value);
        }

        return $builder->where('id', '=', $value);
    }
}
