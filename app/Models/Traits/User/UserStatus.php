<?php

namespace App\Models\Traits\User;

use Illuminate\Database\Eloquent\Builder;

trait UserStatus
{
    // scope
    public function scopeByUserStatus(Builder $builder, array|int $userStatus): Builder
    {
        if (!is_array($userStatus)) $userStatus = [$userStatus];

        return $builder->whereIn('user_status', $userStatus);
    }

    public function scopeByStatusActivated(Builder $builder): Builder
    {
        return $builder->byUserStatus(USER_STATUS_ACTIVATED);
    }
    // scope:end

    // accessor
    public function getUserActivatedAttribute()
    {
        return ($this->user_status == USER_STATUS_ACTIVATED);
    }

    public function getIsUserRegisterAttribute()
    {
        return ($this->user_status == USER_STATUS_REGISTERED);
    }

    public function getUserInactiveAttribute()
    {
        return ($this->user_status == USER_STATUS_INACTIVE);
    }

    public function getUserBannedAttribute()
    {
        return ($this->user_status == USER_STATUS_BANNED);
    }
    // accessor:end
}
