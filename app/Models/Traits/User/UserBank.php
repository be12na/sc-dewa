<?php

namespace App\Models\Traits\User;

use App\Models\UserBank as ModelsUserBank;

trait UserBank
{
    // relationship
    public function banks()
    {
        return $this->hasMany(ModelsUserBank::class, 'user_id', 'id')->with('bank');
    }

    public function memberActiveBank()
    {
        return $this->hasOne(ModelsUserBank::class, 'user_id', 'id')
            ->byActive()
            ->with('bank');
    }
    // relationship:end

    // accessor
    public function getUserBanksAttribute()
    {
        $userBanks = $this->isGroup('admin')
            ? ModelsUserBank::query()->where('user_id', '=', 0)->with('bank')->get()
            : $this->banks;

        return $userBanks->sortBy([['is_active', 'desc'], ['bank.code', 'asc'], ['acc_no', 'asc'], ['id', 'asc']])->values();
    }

    public function getActiveBankAttribute()
    {
        $userBanks = $this->user_banks->where('is_active', '=', true)->values()->sortBy('id')->values();

        return $userBanks->first();
    }

    public function getHasActiveBankAttribute()
    {
        return !empty($this->active_bank);
    }
    // accessor:end
}
