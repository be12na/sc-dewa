<?php

namespace App\Models\Traits\User;

use App\Models\Package;
use App\Models\Reward;
use App\Models\UserBonus;
use App\Models\UserReward;
use App\Models\UserWithdraw;
use Illuminate\Support\Collection;
use stdClass;

trait UserBonuses
{
    // relationship
    public function bonuses()
    {
        $inTypes = [BONUS_SPONSOR];
        if (BONUS_EXTRA_ENABLED) $inTypes[] = BONUS_EXTRA;
        if (BONUS_PAIR_ENABLED) $inTypes[] = BONUS_PAIR;
        if (BONUS_LEVEL_ENABLED) $inTypes[] = BONUS_LEVEL;
        if (BONUS_RO_ENABLED) $inTypes[] = BONUS_RO;
        if (BONUS_UPGRADE_ENABLED) $inTypes[] = BONUS_UPGRADE;

        return $this->hasMany(UserBonus::class, 'user_id', 'id')->byBonusType($inTypes);
    }

    public function bonusSponsor()
    {
        return $this->hasMany(UserBonus::class, 'user_id', 'id')->bonusSponsor();
    }

    public function bonusExtra()
    {
        return $this->hasMany(UserBonus::class, 'user_id', 'id')->bonusExtra();
    }

    public function bonusPair()
    {
        return $this->hasMany(UserBonus::class, 'user_id', 'id')->bonusPair();
    }

    public function bonusLevel()
    {
        return $this->hasMany(UserBonus::class, 'user_id', 'id')->bonusLevel();
    }

    public function bonusRO()
    {
        return $this->hasMany(UserBonus::class, 'user_id', 'id')->bonusRO();
    }

    public function bonusUpgrade()
    {
        return $this->hasMany(UserBonus::class, 'user_id', 'id')->bonusUpgrade();
    }

    public function bonusRewards()
    {
        return $this->hasMany(UserReward::class, 'user_id', 'id');
    }

    public function withdraw()
    {
        return $this->hasMany(UserWithdraw::class, 'user_id', 'id');
    }
    // relationship:end

    // private
    private function summaryWithdrawQuery(int $type = null)
    {
        if (!is_null($type)) {
            $inTypes = [$type];
        } else {
            $inTypes = [BONUS_SPONSOR];
            if (BONUS_EXTRA_ENABLED) $inTypes[] = BONUS_EXTRA;
            if (BONUS_PAIR_ENABLED) $inTypes[] = BONUS_PAIR;
            if (BONUS_LEVEL_ENABLED) $inTypes[] = BONUS_LEVEL;
            if (BONUS_RO_ENABLED) $inTypes[] = BONUS_RO;
            if (BONUS_UPGRADE_ENABLED) $inTypes[] = BONUS_UPGRADE;
        }

        return $this->withdraw()
            ->byTransStatus([WD_PROCESSING, WD_COMPLETE])
            ->byBonusType($inTypes)
            ->whereHas('bonuses', function ($bonus) use ($inTypes) {
                return $bonus->byBonusType($inTypes);
            });
    }

    private function summaryBonusWithdraw($totalBonus, Collection $withdraw)
    {
        $result = (object) [
            'total_bonus' => 0,
            'total_withdraw' => 0,
            'total_withdraw_complete' => 0,
            'total_withdraw_processing' => 0,
            'remaining_bonus' => 0,
        ];

        $result->total_bonus = $totalBonus;

        if ($withdraw->isNotEmpty()) {
            $result->total_withdraw_complete = $withdraw->where('status', '=', WD_COMPLETE)->values()->sum('total_bonus');
            $result->total_withdraw_processing = $withdraw->where('status', '=', WD_PROCESSING)->values()->sum('total_bonus');
            $result->total_withdraw = $result->total_withdraw_complete + $result->total_withdraw_processing;
            $result->remaining_bonus = $result->total_bonus - $result->total_withdraw;
        } else {
            $result->remaining_bonus = $result->total_bonus;
        }

        return $result;
    }
    // private:end

    // accessor
    public function getSummaryOfBonusSponsorAttribute()
    {
        $wd = $this->summaryWithdrawQuery(BONUS_SPONSOR)->get();

        return $this->summaryBonusWithdraw($this->bonusSponsor()->sum('bonus_amount'), $wd);
    }

    public function getSummaryOfBonusExtraAttribute()
    {
        $wd = $this->summaryWithdrawQuery(BONUS_EXTRA)->get();

        return $this->summaryBonusWithdraw($this->bonusExtra()->sum('bonus_amount'), $wd);
    }

    public function getSummaryOfBonusPairAttribute()
    {
        $wd = $this->summaryWithdrawQuery(BONUS_PAIR)->get();

        return $this->summaryBonusWithdraw($this->bonusPair()->sum('bonus_amount'), $wd);
    }

    public function getSummaryOfBonusLevelAttribute()
    {
        $wd = $this->summaryWithdrawQuery(BONUS_LEVEL)->get();

        return $this->summaryBonusWithdraw($this->bonusLevel()->sum('bonus_amount'), $wd);
    }

    public function getSummaryOfBonusRoAttribute()
    {
        $wd = $this->summaryWithdrawQuery(BONUS_RO)->get();

        return $this->summaryBonusWithdraw($this->bonusRO()->sum('bonus_amount'), $wd);
    }

    public function getSummaryOfBonusUpgradeAttribute()
    {
        $wd = $this->summaryWithdrawQuery(BONUS_UPGRADE)->get();

        return $this->summaryBonusWithdraw($this->bonusUpgrade()->sum('bonus_amount'), $wd);
    }

    public function getSummaryOfBonusesAttribute()
    {
        $wd = $this->summaryWithdrawQuery()->get();

        return $this->summaryBonusWithdraw($this->bonuses()->sum('bonus_amount'), $wd);
    }

    public function getCanWithdrawBonusRoAttribute()
    {
        if ($this->has_package && ($this->my_package_id == TOP_PACKAGE)) {
            $transRO = $this->orderPackages()
                ->byTransStatus(TRANS_CONFIRMED)
                ->byRO()
                ->whereDoestHave('bonusRO')
                ->get();

            $bonusRO = $this->bonusRO()
                ->whereDoesntHave(['withdraw', 'transRO'])
                ->get();

            return ($bonusRO->isNotEmpty() && $transRO->isNotEmpty());
        }

        return false;
    }

    // bonus reward
    // private reward
    private function canClaimReward(Reward $reward, int $remainingLeft, int $remainingRight): bool
    {
        $needLeft = $reward->left;
        $needRight = $reward->right;
        $canClaim = (($needLeft > 0) && ($needRight > 0));

        return ($canClaim && ($remainingLeft >= $needLeft) && ($remainingRight >= $needRight));
    }

    // private reward:end
    public function getSummaryOfBonusRewardAttribute()
    {
        if (!$this->has_package || !$this->structure) return null;

        $claims = $this->bonusRewards()
            ->byTransStatus([WD_PROCESSING, WD_COMPLETE])
            ->with('reward')
            ->get();

        $structure = $this->structure;
        $leftMembers = $structure->left_members;
        $rightMembers = $structure->right_members;
        $iAmAgen = ($this->my_package_id == TOP_PACKAGE);

        // reseller
        $pkgReseller = Package::query()->byId(LOW_PACKAGE)->first();
        $rowReseller = null;

        if ($pkgReseller->get_reward) {
            $pointLeftReseller = $leftMembers->sum(function ($item) {
                $startPkg = $item->user->startPackage;
                return empty($startPkg) ? 0 : $startPkg->package->reward_point;
            });

            $pointRightReseller = $rightMembers->sum(function ($item) {
                $startPkg = $item->user->startPackage;
                return empty($startPkg) ? 0 : $startPkg->package->reward_point;
            });

            $resellerReward = Reward::query()->byPackage($pkgReseller)->first();
            $resellerClaims = $claims->where('reward.package_id', '=', $pkgReseller->id)->values();
            $claimedLeftReseller = $resellerClaims->sum('reward_left');
            $claimedRightReseller = $resellerClaims->sum('reward_right');
            $remainingLeftReseller = $pointLeftReseller - $claimedLeftReseller; // reset to remaining
            $remainingRightReseller = $pointRightReseller - $claimedRightReseller; // reset to remaining

            $rowReseller = (object) [
                'package' => $pkgReseller,
                'reward' => $resellerReward,
                'claims' => $resellerClaims,
                'total_user_left' => $pointLeftReseller,
                'total_user_right' => $pointRightReseller,
                'total_claimed_left' => $claimedLeftReseller,
                'total_claimed_right' => $claimedRightReseller,
                'remaining_left' => $remainingLeftReseller,
                'remaining_right' => $remainingRightReseller,
                'can_claim' => $this->canClaimReward($resellerReward, $remainingLeftReseller, $remainingRightReseller),
                'trans_ro' => null,
            ];
        }

        // agen
        $pkgAgen = Package::query()->byId(TOP_PACKAGE)->first();
        $rowAgen = null;

        if ($pkgAgen->get_reward) {
            $pointLeftAgen = $leftMembers
                ->sum(function ($item) {
                    return $item->user->transUpgrade->sum('package.reward_point') + $item->user->transRO->sum('package.reward_point');
                });

            $pointRightAgen = $rightMembers
                ->sum(function ($item) {
                    return $item->user->transUpgrade->sum('package.reward_point') + $item->user->transRO->sum('package.reward_point');
                });
            $agenRewards = Reward::query()->byPackage($pkgAgen)->orderBy('left')->orderBy('right')->orderBy('id')->get();
            $agenClaims = $claims->where('reward.package_id', '=', $pkgAgen->id)->values();
            $claimedLeftAgen = $agenClaims->sum('reward_left');
            $claimedRightAgen = $agenClaims->sum('reward_right');

            $rowAgen = (object) [
                'package' => $pkgAgen,
                'rewards' => collect(),
                'total_user_left' => $pointLeftAgen,
                'total_user_right' => $pointRightAgen,
                'total_claimed_left' => $claimedLeftAgen,
                'total_claimed_right' => $claimedRightAgen,
                'remaining_left' => $pointLeftAgen, // akumulasi
                'remaining_right' => $pointRightAgen, // akumulasi
                'trans_ro' => $this->transROForBonusReward,
            ];

            $hasRO = !empty($rowAgen->trans_ro);

            foreach ($agenRewards as $agenReward) {
                $agenRewardClaim = $claims->where('reward_id', '=', $agenReward->id)->first();

                $row = (object) [
                    'reward' => $agenReward,
                    'claim' => $agenRewardClaim,
                    'can_claim' => $hasRO && empty($agenRewardClaim) && $this->canClaimReward($agenReward, $pointLeftAgen, $pointRightAgen),
                    'claimed' => !empty($agenRewardClaim),
                ];

                $rowAgen->rewards->push($row);
            }
        }

        $result = (object) [
            'current_package' => $this->package,
            'is_agen' => $iAmAgen,
            'reseller' => $rowReseller,
            'agen' => $rowAgen
        ];

        return $result;
    }
    // bonus reward:end
    // accessor:end
}
