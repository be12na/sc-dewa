<?php

namespace App\Models\Traits\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

trait UserType
{
    private $userGroupAdmin = 'admin';
    private $userGroupMember = 'member';

    // scope
    public function scopeByUserType(Builder $builder, int|array $userType): Builder
    {
        if (!is_array($userType)) $userType = [$userType];

        return $builder->whereIn('user_type', $userType);
    }

    public function scopeByMemberUser(Builder $builder): Builder
    {
        return $builder->byUserType(USER_TYPE_MEMBER);
    }

    public function scopeByAdminUser(Builder $builder, array|int $userType = null): Builder
    {
        $only = [USER_TYPE_MASTER, USER_TYPE_ADMIN];
        if (!is_null($userType)) {
            if (!is_array($userType)) $userType = [$userType];
        } else {
            $userType = $only;
        }

        $userTypes = [];

        foreach ($userType as $type) {
            if (in_array($type, $only)) $userTypes[] = $type;
        }

        return $builder->byUserType($userTypes);
    }
    // scope:end

    // accessor
    public function getUserGroupAttribute()
    {
        return in_array($this->user_type, USER_GROUP_ADMIN) ? $this->userGroupAdmin : $this->userGroupMember;
    }

    public function getUserTypeNameAttribute()
    {
        return Arr::get(USER_TYPE_NAMES, $this->user_type, 'Anonymous');
    }

    public function getIsSuperUserAttribute()
    {
        return ($this->user_type == USER_TYPE_SUPER);
    }

    public function getIsMasterUserAttribute()
    {
        return ($this->user_type == USER_TYPE_MASTER);
    }

    public function getIsAdminUserAttribute()
    {
        return ($this->user_type == USER_TYPE_ADMIN);
    }

    public function getIsMemberUserAttribute()
    {
        return ($this->user_type == USER_TYPE_MEMBER);
    }

    public function getGroupRoleAttribute()
    {
        return $this->user_group;
    }

    public function getTypeRoleAttribute()
    {
        return Arr::get(USER_TYPE_ROLES, $this->user_type, 'none');
    }

    public function getIsMemberTopLevelAttribute()
    {
        return (empty($this->referral_id) && $this->is_member_user);
    }

    // public
    public function isGroup(string $name): bool
    {
        $name = strtolower($name);

        if (!in_array($name, [$this->userGroupAdmin, $this->userGroupMember])) return false;

        return ($this->user_group == $name);
    }
}
