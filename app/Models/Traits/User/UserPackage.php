<?php

namespace App\Models\Traits\User;

use App\Models\Package;
use App\Models\TransPackage;
use Closure;
use Illuminate\Database\Eloquent\Builder;

trait UserPackage
{
    // relationship
    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }

    public function orderPackages()
    {
        return $this->hasMany(TransPackage::class, 'buyer_id', 'id');
    }

    public function startPackage()
    {
        return $this->hasOne(TransPackage::class, 'buyer_id', 'id')
            ->byTransType(TRANS_PKG_START);
    }

    public function sellPackages()
    {
        return $this->hasMany(TransPackage::class, 'seller_id', 'id');
    }

    public function transUpgrade()
    {
        return $this->hasMany(TransPackage::class, 'buyer_id', 'id')->byUpgrade();
    }

    public function transRO()
    {
        return $this->hasMany(TransPackage::class, 'buyer_id', 'id')->byRO();
    }

    public function transROForBonusRO()
    {
        return $this->hasOne(TransPackage::class, 'buyer_id', 'id')
            ->byRO()
            ->byTransStatus(TRANS_CONFIRMED)
            ->whereDoesntHave('bonusRO');
    }

    public function transROForBonusUpgrade()
    {
        return $this->hasOne(TransPackage::class, 'buyer_id', 'id')
            ->byRO()
            ->byTransStatus(TRANS_CONFIRMED)
            ->whereDoesntHave('bonusUpgrade');
    }

    public function transROForBonusReward()
    {
        return $this->hasOne(TransPackage::class, 'buyer_id', 'id')
            ->byRO()
            ->byTransStatus(TRANS_CONFIRMED)
            ->whereDoesntHave('bonusReward');
    }
    // relationship:end

    // public
    public function transQuery(string $as = null, int $status = null, Closure $conditions = null): Builder
    {
        $query = TransPackage::query()->byNotRO();

        if ($this->is_member_user) {
            if ($as == 'buyer') {
                $query = $query->where('buyer_id', '=', $this->id);
            } elseif ($as == 'seller') {
                $query = $query->where('seller_id', '=', $this->id);
            } else {
                $userId = $this->id;
                $query = $query->where(function ($q) use ($userId) {
                    return $q->where('buyer_id', '=', $userId)
                        ->orWhere('seller_id', '=', $userId);
                });
            }
        } else {
            $query = $query->where('seller_id', '=', 0);
        }

        if (!is_null($status)) $query = $query->where('status', '=', $status);

        if (!is_null($conditions)) $query = $conditions($query, $this);

        return $query;
    }
    // public:end

    // scope
    public function scopeByPackage(Builder $builder, Package|int $package): Builder
    {
        return $builder->where('package_id', '=', ($package instanceof Package) ? $package->id : $package);
    }

    public function scopeByInactiveMember(Builder $builder): Builder
    {
        return $builder->whereDoesntHave('startPackage', function ($order) {
            return $order->byTransStatus(TRANS_CONFIRMED);
        });
    }
    // scope:end

    // accessor
    public function getTransOrderPackagesAttribute()
    {
        if ($this->is_member_user) return $this->orderPackages;

        return collect();
    }

    public function getTransSellPackagesAttribute()
    {
        return $this->is_member_user
            ? $this->sellPackages
            : $this->transQuery('seller', TRANS_ORDER)->get();
    }

    public function getHistoryPackagesAttribute()
    {
        return $this->transQuery()->get();
    }

    public function getCountPendingSellPackageAttribute()
    {
        return $this->trans_sell_packages->where('status', '=', TRANS_ORDER)->values()->count();
    }

    public function getHasPendingSellPackageAttribute()
    {
        return ($this->count_pending_sell_package > 0);
    }

    public function getCountPendingOrderPackageAttribute()
    {
        return $this->is_member_user
            ? $this->orderPackages->where('status', '=', TRANS_ORDER)->values()->count()
            : 0;
    }

    public function getHasPendingOrderPackageAttribute()
    {
        return ($this->count_pending_order_package > 0);
    }

    public function getLatestPackageAttribute()
    {
        return $this->is_member_user
            ? $this->transQuery('buyer', TRANS_CONFIRMED, function ($query, $user) {
                return $query->where('package_id', '=', $user->package_id);
            })->orderBy('status_at', 'desc')->first()
            : null;
    }

    public function getHasPackageAttribute()
    {
        return ($this->is_member_user && !empty($this->package) && !empty($this->latest_package));
    }

    public function getUpgradablePackageAttribute()
    {
        return ($this->is_member_user && (!$this->has_package || ($this->package_id < TOP_PACKAGE)));
    }

    public function getMyPackageIdAttribute()
    {
        return $this->latest_package ? $this->latest_package->package_id : null;
    }
    // accessor:end
}
