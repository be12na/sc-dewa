<?php

namespace App\Models\Traits\User;

use Illuminate\Support\Facades\Session;

trait AuthSession
{
    public function manageSession(): void
    {
        if (SINGLE_SESSION === true) {
            $newSessionId = Session::getId();

            if ($this->session_id && ($handler = Session::getHandler())->read($this->session_id)) {
                $handler->destroy($this->session_id);
            }

            $this->timestamps = false;
            $this->session_id = $newSessionId;
            $this->save();
        }
    }
}
