<?php

namespace App\Models;

use App\Models\Traits\ModelId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PinType extends Model
{
    use HasFactory;
    use ModelId;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'price',
        'type',
        'plan',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'price' => 'float',
        'type' => 'integer',
        'plan' => 'integer',
    ];

    // scope
    public function scopeByPlanName(Builder $builder, string $name, int $plan = null): Builder
    {
        if (is_null($plan)) $plan = 1;

        return $builder->where(function ($where) use ($name, $plan) {
            return $where->where('name', '=', $name)
                ->where('plan', '=', $plan);
        });
    }

    public function scopeByPlan(Builder $builder, int $plan): Builder
    {
        return $builder->where('plan', '=', $plan);
    }

    public function scopeByType(Builder $builder, int $type): Builder
    {
        return $builder->where('type', '=', $type);
    }

    public function scopeByUniqueKey(Builder $builder, string $name, int $plan, int $type): Builder
    {
        return $builder->where(function ($where) use ($name, $plan, $type) {
            return $where->where('name', '=', $name)
                ->where('plan', '=', $plan)
                ->where('type', '=', $type);
        });
    }
}
