<?php

namespace App\Models;

use App\Models\Traits\ModelId;
use App\Models\Traits\ModelUser;
use Carbon\Carbon;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBonus extends Model
{
    use HasFactory;
    use ModelId, ModelUser;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'from_user_id',
        'bonus_type',
        'bonus_date',
        'bonus_amount',
        'bonus_text',
        'bonus_claimed',
        'wd_id',
        'ro_id',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'bonus_type' => 'integer',
        'bonus_date' => 'date',
        'bonus_amount' => 'integer',
        'bonus_claimed' => 'boolean',
    ];

    // static
    public static function createBonusSponsor(User|int $fromUser, Carbon $date = null)
    {
        if (!($fromUser instanceof User)) $fromUser = User::query()->byId($fromUser)->first();

        if (empty($fromUser)) return null;

        $sponsor = $fromUser->referral;

        if (empty($sponsor) || !$sponsor->has_package || empty($package = $sponsor->package)) return null;

        if ($package->actual_bonus_sponsor > 0) {
            $values = [
                'user_id' => $sponsor->id,
                'from_user_id' => $fromUser->id,
                'bonus_type' => BONUS_SPONSOR,
                'bonus_date' => carbonToday($date),
                'bonus_amount' => $package->actual_bonus_sponsor,
            ];

            return static::create($values);
        }

        return null;
    }

    public static function createBonusExtra(User|int $fromUser, Carbon $date = null)
    {
        if (!BONUS_EXTRA_ENABLED) return null;

        if (!($fromUser instanceof User)) $fromUser = User::query()->byId($fromUser)->first();

        if (empty($fromUser)) return null;

        $sponsor = $fromUser->referral;

        if (empty($sponsor)) return null;

        $spG1 = $sponsor->referral;

        if (empty($spG1) || !$spG1->has_package || empty($package = $spG1->package)) return null;

        if ($package->bonus_extra > 0) {
            $values = [
                'user_id' => $spG1->id,
                'from_user_id' => $fromUser->id,
                'bonus_type' => BONUS_EXTRA,
                'bonus_date' => carbonToday($date),
                'bonus_amount' => $package->bonus_extra,
            ];

            return static::create($values);
        }

        return null;
    }

    public static function createBonusRO(User|int $fromUser, Carbon $date = null)
    {
        if (!BONUS_RO_ENABLED) return null;

        if (!($fromUser instanceof User)) $fromUser = User::query()->byId($fromUser)->first();

        if (empty($fromUser)) return null;

        $sponsor = $fromUser->referral;

        if (empty($sponsor) || !$sponsor->has_package || empty($package = $sponsor->package)) return null;

        if ($package->bonus_ro > 0) {
            $values = [
                'user_id' => $sponsor->id,
                'from_user_id' => $fromUser->id,
                'bonus_type' => BONUS_RO,
                'bonus_date' => carbonToday($date),
                'bonus_amount' => $package->bonus_ro,
            ];

            return static::create($values);
        }

        return null;
    }

    public static function createBonusUpgrade(User|int $fromUser, Carbon $date = null)
    {
        if (!BONUS_UPGRADE_ENABLED) return null;

        if (!($fromUser instanceof User)) $fromUser = User::query()->byId($fromUser)->first();

        if (empty($fromUser)) return null;

        $sponsor = $fromUser->referral;

        if (empty($sponsor) || !$sponsor->has_package || empty($package = $sponsor->package)) return null;

        if ($package->bonus_upgrade > 0) {
            $values = [
                'user_id' => $sponsor->id,
                'from_user_id' => $fromUser->id,
                'bonus_type' => BONUS_UPGRADE,
                'bonus_date' => carbonToday($date),
                'bonus_amount' => $package->bonus_upgrade,
            ];

            return static::create($values);
        }

        return null;
    }
    // static:end

    // relationship
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function from_user()
    {
        return $this->belongsTo(User::class, 'from_user_id', 'id');
    }

    public function withdraw()
    {
        return $this->belongsTo(UserWithdraw::class, 'wd_id', 'id')
            ->byTransStatus([WD_PROCESSING, WD_COMPLETE]);
    }

    public function transRO()
    {
        return $this->belongsTo(TransPackage::class, 'ro_id', 'id')
            ->byTransStatus([TRANS_CONFIRMED]);
    }
    // relationship:end

    // scope
    public function scopeByBonusType(Builder $builder, array|int $type): Builder
    {
        if (is_array($type)) return $builder->whereIn('bonus_type', $type);

        return $builder->where('bonus_type', '=', $type);
    }

    public function scopeBonusSponsor(Builder $builder): Builder
    {
        return $builder->byBonusType(BONUS_SPONSOR);
    }

    public function scopeBonusExtra(Builder $builder): Builder
    {
        return $builder->byBonusType(BONUS_EXTRA);
    }

    public function scopeBonusPair(Builder $builder): Builder
    {
        return $builder->byBonusType(BONUS_PAIR);
    }

    public function scopeBonusLevel(Builder $builder): Builder
    {
        return $builder->byBonusType(BONUS_LEVEL);
    }

    public function scopeBonusRO(Builder $builder): Builder
    {
        return $builder->byBonusType(BONUS_RO);
    }

    public function scopeBonusUpgrade(Builder $builder): Builder
    {
        return $builder->byBonusType(BONUS_UPGRADE);
    }

    public function scopeByBonusClaimed(Builder $builder, bool $claimed = null): Builder
    {
        if (is_null($claimed)) return $builder;

        return $builder->where('bonus_claimed', '=', $claimed);
    }

    public function scopeForWithdraw(Builder $builder, Carbon $date = null, $operator = '<', Closure $userConditions = null): Builder
    {
        if (is_null($date)) $date = Carbon::today();

        return $builder
            ->where('bonus_date', $operator, $date->format('Y-m-d'))
            ->whereDoesntHave('withdraw')
            ->whereHas('user', function ($user) use ($userConditions) {
                if (!is_null($userConditions)) $user = $userConditions($user);

                return $user->whereHas('memberActiveBank');
            });
    }
    // scope:end
}
