<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;

if (!function_exists('isLive')) {
    function isLive(): bool
    {
        return (strtolower(config('app.env')) == 'production');
    }
}

if (!function_exists('hasGroup')) {
    function hasGroup(array|string $group, User $user = null): bool
    {
        if (empty($user)) $user = auth()->user();

        $groups = array_unique(
            is_array($group)
                ? $group
                : array_map(function ($value) {
                    return trim($value);
                }, array_values(explode(',', $group)))
        );

        $list = array_map(function ($v) {
            return explode('.', $v)[0];
        }, array_unique(array_values(USER_TYPE_ROLES)));

        $available = array_intersect($groups, $list);

        return in_array($user->group_role, $available);
    }
}

if (!function_exists('hasRole')) {
    function hasRole(array|string $role, User $user = null): bool
    {
        if (empty($user)) $user = auth()->user();

        $list = array_unique(array_values(USER_TYPE_ROLES));
        $roles = is_array($role)
            ? $role
            : array_map(function ($value) {
                return trim($value);
            }, array_values(explode(',', $role)));

        $available = array_intersect($roles, $list);

        return in_array($user->type_role, $available);
    }
}

if (!function_exists('isPackageRequirePIN')) {
    function isPackageRequirePIN(): bool
    {
        return (PACKAGE_USE_PIN && PIN_ENABLED);
    }
}

if (!function_exists('isTrueConfig')) {
    function isTrueConfig($definedConfig, $trueValue): bool
    {
        return ($definedConfig === $trueValue);
    }
}

if (!function_exists('planName')) {
    function planName(int $id)
    {
        return Arr::get(PLANS, $id, '-');
    }
}

if (!function_exists('arrayAsString')) {
    function arrayAsString(array|string|int $value, string $separator = null): string
    {
        if (is_null($separator)) $separator = ' ';

        return implode($separator, array_values(is_array($value) ? $value : [$value]));
    }
}

if (!function_exists('formatNumber')) {
    function formatNumber($value, int $decimal = null, bool $isCurrency = null): string
    {
        $isCurrency = ($isCurrency === true);
        $isIDR = (config('app.locale') == 'id');

        if ($isCurrency) {
            if ($isIDR) {
                $decimal = 0; // override
            } else {
                $decimal = is_null($decimal) ? CURRENCY_DECIMAL : $decimal;
            }
        } else {
            $decimal = is_null($decimal) ? 0 : $decimal;
        }

        return number_format($value, ($decimal >= 0) ? $decimal : 0, __('format.decimal'), __('format.thousand'));
    }
}

if (!function_exists('carbonToday')) {
    function carbonToday(Carbon $date = null): Carbon
    {
        return is_null($date) ? Carbon::today() : $date->startOfDay();
    }
}

if (!function_exists('selfTranslate')) {
    function selfTranslate(string $key, array $replace = null, string $locale = null)
    {
        if (is_null($replace)) $replace = [];

        return arrayAsString(__($key, $replace, $locale));
    }
}

if (!function_exists('translateDatetime')) {
    function translateDatetime(string|Carbon $time, string $format = null): string
    {
        if (empty($time)) return null;
        if (empty($format)) $format = __('format.date.short') . ' H:i:s';

        $carbon = $time;

        if (!($time instanceof Carbon)) {
            if (is_int($time)) {
                $carbon = Carbon::createFromTimestamp($time);
            } else {
                try {
                    $carbon = Carbon::createFromTimeString($time);
                } catch (\Exception $e) {
                    $carbon = Carbon::createFromTimeString($time . ' 00:00:00');
                }
            }
        }

        return $carbon->translatedFormat($format);
    }
}

if (!function_exists('translatedDateTimeToCarbon')) {
    function translatedDateTimeToCarbon(string $time, string $fromFormat, string $locale = null, string $tz = null): Carbon|null
    {
        if (empty($time)) return null;
        if (is_null($locale)) $locale = config('app.locale');

        return Carbon::createFromLocaleFormat($fromFormat, $locale, $time, $tz);
    }
}

if (!function_exists('matchRoute')) {
    function matchRoute(array|string $routeNames): bool
    {
        $match = false;
        $routeList = is_array($routeNames) ? $routeNames : [$routeNames];
        $currentNames = explode('.', $curName = Route::currentRouteName());

        if (!empty($curName) && !empty($routeList)) {
            $tmpName = '';
            $tmpList = [];
            foreach ($currentNames as $name) {
                $tmpName .= ".{$name}";
                $tmpName = ltrim($tmpName, '.');
                $tmpList[] = $tmpName;
            }

            $match = !empty(array_intersect($tmpList, $routeList));
        }

        return $match;
    }
}

if (!function_exists('optionSelected')) {
    function optionSelected($data_value, $true_value): string
    {
        $data_value = (string) $data_value;
        $true_value = (string) $true_value;

        return ($data_value == $true_value) ? 'selected' : '';
    }
}

if (!function_exists('checkboxChecked')) {
    function checkboxChecked(string|int $value = null, array|string|int $checkList = null, bool $defaultCheck = null): string
    {
        // $checked = (is_null($value) || is_null($checkList)) ? false : (is_array($checkList) ? in_array($value, $checkList) : ($value == $checkList));
        // $default = is_null($defaultCheck) ? false : $defaultCheck;

        // return $checked ? 'checked' : ($default ? 'checked' : '');

        $checked = '';

        if (is_null($value)) {
            $checked = $defaultCheck ? 'checked' : '';
        } else {
            if (!is_null($checkList)) {
                if (is_array($checkList)) {
                    $checked = in_array($value, $checkList) ? 'checked' : '';
                } else {
                    $checked = ($value == $checkList) ? 'checked' : '';
                }
            }
        }

        return $checked;
    }
}

if (!function_exists('contentCheck')) {
    function contentCheck(bool $isTrue, $trueColor = 'text-success', $falseColor = 'text-danger'): string
    {
        $icon = $isTrue ? 'fa-square-check' : 'fa-rectangle-xmark';
        $color = $isTrue ? $trueColor : $falseColor;

        return "<i class=\"fa {$icon} {$color}\"></i>";
    }
}

if (!function_exists('errorMessageContent')) {
    function errorMessageContent(Validator|array|string $messages, string $errorLevel = 'danger', bool $showHeader = null, bool $showBtnClose = null): string
    {
        $message = '';

        if ($messages instanceof Validator) {
            $message .= '<ul class="mb-0 ps-3">';
            foreach ($messages->errors()->toArray() as $errors) {
                $message .= '<li>' . $errors[0] . '</li>';
            }
            $message .= '</ul>';
        } elseif (is_array($messages)) {
            $message .= '<ul class="mb-0 ps-3">';
            foreach ($messages as $error) {
                $message .= '<li>' . $error . '</li>';
            }
            $message .= '</ul>';
        } else {
            $message .= $messages;
        }

        return view('partials.alert', [
            'message' => $message,
            'messageClass' => $errorLevel,
            'showHeader' => $showHeader,
            'showBtnClose' => $showBtnClose,
        ])->render();
    }
}

if (!function_exists('ajaxValidationError')) {
    function ajaxValidationError(Validator|array $validator, string $headerMessage = null): array
    {
        $messages = [];
        $isValidatorInstance = ($validator instanceof Validator);
        $errors = $isValidatorInstance ? $validator->errors()->toArray() : $validator;

        foreach ($errors as $key => $values) {
            $messages[$key] = $isValidatorInstance ? $values[0] : $values;
        }

        return [
            'message' => errorMessageContent($headerMessage ?: __('message.validation-error'), 'danger'),
            'validationError' => true,
            'messages' => $messages
        ];
    }
}

if (!function_exists('ajaxResponse')) {
    function ajaxResponse(array|string $message = null, $status = 500)
    {
        if (empty($message)) $message = "<h5 class=\"text-danger\">Error</h5>";

        return response($message, $status);
    }
}

if (!function_exists('pageMessageResponse')) {
    function pageMessageResponse(string $message = null, string $redirectRoute = null, array $routeParams = null, $errorLevel = 'danger')
    {
        if (empty($message)) $message = 'Page Error';
        if (empty($redirectRoute)) $redirectRoute = 'dashboard.index';
        if (is_null($routeParams)) $routeParams = [];

        return redirect()->route($redirectRoute, $routeParams)
            ->with('message', $message)
            ->with('messageClass', $errorLevel);
    }
}

if (!function_exists('fallbackRouteBinding')) {
    function fallbackRouteBinding($data, string $fallbackMessage = '', string $fallbackRoute = null, array $fallbackRouteParams = null)
    {
        if (!empty($data)) return $data;

        if (empty($fallbackMessage)) $fallbackMessage = __('message.not-found');
        if (empty($fallbackRoute)) $fallbackRoute = 'dashboard.index';
        if (is_null($fallbackRouteParams)) $fallbackRouteParams = [];

        throw new HttpResponseException(
            request()->ajax()
                ? ajaxResponse($fallbackMessage, 404)
                : pageMessageResponse($fallbackMessage, $fallbackRoute, $fallbackRouteParams)
        );
    }
}
