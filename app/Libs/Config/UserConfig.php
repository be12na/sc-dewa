<?php

// USER TYPE
if (!defined('USER_TYPE_SUPER')) define('USER_TYPE_SUPER', 1);
if (!defined('USER_TYPE_MASTER')) define('USER_TYPE_MASTER', 2);
if (!defined('USER_TYPE_ADMIN')) define('USER_TYPE_ADMIN', 3);
if (!defined('USER_TYPE_MEMBER')) define('USER_TYPE_MEMBER', 10);
// if (!defined('USER_TYPE_STOCKIST')) define('USER_TYPE_STOCKIST', 20);
// if (!defined('USER_TYPE_MEMBER_STOCKIST')) define('USER_TYPE_MEMBER_STOCKIST', 30);

if (!defined('USER_GROUP_ADMIN')) define('USER_GROUP_ADMIN', [
    USER_TYPE_SUPER,
    USER_TYPE_MASTER,
    USER_TYPE_ADMIN,
]);

if (!defined('USER_GROUP_MEMBER')) define('USER_GROUP_MEMBER', [
    USER_TYPE_MEMBER,
]);

if (!defined('USER_TYPE_NAMES')) define('USER_TYPE_NAMES', [
    USER_TYPE_SUPER => 'Super Administrator',
    USER_TYPE_MASTER => 'Master Administrator',
    USER_TYPE_ADMIN => 'Administrator',
    USER_TYPE_MEMBER => 'Member',
]);

if (!defined('USER_TYPE_ROLES')) define('USER_TYPE_ROLES', [
    USER_TYPE_SUPER => 'admin.super',
    USER_TYPE_MASTER => 'admin.master',
    USER_TYPE_ADMIN => 'admin.admin',
    USER_TYPE_MEMBER => 'member.member',
]);

// USER STATUS
if (!defined('USER_STATUS_INACTIVE')) define('USER_STATUS_INACTIVE', 0);
if (!defined('USER_STATUS_REGISTERED')) define('USER_STATUS_REGISTERED', 1);
if (!defined('USER_STATUS_ACTIVATED')) define('USER_STATUS_ACTIVATED', 2);
if (!defined('USER_STATUS_BANNED')) define('USER_STATUS_BANNED', 3);

// USER CAN LOGIN AFTER REGISTERED
if (!defined('USER_REGISTER_LOGIN')) define('USER_REGISTER_LOGIN', true);

// STRUCTURE
if (!defined('USER_LEFT_POSITION')) define('USER_LEFT_POSITION', 1);
if (!defined('USER_RIGHT_POSITION')) define('USER_RIGHT_POSITION', 2);
