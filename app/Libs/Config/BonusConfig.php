<?php

// SETTING BONUS AND REWARD
if (!defined('BONUS_SPONSOR_BY_PACKAGE')) define('BONUS_SPONSOR_BY_PACKAGE', true);
if (!defined('BONUS_SPONSOR_TYPE')) define('BONUS_SPONSOR_TYPE', 'currency'); // currency, percent
if (!defined('BONUS_SPONSOR')) define('BONUS_SPONSOR', 1);

if (!defined('BONUS_EXTRA_ENABLED')) define('BONUS_EXTRA_ENABLED', true);
if (!defined('BONUS_EXTRA_TYPE')) define('BONUS_EXTRA_TYPE', 'currency'); // currency, percent
if (!defined('BONUS_EXTRA')) define('BONUS_EXTRA', 2);

if (!defined('BONUS_PACKAGE_ENABLED')) define('BONUS_PACKAGE_ENABLED', false);
if (!defined('BONUS_PACKAGE_TYPE')) define('BONUS_PACKAGE_TYPE', 'currency'); // currency, percent
if (!defined('BONUS_PACKAGE')) define('BONUS_PACKAGE', 3);

if (!defined('BONUS_PAIR_ENABLED')) define('BONUS_PAIR_ENABLED', false);
if (!defined('BONUS_PAIR_TYPE')) define('BONUS_PAIR_TYPE', 'point'); // point, currency, percent
if (!defined('BONUS_PAIR')) define('BONUS_PAIR', 4);

if (!defined('BONUS_LEVEL_ENABLED')) define('BONUS_LEVEL_ENABLED', false);
if (!defined('BONUS_LEVEL_TYPE')) define('BONUS_LEVEL_TYPE', 'currency'); // currency, percent
if (!defined('BONUS_LEVEL_PER_PACKAGE')) define('BONUS_LEVEL_PER_PACKAGE', true);
if (!defined('BONUS_LEVEL')) define('BONUS_LEVEL', 5);

if (!defined('BONUS_RO_ENABLED')) define('BONUS_RO_ENABLED', true);
if (!defined('BONUS_RO_TYPE')) define('BONUS_RO_TYPE', 'currency'); // currency, percent
if (!defined('BONUS_RO')) define('BONUS_RO', 6);

if (!defined('BONUS_UPGRADE_ENABLED')) define('BONUS_UPGRADE_ENABLED', true);
if (!defined('BONUS_UPGRADE_TYPE')) define('BONUS_UPGRADE_TYPE', 'currency'); // currency, percent
if (!defined('BONUS_UPGRADE')) define('BONUS_UPGRADE', 7);

if (!defined('REWARD_ENABLED')) define('REWARD_ENABLED', true);
if (!defined('REWARD_BY_TYPE')) define('REWARD_BY_TYPE', 'point'); // point, currency
if (!defined('BONUS_REWARD')) define('BONUS_REWARD', 8);

// WITHDRAW
if (!defined('WD_PROCESSING')) define('WD_PROCESSING', 0);
if (!defined('WD_COMPLETE')) define('WD_COMPLETE', 1);
if (!defined('WD_REJECT')) define('WD_REJECT', 5);

// WITHDRAW FEE
if (!defined('WD_FEE')) define('WD_FEE', 10000);
