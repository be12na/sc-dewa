<?php

if (!defined('PIN_ENABLED')) define('PIN_ENABLED', false);
if (!defined('PIN_GLOBAL')) define('PIN_GLOBAL', false);
if (!defined('PACKAGE_USE_PIN')) define('PACKAGE_USE_PIN', true);

// type of pin
if (!defined('PIN_TYPE_GLOBAL')) define('PIN_TYPE_GLOBAL', 0);
if (!defined('PIN_TYPE_PACKAGE')) define('PIN_TYPE_PACKAGE', 1);

if (!defined('PLAN_A')) define('PLAN_A', 1);
if (!defined('PLAN_B')) define('PLAN_B', 2);

if (!defined('PLANS')) define('PLANS', [
    PLAN_A => 'A',
    PLAN_B => 'B',
]);

if (!defined('TRANS_ORDER')) define('TRANS_ORDER', 0);
if (!defined('TRANS_TRANSFERRED')) define('TRANS_TRANSFERRED', 1);
if (!defined('TRANS_CONFIRMED')) define('TRANS_CONFIRMED', 2);
if (!defined('TRANS_REJECTED')) define('TRANS_REJECTED', 3);
if (!defined('TRANS_CANCELED')) define('TRANS_CANCELED', 4);

if (!defined('TRANS_PKG_START')) define('TRANS_PKG_START', 1);
if (!defined('TRANS_PKG_UPGRADE')) define('TRANS_PKG_UPGRADE', 2);
if (!defined('TRANS_PKG_RO')) define('TRANS_PKG_RO', 3);

if (!defined('LOW_PACKAGE')) define('LOW_PACKAGE', 1);
if (!defined('TOP_PACKAGE')) define('TOP_PACKAGE', 2);
