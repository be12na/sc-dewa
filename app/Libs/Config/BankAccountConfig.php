<?php

// BANK SETTING
if (!defined('COMPANY_BANK')) define('COMPANY_BANK', true);

// MEMBER BANK
if (!defined('MEMBER_BANK')) define('MEMBER_BANK', true);
if (!defined('MEMBER_SINGLE_ACTIVE_BANK')) define('MEMBER_SINGLE_ACTIVE_BANK', true); // false is multi active bank
// LIMIT MEMBER BANK
if (!defined('MEMBER_BANK_LIMIT')) define('MEMBER_BANK_LIMIT', 5); // 1 = unique
