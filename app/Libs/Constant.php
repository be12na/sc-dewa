<?php

if (!defined('SINGLE_SESSION')) define('SINGLE_SESSION', true);

if (!defined('CURRENCY_DECIMAL')) define('CURRENCY_DECIMAL', 0); // digunakan untuk currency non-IDR. Tergantung pada locale
if (!defined('PERCENT_DECIMAL')) define('PERCENT_DECIMAL', 2); // digunakan untuk decimal digit dari percent

// error
if (!defined('MESSAGE_500')) define('MESSAGE_500', 'Terjadi kesalahan pada server. Silahkan dicoba lagi.');
// error:end

include "Config/UserConfig.php";
include "Config/BankAccountConfig.php";
include "Config/BonusConfig.php";
include "Config/PinPackageConfig.php";
