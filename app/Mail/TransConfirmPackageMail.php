<?php

namespace App\Mail;

use App\Models\TransPackage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class TransConfirmPackageMail extends Mailable
{
    use Queueable, SerializesModels;

    private TransPackage $transPackage;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TransPackage $transPackage)
    {
        $this->transPackage = $transPackage;
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        // subject: $this->transPackage->is_upgrade
        //     ? __('header.member.package-upgraded')
        //     : ($this->transPackage->is_repeat_order
        //         ? __('header.package.confirm-ro')
        //         : __('header.member.package-activated')),
        return new Envelope(
            subject: __('header.package.purchase-info', ['attr' => $this->transPackage->trans_type_name]),
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'transactions.package.mail-confirm-package',
            with: [
                'transPackage' => $this->transPackage,
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
