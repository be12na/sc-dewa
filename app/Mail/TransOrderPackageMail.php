<?php

namespace App\Mail;

use App\Models\TransPackage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class TransOrderPackageMail extends Mailable
{
    use Queueable, SerializesModels;

    private TransPackage $transPackage;
    private string $sendTo;
    private string $titleAttr;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TransPackage $transPackage, string $sendTo)
    {
        $this->transPackage = $transPackage;
        $this->sendTo = $sendTo;
        $this->titleAttr = $transPackage->is_upgrade ? __('label.upgrade') : __('label.purchase');
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        return new Envelope(
            subject: __('header.package.purchase-info', ['attr' => $this->titleAttr]),
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'transactions.package.mail-order-package',
            with: [
                'transPackage' => $this->transPackage,
                'sendTo' => $this->sendTo,
            ],
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
