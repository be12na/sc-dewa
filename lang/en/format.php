<?php

return [
    'thousand' => ',',
    'decimal' => '.',
    'currency' => [
        'text' => 'USD',
        'symbol' => [
            'text' => '$',
            'start' => '$ ',
            'end' => '',
        ],
    ],
    'date' => [
        'short' => 'Y-m-d',
        'medium' => 'M d Y',
        'full' => 'j F Y',
    ],
    'datetime' => [
        'short' => 'Y-m-d, H:i',
        'medium' => 'M d Y, H:i',
        'full' => 'j F Y, H:i',
    ],
];
