<?php

return [
    'dashboard' => 'Dashboard',
    'password' => 'Password',
    'profile' => 'Profile',
    'bank' => 'Bank Account',
    'settings' => [
        'bank' => ['Setting', 'Bank Account'],
        'package' => ['Setting', 'Package'],
        'bonus' => [
            'sponsor' => ['Setting', 'Bonus', 'Sponsor'],
            'level' => ['Setting', 'Bonus', 'Level'],
        ],
    ],
    'package' => [
        'select' => ['Package', 'Select'],
        'activate' => ['Package', 'Aktivasi'],
        'upgrade' => ['Package', 'Upgrade'],
        'ro' => ['Package', 'Repeat Order'],
        'transaction' => [
            'list' => ['Package', 'Transaction'],
        ],
        'history' => ['Package', 'Histories'],
    ],
    'member' => [
        'register' => ['Member', 'Register'],
        'not-active' => ['Member', 'Inactive'],
        'inactivated' => ['Member', 'Not Yet Activated'],
        'placement' => ['Member', 'Placement'],
        'sponsored' => ['Member', 'Sponsored'],
    ],
    'network' => [
        'tree' => ['Network', 'Structure'],
        'list' => ['Network', 'List'],
        'sponsored' => ['Network', 'Sponsored'],
    ],
    'bonus' => [
        'sponsor' => ['Bonus', 'Sponsor'],
        'level' => ['Bonus', 'Level'],
        'extra' => ['Bonus', 'Extra'],
        'ro' => ['Bonus', 'Repeat Order'],
        'upgrade' => ['Bonus', 'Upgrade'],
        'package' => ['Bonus', 'Package'],
        'reward' => ['Bonus', 'Reward'],
        'summary' => ['Bonus', 'Summary'],
    ],
    'withdraw' => [
        'bonus' => [
            'sponsor' => ['Withdrawal', 'Bonus', 'Sponsor'],
            'extra' => ['Withdrawal', 'Bonus', 'Extra'],
            'ro' => ['Withdrawal', 'Bonus', 'Repeat Order'],
            'upgrade' => ['Withdrawal', 'Bonus', 'Upgrade'],
            'level' => ['Withdrawal', 'Bonus', 'Level'],
        ],
        'histories' => ['Withdrawal', 'Bonus', 'Histories'],
        'reward' => ['Withdrawal', 'Reward'],
    ],
];
