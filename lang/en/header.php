<?php

return [
    'confirmation' => 'Confirmation',
    'dashboard' => 'Dashboard',
    'password' => [
        'change' => 'Change Password',
        'forgot' => 'Forgot Password',
    ],
    'bank' => [
        'list' => 'Bank Account List',
        'new' => 'Add Bank Account',
        'edit' => 'Edit Bank Account',
        'activate' => 'Activate Bank Account'
    ],
    'package' => [
        'list' => 'Package List',
        'transaction' => 'Package Transaction',
        'history' => 'Package Transaction Histories',
        'edit' => 'Edit Package',
        'detail' => 'Package Detail',
        'select' => 'Select Package',
        'confirm-selected' => 'Confirm Package Selection',
        'confirm-activate' => 'Confirm Activate Member',
        'confirm-upgrade' => 'Confirm Upgrade Member',
        'confirm-ro' => 'Confirm Repeat Order',
        'purchase-info' => 'Package :attr Information',
    ],
    'bonus' => [
        'sponsor' => [
            'index' => 'Bonus Sponsor',
            'edit' => 'Edit Bonus',
        ],
        'level' => [
            'index' => 'Bonus Level',
            'add' => 'Add Bonus Level',
            'edit' => 'Edit Bonus Level',
        ],
        'level' => [
            'index' => 'Bonus Extra',
            'edit' => 'Edit Bonus Extra',
        ],
        'ro' => [
            'index' => 'Bonus Repeat Order',
            'edit' => 'Edit Bonus Repeat Order',
        ],
        'upgrade' => [
            'index' => 'Bonus Upgrade',
            'edit' => 'Edit Bonus Upgrade',
        ],
        'summary' => [
            'index' => 'Summary of Bonuses',
            'info' => 'Information of Bonus Summary',
        ],
    ],
    'member' => [
        'list' => 'Member List',
        'edit-data' => 'Edit Member Data',
        'edit-password' => 'Edit Member Password',
        'registration' => 'Registration',
        'registration-info' => 'Registration Information',
        'private-data' => 'Private Data',
        'referral-data' => 'Referral Data',
        'package-activated' => 'Member Activated',
        'package-upgraded' => 'Member Upgraded',
        'sponsored' => 'Sponsored Members',
        'inactivated' => 'Member Belum Aktif',
        'placement' => 'Member Placement',
        'placement-select' => 'Select a Member',
        'detail' => 'Detail Member',
    ],
    'network' => [
        'tree' => 'Network Structure',
    ],
    'user' => [
        'profile' => 'Profile',
    ],
    'withdraw' => [
        'sponsor' => 'Withdraw Bonus Sponsor',
        'extra' => 'Withdraw Bonus Extra',
        'ro' => 'Withdraw Bonus Repeat Order',
        'upgrade' => 'Withdraw Bonus Upgrade',
        'history' => 'Withdrawal Histories',
        'reward' => 'Withdrawal Bonus Reward',
        'info' => 'Withdrawal Information',
    ],
];
