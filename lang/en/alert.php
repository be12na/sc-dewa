<?php

return [
    'title' => [
        'success' => 'Success',
        'warning' => 'Warning',
        'danger' => 'Error',
        'info' => 'Information',
        'question' => 'Question'
    ],
];
