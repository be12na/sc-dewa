<?php

return [
    'confirmation' => 'Konfirmasi',
    'dashboard' => 'Dashboard',
    'password' => [
        'change' => 'Ganti Password',
        'forgot' => 'Lupa Password',
    ],
    'bank' => [
        'list' => 'Daftar Rekening Bank',
        'new' => 'Tambah Rekening Bank',
        'edit' => 'Edit Rekening Bank',
        'activate' => 'Aktifkan Rekening Bank',
    ],
    'package' => [
        'list' => 'Daftar Paket',
        'transaction' => 'Transaksi Paket',
        'history' => 'Riwayat Transaksi Paket',
        'edit' => 'Edit Paket',
        'detail' => 'Detail Paket',
        'select' => 'Pilih Paket',
        'confirm-selected' => 'Konfirmasi Pilihan Paket',
        'confirm-activate' => 'Konfirmasi Aktifasi Member',
        'confirm-upgrade' => 'Konfirmasi Upgrade Member',
        'confirm-ro' => 'Konfirmasi Repeat Order',
        'purchase-info' => 'Informasi :attr Paket',
    ],
    'bonus' => [
        'sponsor' => [
            'index' => 'Bonus Sponsor',
            'edit' => 'Edit Bonus',
        ],
        'level' => [
            'index' => 'Bonus Level',
            'add' => 'Tambah Bonus Level',
            'edit' => 'Edit Bonus Level',
        ],
        'extra' => [
            'index' => 'Bonus Extra',
            'edit' => 'Edit Bonus Extra',
        ],
        'ro' => [
            'index' => 'Bonus Repeat Order',
            'edit' => 'Edit Bonus Repeat Order',
        ],
        'upgrade' => [
            'index' => 'Bonus Upgrade',
            'edit' => 'Edit Bonus Upgrade',
        ],
        'summary' => [
            'index' => 'Ringkasan Bonus',
            'info' => 'Informasi Ringkasan Bonus',
        ],
    ],
    'member' => [
        'list' => 'Daftar Member',
        'edit-data' => 'Edit Data Member',
        'edit-password' => 'Edit Password Member',
        'registration' => 'Pendaftaran',
        'registration-info' => 'Informasi Pendaftaran',
        'private-data' => 'Data Pribadi',
        'referral-data' => 'Data Referral',
        'package-activated' => 'Aktifasi Member',
        'package-upgraded' => 'Upgrade Member',
        'sponsored' => 'Member Disponsori',
        'inactivated' => 'Member Belum Aktif',
        'placement' => 'Penempatan Member',
        'placement-select' => 'Pilih Member',
        'detail' => 'Detail Member',
    ],
    'network' => [
        'tree' => 'Struktur Jaringan',
    ],
    'user' => [
        'profile' => 'Profile',
    ],
    'withdraw' => [
        'sponsor' => 'Penarikan Bonus Sponsor',
        'extra' => 'Penarikan Bonus Extra',
        'ro' => 'Penarikan Bonus Repeat Order',
        'upgrade' => 'Penarikan Bonus Upgrade',
        'history' => 'Riwayat Penarikan Bonus',
        'reward' => 'Penarikan Bonus Reward',
        'info' => 'Informasi Penarikan Bonus',
    ],
];
