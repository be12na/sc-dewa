<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute harus diterima.',
    'accepted_if' => ':attribute harus diterima ketika :other adalah :value.',
    'active_url' => ':attribute bukan URL aktif.',
    'after' => ':attribute harus tanggal setelah :date.',
    'after_or_equal' => ':attribute harus tanggal setelah atau sama dengan :date.',
    'alpha' => ':attribute hanya boleh berisi huruf.',
    'alpha_dash' => ':attribute hanya boleh berisi huruf, angka, - dan _.',
    'alpha_num' => ':attribute hanya boleh berisi huruf dan angka.',
    'array' => ':attribute harus berisikan daftar list.',
    'before' => ':attribute harus tanggal sebelum :date.',
    'before_or_equal' => ':attribute harus tanggal sebelum atau sama dengan :date.',
    'between' => [
        'array' => ':attribute harus berisi antara :min and :max item.',
        'file' => ':attribute harus berisi antara :min and :max kb.',
        'numeric' => ':attribute harus berisi antara :min and :max.',
        'string' => ':attribute harus berisi antara :min and :max karakter.',
    ],
    'boolean' => ':attribute harus berisi ya atau tidak.',
    'confirmed' => 'Konfirmasi :attribute tidak sama.',
    'current_password' => ':attribute salah.',
    'date' => ':attribute bukan tanggal yang benar.',
    'date_equals' => ':attribute harus sama dengan :date.',
    'date_format' => 'Format :attribute tidak sesuai dengan format :format.',
    'declined' => ':attribute harus ditolak.',
    'declined_if' => ':attribute harus ditolak ketika :other adalah :value.',
    'different' => ':attribute dan :other harus berbeda.',
    'digits' => ':attribute harus terdiri dari :digits digit.',
    'digits_between' => ':attribute harus berisi antara :min dan :max digit.',
    'dimensions' => 'Dimensi :attribute tidak benar.',
    'distinct' => ':attribute memiliki isian yang sama.',
    'doesnt_end_with' => ':attribute tidak boleh diakhiri: :values.',
    'doesnt_start_with' => ':attribute tidak boleh diawali: :values.',
    'email' => ':attribute harus berupa alamat email yang benar.',
    'ends_with' => ':attribute harus berakhiran: :values.',
    'enum' => ':attribute yang dipilih tidak benar.',
    'exists' => 'Pilihan :attribute tidak tersedia.',
    'file' => ':attribute harus berupa file.',
    'filled' => ':attribute harus memiliki isian yang benar.',
    'gt' => [
        'array' => ':attribute harus lebih dari :value item.',
        'file' => ':attribute harus lebih besar dari :value kb.',
        'numeric' => ':attribute harus lebih besar dari :value.',
        'string' => ':attribute harus lebih dari :value karakter.',
    ],
    'gte' => [
        'array' => ':attribute harus memiliki :value item atau lebih.',
        'file' => ':attribute harus lebih besar atau sama dengan :value kb.',
        'numeric' => ':attribute harus lebih besar atau sama dengan :value.',
        'string' => ':attribute harus lebih besar atau sama dengan :value karakter.',
    ],
    'image' => ':attribute harus berupa file gambar.',
    'in' => 'Pilihan :attribute tidak tersedia.',
    'in_array' => ':attribute tidak tersedia di :other.',
    'integer' => ':attribute harus berupa angka bilangan bulat.',
    'ip' => ':attribute harus berupa alamat IP yang benar.',
    'ipv4' => ':attribute harus berupa alamat IPv4 yang benar.',
    'ipv6' => ':attribute harus berupa alamat IPv6 yang benar.',
    'json' => ':attribute harus berupa JSON string.',
    'lowercase' => ':attribute harus berupa huruf kecil.',
    'lt' => [
        'array' => ':attribute harus kurang dari :value item.',
        'file' => ':attribute harus kurang dari :value kb.',
        'numeric' => ':attribute harus kurang dari :value.',
        'string' => ':attribute harus kurang dari :value karakter.',
    ],
    'lte' => [
        'array' => ':attribute tidak boleh lebih dari :value item.',
        'file' => ':attribute harus kurang atau sama dengan :value kb.',
        'numeric' => ':attribute harus kurang atau sama dengan :value.',
        'string' => ':attribute harus kurang atau sama dengan :value karakter.',
    ],
    'mac_address' => ':attribute harus berupa alamat MAC yang benar.',
    'max' => [
        'array' => ':attribute tidak boleh lebih dari :max item.',
        'file' => ':attribute tidak boleh lebih dari :max kb.',
        'numeric' => ':attribute tidak boleh lebih dari :max.',
        'string' => ':attribute tidak boleh lebih dari :max karakter.',
    ],
    'max_digits' => ':attribute tidak boleh lebih dari :max digit.',
    'mimes' => ':attribute harus berupa file dengan jenis: :values.',
    'mimetypes' => ':attribute harus berupa file dengan jenis: :values.',
    'min' => [
        'array' => ':attribute tidak boleh kurang dari :min item.',
        'file' => ':attribute tidak boleh kurang dari :min kb.',
        'numeric' => ':attribute tidak boleh kurang dari :min.',
        'string' => ':attribute tidak boleh kurang dari :min karakter.',
    ],
    'min_digits' => ':attribute tidak boleh kurang dari :min digit.',
    'multiple_of' => ':attribute harus kelipatan dari :value.',
    'not_in' => 'Pilihan :attribute tidak tersedia.',
    'not_regex' => 'Format :attribute tidak benar.',
    'numeric' => ':attribute harus berupa angka.',
    'password' => [
        'letters' => ':attribute harus memiliki sedikitnya 1 karakter huruf.',
        'mixed' => ':attribute harus memiliki sedikitnya 1 huruf besar dan 1 huruf kecil.',
        'numbers' => ':attribute harus memiliki sedikitnya 1 karakter angka.',
        'symbols' => ':attribute harus memiliki sedikitnya 1 huruf simbol.',
        'uncompromised' => ':attribute mudah ditebak. Silahkan pilih :attribute yang lain.',
    ],
    'present' => ':attribute harus sudah ada.',
    'prohibited' => ':attribute tidak menerima isian.',
    'prohibited_if' => ':attribute tidak menerima isian jika :other adalah :value.',
    'prohibited_unless' => ':attribute tidak menerima isian kecuali :other terdiri dari :values.',
    'prohibits' => ':attribute tidak boleh ada di :other.',
    'regex' => 'Format :attribute tidak benar.',
    'required' => ':attribute harus diisi.',
    'required_array_keys' => ':attribute harus berisi isian untuk: :values.',
    'required_if' => ':attribute harus diisi jika :other adalah :value.',
    'required_if_accepted' => ':attribute harus diisi ketika :other setuju.',
    'required_unless' => ':attribute harus diisi kecuali jika :other terdiri dari :values.',
    'required_with' => ':attribute harus diisi ketika :values sudah ada.',
    'required_with_all' => ':attribute harus diisi ketika :values sudah ada.',
    'required_without' => ':attribute harus diisi ketika :values tidak ada.',
    'required_without_all' => ':attribute harus diisi ketika :values tidak ada.',
    'same' => ':attribute dan :other harus sama.',
    'size' => [
        'array' => ':attribute harus terdiri dari :size item.',
        'file' => ':attribute harus berukuran :size kb.',
        'numeric' => ':attribute harus bernilai :size.',
        'string' => ':attribute harus terdiri dari :size karakter.',
    ],
    'starts_with' => ':attribute harus berawalan: :values.',
    'string' => ':attribute harus berupa karakter.',
    'timezone' => ':attribute harus berupa waktu zona yang benar.',
    'unique' => ':attribute sudah digunakan.',
    'uploaded' => ':attribute gagal diunggah.',
    'uppercase' => ':attribute harus berupa huruf besar.',
    'url' => ':attribute harus URL yang benar.',
    'uuid' => ':attribute harus UUID yang benar.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        // 'attribute-name' => [
        //     'rule-name' => 'custom-message',
        // ],

        'acc_no_company' => [
            'unique' => ':attribute sudah digunakan.',
        ],
        'acc_no_member' => [
            'unique' => (MEMBER_BANK_LIMIT > 1) ? ':attribute sudah melebihi batas maksimum penggunaan.' : ':attribute sudah digunakan.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'username' => 'Username',
        'password' => 'Password',
        'old_password' => 'Password Lama',
        'new_password' => 'Password Baru',
        'bank_id' => 'Bank',
        'acc_no' => 'Nomor Rekening',
        'acc_no_company' => 'Nomor Rekening',
        'acc_no_member' => 'Nomor Rekening',
        'acc_name' => 'Pemilik Rekening',
        'package_name' => 'Nama Paket',
        'package_bonus' => 'Keterangan Bonus Paket',
        'package_left' => 'Jumlah Paket Kiri',
        'package_right' => 'Jumlah Paket Kanan',
        'pin_count' => 'Jumlah PIN',
        'description' => 'Ketarangan',
        'bonus' => 'Bonus',
        'bonus_sponsor' => 'Bonus Sponsor',
        'bonus_level' => 'Bonus Level',
        'level' => 'Level',
        'pair_point' => 'Poin Pasangan',
        'reward_point' => 'Poin Reward',
        'phone' => 'Nomor Handphone',
        'referral_username' => 'Referral',
    ],
];
