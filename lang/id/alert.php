<?php

return [
    'title' => [
        'success' => 'Berhasil',
        'warning' => 'Perhatian',
        'danger' => 'Error',
        'info' => 'Pemberitahuan',
        'question' => 'Penting'
    ],
];
