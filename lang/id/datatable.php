<?php

return [
    "decimal" =>        ",",
    "emptyTable" =>     "Tidak ada data",
    "info" =>           "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
    "infoEmpty" =>      "",
    "infoFiltered" =>   "(disaring dari _MAX_ total data)",
    "infoPostFix" =>    "",
    "thousands" =>      ".",
    "lengthMenu" =>     "Tampilkan _MENU_ data",
    "loadingRecords" => "Memproses...",
    "processing" =>     "Memproses...",
    "search" =>         "",
    "zeroRecords" =>    "Tidak ada data",
    "paginate" => [
        'first' => '&lt;&lt;',
        'previous' => '&lt;',
        'next' => '&gt;',
        'last' => '&gt;&gt;'
    ],
    "aria" => [
        "sortAscending" =>  ": aktifkan untuk mengurutkan kolom secara naik",
        "sortDescending" => ": aktifkan untuk mengurutkan kolom secara menurun",
    ],
    "searchPlaceholder" => "Cari",
];
