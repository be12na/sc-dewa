<?php

return [
    'thousand' => '.',
    'decimal' => ',',
    'currency' => [
        'text' => 'Rupiah',
        'symbol' => [
            'text' => 'Rp',
            'start' => 'Rp. ',
            'end' => ';-',
        ],
    ],
    'date' => [
        'short' => 'd-m-Y',
        'medium' => 'j M Y',
        'full' => 'j F Y',
    ],
    'datetime' => [
        'short' => 'd-m-Y, H:i',
        'medium' => 'd M Y, H:i',
        'full' => 'j F Y, H:i',
    ],
];
