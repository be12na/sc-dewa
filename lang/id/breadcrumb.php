<?php

return [
    'dashboard' => 'Dashboard',
    'password' => 'Password',
    'profile' => 'Profile',
    'bank' => 'Rekening Bank',
    'settings' => [
        'bank' => ['Pengaturan', 'Rekening Bank'],
        'package' => ['Pengaturan', 'Paket'],
        'bonus' => [
            'sponsor' => ['Pengaturan', 'Bonus', 'Sponsor'],
            'level' => ['Pengaturan', 'Bonus', 'Level'],
        ],
    ],
    'package' => [
        'select' => ['Paket', 'Pilih'],
        'activate' => ['Paket', 'Aktivasi'],
        'upgrade' => ['Paket', 'Upgrade'],
        'ro' => ['Paket', 'Repeat Order'],
        'transaction' => [
            'list' => ['Paket', 'Transaksi'],
        ],
        'history' => ['Paket', 'Riwayat'],
    ],
    'member' => [
        'register' => ['Member', 'Pendaftaran'],
        'not-active' => ['Member', 'Tidak Aktif'],
        'inactivated' => ['Member', 'Belum Aktif'],
        'placement' => ['Member', 'Penempatan'],
        'sponsored' => ['Member', 'Disponsori'],
    ],
    'network' => [
        'tree' => ['Jaringan', 'Struktur'],
        'list' => ['Jaringan', 'Daftar'],
        'sponsored' => ['Jaringan', 'Disponsori'],
    ],
    'bonus' => [
        'sponsor' => ['Bonus', 'Sponsor'],
        'level' => ['Bonus', 'Level'],
        'extra' => ['Bonus', 'Extra'],
        'ro' => ['Bonus', 'Repeat Order'],
        'upgrade' => ['Bonus', 'Upgrade'],
        'package' => ['Bonus', 'Paket'],
        'reward' => ['Bonus', 'Reward'],
        'summary' => ['Bonus', 'Ringkasan'],
    ],
    'withdraw' => [
        'bonus' => [
            'sponsor' => ['Penarikan', 'Bonus', 'Sponsor'],
            'extra' => ['Penarikan', 'Bonus', 'Extra'],
            'ro' => ['Penarikan', 'Bonus', 'Repeat Order'],
            'upgrade' => ['Penarikan', 'Bonus', 'Upgrade'],
            'level' => ['Penarikan', 'Bonus', 'Level'],
        ],
        'histories' => ['Penarikan', 'Bonus', 'Riwayat'],
        'reward' => ['Penarikan', 'Reward'],
    ],
];
