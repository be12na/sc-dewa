<?php

return [
    'registration' => [
        'success' => 'Pendaftaran berhasil.',
        'fail' => 'Pendaftaran gagal.',
    ],
    'password' => [
        'changed' => 'Password berhasil diubah.',
        'failed' => 'Gagal merubah password',
    ],
    'validation-error' => 'Gagal Validasi',
    'bank' => [
        'added' => 'Rekening Bank berhasil ditambahkan.',
        'updated' => 'Rekening Bank berhasil diperbarui.',
        'activate' => 'Aktifkan Rekening Bank?',
        'activated' => 'Rekening Bank berhasil diaktifkan.',
        'deactivate' => 'Menonaktifkan Rekening Bank?',
        'deactivated' => 'Rekening Bank berhasil dinonaktifkan.',
        'incomplete' => 'Anda belum memiliki data Rekening Bank.',
    ],
    'package' => [
        // 'added' => 'Paket berhasil ditambahkan.',
        'updated' => 'Paket berhasil diperbarui.',
        'dont-have' => 'Akun anda belum aktif, silahkan aktivasi!',
        'completed' => 'Anda sudah memiliki paket.',
        'incomplete' => 'Transaksi paket anda belum selesai.',
        'confirmed' => 'Transaksi paket berhasil dikonfirmasi.',
        'canceled' => 'Transaksi paket berhasil dibatalkan.',
        'rejected' => 'Transaksi paket berhasil ditolak.',
        // 'ordered' => 'Paket yang dipilih telah dipesankan.<br>
        // Silahkan Klik No. Transaksi untuk details pembayaran.<br>
        // Konfirmasi Ke Whatsapp: 0859-3925-9932',
        'ordered' => 'Paket yang dipilih telah dipesankan.',
        'top-level-reached' => 'Anda sudah memiliki paket tertinggi, tidak dapat diupgrade.',
        'admin-top-level-reached' => 'Member sudah memiliki paket tertinggi, tidak dapat diupgrade.',
        'cant-ro' => 'Anda belum dapat melakukan transaksi Repeat Order.',
        // 'reordered' => 'Transaksi Repeat Order telah dipesankan.<br>
        // Silahkan Klik No. Transaksi untuk details pembayaran.<br>
        // Konfirmasi Ke Whatsapp: 0859-3925-9932',
        'reordered' => 'Transaksi Repeat Order telah dipesankan.',
    ],
    'bonus' => [
        'sponsor' => [
            'updated' => 'Bonus Sponsor berhasil diperbarui.',
        ],
        'level' => [
            'added' => 'Bonus Level berhasil ditambahkan.',
            'updated' => 'Bonus Level berhasil diperbarui.',
        ],
    ],
    'profile' => [
        'incomplete' => 'Silahkan lengkapi data pribadi anda.',
        'updated' => 'Profile berhasil diperbarui.',
    ],
    'mail' => [
        'activated' => 'Selamat! Akun Anda telah aktif',
        'upgraded' => 'Selamat! Akun Anda telah di-upgrade',
        'repeat-order' => 'Repeat order sudah dikonfirmasi',
    ],
    'member' => [
        'inactive' => 'Member sudah tidak aktif.',
        'banned' => 'Member sudah diblokir.',
        'not-found' => 'Member tidak ditemukan.',
        'not-placement' => 'Member belum ditempatkan dalam struktur.',
        'success-placement' => 'Penempatan member telah berhasil.',
        'fail-placement' => 'Penempatan member gagal.',
        'updated' => 'Data Member berhasil diperbarui.',
        'password-changed' => 'Password Member berhasil diperbarui.',
    ],
    'forbidden' => 'Anda tidak memiliki akses untuk :attribute tersebut.',
    'not-owner' => ':attribute bukan milik anda.',
    'not-found' => 'Data tidak ditemukan.',
    'withdraw' => [
        'not-available' => 'Jenis penarikan tidak tersedia.',
        'not-selection' => 'Tidak ada data yang dipilih.',
        'transfer-success' => 'Penarikan bonus yang ditransfer berhasil disimpan.',
        'transfer-fail' => 'Penarikan bonus yang ditransfer gagal disimpan.',
        'confirm-transfer' => 'Benarkah data yang dipilih adalah data yang sudah ditransfer ?',
    ],
    'reward' => [
        'claimed' => 'Reward sudah diklaim.',
        'cant-claim' => 'Anda belum dapat mengklaim reward tersebut.',
        'claim-success' => 'Reward berhasil diklaim.',
        'claim-failed' => 'Reward gagal diklaim.',
        'confirm-success' => 'Reward berhasil dikonfirmasi.',
        'confirm-fail' => 'Reward gagal dikonfirmasi.',
        'reject-success' => 'Reward berhasil ditolak.',
        'reject-fail' => 'Reward gagal ditolak.',
        'alert-confirm' => 'Yakin untuk mengkonfirmasi reward :reward untuk :member.',
    ],
];
