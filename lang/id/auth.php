<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Username dan password tidak terdaftar.',
    'password' => 'Password yang dimasukkan salah.',
    'throttle' => 'Terlalu banyak gagal login. Silahkan coba lagi setelah :seconds detik.',
    'end' => 'Sesi anda telah habis.',

];
