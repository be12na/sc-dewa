<?php

return [
    'dashboard' => 'Dashboard',
    'setting' => 'Pengaturan',
    'bank' => 'Rekening Bank',
    'password' => 'Password',
    'package' => 'Paket',
    'order' => 'Order',
    'upgrade' => 'Upgrade',
    'history' => 'Riwayat',
    'logout' => 'Keluar',
    'contact_center' => 'Contact Center',
    'bonus' => 'Bonus',
    'bonus-sponsor' => 'Bonus Sponsor',
    'bonus-level' => 'Bonus Level',
    'bonus-extra' => 'Bonus Extra',
    'bonus-ro' => 'Bonus RO',
    'bonus-upgrade' => 'Bonus Upgrade',
    'bonus-package' => 'Bonus Paket',
    'bonus-reward' => 'Bonus Reward',
    'bonus-summary' => 'Ringkasan Bonus',
    'summary' => 'Ringkasan',
    'member' => 'Member',
    'register' => 'Pendaftaran',
    'active' => 'Aktif',
    'inactive' => 'Tidak Aktif',
    'inactivated' => 'Belum Aktif',
    'all' => 'Semua',
    'withdraw' => 'Penarikan',
    'reports' => 'Laporan',
    'report' => 'Laporan',
    'transaction' => 'Transaksi',
    'placement' => 'Penempatan',
    'networks' => 'Jaringan',
    'structure-tree' => 'Struktur Pohon',
    'sponsored' => 'Disponsori',
    'network-list' => 'Daftar Jaringan',
    'profile' => 'Profile',
    'reward' => 'Reward',
    'ro' => 'Repeat Order',
];
// 'social-media' => 'Sosial Media',
