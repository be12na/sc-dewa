<?php

use Illuminate\Support\Facades\Route;

Route::prefix('package')
    ->name('package.')
    ->group(function () {

        Route::middleware('auth.package:1')
            ->group(function () {
                Route::prefix('transaction')
                    ->name('transaction.')
                    ->group(function () {
                        // package.transaction.index => /package/transaction
                        Route::get('/', [\App\Http\Controllers\PackageController::class, 'transactions'])->name('index');
                        // package.transaction.dataTable => /package/transaction/data
                        Route::get('/data', [\App\Http\Controllers\PackageController::class, 'transactions'])->name('dataTable');
                    });

                Route::prefix('histories')
                    ->name('history.')
                    ->group(function () {
                        // package.history.index => /package/histories
                        Route::get('/', [\App\Http\Controllers\PackageController::class, 'transHistories'])->name('index');
                        // package.history.dataTable => /package/histories/data
                        Route::get('/data', [\App\Http\Controllers\PackageController::class, 'transHistories'])->name('dataTable');
                    });

                // package.detail => /package/detail/{transPackage}
                Route::get('/detail/{transPackage}', [\App\Http\Controllers\PackageController::class, 'transDetail'])->name('detail');
            });

        Route::prefix('transaction')
            ->name('transaction.')
            // ->middleware(['auth.role:admin.super,admin.master,member.member', 'auth.package'])
            ->middleware(['auth.role:admin.super,admin.master'])
            ->group(function () {
                // package.transaction.viewConfirm => /package/confirm-modal/{transPackage}/viewConfirm
                Route::get('/trans/{transPackage}/view', [\App\Http\Controllers\PackageController::class, 'transConfirmation'])->name('viewConfirm');
                // package.transaction.confirm => /package/trans/{transPackage}/confirm
                Route::post('/trans/{transPackage}/confirm', [\App\Http\Controllers\PackageController::class, 'transConfirmation'])->name('confirm');
            });

        Route::middleware('auth.group:member')
            ->group(function () {
                Route::prefix('activate')
                    ->name('activate.')
                    ->middleware('auth.not.package')
                    ->group(function () {
                        // package.activate.index => /package/start
                        Route::get('/', [\App\Http\Controllers\PackageController::class, 'memberPackageActivate'])->name('index');
                        // package.activate.selected => /package/start/selected/{package}
                        Route::get('/selected/{package}', [\App\Http\Controllers\PackageController::class, 'memberPackageActivate'])->name('selected');
                        // package.activate.confirmed => /package/start/confirmed/{package}
                        Route::post('/confirmed/{package}', [\App\Http\Controllers\PackageController::class, 'memberPackageActivate'])->name('confirmed');
                    });

                Route::prefix('upgrade')
                    ->name('upgrade.')
                    ->middleware('auth.package:2')
                    ->group(function () {
                        // package.upgrade.index => /package/upgrade
                        Route::get('/', [\App\Http\Controllers\PackageController::class, 'memberPackageUpgrade'])->name('index');
                        // package.upgrade.selected => /package/upgrade/selected/{package}
                        Route::get('/selected/{package}', [\App\Http\Controllers\PackageController::class, 'memberPackageUpgrade'])->name('selected');
                        // package.upgrade.confirmed => /package/upgrade/confirmed/{package}
                        Route::post('/confirmed/{package}', [\App\Http\Controllers\PackageController::class, 'memberPackageUpgrade'])->name('confirmed');
                    });

                Route::prefix('repeat-order')
                    ->name('ro.')
                    ->middleware(['auth.package', 'auth.ro'])
                    ->group(function () {
                        // package.ro.index => /package/repeat-order
                        Route::get('/', [\App\Http\Controllers\PackageController::class, 'memberPackageRepeatOrder'])->name('index');
                        // package.ro.selected => /package/repeat-order/selected
                        Route::get('/selected', [\App\Http\Controllers\PackageController::class, 'memberPackageRepeatOrder'])->name('selected');
                        // package.ro.confirmed => /package/repeat-order/confirmed
                        Route::post('/confirmed', [\App\Http\Controllers\PackageController::class, 'memberPackageRepeatOrder'])->name('confirmed');
                    });
            });
    });
