<?php

use Illuminate\Support\Facades\Route;

Route::prefix('withdraw')
    ->name('withdraw.')
    ->middleware('auth.package')
    ->group(function () {
        Route::prefix('sponsor')
            ->name('sponsor.')
            ->group(function () {
                Route::middleware('auth.group:admin')
                    ->group(function () {
                        Route::get('/', [\App\Http\Controllers\WithdrawController::class, 'indexAdmin'])->name('index');
                        Route::get('/data', [\App\Http\Controllers\WithdrawController::class, 'dataTableAdmin'])->name('dataTable');
                    });
            });

        Route::prefix('extra')
            ->name('extra.')
            ->group(function () {
                Route::middleware('auth.group:admin')
                    ->group(function () {
                        Route::get('/', [\App\Http\Controllers\WithdrawController::class, 'indexAdmin'])->name('index');
                        Route::get('/data', [\App\Http\Controllers\WithdrawController::class, 'dataTableAdmin'])->name('dataTable');
                    });
            });

        Route::prefix('repeat-order')
            ->name('ro.')
            ->group(function () {
                Route::middleware('auth.group:admin')
                    ->group(function () {
                        Route::get('/', [\App\Http\Controllers\WithdrawController::class, 'indexAdmin'])->name('index');
                        Route::get('/data', [\App\Http\Controllers\WithdrawController::class, 'dataTableAdmin'])->name('dataTable');
                    });
            });

        Route::prefix('upgrade')
            ->name('upgrade.')
            ->group(function () {
                Route::middleware('auth.group:admin')
                    ->group(function () {
                        Route::get('/', [\App\Http\Controllers\WithdrawController::class, 'indexAdmin'])->name('index');
                        Route::get('/data', [\App\Http\Controllers\WithdrawController::class, 'dataTableAdmin'])->name('dataTable');
                    });
            });

        Route::middleware('auth.group:admin')
            ->group(function () {
                Route::prefix('histories')
                    ->name('histories.')
                    ->group(function () {
                        Route::get('/', [\App\Http\Controllers\WithdrawController::class, 'indexHistoriesAdmin'])->name('index');
                        Route::get('/data', [\App\Http\Controllers\WithdrawController::class, 'dataTableHistoriesAdmin'])->name('dataTable');
                    });

                Route::get('/transfer', [\App\Http\Controllers\WithdrawController::class, 'transfer'])->name('viewTransfer');
                Route::post('/transfer', [\App\Http\Controllers\WithdrawController::class, 'transfer'])->name('saveTransfer');
            });

        Route::prefix('reward')
            ->name('reward.')
            ->middleware('auth.group:admin')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\WithdrawController::class, 'indexRewardAdmin'])->name('index');
                Route::get('/data', [\App\Http\Controllers\WithdrawController::class, 'dataTableRewardAdmin'])->name('dataTable');
                Route::get('/action/{userReward}/{action}', [\App\Http\Controllers\WithdrawController::class, 'actionRewardAdmin'])->name('viewAction');
                Route::post('/action/{userReward}/{action}', [\App\Http\Controllers\WithdrawController::class, 'actionRewardAdmin'])->name('postAction');
            });
    });
