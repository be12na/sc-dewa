<?php

use Illuminate\Support\Facades\Route;

Route::prefix('profile')
    ->name('profile.')
    ->middleware('auth.group:member')
    ->group(function () {
        // profile.index => /profile
        Route::get('/', [App\Http\Controllers\ProfileController::class, 'index'])->name('index');
        // profile.store => /profile/store
        Route::post('/update', [App\Http\Controllers\ProfileController::class, 'update'])->name('update');
    });
