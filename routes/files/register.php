<?php

use Illuminate\Support\Facades\Route;

Route::prefix('register')
    ->name('register.')
    ->group(function () {
        // register.index => /register
        Route::get('/', [App\Http\Controllers\RegistrationController::class, 'index'])->name('index');
        // register.store => /register/store
        Route::post('/store', [App\Http\Controllers\RegistrationController::class, 'store'])->name('store');
    });
