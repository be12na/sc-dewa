<?php

use Illuminate\Support\Facades\Route;

Route::prefix('setting')
    ->name('setting.')
    ->middleware('auth.group:admin')
    ->group(function () {
        include("bank.php");

        Route::prefix('package')
            ->name('package.')
            ->group(function () {
                // package.index => /package
                Route::get('/', [\App\Http\Controllers\PackageController::class, 'index'])->name('index');
                // package.detail => /package/detail/{package}
                Route::get('/detail/{package}', [\App\Http\Controllers\PackageController::class, 'detail'])->name('detail');

                Route::middleware('auth.role:admin.super')
                    ->group(function () {
                        // package.edit => /package/edit/{package}
                        Route::get('/edit/{package}', [\App\Http\Controllers\PackageController::class, 'edit'])->name('edit');
                        // package.update => /package/update/{package}
                        Route::post('/update/{package}', [\App\Http\Controllers\PackageController::class, 'update'])->name('update');
                    });
            });

        Route::prefix('bonus')
            ->name('bonus.')
            ->group(function () {
                // bonus sponsor
                Route::prefix('sponsor')
                    ->name('sponsor.')
                    ->group(function () {
                        // setting.bonus.sponsor.index => /setting/bonus/sponsor
                        Route::get('/', [App\Http\Controllers\BonusController::class, 'indexSettingBonusSponsor'])->name('index');
                        // setting.bonus.sponsor.update => /setting/bonus/sponsor/update
                        Route::post('/update', [App\Http\Controllers\BonusController::class, 'updateSettingBonusSponsor'])
                            ->middleware('auth.role:admin.super')
                            ->name('update');
                    });

                // bonus level
                Route::prefix('level')
                    ->name('level.')
                    ->group(function () {
                        // setting.bonus.level.index => /setting/bonus/level
                        Route::get('/', [App\Http\Controllers\BonusController::class, 'indexSettingBonusLevel'])->name('index');
                        // setting.bonus.level.dataTable => /setting/bonus/level/data
                        Route::get('/data', [App\Http\Controllers\BonusController::class, 'dataTableSettingBonusLevel'])->name('dataTable');

                        Route::middleware('auth.role:admin.super')
                            ->group(function () {
                                // setting.bonus.level.add => /setting/bonus/level/add
                                Route::get('/add', [App\Http\Controllers\BonusController::class, 'addSettingBonusLevel'])->name('add');
                                // setting.bonus.level.update => /setting/bonus/level/update
                                Route::post('/store', [App\Http\Controllers\BonusController::class, 'storeSettingBonusLevel'])->name('store');
                                // setting.bonus.level.edit => /setting/bonus/level/edit/{bonusLevel}
                                Route::get('/edit/{bonusLevel}', [App\Http\Controllers\BonusController::class, 'editSettingBonusLevel'])->name('edit');
                                // setting.bonus.level.update => /setting/bonus/level/update
                                Route::post('/update/{bonusLevel}', [App\Http\Controllers\BonusController::class, 'updateSettingBonusLevel'])->name('update');
                            });
                    });
            });
    });
