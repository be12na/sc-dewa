<?php

use Illuminate\Support\Facades\Route;

Route::prefix('member')
    ->name('member.')
    ->group(function () {
        Route::middleware('auth.group:admin')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\MemberController::class, 'index'])->name('index');
                Route::get('/data', [\App\Http\Controllers\MemberController::class, 'dataTable'])->name('dataTable');

                Route::prefix('edit/{userMember}')
                    ->name('edit.')
                    ->middleware('auth.role:admin.super,admin.master')
                    ->group(function () {
                        Route::get('/data', [\App\Http\Controllers\MemberController::class, 'editMemberByAdmin'])->name('data');
                        Route::get('/password', [\App\Http\Controllers\MemberController::class, 'editMemberByAdmin'])->name('password');
                        Route::post('/update', [\App\Http\Controllers\MemberController::class, 'updateByAdmin'])->name('update');
                    });
            });

        Route::middleware(['auth.group:member', 'auth.package'])
            ->group(function () {
                Route::prefix('inactive')
                    ->name('inactive.')
                    ->group(function () {
                        // member.inactive.index
                        Route::get('/', [\App\Http\Controllers\MemberController::class, 'indexInactive'])->name('index');
                        // member.inactive.dataTable
                        Route::get('/data', [\App\Http\Controllers\MemberController::class, 'dataTableInactive'])->name('dataTable');
                    });

                Route::prefix('placement')
                    ->name('placement.')
                    ->group(function () {
                        // member.placement.index
                        Route::get('/', [\App\Http\Controllers\MemberController::class, 'placement'])->name('index');
                        // member.placement.select
                        Route::get('/select/{member}/{position}', [\App\Http\Controllers\MemberController::class, 'placement'])->name('select');
                        // member.placement.save
                        Route::post('/save', [\App\Http\Controllers\MemberController::class, 'placement'])->name('save');
                    });

                Route::prefix('register')
                    ->name('register.')
                    ->group(function () {
                        // register.index => /register
                        Route::get('/', [App\Http\Controllers\RegistrationController::class, 'index'])->name('index');
                        // register.store => /register/store
                        Route::post('/store', [App\Http\Controllers\RegistrationController::class, 'store'])->name('store');
                    });
            });
    });
