<?php

use Illuminate\Support\Facades\Route;

Route::prefix('bonus')
    ->name('bonus.')
    ->middleware('auth.package')
    ->group(function () {
        Route::prefix('sponsor')
            ->name('sponsor.')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\BonusController::class, 'indexBonusSponsor'])->name('index');
                Route::get('/data', [\App\Http\Controllers\BonusController::class, 'dataBonusSponsor'])->name('dataTable');
            });

        Route::prefix('extra')
            ->name('extra.')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\BonusController::class, 'indexBonusExtra'])->name('index');
                Route::get('/data', [\App\Http\Controllers\BonusController::class, 'dataBonusExtra'])->name('dataTable');
            });

        Route::prefix('repeat-order')
            ->name('ro.')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\BonusController::class, 'indexBonusRO'])->name('index');
                Route::get('/data', [\App\Http\Controllers\BonusController::class, 'dataBonusRO'])->name('dataTable');
            });

        Route::prefix('upgrade')
            ->name('upgrade.')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\BonusController::class, 'indexBonusUpgrade'])->name('index');
                Route::get('/data', [\App\Http\Controllers\BonusController::class, 'dataBonusUpgrade'])->name('dataTable');
            });

        Route::prefix('package')
            ->name('package.')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\BonusController::class, 'indexBonusPackage'])->name('index');
            });

        Route::prefix('summary')
            ->name('summary.')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\BonusController::class, 'indexBonusSummary'])->name('index');
                Route::get('/data', [\App\Http\Controllers\BonusController::class, 'dataBonusSummary'])->name('dataTable');
            });

        Route::prefix('reward')
            ->name('reward.')
            ->middleware('auth.group:member')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\BonusController::class, 'indexBonusReward'])->name('index');
                Route::get('/claim/{reward}', [\App\Http\Controllers\BonusController::class, 'claimBonusReward'])->name('claim');
                Route::post('/claim/{reward}', [\App\Http\Controllers\BonusController::class, 'claimBonusReward'])->name('postClaim');
            });
    });
