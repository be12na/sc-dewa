<?php

use Illuminate\Support\Facades\Route;

Route::prefix('network')
    ->name('network.')
    ->middleware('auth.package')
    ->group(function () {
        Route::prefix('sponsored')
            ->name('sponsored.')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\NetworkController::class, 'indexSponsored'])->name('index');
                Route::get('/data', [\App\Http\Controllers\NetworkController::class, 'dataTableSponsored'])->name('dataTable');
            });

        Route::prefix('tree')
            ->name('tree.')
            ->middleware('auth.structure')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\NetworkController::class, 'indexTree'])->name('index');
                Route::get('/detail/{member}', [\App\Http\Controllers\NetworkController::class, 'treeNodeDetail'])->name('detail');
            });
    });
