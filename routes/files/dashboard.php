<?php

use Illuminate\Support\Facades\Route;

Route::prefix('dashboard')
    ->name('dashboard.')
    ->middleware('auth.package')
    ->group(function () {
        // dashboard.index => /dashboard
        Route::get('/', [\App\Http\Controllers\DashboardController::class, 'index'])->name('index');
        // dashboard.item => /dashboard/item
        Route::get('/item', [\App\Http\Controllers\DashboardController::class, 'dashboardItem'])->name('item');
    });
