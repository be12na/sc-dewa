<?php

use Illuminate\Support\Facades\Route;

Route::prefix('password')
    ->name('password.')
    ->group(function () {
        // password.change => /password/change
        Route::get('/change', [App\Http\Controllers\PasswordController::class, 'updatePassword'])->name('change');
        // password.update => /password/update
        Route::post('/update', [App\Http\Controllers\PasswordController::class, 'updatePassword'])->name('update');
    });
