<?php

use Illuminate\Support\Facades\Route;

Route::prefix('bank')
    ->name('bank.')
    ->group(function () {
        // bank.index => /bank
        Route::get('/', [App\Http\Controllers\BankController::class, 'index'])->name('index');
        // bank.add => /bank/add
        Route::middleware('auth.role:admin.super,member.member')
            ->group(function () {
                Route::get('/add', [App\Http\Controllers\BankController::class, 'add'])->name('add');
                // bank.store => /bank/store
                Route::post('/store', [App\Http\Controllers\BankController::class, 'store'])->name('store');
                // bank.edit => /bank/edit/{userBank}
                Route::get('/edit/{userBank}', [App\Http\Controllers\BankController::class, 'edit'])->name('edit');
                // bank.update => /bank/update/{userBank}
                Route::post('/update/{userBank}', [App\Http\Controllers\BankController::class, 'update'])->name('update');
                // bank.status => /bank/status/{userBank}
                Route::get('/status/{userBank}/{userBankStatus}', [App\Http\Controllers\BankController::class, 'status'])->name('status');
                // bank.updateStatus => /bank/update-status/{userBank}
                Route::post('/update-status/{userBank}', [App\Http\Controllers\BankController::class, 'updateStatus'])->name('updateStatus');
            });
    });
