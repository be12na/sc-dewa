<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('welcome');
Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('welcome');

Route::middleware('guest')
    ->group(function () {
        Auth::routes([
            'logout' => false,
            'register' => false,
            'confirm' => false,
            'verify' => false,
        ]);

        // Route::prefix('login')
        //     ->group(function () {
        //         Route::get('/', [\App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
        //         Route::post('/', [\App\Http\Controllers\Auth\LoginController::class, 'login']);
        //     });

        // Route::prefix('register')
        //     ->group(function () {
        //         Route::get('/', [\App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
        //         Route::post('/', [\App\Http\Controllers\Auth\RegisterController::class, 'login']);
        //     });

        Route::prefix('register/ref/{referralUser}')
            ->name('registerReferral.')
            ->group(function () {
                // register.index => /register
                Route::get('/', [App\Http\Controllers\RegistrationController::class, 'index'])->name('index');
                // register.store => /register/store
                Route::post('/store', [App\Http\Controllers\RegistrationController::class, 'store'])->name('store');
            });
    });

Route::prefix('logout')
    ->group(function () {
        Route::get('/', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
        Route::post('/', [\App\Http\Controllers\Auth\LoginController::class, 'logout']);
    });

Route::middleware('auth')
    ->group(function () {
        Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

        include('files/profile.php');
        include('files/password.php');
        include("files/package.php");
        include('files/dashboard.php');
        include('files/setting.php');
        include('files/member.php');
        include("files/bonus.php");
        include("files/withdraw.php");

        Route::middleware('auth.group:member')
            ->group(function () {
                include('files/network.php');
                include("files/bank.php");
            });
    });
