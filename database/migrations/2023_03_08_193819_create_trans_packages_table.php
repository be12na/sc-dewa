<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_packages', function (Blueprint $table) {
            $table->id();
            $table->string('code', 30)->unique('uq_trans_pkg');
            $table->unsignedBigInteger('seller_id');
            $table->unsignedBigInteger('buyer_id');
            $table->unsignedInteger('package_id');
            $table->string('description', 250)->nullable();
            $table->unsignedInteger('pin_count');
            $table->unsignedSmallInteger('trans_type');
            $table->unsignedDecimal('package_price', 20, 4);
            $table->unsignedInteger('unique_digit')->default(0);
            $table->unsignedSmallInteger('status')->default(TRANS_ORDER);
            $table->timestamp('status_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_packages');
    }
};
