<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pin_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->unsignedSmallInteger('type')->default(PIN_TYPE_GLOBAL);
            $table->unsignedDecimal('price', 20, 4);
            $table->unsignedSmallInteger('plan')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();

            $table->unique(['name', 'type', 'plan'], 'uq_pin_type');
        });

        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30)->unique('uq_pkg_name');
            $table->unsignedInteger('pin_type_id')->default(1);
            $table->unsignedInteger('pin_count')->default(0);
            $table->unsignedDecimal('price', 20, 4)->default(0);
            $table->unsignedDecimal('bonus_sponsor', 20, 4)->default(0);
            $table->unsignedDecimal('bonus_extra', 20, 4)->default(0);
            $table->boolean('bonus_package')->default(false);
            $table->unsignedInteger('package_left')->default(1);
            $table->unsignedInteger('package_right')->default(1);
            $table->string('package_bonus', 250)->nullable();
            $table->unsignedInteger('pair_point')->default(1);
            $table->boolean('get_reward')->default(false);
            $table->boolean('give_reward')->default(false);
            $table->unsignedInteger('reward_point')->default(1);
            $table->boolean('is_active')->default(true);
            $table->string('logo', 15)->nullable();
            $table->string('description', 250)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
        Schema::dropIfExists('pin_types');
    }
};
