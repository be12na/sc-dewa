<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username', 32)->unique('uq_user_username');
            $table->string('name', 100);
            $table->string('email', 100);
            $table->string('password', 250);

            $table->unsignedSmallInteger('user_type');
            $table->unsignedSmallInteger('user_status')->default(USER_STATUS_ACTIVATED);
            $table->string('status_note', 250)->nullable();
            $table->boolean('user_login')->default(USER_REGISTER_LOGIN);

            $table->unsignedBigInteger('referral_id')->nullable();
            $table->unsignedInteger('package_id')->nullable();

            $table->boolean('has_profile')->default(false);
            $table->text('address')->nullable();
            $table->string('pos_code', 5)->nullable();
            $table->string('phone', 16)->nullable();

            $table->string('session_id', 255)->nullable();

            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('registered_at')->nullable();
            $table->timestamp('inactive_at')->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('banned_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
