<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rewards', function (Blueprint $table) {
            $table->unsignedInteger('id')->primary();
            $table->unsignedInteger('package_id');
            $table->string('value', 255);
            $table->unsignedInteger('left');
            $table->unsignedInteger('right');
            $table->boolean('repeatable')->default(false);
            $table->boolean('is_active')->default(true);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('user_rewards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            // $table->unsignedSmallInteger('reward_type');
            $table->unsignedInteger('reward_id');
            $table->unsignedInteger('point_left');
            $table->unsignedInteger('point_right');
            $table->string('reward_value', 255);
            $table->unsignedInteger('reward_left');
            $table->unsignedInteger('reward_right');
            $table->unsignedSmallInteger('status')->default(WD_PROCESSING);
            $table->timestamp('status_at')->nullable();
            $table->string('reject_note', 255)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rewards');
        Schema::dropIfExists('rewards');
    }
};
