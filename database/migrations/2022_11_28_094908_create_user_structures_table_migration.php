<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('user_structures', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedInteger('position');
            $table->unsignedInteger('side_position');
            $table->softDeletes();

            $table->foreign('parent_id')
                ->references('id')
                ->on('user_structures')
                ->onDelete('set null');
        });

        Schema::create('user_structure_closure', function (Blueprint $table) {
            $table->id('closure_id');

            $table->unsignedBigInteger('ancestor');
            $table->unsignedBigInteger('descendant');
            $table->unsignedInteger('depth');

            $table->foreign('ancestor')
                ->references('id')
                ->on('user_structures')
                ->onDelete('cascade');

            $table->foreign('descendant')
                ->references('id')
                ->on('user_structures')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_structure_closure');
        Schema::dropIfExists('user_structures');
    }
};
