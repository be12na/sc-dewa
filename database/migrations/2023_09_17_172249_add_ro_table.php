<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->unsignedDecimal('bonus_ro', 20, 4)->default(100000)->after('bonus_extra');
        });

        Schema::table('user_bonuses', function (Blueprint $table) {
            $table->unsignedBigInteger('ro_id')->nullable()->after('wd_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_bonuses', function (Blueprint $table) {
            $table->dropColumn(['ro_id']);
        });

        Schema::table('packages', function (Blueprint $table) {
            $table->dropColumn(['bonus_ro']);
        });
    }
};
