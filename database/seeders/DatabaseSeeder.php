<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Bank;
use App\Models\Package;
use App\Models\PinType;
use App\Models\Reward;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->seedDefaultUser();
        $this->seedBanks();
        $this->seedPackages();
        $this->seedRewards();
    }

    private function seedDefaultUser()
    {
        $dateTimeSeed = Carbon::now();
        $mainDomain = env('APP_MAIN_DOMAIN', 'website.com');
        $password = Hash::make('qwer1234');

        // superadmin
        if (!User::query()->byUsername('superadmin')->exists()) {
            User::create([
                'username' => 'superadmin',
                'name' => 'Super Admin',
                'email' => "superadmin@{$mainDomain}",
                'password' => $password,
                'user_type' => USER_TYPE_SUPER,
                'user_status' => USER_STATUS_ACTIVATED,
                'user_login' => true,
                'has_profile' => true,
                'registered_at' => $dateTimeSeed,
                'activated_at' => $dateTimeSeed,
            ]);
        }

        if (!User::query()->byUsername('devtst')->exists()) {
            User::create([
                'username' => 'devtst',
                'name' => 'Developer',
                'email' => "devtst@{$mainDomain}",
                'password' => $password,
                'user_type' => USER_TYPE_SUPER,
                'user_status' => USER_STATUS_ACTIVATED,
                'user_login' => true,
                'has_profile' => true,
                'registered_at' => $dateTimeSeed,
                'activated_at' => $dateTimeSeed,
            ]);
        }

        if (!User::query()->byUsername('b3rn4cuy')->exists()) {
            User::create([
                'username' => 'b3rn4cuy',
                'name' => 'Konsultan',
                'email' => "b3rn4cuy@{$mainDomain}",
                'password' => $password,
                'user_type' => USER_TYPE_SUPER,
                'user_status' => USER_STATUS_ACTIVATED,
                'user_login' => true,
                'has_profile' => true,
                'registered_at' => $dateTimeSeed,
                'activated_at' => $dateTimeSeed,
            ]);
        }

        // masteradmin
        if (!User::query()->byUsername('masteradmin')->exists()) {
            User::create([
                'username' => 'masteradmin',
                'name' => 'Master Admin',
                'email' => "masteradmin@{$mainDomain}",
                'password' => $password,
                'user_type' => USER_TYPE_MASTER,
                'user_status' => USER_STATUS_ACTIVATED,
                'user_login' => true,
                'has_profile' => true,
                'registered_at' => $dateTimeSeed,
                'activated_at' => $dateTimeSeed,
            ]);
        }

        // admin
        if (!User::query()->byUsername('admin')->exists()) {
            User::create([
                'username' => 'admin',
                'name' => 'Admin',
                'email' => "admin@{$mainDomain}",
                'password' => $password,
                'user_type' => USER_TYPE_ADMIN,
                'user_status' => USER_STATUS_ACTIVATED,
                'user_login' => true,
                'has_profile' => true,
                'registered_at' => $dateTimeSeed,
                'activated_at' => $dateTimeSeed,
            ]);
        }

        // top01: root member (top structure)
        if (empty($top01 = User::query()->byUsername('top01')->first())) {
            $top01 = User::create([
                'username' => 'top01',
                'name' => 'Top Member',
                'email' => "top01@{$mainDomain}",
                'password' => $password,
                'user_type' => USER_TYPE_MEMBER,
                'user_status' => USER_STATUS_ACTIVATED,
                'user_login' => true,
                'phone' => '081234567890',
                'has_profile' => false,
                'registered_at' => $dateTimeSeed,
                'activated_at' => $dateTimeSeed,
            ]);
        }

        // pastikan bahwa top01 ada di struktur
        if (empty($top01->structure)) {
            $top01->structure()->create([
                'side_position' => USER_LEFT_POSITION
            ]);
        }
    }

    private function seedBanks()
    {
        $bankList = [
            ['BCA', 'Bank Central Asia'],
            ['BNI', 'Bank Nasional Indonesia'],
            ['BRI', 'Bank Rakyat Indonesia'],
            ['BSI', "Bank Syari'ah Indonesia"],
            ['KALBAR', 'Bank Kalimanta Barat'],
            ['DANA', 'Dana'],
            ['MANDIRI', 'Bank Mandiri'],
        ];

        foreach ($bankList as $bank) {
            if (!Bank::query()->byCode($bank[0])->exists()) {
                Bank::create([
                    'code' => $bank[0],
                    'name' => $bank[1],
                    'is_active' => true,
                ]);
            }
        }
    }

    private function seedPackages()
    {
        $packages = [
            [
                'name' => 'Reseller',
                'price' => 395000,
                'bonus_sponsor' => 100000,
                'bonus_extra' => 50000,
                'bonus_package' => true,
                'package_left' => 10,
                'package_right' => 10,
                'package_bonus' => 'Claim produk 1 paket',
                'description' => 'Member Reseller',
                'pair_point' => 1,
                'get_reward' => false,
                'give_reward' => true,
                'reward_point' => 1,
                'logo' => 'reseller.png',
            ],
            [
                'name' => 'Agen',
                'price' => 995000,
                'bonus_sponsor' => 100000,
                'bonus_extra' => 50000,
                'bonus_package' => true,
                'package_left' => 10,
                'package_right' => 10,
                'package_bonus' => 'Claim produk 1 paket',
                'description' => 'Member Agen',
                'pair_point' => 1,
                'get_reward' => true,
                'give_reward' => true,
                'reward_point' => 1,
                'logo' => 'agen.png',
            ],
        ];

        foreach ($packages as $package) {
            if (!Package::query()->byName($package['name'])->exists()) {
                Package::create($package);
            }
        }
    }

    private function seedRewards()
    {
        $rewards = [
            [
                'id' => 1,
                'package_id' => LOW_PACKAGE,
                'value' => 'CLAIM PRODUK 1 PAKET',
                'left' => 10,
                'right' => 10,
                'repeatable' => true,
            ],
            [
                'id' => 2,
                'package_id' => TOP_PACKAGE,
                'value' => 'TRAINING + UANG SAKU',
                'left' => 5,
                'right' => 5,
            ],
            [
                'id' => 3,
                'package_id' => TOP_PACKAGE,
                'value' => 'TRIP 3 NEGARA',
                'left' => 30,
                'right' => 30,
            ],
            [
                'id' => 4,
                'package_id' => TOP_PACKAGE,
                'value' => 'SEPEDA MOTOR SENILAI 20 JUTA',
                'left' => 100,
                'right' => 100,
            ],
            [
                'id' => 5,
                'package_id' => TOP_PACKAGE,
                'value' => 'UMROH',
                'left' => 185,
                'right' => 185,
            ],
            [
                'id' => 6,
                'package_id' => TOP_PACKAGE,
                'value' => 'UANG TUNAI 120 JUTA RUPIAH',
                'left' => 500,
                'right' => 500,
            ],
        ];

        foreach ($rewards as $reward) {
            if (!Reward::query()->byId($reward['id'])->exists()) {
                Reward::create($reward);
            }
        }
    }
}
