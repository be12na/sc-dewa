function dataTableDom(options)
{
    const responsive = (options.responsive && (options.responsive === true));
    const control = (options.control && (options.control === true));
    const lengthChange = (options.lengthChange && (options.lengthChange === true));
    const search = (options.search && (options.search === true));
    const info = (options.info && (options.info === true));
    const paging = (options.paging && (options.paging === true));
    const buttons = (control && options.buttons && (options.buttons === true));
    const hasControl = (control || lengthChange || search);
    const hasDtControl = (lengthChange || search);

    let dom = [];

    if (hasControl) {
        let clsContainer = 'row g-2 mb-2';

        if (buttons && hasDtControl && !lengthChange) {
            clsContainer = clsContainer + ' justify-content-between';
        }
    
        dom.push('<"container-fluid gx-3"');
        dom.push('<"' + clsContainer + '"');
    
        if (control) {
            dom.push('<"col-auto');
    
            if (buttons) {
                dom.push('"<"row g-2"');
                // dom.push('<"col-auto buttons"B>');
                dom.push('<"col-auto dt-control">');
                dom.push('>>');
            } else {
                dom.push(' dt-control">');
            }
        }
    
        if (hasDtControl) {
            const domLength = '<"col-auto me-auto"l>'; 
            const domSearch = '<"col-auto"f>'; 
    
            if (control) {
                dom.push('<"col-auto flex-fill"');
                if (lengthChange && search) {
                    dom.push('<"row g-2 justify-content-between"');
                } else {
                    dom.push('<"row g-2');
                }
            }
    
            if (lengthChange) {
                dom.push(domLength);
            }
            
            if (search) {
                dom.push(domSearch);
            }
            
            if (control) {
                dom.push('>>');
            }
        } else {
            dom.push('lf');
        }
    
        dom.push('>>');
    } else {
        dom.push('lf');
    }

    dom.push(responsive ? 'r<"d-block w-100"t>' : 'r<"table-responsive border-top border-translucent mb-2"t>');

    if (info || paging) {
        dom.push('<"row g-1"');

        if (info) {
            dom.push('<"col-12 col-md"i>');
        }
        
        if (paging) {
            if (info) {
                dom.push('<"col-12 col-md"p>');
            } else {
                dom.push('<"col-12 col-md ms-md-auto"p>');
            }
        }

        dom.push('>');
    } else {
        dom.push('ip');
    }

    return dom.join('');
}

function dataTableRefresh(t)
{
    t.ajax.reload();
}

function setControlToDataTable(tableId, control, insertMethod = 'append')
{
    tableId = '#' + tableId.replace('#', '');

    const wrapper = $(tableId + '_wrapper');
    const controlContainer = $('.dt-control', wrapper);
    if (controlContainer.length) {
        if (insertMethod == 'prepend') {
            controlContainer.prepend(control);
        } else {
            controlContainer.append(control);
        }

        control.removeClass('d-none');
    }
}

function setDataTableLoading(tableId, text = 'Loading...')
{
    const loader = '<div class="d-flex flex-column align-items-center justify-content-center h-100 text-center text-success fw-bold" style="--bs-text-opacity:0.75;"><i class="fas fa-spinner fa-spin fs-3"></i><div class="fa-beat-fade mt-2">' + text + '</div></div>';

    tableId = '#' + tableId.replace('#', '');
    const pro = $(tableId + '_processing.dataTables_processing');
    pro.empty().removeClass('card').append(loader);
}
