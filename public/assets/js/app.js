const APP = new class
{
    remoteLoad = async function()
    {
        async function getRemoteData(element)
        {
            const h = (element instanceof jQuery) ? element : $(element);
            const remoteData = h.data();
            const url = remoteData.remoteUrl;
            const rmvData = [];
            const rDefault = remoteData.returnDefault;
            
            if (url) {
                const rData = {};
                rmvData.push('data-remote-url');
                rmvData.push('data-return-default');
                
                $.each(remoteData, function(key, value) {
                    if ((key != 'remoteUrl') && (key != 'returnDefault')) {
                        let dataName = key.replace('remote', '').replace('return', '');
                        const dataPrefix = key.replace(dataName, '');
                        dataName = dataName.toLowerCase()
                        rmvData.push('data-' + dataPrefix + '-' + dataName);
                        rData[dataName.replace('-', '_')] = value;
                    }
                });

                await $.get({
                        url: url,
                        data: rData,
                    }).done(function (response) {
                        h.html(response);
                    }).fail(function (response) {
                        if (rDefault != undefined) {
                            h.html('<span class="text-danger">' + rDefault  + '</span>');
                        } else {
                            h.html('<span class="text-danger">Error</span>');
                        }
                    }).always(function(response) {
                        h.removeAttr(rmvData.join(' ')).removeClass('remote-data');
                    });
            }
        }

        const elRemote = $(".remote-data[data-remote-url]");

        if (elRemote.length) {
            await $.each(elRemote, async function (k, v) {
                await getRemoteData(v);
            });
        }
    }

    #remoteClickRender = function()
    {
        $(document).on('click', '.remote-render[data-remote-url][data-render-target]', async function(e) {
            const me = $(this);
            const url = me.data('remote-url');
            const target = $(me.data('render-target'));
            const setActive = me.data('set-active');

            target.empty()
            
            await $.get({
                url: url
            }).done(function(response) {
                target.html(response);
            }).fail(function(response) {
                target.html('<span class="text-danger">Error</span>');
            }).always(function(response) {
                if ((setActive != undefined) && (setActive === 'active')) {
                    const parent = me.parent();

                    $('.remote-render', parent).removeClass('active');
                    me.addClass('active');
                }
            });

            return false;
        });
    }

    #buttonHref = function()
    {
        $(document).on('click', 'button[data-href^="http"]:not([type="submit"]):not([type="reset"])', function(e) {
            window.location.href = $(this).data('href');
        })
    }

    createAlert = function(msg, type, useHeading, useClose)
    {
        type = type.toLowerCase();
        let icon = 'question-circle';
        let title = '';

        if (type == 'danger') {
            icon = 'times-circle';
            title = 'Error';
        } else if (type == 'success') {
            icon = 'check-circle';
            title = 'Success';
        } else if (type == 'warning') {
            icon = 'exclamation-circle';
            title = 'Warning';
        } else if (type == 'info') {
            icon = 'info-circle';
            title = 'Info';
        } else {
            title = 'Question';
        }

        useHeading = (useHeading && useHeading === true);
        useClose = (useClose && useClose === true);
        
        const alertCssFlex = useHeading ? 'pe-3' : 'd-flex flex-nowrap';
        const alertCssClass = 'alert alert-' + type + ' alert-dismissible ' + alertCssFlex;
        const alertClose = useClose
            ? '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            : '';
        const alertHeading = (useHeading && useHeading === true)
            ? '<h5 class="alert-heading pe-5">' + title + '</h5>'
            : '';
        const alertMessage = '<div>' + msg + '</div>' + (!useHeading && useClose ? alertClose : '');
        const alertContent = '<div class="flex-shrink-0 me-3"><i class="fa fa-' + icon + '"></i></div>' + alertMessage;

        return '<div class="' + alertCssClass + '" role="alert">' + alertHeading + alertContent + '</div>';
    }

    errorResponse = function(response, useHeading, useClose)
    {
        if (typeof response == 'object') {
            const msg = (response.responseJSON && response.responseJSON.message)
                ? response.responseJSON.message
                : (response.responseText
                    ? response.responseText
                    : (response.statusText
                        ? response.statusText
                        : response
                    )
                );

            if (msg.indexOf('<div') == 0) {
                return msg;
            } else {
                return this.createAlert(msg, 'danger', useHeading, useClose);
            }
        } else {
            return response;
        }
    }

    #createAlertContainer = function(target)
    {
        let alertContainer;
        let containerId = target.data('alert-container');

        if (containerId) {
            alertContainer = $(containerId);
        } else {
            const id = 'alert-container-' + (Math.random() * 1000);
            alertContainer = $('<div id="' + id + '" class="d-block"></div>');
            target.before(alertContainer);
            target.data('alert-container', '#' + id);
        }

        return alertContainer;
    }

    submitForm = function(form)
    {
        const frm = (form instanceof jQuery) ? form : $(form);
        const url = frm.attr('action');
        const frmEnc = frm.attr('enctype');
        const isMultipart = (frmEnc == 'multipart/form-data');
        const msg = this.#createAlertContainer(frm);

        const options = {
            url: url
        };

        if (isMultipart) {
            options.data = new FormData(frm[0]);
            options.cache = false;
            options.contentType = false;
            options.processData = false;
        } else {
            options.data = frm.serialize();
        }

        msg.empty();
        $('.is-invalid', frm).removeClass('is-invalid');
        $('.invalid-feedback', frm).remove();
        frm.addClass('submitting');
        
        $.post(options)
            .done(function(response) {
                window.location.replace(response);
            }).fail(function(response) {
                console.log(response);
                if (response.status == 400) {
                    const responseJSON = response.responseJSON;
                    if ((responseJSON.validationError === true) && responseJSON.messages) {
                        $.each(responseJSON.messages, function(name, message) {
                            const elmName = '[name="' + name + '"]';
                            const input = $(elmName, frm);
                            input.addClass('is-invalid');
                            const parent = input.parent();
                            const feedback = $('<div class="invalid-feedback">' + message + '</div>');
                            parent.append(feedback);
                        });                        
                    }
                }

                msg.html(APP.errorResponse(response, false, true));
            }).always(function(response) {
                frm.removeClass('submitting');
            });

        return false;
    }

    #formInitialized = false;
    #initForm = function()
    {
        if (this.#formInitialized !== true) {
            $(document).on('submit', 'form.remote-submit[method="POST"]', function(e) {
                return APP.submitForm(this);
            });
        }

        this.#formInitialized = true;
    }

    errorRemoteModal = function(dlg, msg)
    {
        const dialog = (dlg instanceof jQuery) ? dlg : $(dlg);
        const content = $('<div class="modal-content"><div class="modal-header"><h5 class="modal-title">Error</h5><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button></div></div>');
        const modalBody = $('<div class="modal-body"></div>');
        const modalFooter = $('<div class="modal-footer justify-content-center"><button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button></div>');
        modalBody.append($(msg).addClass('mb-0'));
        content.append(modalBody).append(modalFooter);
        dialog.append(content);
    }

    openModal = function(sender, modal, isModalShown)
    {
        modal = (modal instanceof jQuery) ? modal : $(modal);
        sender = (sender instanceof jQuery) ? sender : $(sender);
        const url = sender.data('modal-url');
        const isRemote = (url != undefined && url != '');

        if (isRemote) {
            if (url) {
                const dialog = $('.modal-dialog', modal);
                dialog.empty();
                
                $.get({
                    url: url,
                }).done(function(response) {
                    dialog.html(response);
                }).fail(function(response) {
                    APP.errorRemoteModal(dialog, APP.errorResponse(response));
                });
            }
        }

        if (isModalShown !== true) {
            modal.modal('show');
        }
    }

    #initShowModal = function()
    {
        $(document).on('show.bs.modal', function(e) {
            APP.openModal($(e.relatedTarget), $(this), true);
        });
    }

    run = function()
    {
        this.#buttonHref();
        this.#initForm();
        this.#remoteClickRender();
        this.#initShowModal();
        this.remoteLoad();
    }
}

$(function() {
    APP.run();
});
